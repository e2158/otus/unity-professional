﻿using UnityEngine;
using Cysharp.Threading.Tasks;

namespace Game.Utils
{
    public sealed class GameObjectReferenceLoadTrigger : MonoBehaviour
    {
        private static readonly ILogger Logger = Debug.unityLogger;

        [SerializeField]
        private GameObjectReferenceLoader referenceLoader;

        [Space, SerializeField]
        private LoadTriggerComponent[] components;

        private string LogTag => nameof(GameObjectReferenceLoadTrigger) + " " + gameObject.name;

        private void OnValidate()
        {
            components = GetComponents<LoadTriggerComponent>();
        }

        private void OnEnable()
        {
            foreach (LoadTriggerComponent component in components)
                component.Triggered += OnTriggered;
        }

        private void OnDisable()
        {
            foreach (LoadTriggerComponent component in components)
                component.Triggered -= OnTriggered;
        }

        private void OnTriggered()
        {
            Logger.Log(LogTag, $"Triggered \"{referenceLoader.name}\"");
            referenceLoader.LoadAsync().Forget();
        }
    }
}