﻿using UnityEngine;

namespace Game.Utils
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(GameObjectReferenceLoadTrigger))]
    public sealed class CollisionLoadTriggerComponent : LoadTriggerComponent
    {
        [SerializeField]
        private Collider collider;

        private void OnValidate()
        {
            collider ??= GetComponent<Collider>();
        }

        private void OnTriggerEnter(Collider other)
        {
            Trigger();
        }
    }
}