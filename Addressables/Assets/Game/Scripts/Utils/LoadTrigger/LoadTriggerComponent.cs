﻿using System;
using UnityEngine;

namespace Game.Utils
{
    [RequireComponent(typeof(GameObjectReferenceLoadTrigger))]
    public abstract class LoadTriggerComponent : MonoBehaviour
    {
        public event Action Triggered;

        protected void Trigger() =>
            Triggered?.Invoke();
    }
}