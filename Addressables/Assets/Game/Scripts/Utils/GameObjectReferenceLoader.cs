﻿using Zenject;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game.Utils
{
    [TypeInfoBox("Подгружает указанный AssetReferenceGameObject внутрь себя по запросу или на старте.\n" +
                 "Гарантирует наличие только одного загруженного объекта.\n" +
                 "Выгружает объект из памяти по запросу или при своём уничтожении.")]
    public sealed class GameObjectReferenceLoader : MonoBehaviour
    {
        private static readonly ILogger Logger = Debug.unityLogger;

        [Title("Configuration")]
        [SerializeField, DisableInPlayMode] private AssetReferenceGameObject gameObjectReference;
        [SerializeField, DisableInPlayMode] private bool autoLoadOnStart;
        [SerializeField, DisableInPlayMode, ShowIf(nameof(autoLoadOnStart))] private bool disableOnAutoLoad;

        [PropertySpace, Title("Info")]
        [ShowInInspector, ReadOnly, CanBeNull] public GameObject LoadedObject { get; private set; }
        [ShowInInspector, ReadOnly] private bool HasActiveLoading { get; set; }
        [ShowInInspector, ReadOnly] private int RequestsCount { get; set; }

        private int FirstExternalRequestNumber => autoLoadOnStart ? 2 : 1;
        private CancellationToken Ct => this.GetCancellationTokenOnDestroy();
        private string LogTag => nameof(GameObjectReferenceLoader) + " " + name;

        private IInstantiator _instantiator;

        private void OnValidate()
        {
            disableOnAutoLoad &= autoLoadOnStart;
        }

        [Inject]
        private void Construct(IInstantiator instantiator)
        {
            _instantiator = instantiator;
        }

        private async void Start()
        {
            if (autoLoadOnStart)
            {
                GameObject loadedObject = await LoadAsync();

                bool noExternalRequests = RequestsCount < FirstExternalRequestNumber;
                if (disableOnAutoLoad && noExternalRequests)
                    loadedObject.SetActive(false);
            }
        }

        private void OnDestroy()
        {
            UnloadAsync().Forget();
        }

        [Button]
        public async UniTask<GameObject> LoadAsync()
        {
            RequestsCount++;
            int currentRequestNumber = RequestsCount;

            if (HasActiveLoading)
                await UniTask.WaitWhile(() => HasActiveLoading, cancellationToken: Ct);

            if (LoadedObject == null)
            {
                HasActiveLoading = true;
                Logger.Log(LogTag, "Loading...");

                GameObject prefab = await gameObjectReference.LoadAssetAsync(); // can't cancel
                LoadedObject = prefab != null ? _instantiator.InstantiatePrefab(prefab, transform) : null;

                if (LoadedObject != null) Logger.Log(LogTag, $"Loaded \"{LoadedObject.name}\"");
                else Logger.LogError(LogTag, "Failed to load");
                HasActiveLoading = false;
            }

            bool isFirstExternalRequest = currentRequestNumber == FirstExternalRequestNumber;
            if (disableOnAutoLoad && isFirstExternalRequest)
                LoadedObject?.SetActive(true);

            return LoadedObject;
        }

        [Button]
        public async UniTaskVoid UnloadAsync()
        {
            if (HasActiveLoading)
                // ReSharper disable once MethodSupportsCancellation
                await UniTask.WaitWhile(() => HasActiveLoading);

            if (LoadedObject!= null)
            {
                string loadedObjectName = LoadedObject.name;

                Destroy(LoadedObject);
                LoadedObject = null;
                gameObjectReference.ReleaseAsset();

                Logger.Log(LogTag, $"Unloaded \"{loadedObjectName}\"");
            }
        }
    }
}