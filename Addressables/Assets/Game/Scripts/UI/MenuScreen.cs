using Game.Application;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.UI
{
    public sealed class MenuScreen : MonoBehaviour
    {
        [SerializeField]
        private Button startButton;

        [SerializeField]
        private Button exitButton;

        private ApplicationExiter _applicationExiter;
        private GameLoader _gameLoader;

        [Inject]
        public void Construct(ApplicationExiter applicationFinisher, GameLoader gameLoader)
        {
            _gameLoader = gameLoader;
            _applicationExiter = applicationFinisher;
        }

        private void OnEnable()
        {
            startButton.onClick.AddListener(_gameLoader.LoadGame);
            exitButton.onClick.AddListener(_applicationExiter.ExitApp);
        }

        private void OnDisable()
        {
            startButton.onClick.RemoveListener(_gameLoader.LoadGame);
            exitButton.onClick.RemoveListener(_applicationExiter.ExitApp);
        }
    }
}