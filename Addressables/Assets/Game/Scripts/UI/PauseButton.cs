using Game.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public sealed class PauseButton : MonoBehaviour
    {
        [SerializeField]
        private Button button;

        [SerializeField]
        private GameObjectReferenceLoader pauseScreenLoader;

        private void OnEnable()
        {
            button.onClick.AddListener(ShowPauseScreen);
        }

        private void OnDisable()
        {
            button.onClick.RemoveListener(ShowPauseScreen);
        }

        private async void ShowPauseScreen()
        {
            (await pauseScreenLoader.LoadAsync())
                .GetComponent<PauseScreen>()
                .Show();
        }
    }
}