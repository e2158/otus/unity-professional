using Game.Application;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.UI
{
    public sealed class PauseScreen : MonoBehaviour
    {
        [SerializeField]
        private Button resumeButton;

        [SerializeField]
        private Button exitButton;

        private MenuLoader _menuLoader;

        [Inject]
        public void Construct(MenuLoader menuLoader, GameLoader gameLoader)
        {
            _menuLoader = menuLoader;
            gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            resumeButton.onClick.AddListener(Hide);
            exitButton.onClick.AddListener(_menuLoader.LoadMenu);
        }

        private void OnDisable()
        {
            resumeButton.onClick.RemoveListener(Hide);
            exitButton.onClick.RemoveListener(_menuLoader.LoadMenu);
        }

        public void Show()
        {
            Time.timeScale = 0; //KISS
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            Time.timeScale = 1; //KISS
            gameObject.SetActive(false);
        }
    }
}