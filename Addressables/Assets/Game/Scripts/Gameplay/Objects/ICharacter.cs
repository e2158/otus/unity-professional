using UnityEngine;

namespace Game.Gameplay.Objects
{
    public interface ICharacter
    {
        void Move(Vector3 direction, float deltaTime);

        Vector3 GetPosition();
    }
}