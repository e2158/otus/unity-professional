using UnityEngine;

namespace Game.Gameplay.Systems
{
    public interface IMoveInput
    {
        Vector3 GetDirection();
    }
}