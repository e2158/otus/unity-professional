﻿using Zenject;
using UnityEngine;
using JetBrains.Annotations;

namespace Game.Application
{
    [UsedImplicitly]
    public static class Bootstrap
    {
        private static MenuLoader MenuLoader => DiContainer.Resolve<MenuLoader>();
        private static DiContainer DiContainer => ProjectContext.Instance.Container;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Initialize()
        {
            MenuLoader.LoadMenu();
        }
    }
}