using UnityEngine.AddressableAssets;

namespace Game.Application
{
    public sealed class GameLoader
    {
        public void LoadGame()
        {
            Addressables.LoadSceneAsync("Game");
        }
    }
}