using UnityEngine.AddressableAssets;

namespace Game.Application
{
    public sealed class MenuLoader
    {
        public void LoadMenu()
        {
            Addressables.LoadSceneAsync("Menu");
        }
    }
}