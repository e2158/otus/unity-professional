﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;
using System.Collections.Generic;

namespace GameEngine
{
    [CreateAssetMenu(fileName = nameof(UnitPrefabRepository),
        menuName = nameof(GameEngine) + "/" + nameof(UnitPrefabRepository))]
    public sealed class UnitPrefabRepository : ScriptableObject
    {
        [Required, AssetSelector, ListDrawerSettings(Expanded = true)]
        [SerializeField] private Unit[] _prefabs;

        private Dictionary<string, Unit> _cache;

        public IReadOnlyDictionary<string, Unit> Prefabs =>
            this._cache ??= this._prefabs.ToDictionary(p => p.Type, p => p);
    }
}