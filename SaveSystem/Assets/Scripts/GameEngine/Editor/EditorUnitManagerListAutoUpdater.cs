﻿using System;
using System.Reflection;
using System.Collections.Generic;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;

namespace GameEngine.Editor
{
    [DisallowMultipleComponent]
    [InfoBox("Для возможности удалять и дублировать юнитов в рантайме в редакторе")]
    // Если asmDef в Editor только выставить, то монобех нельзя будет повесить на сцену.
    // С более сложным поведением заморачиваться не хотелось.
    internal sealed class EditorUnitManagerListAutoUpdater : MonoBehaviour
    {
        #if UNITY_EDITOR
        private UnitManager _unitManager;

        [Inject]
        private void Constructor(UnitManager unitManager)
        {
            this._unitManager = unitManager;
        }

        private void Update()
        {
            Type unitManagerType = this._unitManager.GetType();
            FieldInfo sceneUnitsField = unitManagerType.GetField("sceneUnits", BindingFlags.NonPublic | BindingFlags.Instance);

            if (sceneUnitsField != null)
            {
                HashSet<Unit> sceneUnitsValue = new(FindObjectsOfType<Unit>());
                sceneUnitsField.SetValue(this._unitManager, sceneUnitsValue);
            }
        }
        #endif
    }
}