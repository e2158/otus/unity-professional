﻿using System.Linq;
using System.Collections.Generic;
using SaveSystem;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace GameEngine.SaveData
{
    public sealed class ResourceFeatureSaveMediator : IFeatureSaveMediator
    {
        [NotNull] private readonly SaveService _saveService;
        [NotNull] private readonly ResourceService _resourceService;

        public ResourceFeatureSaveMediator(
            [NotNull] SaveService saveService, [NotNull] ResourceService resourceService)
        {
            this._saveService = saveService;
            this._resourceService = resourceService;
        }

        public async UniTask Save()
        {
            ResourceFeatureSaveData featureSaveData = this.ExtractFeatureSaveData();
            await this._saveService.Save(featureSaveData);
        }

        public async UniTask Load()
        {
            ResourceFeatureSaveData featureSaveData =
                await this._saveService.Load<ResourceFeatureSaveData>();

            this.ApplyFeatureSaveData(featureSaveData);
        }

        private ResourceFeatureSaveData ExtractFeatureSaveData()
        {
            ResourceSaveData[] saveDatas =
                this._resourceService.GetResources()
                    .Select(this.ExtractSaveData).ToArray();

            return new ResourceFeatureSaveData(saveDatas);
        }

        private void ApplyFeatureSaveData(ResourceFeatureSaveData featureSaveData)
        {
            Dictionary<string, Resource> resources =
                this._resourceService.GetResources()
                    .ToDictionary(r => r.ID, r => r);

            foreach (ResourceSaveData saveData in featureSaveData.Resources)
            {
                Resource resource = resources[saveData.ID];
                this.ApplySaveData(resource, saveData);
            }
        }

        private ResourceSaveData ExtractSaveData(Resource resource) =>
            new(resource.ID, resource.Amount);

        private void ApplySaveData(Resource resource, ResourceSaveData saveData) =>
            resource.Amount = saveData.Amount;
    }
}