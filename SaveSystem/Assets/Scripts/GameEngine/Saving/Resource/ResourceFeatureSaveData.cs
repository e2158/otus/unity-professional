﻿using System;
using SaveSystem;

namespace GameEngine.SaveData
{
    [Serializable]
    public class ResourceFeatureSaveData : ISaveData
    {
        public ResourceSaveData[] Resources { get; }

        public ResourceFeatureSaveData(ResourceSaveData[] resources)
        {
            this.Resources = resources;
        }
    }
}