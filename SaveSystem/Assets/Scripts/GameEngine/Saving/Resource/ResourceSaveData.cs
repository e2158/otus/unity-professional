﻿using System;

namespace GameEngine.SaveData
{
    [Serializable]
    public class ResourceSaveData
    {
        public string ID { get; }
        public int Amount { get; }

        public ResourceSaveData(string id, int amount)
        {
            this.ID = id;
            this.Amount = amount;
        }
    }
}