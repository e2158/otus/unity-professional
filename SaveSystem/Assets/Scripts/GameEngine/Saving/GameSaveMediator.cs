﻿using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using System.Collections.Generic;

namespace GameEngine.SaveData
{
    public sealed class GameSaveMediator : IFeatureSaveMediator
    {
        [NotNull, ItemNotNull] private readonly IReadOnlyList<IFeatureSaveMediator> _featureSaveMediators;

        public GameSaveMediator([NotNull, ItemNotNull] List<IFeatureSaveMediator> featureSaveMediators) =>
            this._featureSaveMediators = featureSaveMediators;

        public UniTask Save() =>
            UniTask.WhenAll(this._featureSaveMediators.Select(m => m.Save()));

        public UniTask Load() =>
            UniTask.WhenAll(this._featureSaveMediators.Select(m => m.Load()));
    }
}