﻿using Cysharp.Threading.Tasks;

namespace GameEngine.SaveData
{
    public interface IFeatureSaveMediator
    {
        UniTask Save();
        UniTask Load();
    }
}