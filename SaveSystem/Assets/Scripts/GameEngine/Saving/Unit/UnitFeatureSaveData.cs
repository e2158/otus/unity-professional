﻿using System;
using SaveSystem;

namespace GameEngine.SaveData
{
    [Serializable]
    public class UnitFeatureSaveData : ISaveData
    {
        public UnitSaveData[] Units { get; }

        public UnitFeatureSaveData(UnitSaveData[] units)
        {
            this.Units = units;
        }
    }
}