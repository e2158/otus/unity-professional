﻿using System;
using UnityEngine;

namespace GameEngine.SaveData
{
    [Serializable]
    public class UnitSaveData
    {
        public string Type { get; }
        public int HitPoints { get; }

        public SerializableVector3 Position { get; }
        public SerializableVector3 Rotation { get; }

        public UnitSaveData(string type, int hitPoints, Vector3 position, Vector3 rotation)
        {
            this.Type = type;
            this.HitPoints = hitPoints;

            this.Position = position;
            this.Rotation = rotation;
        }
    }
}