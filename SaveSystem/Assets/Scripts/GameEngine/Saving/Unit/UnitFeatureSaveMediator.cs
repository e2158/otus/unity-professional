﻿using System.Linq;
using System.Collections.Generic;
using SaveSystem;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace GameEngine.SaveData
{
    public sealed class UnitFeatureSaveMediator : IFeatureSaveMediator
    {
        [NotNull] private readonly UnitManager _unitManager;
        [NotNull] private readonly SaveService _saveService;
        [NotNull] private readonly UnitPrefabRepository _prefabRepository;

        public UnitFeatureSaveMediator([NotNull] UnitManager unitManager,
            [NotNull] SaveService saveService, [NotNull] UnitPrefabRepository prefabRepository)
        {
            this._unitManager = unitManager;
            this._saveService = saveService;
            this._prefabRepository = prefabRepository;
        }

        public async UniTask Save()
        {
            UnitFeatureSaveData featureSaveData = this.ExtractFeatureSaveData();
            await this._saveService.Save(featureSaveData);
        }

        public async UniTask Load()
        {
            UnitFeatureSaveData featureSaveData =
                await this._saveService.Load<UnitFeatureSaveData>();

            this.ApplyFeatureSaveData(featureSaveData);
        }

        private UnitFeatureSaveData ExtractFeatureSaveData()
        {
            UnitSaveData[] saveDatas = this._unitManager.GetAllUnits()
                .Select(this.ExtractSaveData).ToArray();

            return new UnitFeatureSaveData(saveDatas);
        }

        private void ApplyFeatureSaveData(UnitFeatureSaveData featureSaveData)
        {
            this.DestroyExtraUnits(this._unitManager, featureSaveData);
            this.SpawnMissingUnits(this._unitManager, featureSaveData);

            IEnumerable<IGrouping<string,Unit>> spawnedUnitsByType =
                this._unitManager.GetAllUnits().GroupBy(u => u.Type);

            IEnumerable<IGrouping<string,UnitSaveData>> saveDatasByType =
                featureSaveData.Units.GroupBy(u => u.Type);

            var spawnedUnitsWithSaveDatas = spawnedUnitsByType
                .Join(
                    saveDatasByType,
                    spawned => spawned.Key,
                    saved => saved.Key,
                    (spawned, saved) => spawned.Zip(saved,
                        (unit, data) => new { Unit = unit, Data = data }))
                .SelectMany(en => en);

            foreach (var unitWithSaveData in spawnedUnitsWithSaveDatas)
                this.ApplySaveData(unitWithSaveData.Unit, unitWithSaveData.Data);
        }

        private void DestroyExtraUnits(UnitManager unitManager, UnitFeatureSaveData featureSaveData)
        {
            Dictionary<string, int> savedUnitTypesWithCount = featureSaveData.Units
                .GroupBy(u => u.Type).ToDictionary(g => g.Key, g => g.Count());

            IEnumerable<IGrouping<string, Unit>> spawnedUnitsByType =
                unitManager.GetAllUnits().GroupBy(u => u.Type);

            foreach (var unitsOfType in spawnedUnitsByType)
            {
                string type = unitsOfType.Key;
                int savedUnitsCount = savedUnitTypesWithCount.GetValueOrDefault(type, 0);
                IEnumerable<Unit> unitsToDestroy = unitsOfType.Skip(savedUnitsCount);

                foreach (Unit unit in unitsToDestroy)
                    unitManager.DestroyUnit(unit);
            }
        }

        private void SpawnMissingUnits(UnitManager unitManager, UnitFeatureSaveData featureSaveData)
        {
            Dictionary<string, int> spawnedUnitTypesWithCount = unitManager.GetAllUnits()
                .GroupBy(u => u.Type).ToDictionary(g => g.Key, g => g.Count());

            Dictionary<string, int> savedUnitTypesWithCount = featureSaveData.Units
                .GroupBy(d => d.Type).ToDictionary(g => g.Key, g => g.Count());

            foreach (string type in savedUnitTypesWithCount.Keys)
            {
                int savedCount = savedUnitTypesWithCount[type];
                int spawnedCount = spawnedUnitTypesWithCount[type];

                for (int i = spawnedCount; i < savedCount; i++)
                {
                    Unit prefab = this._prefabRepository.Prefabs[type];
                    unitManager.SpawnUnit(prefab, default, default);
                }
            }
        }

        private UnitSaveData ExtractSaveData(Unit unit) =>
            new(unit.Type, unit.HitPoints, unit.Position, unit.Rotation);

        private void ApplySaveData(Unit unit, UnitSaveData unitSaveData)
        {
            unit.HitPoints = unitSaveData.HitPoints;
            unit.transform.position = unitSaveData.Position;
            unit.transform.eulerAngles = unitSaveData.Rotation;
        }
    }
}