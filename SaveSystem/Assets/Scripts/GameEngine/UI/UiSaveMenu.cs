﻿using Zenject;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using GameEngine.SaveData;
using Sirenix.OdinInspector;

namespace GameEngine.UI
{
    [DisallowMultipleComponent]
    internal sealed class UiSaveMenu : MonoBehaviour
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private Button _saveButton;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private Button _loadButton;

        private GameSaveMediator _gameSaveMediator;

        private void OnValidate()
        {
            this._saveButton ??= this.GetComponentsInChildren<Button>().FirstOrDefault();
            this._loadButton ??= this.GetComponentsInChildren<Button>().LastOrDefault();
        }

        [Inject]
        private void Construct(GameSaveMediator gameSaveMediator)
        {
            this._gameSaveMediator = gameSaveMediator;
        }

        private void Start()
        {
            this._saveButton.onClick.AddListener(this.OnSaveButtonClicked);
            this._loadButton.onClick.AddListener(this.OnLoadButtonClicked);
        }

        private void OnDestroy()
        {
            this._saveButton.onClick.RemoveListener(this.OnSaveButtonClicked);
            this._loadButton.onClick.RemoveListener(this.OnLoadButtonClicked);
        }

        private void OnSaveButtonClicked() => this._gameSaveMediator.Save().Forget();
        private void OnLoadButtonClicked() => this._gameSaveMediator.Load().Forget();
    }
}