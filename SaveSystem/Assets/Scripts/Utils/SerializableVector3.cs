﻿using System;
using UnityEngine;

[Serializable]
public struct SerializableVector3
{
    public float X;
    public float Y;
    public float Z;

    public SerializableVector3(float x, float y, float z)
    {
        this.X = x;
        this.Y = y;
        this.Z = z;
    }

    public static implicit operator Vector3(SerializableVector3 serVec) =>
        new(serVec.X, serVec.Y, serVec.Z);

    public static implicit operator SerializableVector3(Vector3 vec) =>
        new(vec.x, vec.y, vec.z);
}