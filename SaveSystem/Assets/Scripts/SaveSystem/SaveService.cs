﻿using System;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace SaveSystem
{
    public sealed class SaveService
    {
        [NotNull] private readonly ISerializer _serializer;
        [NotNull] private readonly IDataStorage _dataStorage;
        [NotNull] private readonly IDataKeysRepository _keysRepository;

        public SaveService([NotNull] ISerializer serializer,
            [NotNull] IDataStorage dataStorage, [NotNull] IDataKeysRepository keysRepository)
        {
            this._serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));
            this._dataStorage = dataStorage ?? throw new ArgumentNullException(nameof(dataStorage));
            this._keysRepository = keysRepository ?? throw new ArgumentNullException(nameof(keysRepository));
        }

        public async UniTask Save<TData>(TData data) where TData : ISaveData
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            try
            {
                string dataKey = this._keysRepository.GetKey<TData>();
                string serializeData = await this._serializer.SerializeData(data);
                await this._dataStorage.Write(dataKey, serializeData);
            }
            catch (Exception e)
            {
                throw new Exception($"Can't save data of type \"{typeof(TData).Name}\":", e);
            }
        }

        public async UniTask<TData> Load<TData>() where TData : ISaveData
        {
            TData data;

            try
            {
                string dataKey = this._keysRepository.GetKey<TData>();
                string serializeData = await this._dataStorage.Read(dataKey);
                data = await this._serializer.DeserializeData<TData>(serializeData);
                if (data == null) throw new NullReferenceException($"Null data with key \"{dataKey}\"");
            }
            catch (Exception e)
            {
                throw new Exception($"Can't load data of type \"{typeof(TData).Name}\":", e);
            }

            return data;
        }
    }
}