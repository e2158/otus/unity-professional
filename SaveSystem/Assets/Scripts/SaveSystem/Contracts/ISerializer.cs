﻿using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace SaveSystem
{
    public interface ISerializer
    {
        UniTask<string> SerializeData<TData>([NotNull] TData data) where TData : ISaveData;
        UniTask<TData> DeserializeData<TData>([CanBeNull] string json) where TData : ISaveData;
    }
}