﻿namespace SaveSystem
{
    public interface IDataKeysRepository
    {
        string GetKey<TType>();
    }
}