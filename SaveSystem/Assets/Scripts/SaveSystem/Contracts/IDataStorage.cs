﻿using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace SaveSystem
{
    public interface IDataStorage
    {
        UniTask<string> Read([NotNull] string key);
        UniTask Write([NotNull] string key, [NotNull] string data);
    }
}