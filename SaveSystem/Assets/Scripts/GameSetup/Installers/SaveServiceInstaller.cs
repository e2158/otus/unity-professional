﻿using System;
using Zenject;
using SaveSystem;

namespace GameSetup
{
    internal sealed class SaveServiceInstaller : Installer
    {
        private static readonly Type serializerType = typeof(AesSerializer);
        private static readonly Type dataStorageType = typeof(StreamingAssetsDataStorage);

        public override void InstallBindings()
        {
            this.InstallSerializer(SaveServiceInstaller.serializerType);
            this.InstallDataStorage(SaveServiceInstaller.dataStorageType);

            this.Container.Bind<IDataKeysRepository>().To<SaveDataKeysRepository>().AsSingle();
            this.Container.Bind<SaveService>().AsSingle();
        }

        private void InstallSerializer(Type serializerType)
        {
            if (serializerType == typeof(NewtonsoftSerializer))
            {
                this.Container.Bind<ISerializer>().To<NewtonsoftSerializer>().AsSingle();
            }
            else if (serializerType == typeof(AesSerializer))
            {
                this.Container.Bind<ISerializer>().To<NewtonsoftSerializer>().WhenInjectedInto<AesSerializer>();
                this.Container.Bind<ISerializer>().To<AesSerializer>().AsSingle();
            }
            else
            {
                throw new Exception($"Unknown serializer type: {SaveServiceInstaller.serializerType.Name}");
            }
        }

        private void InstallDataStorage(Type dataStorageType)
        {
            if (dataStorageType == typeof(PlayerPrefsDataStorage))
                this.Container.Bind<IDataStorage>().To<PlayerPrefsDataStorage>().AsSingle();

            else if (dataStorageType == typeof(StreamingAssetsDataStorage))
                this.Container.Bind<IDataStorage>().To<StreamingAssetsDataStorage>().AsSingle();

            else
                throw new Exception($"Unknown DataStorage type: {SaveServiceInstaller.dataStorageType.Name}");
        }
    }
}