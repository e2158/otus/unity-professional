﻿using Zenject;
using UnityEngine;
using GameEngine;
using GameEngine.SaveData;
using Sirenix.OdinInspector;

namespace GameSetup
{
    [DisallowMultipleComponent]
    internal sealed class ResourceFeatureInstaller : MonoInstaller
    {
        [Title("References")]
        [Required]
        [SerializeField] private Resource[] _resources;

        private void OnValidate()
        {
            if (this._resources == null || this._resources.Length == 0)
                this.FindResources();
        }

        [Title("Controls"), Button(ButtonSizes.Medium)]
        private void FindResources() =>
            this._resources = FindObjectsOfType<Resource>();

        public override void InstallBindings()
        {
            ResourceService resourceService = new();
            resourceService.SetResources(this._resources);

            this.Container.Bind<ResourceService>().FromInstance(resourceService);
            this.Container.BindInterfacesTo<ResourceFeatureSaveMediator>().AsSingle();
        }
    }
}