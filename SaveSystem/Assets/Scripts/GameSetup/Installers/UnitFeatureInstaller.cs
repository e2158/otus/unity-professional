﻿using Zenject;
using GameEngine;
using GameEngine.SaveData;
using UnityEngine;
using Sirenix.OdinInspector;

namespace GameSetup
{
    [DisallowMultipleComponent]
    internal sealed class UnitFeatureInstaller : MonoInstaller
    {
        [Title("References")]
        [Required, AssetSelector]
        [SerializeField] private UnitPrefabRepository _prefabRepository;

        [Required]
        [SerializeField] private Transform _spawnContainer;

        [Required]
        [SerializeField] private Unit[] _initialUnits;

        private void OnValidate()
        {
            if (this._initialUnits == null || this._initialUnits.Length == 0)
                this.FindInitialUnits();
        }

        [Title("Controls"), Button(ButtonSizes.Medium)]
        private void FindInitialUnits() =>
            this._initialUnits = FindObjectsOfType<Unit>();

        public override void InstallBindings()
        {
            UnitManager unitManager = new();
            unitManager.SetContainer(this._spawnContainer);
            unitManager.SetupUnits(this._initialUnits);

            this.Container.Bind<UnitManager>().FromInstance(unitManager);
            this.Container.Bind<UnitPrefabRepository>().FromInstance(this._prefabRepository);

            this.Container.BindInterfacesTo<UnitFeatureSaveMediator>().AsSingle();
        }
    }
}