﻿using Zenject;
using GameEngine.SaveData;

namespace GameSetup
{
    internal sealed class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            this.Container.Install<SaveServiceInstaller>();
            this.Container.Bind<GameSaveMediator>().AsSingle();
        }
    }
}