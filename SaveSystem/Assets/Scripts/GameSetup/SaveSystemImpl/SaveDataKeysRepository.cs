﻿using System;
using System.Collections.Generic;
using SaveSystem;
using GameEngine.SaveData;

namespace GameSetup
{
    internal sealed class SaveDataKeysRepository : IDataKeysRepository
    {
        private readonly IReadOnlyDictionary<Type, string> _keys = new Dictionary<Type, string>()
        {
            { typeof(UnitFeatureSaveData), "UnitFeature" },
            { typeof(ResourceFeatureSaveData), "ResourceFeature" },
        };

        public string GetKey<TType>() =>
            this._keys[typeof(TType)];
    }
}