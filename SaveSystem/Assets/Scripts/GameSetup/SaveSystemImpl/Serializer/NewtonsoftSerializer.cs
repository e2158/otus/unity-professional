﻿using System;
using SaveSystem;
using Newtonsoft.Json;
using Cysharp.Threading.Tasks;

namespace GameSetup
{
    internal sealed class NewtonsoftSerializer : ISerializer
    {
        public UniTask<string> SerializeData<TData>(TData data) where TData : ISaveData
        {
            string json = data == null
                ? throw new ArgumentNullException(nameof(data))
                : JsonConvert.SerializeObject(data);

            return UniTask.FromResult(json);
        }

        public UniTask<TData> DeserializeData<TData>(string json) where TData : ISaveData
        {
            TData data = string.IsNullOrEmpty(json)
                ? default
                : JsonConvert.DeserializeObject<TData>(json);

            return UniTask.FromResult(data);
        }
    }
}