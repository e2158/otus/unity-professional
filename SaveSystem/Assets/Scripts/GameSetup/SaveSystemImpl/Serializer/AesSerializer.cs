﻿using SaveSystem;
using UnityCipher;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace GameSetup
{
    internal sealed class AesSerializer : ISerializer
    {
        [NotNull] private readonly ISerializer _serializer;
        [NotNull] private readonly IDataKeysRepository _keysRepository;

        public AesSerializer([NotNull] ISerializer serializer, [NotNull] IDataKeysRepository keysRepository)
        {
            this._serializer = serializer;
            this._keysRepository = keysRepository;
        }

        public async UniTask<string> SerializeData<TData>(TData data) where TData : ISaveData
        {
            string password = this._keysRepository.GetKey<TData>();
            string serializeData = await this._serializer.SerializeData(data);

            return RijndaelEncryption.Encrypt(serializeData, password);
        }

        public UniTask<TData> DeserializeData<TData>(string json) where TData : ISaveData
        {
            string password = this._keysRepository.GetKey<TData>();
            string decryptData = RijndaelEncryption.Decrypt(json, password);

            return this._serializer.DeserializeData<TData>(decryptData);
        }
    }
}