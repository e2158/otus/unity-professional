﻿using System.IO;
using SaveSystem;
using UnityEngine;
using Cysharp.Threading.Tasks;

namespace GameSetup
{
    internal sealed class StreamingAssetsDataStorage : IDataStorage
    {
        private const string Extension = ".json";

        public UniTask<string> Read(string key)
        {
            string fileName = key + StreamingAssetsDataStorage.Extension;
            string fileDirectory = Application.streamingAssetsPath;
            string filePath = Path.Combine(fileDirectory, fileName);

            return File.ReadAllTextAsync(filePath).AsUniTask();
        }

        public UniTask Write(string key, string data)
        {
            string fileName = key + StreamingAssetsDataStorage.Extension;
            string fileDirectory = Application.streamingAssetsPath;
            string filePath = Path.Combine(fileDirectory, fileName);

            if (!Directory.Exists(fileDirectory))
                Directory.CreateDirectory(fileDirectory);

            return File.WriteAllTextAsync(filePath, data).AsUniTask();
        }
    }
}