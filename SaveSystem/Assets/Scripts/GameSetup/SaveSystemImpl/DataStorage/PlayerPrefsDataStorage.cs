﻿using SaveSystem;
using UnityEngine;
using Cysharp.Threading.Tasks;

namespace GameSetup
{
    internal sealed class PlayerPrefsDataStorage : IDataStorage
    {
        public UniTask<string> Read(string key)
        {
            string data = PlayerPrefs.GetString(key, string.Empty);
            return UniTask.FromResult(data);
        }

        public UniTask Write(string key, string data)
        {
            PlayerPrefs.SetString(key, data);
            PlayerPrefs.Save();

            return UniTask.CompletedTask;
        }
    }
}