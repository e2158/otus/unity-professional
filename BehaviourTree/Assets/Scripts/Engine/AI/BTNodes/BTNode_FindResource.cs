using System;
using AIModule;
using Atomic.Objects;
using UnityEngine;

namespace Game.Engine
{
    [Serializable]
    public sealed class BTNode_FindResource : BTNode
    {
        public override string Name => "Find Resource";

        [SerializeField, BlackboardKey]
        private ushort character;

        [SerializeField, BlackboardKey]
        private ushort resourceService;

        [SerializeField, BlackboardKey]
        private ushort targetResource;

        protected override BTState OnUpdate(IBlackboard blackboard, float deltaTime)
        {
            if (!blackboard.TryGetObject(character, out IAtomicObject characterObj))
                return BTState.FAILURE;

            if (!blackboard.TryGetObject(resourceService, out ResourceService resourceServiceObj))
                return BTState.FAILURE;

            var characterTransform = characterObj.Get<Transform>(ObjectAPI.Transform);

            bool foundClosestResource = resourceServiceObj
                .FindClosestResource(characterTransform.position, out IAtomicObject resourceObj);

            if (!foundClosestResource)
                return BTState.FAILURE;

            blackboard.SetObject(targetResource, resourceObj);
            return BTState.SUCCESS;
        }
    }
}