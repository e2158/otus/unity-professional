﻿using System.Threading;
using AKerp.Infrastructure.Pipeline;
using Cysharp.Threading.Tasks;
using Essentials;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

namespace AKerp.Game
{
    public sealed class GameRunner : MonoBehaviour
    {
        [SerializeField] private bool _autoRunOnStart;

        private TaskRunner _pipeline;

        [CanBeNull] private CancellationTokenSource _cts;

        [Inject]
        private void Construct(TaskRunner pipeline)
        {
            _pipeline = pipeline;
        }

        private void Start()
        {
            if (_autoRunOnStart)
                Run();
        }

        private void OnDestroy()
        {
            Stop();
        }

        [Button]
        public void Run()
        {
            _cts?.Kill();
            _cts = new CancellationTokenSource();
            ProcessAsync(_cts.Token).Forget();
        }

        [Button]
        public void Stop()
        {
            _cts?.Kill();
        }

        private async UniTask ProcessAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await _pipeline.ProcessAsync(cancellationToken);
            }
        }
    }
}