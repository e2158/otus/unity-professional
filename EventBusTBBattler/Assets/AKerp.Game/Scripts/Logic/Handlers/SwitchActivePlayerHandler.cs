﻿using AKerp.Game.Logic.Events;
using AKerp.Game.Logic.Models;
using AKerp.Infrastructure.CommandSystem;
using AKerp.Infrastructure.EventBus;
using JetBrains.Annotations;

namespace AKerp.Game.Logic.Handlers
{
    [UsedImplicitly]
    public sealed class SwitchActivePlayerHandler : Handler<SwitchActivePlayerEvent>
    {
        public SwitchActivePlayerHandler([NotNull] GameModel gameModel, [NotNull] ICommandCollector commandCollector,
            [NotNull] IEventPublisher eventPublisher, [NotNull] IEventSubscriber eventSubscriber)
            : base(gameModel, commandCollector, eventPublisher, eventSubscriber) { }

        protected override void OnHandleEvent(SwitchActivePlayerEvent evt)
        {
            GameModel.SwitchActivePlayer();
        }
    }
}