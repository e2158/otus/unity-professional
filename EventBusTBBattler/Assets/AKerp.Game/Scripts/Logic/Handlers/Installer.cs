﻿using JetBrains.Annotations;

namespace AKerp.Game.Logic.Handlers
{
    [UsedImplicitly]
    public sealed class Installer : Zenject.Installer<Installer>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<AttackHandler>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<DamageHandler>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<SyncAllHeroesDataHandler>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<SwitchActivePlayerHandler>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<SwitchActivePlayerHeroHandler>().AsSingle().NonLazy();
        }
    }
}