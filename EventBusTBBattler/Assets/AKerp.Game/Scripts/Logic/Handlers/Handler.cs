﻿using System;
using UnityEngine;
using JetBrains.Annotations;
using AKerp.Game.Logic.Models;
using AKerp.Infrastructure.EventBus;
using AKerp.Infrastructure.CommandSystem;

namespace AKerp.Game.Logic.Handlers
{
    public abstract class Handler<TEvent> : IDisposable where TEvent : struct
    {
        protected static readonly ILogger Logger = Debug.unityLogger;

        [NotNull] protected readonly string LogTag;
        [NotNull] protected readonly GameModel GameModel;
        [NotNull] protected readonly ICommandCollector CommandCollector;
        [NotNull] protected readonly IEventPublisher EventPublisher;

        [NotNull] private readonly IEventSubscriber _eventSubscriber;

        protected Handler([NotNull] GameModel gameModel, [NotNull] ICommandCollector commandCollector,
            [NotNull] IEventPublisher eventPublisher, [NotNull] IEventSubscriber eventSubscriber)
        {
            LogTag = GetType().Name;
            GameModel = gameModel ?? throw new ArgumentNullException(nameof(gameModel));
            CommandCollector = commandCollector ?? throw new ArgumentNullException(nameof(commandCollector));
            EventPublisher = eventPublisher ?? throw new ArgumentNullException(nameof(eventPublisher));
            _eventSubscriber = eventSubscriber ?? throw new ArgumentNullException(nameof(eventSubscriber));

            _eventSubscriber.Subscribe<TEvent>(HandleEvent);
        }

        public void Dispose()
        {
            _eventSubscriber.Unsubscribe<TEvent>(HandleEvent);
            OnDispose();
        }

        private void HandleEvent(TEvent evt)
        {
            Logger.Log(LogTag, "Handle Event");
            OnHandleEvent(evt);
        }

        protected abstract void OnHandleEvent(TEvent evt);
        protected virtual void OnDispose() { }
    }
}