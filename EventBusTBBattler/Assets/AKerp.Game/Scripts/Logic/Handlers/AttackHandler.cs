﻿using AKerp.Game.Logic.Configs;
using JetBrains.Annotations;
using AKerp.Game.Logic.Events;
using AKerp.Game.Logic.Models;
using AKerp.Game.Visual.Commands;
using AKerp.Infrastructure.CommandSystem;
using AKerp.Infrastructure.EventBus;

namespace AKerp.Game.Logic.Handlers
{
    [UsedImplicitly]
    public sealed class AttackHandler : Handler<AttackEvent>
    {
        public AttackHandler([NotNull] GameModel gameModel, [NotNull] ICommandCollector commandCollector,
            [NotNull] IEventPublisher eventPublisher, [NotNull] IEventSubscriber eventSubscriber)
            : base(gameModel, commandCollector, eventPublisher, eventSubscriber) { }

        protected override void OnHandleEvent(AttackEvent evt)
        {
            SendDirectVisualCommands(evt);
            SendDirectEvents(evt);

            SendInverseVisualCommands(evt);
            SendInverseEvents(evt);
        }

        private void SendDirectVisualCommands(AttackEvent evt)
        {
            var directCommand = Command.Create<AttackHeroCommand>();
            directCommand.Fill(evt.AttackingPlayerColor, evt.AttackingHeroIndex,
                evt.AttackedPlayerColor, evt.AttackedHeroIndex);

            CommandCollector.Add(directCommand);
        }

        private void SendDirectEvents(AttackEvent evt)
        {
            PlayerModel attackingPlayer = GameModel.Players[(int)evt.AttackingPlayerColor];
            HeroModel attackingHero = attackingPlayer.Heroes[evt.AttackingHeroIndex];
            HeroStatsConfigData attackingHeroStats = attackingHero.Config.Stats;

            DamageEvent damageEvent =
                new(evt.AttackedPlayerColor, evt.AttackedHeroIndex, attackingHeroStats.Damage);

            AbilityEvent abilityEvent =
                new (evt.AttackingPlayerColor, evt.AttackingHeroIndex,
                    evt.AttackedPlayerColor, evt.AttackedHeroIndex);

            EventPublisher.Publish(damageEvent);
            EventPublisher.Publish(abilityEvent);
        }

        private void SendInverseVisualCommands(AttackEvent evt)
        {
            var inverseCommand = Command.Create<AttackHeroCommand>();
            inverseCommand.Fill(evt.AttackedPlayerColor, evt.AttackedHeroIndex,
                evt.AttackingPlayerColor, evt.AttackingHeroIndex);

            CommandCollector.Add(inverseCommand);
        }

        private void SendInverseEvents(AttackEvent evt)
        {
            PlayerModel attackedPlayer = GameModel.Players[(int)evt.AttackedPlayerColor];
            HeroModel attackedHero = attackedPlayer.Heroes[evt.AttackedHeroIndex];
            HeroStatsConfigData attackedHeroStats = attackedHero.Config.Stats;

            DamageEvent damageEvent =
                new(evt.AttackingPlayerColor, evt.AttackingHeroIndex, attackedHeroStats.Damage);

            EventPublisher.Publish(damageEvent);
        }
    }
}