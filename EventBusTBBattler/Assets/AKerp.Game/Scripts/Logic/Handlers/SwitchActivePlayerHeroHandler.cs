﻿using AKerp.Game.Logic.Configs;
using AKerp.Game.Logic.Events;
using AKerp.Game.Logic.Models;
using AKerp.Game.Visual.Commands;
using AKerp.Infrastructure.CommandSystem;
using AKerp.Infrastructure.EventBus;
using Essentials;
using JetBrains.Annotations;

namespace AKerp.Game.Logic.Handlers
{
    [UsedImplicitly]
    public sealed class SwitchActivePlayerHeroHandler : Handler<SwitchActivePlayerHeroEvent>
    {
        public SwitchActivePlayerHeroHandler([NotNull] GameModel gameModel, [NotNull] ICommandCollector commandCollector,
            [NotNull] IEventPublisher eventPublisher, [NotNull] IEventSubscriber eventSubscriber)
            : base(gameModel, commandCollector, eventPublisher, eventSubscriber) { }

        protected override void OnHandleEvent(SwitchActivePlayerHeroEvent evt)
        {
            GameModel.ActivePlayer!.SwitchAliveHero();
            SendVisualCommand();
        }

        private void SendVisualCommand()
        {
            PlayerModel activePlayer = GameModel.ActivePlayer!;
            PlayerColor playerColor = activePlayer.Config.Color;
            HeroModel activeHero = activePlayer.ActiveHero;
            int activeHeroIndex = activePlayer.Heroes.IndexOf(activeHero);

            var command = Command.Create<SelectHeroCommand>();
            command.Fill(playerColor, activeHeroIndex);

            CommandCollector.Add(command);
        }
    }
}