﻿using AKerp.Game.Logic.Events;
using AKerp.Game.Logic.Models;
using AKerp.Game.Visual.Commands;
using AKerp.Infrastructure.CommandSystem;
using AKerp.Infrastructure.EventBus;
using JetBrains.Annotations;

namespace AKerp.Game.Logic.Handlers
{
    [UsedImplicitly]
    public sealed class SyncAllHeroesDataHandler : Handler<SyncAllHeroesDataEvent>
    {
        public SyncAllHeroesDataHandler([NotNull] GameModel gameModel, [NotNull] ICommandCollector commandCollector,
            [NotNull] IEventPublisher eventPublisher, [NotNull] IEventSubscriber eventSubscriber)
            : base(gameModel, commandCollector, eventPublisher, eventSubscriber) { }

        protected override void OnHandleEvent(SyncAllHeroesDataEvent evt)
        {
            foreach (PlayerModel player in GameModel.Players)
            {
                for (int heroIndex = 0; heroIndex < player.Heroes.Count; heroIndex++)
                {
                    HeroModel hero = player.Heroes[heroIndex];
                    SendVisualCommand(player, hero, heroIndex);
                }
            }
        }

        private void SendVisualCommand(PlayerModel player, HeroModel hero, int heroIndex)
        {
            var command = Command.Create<InitHeroCommand>();
            command.Fill(player.Config.Color, heroIndex, hero.Hp, hero.Config.Stats.Damage);

            CommandCollector.Add(command);
        }
    }
}