﻿using AKerp.Game.Logic.Events;
using AKerp.Game.Logic.Models;
using AKerp.Game.Visual.Commands;
using AKerp.Infrastructure.CommandSystem;
using AKerp.Infrastructure.EventBus;
using JetBrains.Annotations;

namespace AKerp.Game.Logic.Handlers
{
    [UsedImplicitly]
    public sealed class DamageHandler : Handler<DamageEvent>
    {
        public DamageHandler([NotNull] GameModel gameModel, [NotNull] ICommandCollector commandCollector,
            [NotNull] IEventPublisher eventPublisher, [NotNull] IEventSubscriber eventSubscriber)
            : base(gameModel, commandCollector, eventPublisher, eventSubscriber) { }

        protected override void OnHandleEvent(DamageEvent damageEvent)
        {
            // Logic
            PlayerModel player = GameModel.Players[(int)damageEvent.PlayerColor];
            HeroModel hero = player.Heroes[damageEvent.HeroIndex];

            hero.GetDamage(damageEvent.Damage);
            int actualHp = hero.Hp;

            // Visual Command
            var command = Command.Create<InitHeroCommand>();
            command.Fill(damageEvent.PlayerColor, damageEvent.HeroIndex,
                actualHp, hero.Config.Stats.Damage);

            CommandCollector.Add(command);
        }
    }
}