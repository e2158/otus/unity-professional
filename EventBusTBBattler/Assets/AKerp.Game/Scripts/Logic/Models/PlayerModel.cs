﻿using System;
using System.Collections.Generic;
using System.Linq;
using AKerp.Game.Logic.Configs;
using AKerp.Game.Logic.States;
using JetBrains.Annotations;
using UnityEngine;

namespace AKerp.Game.Logic.Models
{
    public sealed class PlayerModel
    {
        private static readonly ILogger Logger = Debug.unityLogger;

        [NotNull] private readonly string _logTag;
        [NotNull] private readonly PlayerState _state;
        [NotNull, ItemNotNull] private readonly HeroModel[] _heroes;

        [NotNull] public PlayerConfig Config { get; }

        public PlayerStatus Status => _state.Status;
        [NotNull, ItemNotNull] public IReadOnlyList<HeroModel> Heroes => _heroes;
        [NotNull] public HeroModel ActiveHero => _heroes[_state.ActiveHeroIndex];

        public PlayerModel([NotNull] PlayerState state, [NotNull] PlayerConfig config,
            [NotNull, ItemNotNull] IEnumerable<HeroModel> heroes)
        {
            _state = state ?? throw new ArgumentNullException(nameof(state));
            Config = config ?? throw new ArgumentNullException(nameof(config));

            _heroes = heroes.ToArray();

            if (_heroes.Any(h => h == null))
                throw new ArgumentException("Hero can't be null", nameof(_heroes));

            _logTag = $"{nameof(PlayerModel)} {Config.Name}";
        }

        public void SetStatus(PlayerStatus status)
        {
            if (_state.Status != status)
            {
                _state.Status = status;
                Logger.Log(_logTag, $"Status: {status}");
            }
        }

        public void SwitchAliveHero()
        {
            if (_heroes.All(h => h.IsDead))
            {
                Logger.Log(_logTag, "All heroes are dead");
                return;
            }

            do
            {
                _state.ActiveHeroIndex = (_state.ActiveHeroIndex + 1) % _heroes.Length;
            } while (ActiveHero.IsDead);

            Logger.Log(_logTag, $"Set hero {_state.ActiveHeroIndex} as active");
        }
    }
}