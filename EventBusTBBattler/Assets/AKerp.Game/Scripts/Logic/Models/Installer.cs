﻿using System.Linq;
using AKerp.Game.Logic.Configs;
using AKerp.Game.Logic.States;
using JetBrains.Annotations;

namespace AKerp.Game.Logic.Models
{
    [UsedImplicitly]
    public sealed class Installer : Zenject.Installer<Installer>
    {
        public override void InstallBindings()
        {
            var config = Container.Resolve<GameConfig>();
            var state = Container.Resolve<GameState>();

            GameModel model = CreateGameModel(state, config);

            Container.Bind<GameModel>().FromInstance(model).AsSingle();
        }

        private static GameModel CreateGameModel(GameState state, GameConfig config)
        {
            PlayerModel[] players =
            {
                CreatePlayerModel(state.Players[0], config.RedPlayer),
                CreatePlayerModel(state.Players[1], config.BluePlayer)
            };

            return new GameModel(state, config, players);
        }

        private static PlayerModel CreatePlayerModel(PlayerState state, PlayerConfig config)
        {
            HeroModel[] heroes =
                Enumerable.Range(0, config.Heroes.Count)
                    .Select(i => new HeroModel(state.Heroes[i], config.Heroes[i]))
                    .ToArray();

            return new PlayerModel(state, config, heroes);
        }
    }
}