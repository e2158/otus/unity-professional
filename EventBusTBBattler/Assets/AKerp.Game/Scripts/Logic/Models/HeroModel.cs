﻿using System;
using AKerp.Game.Logic.Configs;
using AKerp.Game.Logic.States;
using JetBrains.Annotations;
using UnityEngine;

namespace AKerp.Game.Logic.Models
{
    public sealed class HeroModel
    {
        private static readonly ILogger Logger = Debug.unityLogger;

        [NotNull] private readonly string _logTag;
        [NotNull] private readonly HeroState _state;

        [NotNull] public HeroConfig Config { get; }

        public int Hp => _state.Hp;
        public bool IsDead => Hp == 0;

        public HeroModel([NotNull] HeroState state, [NotNull] HeroConfig config)
        {
            _state = state ?? throw new ArgumentNullException(nameof(state));
            Config = config ?? throw new ArgumentNullException(nameof(config));

            _logTag = $"{nameof(HeroModel)} {Config.Meta.Name}";
        }

        public void GetDamage(int damage)
        {
            if (damage < 0)
                throw new ArgumentException("Can't be negative.", nameof(damage));

            _state.Hp = Mathf.Max(0, _state.Hp - damage);
            Logger.Log(_logTag, $"Got Damage {damage}. Current Hp {_state.Hp}");
        }
    }
}