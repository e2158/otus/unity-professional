﻿using System;
using System.Collections.Generic;
using System.Linq;
using AKerp.Game.Logic.Configs;
using AKerp.Game.Logic.States;
using JetBrains.Annotations;
using ModestTree;
using UnityEngine;

namespace AKerp.Game.Logic.Models
{
    public sealed class GameModel
    {
        private const string LogTag = nameof(GameModel);
        private static readonly ILogger Logger = Debug.unityLogger;

        [NotNull] private readonly GameState _state;
        [NotNull, ItemNotNull] private readonly PlayerModel[] _players;

        [NotNull] public GameConfig Config { get; }
        [NotNull, ItemNotNull] public IReadOnlyList<PlayerModel> Players => _players;

        [CanBeNull] public PlayerModel ActivePlayer =>
            _players.SingleOrDefault(p => p.Status == PlayerStatus.Turn);

        public GameModel([NotNull] GameState state, [NotNull] GameConfig config,
            [NotNull, ItemNotNull] IEnumerable<PlayerModel> players)
        {
            _state = state ?? throw new ArgumentNullException(nameof(state));
            Config = config ?? throw new ArgumentNullException(nameof(config));

            _players = players.ToArray();

            if (_players.Any(p => p == null))
                throw new ArgumentException("Player can't be null", nameof(players));
        }

        public void SwitchActivePlayer()
        {
            int activePlayerIndex = CalcNextActivePlayerIndex();

            for (int i = 0; i < _players.Length; i++)
                _players[i].SetStatus((i == activePlayerIndex) switch
                {
                    true => PlayerStatus.Turn,
                    false => PlayerStatus.Wait
                });

            Logger.Log(LogTag, $"Set player {activePlayerIndex} as active");
        }

        private int CalcNextActivePlayerIndex()
        {
            if (ActivePlayer == null)
                return default;

            PlayerModel activePlayer = ActivePlayer;
            int activePlayerIndex = _players.IndexOf(activePlayer);

            return (activePlayerIndex + 1) % _players.Length;
        }
    }
}