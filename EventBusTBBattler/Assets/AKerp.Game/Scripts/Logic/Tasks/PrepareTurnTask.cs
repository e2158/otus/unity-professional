﻿using System.Threading;
using AKerp.Game.Logic.Events;
using AKerp.Infrastructure.EventBus;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace AKerp.Game.Logic.Tasks
{
    [UsedImplicitly]
    public sealed class PrepareTurnTask : Task
    {
        public PrepareTurnTask([NotNull] IEventPublisher eventPublisher) : base(eventPublisher)
        { }

        protected override UniTask ProcessAsyncImpl(CancellationToken cancellationToken)
        {
            EventPublisher.Publish(new SyncAllHeroesDataEvent());
            EventPublisher.Publish(new SwitchActivePlayerEvent());
            EventPublisher.Publish(new SwitchActivePlayerHeroEvent());
            return UniTask.CompletedTask;
        }
    }
}