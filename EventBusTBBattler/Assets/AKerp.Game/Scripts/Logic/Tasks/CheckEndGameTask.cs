﻿using System.Threading;
using AKerp.Infrastructure.EventBus;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace AKerp.Game.Logic.Tasks
{
    [UsedImplicitly]
    public sealed class CheckEndGameTask : Task
    {
        public CheckEndGameTask([NotNull] IEventPublisher eventPublisher) : base(eventPublisher)
        { }

        protected override UniTask ProcessAsyncImpl(CancellationToken cancellationToken)
        {
            return UniTask.CompletedTask;
        }
    }
}