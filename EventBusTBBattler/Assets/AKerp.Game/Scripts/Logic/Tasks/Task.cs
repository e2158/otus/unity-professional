﻿using System;
using JetBrains.Annotations;
using AKerp.Infrastructure.EventBus;

namespace AKerp.Game.Logic.Tasks
{
    public abstract class Task : Infrastructure.Pipeline.Task
    {
        [NotNull] protected readonly IEventPublisher EventPublisher;

        protected Task([NotNull] IEventPublisher eventPublisher)
        {
            EventPublisher = eventPublisher ?? throw new ArgumentNullException(nameof(eventPublisher));
        }
    }
}