﻿using System;
using System.Threading;
using AKerp.Game.Logic.Configs;
using AKerp.Game.Logic.Events;
using AKerp.Game.Logic.Models;
using AKerp.Game.Logic.States;
using AKerp.Infrastructure.EventBus;
using Cysharp.Threading.Tasks;
using Essentials;
using JetBrains.Annotations;
using Otus.Protected.UI;
using Otus.Extensions;

namespace AKerp.Game.Logic.Tasks
{
    [UsedImplicitly]
    public sealed class PlayerTurnTask : Task, IDisposable
    {
        [NotNull] private readonly UIService _uiService;
        [NotNull] private readonly GameModel _gameModel;

        [CanBeNull] private UniTaskCompletionSource<AttackEvent> _tcs;

        public PlayerTurnTask([NotNull] UIService uiService, [NotNull] GameModel gameModel,
            [NotNull] IEventPublisher eventPublisher) : base(eventPublisher)
        {
            _uiService = uiService ?? throw new ArgumentNullException(nameof(uiService));
            _gameModel = gameModel ?? throw new ArgumentNullException(nameof(gameModel));
        }

        public void Dispose()
        {
            _tcs?.TrySetCanceled();
            _tcs = null;
        }

        protected override UniTask ProcessAsyncImpl(CancellationToken cancellationToken) =>
            _tcs!.Task.AttachExternalCancellation(cancellationToken);

        protected override void OnStart() => Init();
        protected override void OnCancel() => Deinit();
        protected override void OnFinish()
        {
            AttackEvent attackEvent = _tcs!.GetResult(default);
            Deinit();
            EventPublisher.Publish(attackEvent);
        }

        private void Init()
        {
            _tcs = new UniTaskCompletionSource<AttackEvent>();
            _uiService.GetRedPlayer().OnHeroClicked += OnRedPlayerClicked;
            _uiService.GetBluePlayer().OnHeroClicked += OnBluePlayerClicked;
        }

        private void Deinit()
        {
            _tcs = null;
            _uiService.GetRedPlayer().OnHeroClicked -= OnRedPlayerClicked;
            _uiService.GetBluePlayer().OnHeroClicked -= OnBluePlayerClicked;
        }

        private void OnRedPlayerClicked(HeroView clickedHeroView)
        {
            if (_gameModel.ActivePlayer?.Config.Color == PlayerColor.Blue)
            {
                int clickedPlayerIndex = (int)PlayerColor.Red;
                int clickedHeroIndex = _uiService.FindHeroIndex(clickedPlayerIndex, clickedHeroView);
                HandleClick(clickedPlayerIndex, clickedHeroIndex);
            }
        }

        private void OnBluePlayerClicked(HeroView clickedHeroView)
        {
            if (_gameModel.ActivePlayer?.Config.Color == PlayerColor.Red)
            {
                int clickedPlayerIndex = (int)PlayerColor.Blue;
                int clickedHeroIndex = _uiService.FindHeroIndex(clickedPlayerIndex, clickedHeroView);
                HandleClick(clickedPlayerIndex, clickedHeroIndex);
            }
        }

        private void HandleClick(int attackedPlayerIndex, int attackedHeroIndex)
        {
            PlayerModel attackedPlayer = _gameModel.Players[attackedPlayerIndex];
            HeroModel attackedHero = attackedPlayer.Heroes[attackedHeroIndex];

            Logger.Log(LogTag, $"Clicked player {attackedPlayerIndex}, hero {attackedHeroIndex}");
            if (attackedPlayer.Status == PlayerStatus.Wait && !attackedHero.IsDead)
            {
                var attackingPlayerColor = (PlayerColor)_gameModel.Players.IndexOf(_gameModel.ActivePlayer!);
                int attackingHeroIndex = _gameModel.ActivePlayer!.Heroes.IndexOf(_gameModel.ActivePlayer.ActiveHero);

                Logger.Log(LogTag, $"Send attack event: player {attackedPlayerIndex}, hero {attackedHeroIndex}");
                _tcs!.TrySetResult(new AttackEvent(attackingPlayerColor, attackingHeroIndex,
                    (PlayerColor)attackedPlayerIndex, attackedHeroIndex));
            }
        }
    }
}