﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace AKerp.Game.Logic.Configs
{
    [CreateAssetMenu(fileName = nameof(GameConfig), menuName = "Configs/" + nameof(GameConfig))]
    public sealed class GameConfig : ScriptableObject
    {
        [SerializeField, Required, AssetSelector] private PlayerConfig _redPlayer;
        [SerializeField, Required, AssetSelector] private PlayerConfig _bluePlayer;

        public PlayerConfig RedPlayer => _redPlayer;
        public PlayerConfig BluePlayer => _bluePlayer;
    }
}