﻿using System;
using UnityEngine;

namespace AKerp.Game.Logic.Configs.Ability // DON'T RENAME
{
    [Serializable]
    public sealed class AttackWrongTargetAbility : AbilityConfigData // DON'T RENAME
    {
        [SerializeField, Range(0, 1)] private float _chance;

        public float Chance => _chance;
    }
}