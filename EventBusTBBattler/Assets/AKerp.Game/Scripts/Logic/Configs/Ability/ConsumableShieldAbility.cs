﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace AKerp.Game.Logic.Configs.Ability // DON'T RENAME
{
    [Serializable]
    public sealed class ConsumableShieldAbility : AbilityConfigData // DON'T RENAME
    {
        [SerializeField, MinValue(0)] private int _amount;

        public int Amount => _amount;
    }
}