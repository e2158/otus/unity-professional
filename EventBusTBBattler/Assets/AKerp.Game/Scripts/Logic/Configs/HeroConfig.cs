﻿using System;
using AKerp.Game.Logic.Configs.Ability;
using Sirenix.OdinInspector;
using UnityEngine;

namespace AKerp.Game.Logic.Configs
{
    [CreateAssetMenu(fileName = nameof(HeroConfig), menuName = "Configs/" + nameof(HeroConfig))]
    public sealed class HeroConfig : ScriptableObject
    {
        [BoxGroup("Meta Data"), HideLabel]
        [SerializeField, Required] private HeroMetaConfigData _meta;

        [BoxGroup("Stats Data"), HideLabel]
        [SerializeField, Required] private HeroStatsConfigData _stats;

        [BoxGroup("Abilities Data"), HideLabel]
        [SerializeField, Required] private HeroAbilityConfigData _ability;

        public HeroMetaConfigData Meta => _meta;
        public HeroStatsConfigData Stats => _stats;
        public HeroAbilityConfigData Ability => _ability;
    }

    [Serializable]
    public sealed class HeroMetaConfigData
    {
        [SerializeField, Required] private string _name;

        [PreviewField, AssetSelector]
        [SerializeField, Required] private Sprite _icon;

        public string Name => _name;
        public Sprite Icon => _icon;
    }

    [Serializable]
    public sealed class HeroStatsConfigData
    {
        [SerializeField, MinValue(0)] private int _hp;
        [SerializeField, MinValue(0)] private int _damage;

        public int Hp => _hp;
        public int Damage => _damage;
    }

    [Serializable]
    public sealed class HeroAbilityConfigData
    {
        [SerializeReference, Required] private AbilityConfigData _ability;

        public AbilityConfigData Ability => _ability;
    }
}