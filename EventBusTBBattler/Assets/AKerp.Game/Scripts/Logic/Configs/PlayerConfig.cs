﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace AKerp.Game.Logic.Configs
{
    [CreateAssetMenu(fileName = nameof(PlayerConfig), menuName = "Configs/" + nameof(PlayerConfig))]
    public sealed class PlayerConfig : ScriptableObject
    {
        [SerializeField, Required] private string _name;
        [SerializeField, Required] private PlayerColor _color;

        [Required, AssetList, ListDrawerSettings(ShowIndexLabels = true)]
        [SerializeField] private HeroConfig[] _heroes;

        public string Name => _name;
        public PlayerColor Color => _color;
        public IReadOnlyList<HeroConfig> Heroes => _heroes;
    }

    public enum PlayerColor
    {
        Red = 0,
        Blue = 1
    }

    public static class PlayerColorExtensions
    {
        public static Color ToUnityColor(this PlayerColor color) =>
            color switch
            {
                PlayerColor.Red => Color.red,
                PlayerColor.Blue => Color.blue,
                _ => throw new ArgumentOutOfRangeException(nameof(color), color, null)
            };
    }
}