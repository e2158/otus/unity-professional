﻿using System;
using JetBrains.Annotations;

namespace AKerp.Game.Logic.Configs
{
    [UsedImplicitly]
    public sealed class Installer : Zenject.Installer<GameConfig, Installer>
    {
        [NotNull] private readonly GameConfig _config;

        public Installer([NotNull] GameConfig config)
        {
            _config = config ?? throw new ArgumentNullException(nameof(config));
        }

        public override void InstallBindings()
        {
            Container.Bind<GameConfig>().FromInstance(_config).AsSingle();
        }
    }
}