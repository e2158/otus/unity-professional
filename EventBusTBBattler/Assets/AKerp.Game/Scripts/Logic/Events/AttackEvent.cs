﻿using System;
using AKerp.Game.Logic.Configs;

namespace AKerp.Game.Logic.Events
{
    public readonly struct AttackEvent
    {
        public readonly PlayerColor AttackingPlayerColor;
        public readonly int AttackingHeroIndex;

        public readonly PlayerColor AttackedPlayerColor;
        public readonly int AttackedHeroIndex;

        public AttackEvent(
            PlayerColor attackingPlayerColor, int attackingHeroIndex,
            PlayerColor attackedPlayerColor, int attackedHeroIndex)
        {
            if (attackingHeroIndex < 0) throw new ArgumentOutOfRangeException(nameof(attackingHeroIndex), "Can't be negative");
            if (attackedHeroIndex < 0) throw new ArgumentOutOfRangeException(nameof(attackedHeroIndex), "Can't be negative");

            AttackingPlayerColor = attackingPlayerColor;
            AttackingHeroIndex = attackingHeroIndex;

            AttackedPlayerColor = attackedPlayerColor;
            AttackedHeroIndex = attackedHeroIndex;
        }
    }
}