﻿using System;
using AKerp.Game.Logic.Configs;

namespace AKerp.Game.Logic.Events
{
    public readonly struct DamageEvent
    {
        public readonly PlayerColor PlayerColor;
        public readonly int HeroIndex;
        public readonly int Damage;

        public DamageEvent(PlayerColor playerColor, int heroIndex, int damage)
        {
            if (heroIndex < 0) throw new ArgumentException("Can't be negative.", nameof(heroIndex));
            if (damage < 0) throw new ArgumentException("Can't be negative.", nameof(damage));

            PlayerColor = playerColor;
            HeroIndex = heroIndex;
            Damage = damage;
        }
    }
}