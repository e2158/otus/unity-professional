﻿using System;

namespace AKerp.Game.Logic.States
{
    [Serializable]
    public sealed class PlayerState
    {
        public HeroState[] Heroes;
        public PlayerStatus Status;
        public int ActiveHeroIndex;
    }

    public enum PlayerStatus
    {
        Wait = 0,
        Turn = 1
    }
}