﻿using System;

namespace AKerp.Game.Logic.States
{
    [Serializable]
    public sealed class GameState
    {
        public PlayerState[] Players;
    }
}