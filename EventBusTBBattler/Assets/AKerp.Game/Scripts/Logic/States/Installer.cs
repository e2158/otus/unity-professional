﻿using System.Linq;
using AKerp.Game.Logic.Configs;
using JetBrains.Annotations;

namespace AKerp.Game.Logic.States
{
    [UsedImplicitly]
    public sealed class Installer : Zenject.Installer<Installer>
    {
        public override void InstallBindings()
        {
            var config = Container.Resolve<GameConfig>();

            GameState state = CreateGameState(config);

            Container.Bind<GameState>().FromInstance(state).AsSingle();
        }

        private GameState CreateGameState(GameConfig config) =>
            new()
            {
                Players = new[]
                {
                    CreatePlayerState(config.RedPlayer),
                    CreatePlayerState(config.BluePlayer),
                }
            };

        private PlayerState CreatePlayerState(PlayerConfig config) =>
            new()
            {
                ActiveHeroIndex = -1,
                Status = PlayerStatus.Wait,
                Heroes = config.Heroes
                    .Select(CreateHeroState)
                    .ToArray()
            };

        private HeroState CreateHeroState(HeroConfig config) =>
            new()
            {
                Hp = config.Stats.Hp
            };
    }
}