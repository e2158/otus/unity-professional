﻿using System;
using System.Collections.Generic;
using AKerp.Game.Logic.Configs;
using AKerp.Game.Logic.Tasks;
using AKerp.Game.Visual.CommandProcessors;
using AKerp.Game.Visual.Commands;
using AKerp.Game.Visual.Tasks;
using AKerp.Infrastructure.CommandSystem;
using Otus.Protected.UI;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;
using Task = AKerp.Infrastructure.Pipeline.Task;

namespace AKerp.Game
{
    internal sealed class GameInstaller : MonoInstaller
    {
        [SerializeField, Required, AssetSelector] private GameConfig _gameConfig;
        [SerializeField, Required] private UIService _uiService;

        public override void InstallBindings()
        {
            Container.Bind<UIService>().FromInstance(_uiService).AsSingle();

            Logic.Configs.Installer.Install(Container, _gameConfig);
            Logic.Handlers.Installer.Install(Container);
            Logic.States.Installer.Install(Container);
            Logic.Models.Installer.Install(Container);

            Infrastructure.CommandSystem.Installer.Install(Container, CreateCommandProcessors());
            Infrastructure.Pipeline.Installer.Install(Container, CreateTasks());
            Infrastructure.EventBus.Installer.Install(Container);
        }

        private IEnumerable<KeyValuePair<Type, ICommandProcessor>> CreateCommandProcessors()
        {
            IInstantiator instantiator = Container;

            return new Dictionary<Type, ICommandProcessor>()
            {
                [typeof(InitHeroCommand)] = instantiator.Instantiate<InitHeroCommandProcessor>(),
                [typeof(SelectHeroCommand)] = instantiator.Instantiate<SelectHeroCommandProcessor>(),
                [typeof(AttackHeroCommand)] = instantiator.Instantiate<AttackHeroCommandProcessor>(),
            };
        }

        private IEnumerable<Task> CreateTasks()
        {
            DiContainer subContainer = Container.CreateSubContainer();

            subContainer.BindInterfacesAndSelfTo<PlayerTurnTask>().AsSingle();
            subContainer.BindInterfacesAndSelfTo<PrepareTurnTask>().AsSingle();
            subContainer.BindInterfacesAndSelfTo<CheckEndGameTask>().AsSingle();
            subContainer.BindInterfacesAndSelfTo<ProcessVisualCommandsTask>().AsSingle();

            yield return subContainer.Resolve<PrepareTurnTask>();
            yield return subContainer.Resolve<ProcessVisualCommandsTask>();
            yield return subContainer.Resolve<PlayerTurnTask>();
            yield return subContainer.Resolve<ProcessVisualCommandsTask>();
            yield return subContainer.Resolve<CheckEndGameTask>();
        }
    }
}