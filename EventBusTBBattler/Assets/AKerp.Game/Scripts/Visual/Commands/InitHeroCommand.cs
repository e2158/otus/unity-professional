﻿using System;
using AKerp.Game.Logic.Configs;
using AKerp.Infrastructure.CommandSystem;

namespace AKerp.Game.Visual.Commands
{
    public sealed class InitHeroCommand : Command
    {
        public PlayerColor PlayerColor { get; private set; }
        public int HeroIndex { get; private set; }
        public int Damage { get; private set; }
        public int Hp { get; private set; }

        public void Fill(PlayerColor playerColor, int heroIndex, int hp, int damage)
        {
            if (heroIndex < 0) throw new ArgumentOutOfRangeException(nameof(heroIndex), "Can't be negative.");
            if (damage < 0) throw new ArgumentOutOfRangeException(nameof(damage), "Can't be negative.");
            if (hp < 0) throw new ArgumentOutOfRangeException(nameof(hp), "Can't be negative.");

            PlayerColor = playerColor;
            HeroIndex = heroIndex;
            Damage = damage;
            Hp = hp;
        }
    }
}