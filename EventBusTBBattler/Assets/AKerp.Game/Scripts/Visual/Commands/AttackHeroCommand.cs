﻿using System;
using AKerp.Game.Logic.Configs;
using AKerp.Infrastructure.CommandSystem;

namespace AKerp.Game.Visual.Commands
{
    public sealed class AttackHeroCommand : Command
    {
        public PlayerColor AttackingPlayerColor { get; private set; }
        public int AttackingHeroIndex { get; private set; }

        public PlayerColor AttackedPlayerColor { get; private set; }
        public int AttackedHeroIndex { get; private set; }

        public void Fill(
            PlayerColor attackingPlayerColor, int attackingHeroIndex,
            PlayerColor attackedPlayerColor, int attackedHeroIndex)
        {
            if (attackingHeroIndex < 0) throw new ArgumentOutOfRangeException(nameof(attackingHeroIndex), "Can't be negative");
            if (attackedHeroIndex < 0) throw new ArgumentOutOfRangeException(nameof(attackedHeroIndex), "Can't be negative");

            AttackingPlayerColor = attackingPlayerColor;
            AttackingHeroIndex = attackingHeroIndex;

            AttackedPlayerColor = attackedPlayerColor;
            AttackedHeroIndex = attackedHeroIndex;
        }
    }
}