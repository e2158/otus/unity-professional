﻿using System;
using AKerp.Game.Logic.Configs;
using AKerp.Infrastructure.CommandSystem;

namespace AKerp.Game.Visual.Commands
{
    public sealed class SelectHeroCommand : Command
    {
        public PlayerColor PlayerColor { get; private set; }
        public int HeroIndex { get; private set; }

        public void Fill(PlayerColor playerColor, int heroIndex)
        {
            if (heroIndex < 0) throw new ArgumentOutOfRangeException(nameof(heroIndex), "Can't be negative");
            PlayerColor = playerColor;
            HeroIndex = heroIndex;
        }
    }
}