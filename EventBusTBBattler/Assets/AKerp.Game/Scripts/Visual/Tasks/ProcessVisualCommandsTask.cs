﻿using System;
using System.Threading;
using AKerp.Infrastructure.CommandSystem;
using AKerp.Infrastructure.Pipeline;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace AKerp.Game.Visual.Tasks
{
    [UsedImplicitly]
    public sealed class ProcessVisualCommandsTask : Task
    {
        [NotNull] private readonly ICommandQueue _commandQueue;
        [NotNull] private readonly CommandRunner _commandRunner;

        public ProcessVisualCommandsTask([NotNull] ICommandQueue commandQueue, [NotNull] CommandRunner commandRunner)
        {
            _commandQueue = commandQueue ?? throw new ArgumentNullException(nameof(commandQueue));
            _commandRunner = commandRunner ?? throw new ArgumentNullException(nameof(commandRunner));
        }

        protected override async UniTask ProcessAsyncImpl(CancellationToken cancellationToken)
        {
            await _commandRunner.RunAsync(_commandQueue.Commands, cancellationToken);
            _commandQueue.Clear();
        }
    }
}