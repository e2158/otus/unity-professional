﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using AKerp.Game.Logic.Configs;
using AKerp.Game.Visual.Commands;
using AKerp.Infrastructure.CommandSystem;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Otus.Protected.UI;

namespace AKerp.Game.Visual.CommandProcessors
{
    [UsedImplicitly]
    public sealed class SelectHeroCommandProcessor : CommandProcessor<SelectHeroCommand>
    {
        [NotNull] private readonly UIService _uiService;

        public SelectHeroCommandProcessor([NotNull] UIService uiService)
        {
            _uiService = uiService ?? throw new ArgumentNullException(nameof(uiService));
        }

        protected override UniTask ProcessAsyncImpl(SelectHeroCommand command, CancellationToken cancellationToken)
        {
            UnselectAllHeroes();
            SelectHeroViewFromCommand(command);
            return UniTask.CompletedTask;
        }

        private void UnselectAllHeroes()
        {
            IEnumerable<HeroView> allHeroViews =
                _uiService.GetRedPlayer().GetViews()
                    .Union(_uiService.GetBluePlayer().GetViews());

            foreach (HeroView heroView in allHeroViews)
                heroView.SetActive(false);

            Logger.Log(LogTag, "Unselected all hero views");
        }

        private void SelectHeroViewFromCommand(SelectHeroCommand command)
        {
            HeroListView heroListView = command.PlayerColor switch
            {
                PlayerColor.Red => _uiService.GetRedPlayer(),
                PlayerColor.Blue => _uiService.GetBluePlayer(),
                _ => throw new ArgumentOutOfRangeException()
            };

            HeroView heroView = heroListView.GetView(command.HeroIndex);
            heroView.SetActive(true);

            Logger.Log(LogTag, $"Selected hero {command.HeroIndex} of player {command.PlayerColor}");
        }
    }
}