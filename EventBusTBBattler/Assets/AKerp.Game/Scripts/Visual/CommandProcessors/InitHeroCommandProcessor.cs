﻿using System;
using System.Threading;
using AKerp.Game.Visual.Commands;
using AKerp.Infrastructure.CommandSystem;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Otus.Extensions;
using Otus.Protected.UI;

namespace AKerp.Game.Visual.CommandProcessors
{
    [UsedImplicitly]
    public sealed class InitHeroCommandProcessor : CommandProcessor<InitHeroCommand>
    {
        [NotNull] private readonly UIService _uiService;

        public InitHeroCommandProcessor([NotNull] UIService uiService)
        {
            _uiService = uiService ?? throw new ArgumentNullException(nameof(uiService));
        }

        protected override UniTask ProcessAsyncImpl(InitHeroCommand command, CancellationToken cancellationToken)
        {
            HeroView heroView = _uiService.FindHeroView((int)command.PlayerColor, command.HeroIndex);
            heroView.SetStats($"{command.Hp}/{command.Damage}");
            return UniTask.CompletedTask;
        }
    }
}