﻿using System;
using System.Threading;
using AKerp.Game.Visual.Commands;
using AKerp.Infrastructure.CommandSystem;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Otus.Extensions;
using Otus.Protected.UI;

namespace AKerp.Game.Visual.CommandProcessors
{
    [UsedImplicitly]
    public sealed class AttackHeroCommandProcessor : CommandProcessor<AttackHeroCommand>
    {
        [NotNull] private readonly UIService _uiService;

        public AttackHeroCommandProcessor([NotNull] UIService uiService)
        {
            _uiService = uiService ?? throw new ArgumentNullException(nameof(uiService));
        }

        protected override async UniTask ProcessAsyncImpl(AttackHeroCommand command, CancellationToken cancellationToken)
        {
            Logger.Log(LogTag, $"Hero {command.AttackingHeroIndex} of player {command.AttackingPlayerColor} " +
                               $"attacking hero {command.AttackedHeroIndex} of player {command.AttackedPlayerColor}");

            HeroView attackingHeroView = _uiService.FindHeroView((int)command.AttackingPlayerColor, command.AttackingHeroIndex);
            HeroView attackedHeroView = _uiService.FindHeroView((int)command.AttackedPlayerColor, command.AttackedHeroIndex);

            await attackingHeroView.AnimateAttack(attackedHeroView).AttachExternalCancellation(cancellationToken);
        }
    }
}