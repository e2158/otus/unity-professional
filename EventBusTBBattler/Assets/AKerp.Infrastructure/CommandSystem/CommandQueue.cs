﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace AKerp.Infrastructure.CommandSystem
{
    [UsedImplicitly]
    internal sealed class CommandQueue : ICommandCollector, ICommandQueue
    {
        private const string LogTag = nameof(CommandQueue);
        private static readonly ILogger Logger = Debug.unityLogger;

        private readonly List<Command> _commands = new();

        public IEnumerable<Command> Commands => _commands;

        public void Clear()
        {
            foreach (Command command in _commands)
                command.Release();

            _commands.Clear();

            Logger.Log(LogTag, "Cleared");
        }

        public void Add<TCommand>(TCommand command) where TCommand : Command
        {
            _commands.Add(command);
            Logger.Log(LogTag, $"Added: {typeof(TCommand).Name}");
        }
    }
}