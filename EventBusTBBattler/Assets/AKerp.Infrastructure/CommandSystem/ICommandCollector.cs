﻿namespace AKerp.Infrastructure.CommandSystem
{
    public interface ICommandCollector
    {
        void Add<TCommand>(TCommand command) where TCommand : Command;
    }
}