﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace AKerp.Infrastructure.CommandSystem
{
    [UsedImplicitly]
    public sealed class Installer : Zenject.Installer<IEnumerable<KeyValuePair<Type, ICommandProcessor>>, Installer>
    {
        [NotNull] private readonly IEnumerable<KeyValuePair<Type, ICommandProcessor>> _processors;

        public Installer([NotNull] IEnumerable<KeyValuePair<Type, ICommandProcessor>> processors)
        {
            _processors = processors ?? throw new ArgumentNullException(nameof(processors));
        }

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<CommandQueue>().AsSingle();
            Container.Bind<CommandRunner>().AsSingle().WithArguments(_processors);
        }
    }
}