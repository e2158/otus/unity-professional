﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine;

namespace AKerp.Infrastructure.CommandSystem
{
    public interface ICommandProcessor
    {
        UniTask ProcessAsync([NotNull] Command command, CancellationToken cancellationToken);
    }

    public abstract class CommandProcessor<TCommand> : ICommandProcessor where TCommand : Command
    {
        protected readonly string LogTag;
        protected readonly ILogger Logger;

        protected CommandProcessor()
        {
            LogTag = GetType().Name;
            Logger = Debug.unityLogger;
        }

        UniTask ICommandProcessor.ProcessAsync(Command command, CancellationToken cancellationToken) =>
            ProcessAsync((TCommand)command, cancellationToken);

        public async UniTask ProcessAsync([NotNull] TCommand command, CancellationToken cancellationToken)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            Logger.Log(LogTag, "Processing...");
            await ProcessAsyncImpl(command, cancellationToken);
            Logger.Log(LogTag, "Processed");
        }

        protected abstract UniTask ProcessAsyncImpl(TCommand command, CancellationToken cancellationToken);
    }
}