﻿using System;
using System.Collections.Generic;

namespace AKerp.Infrastructure.CommandSystem
{
    public abstract class Command
    {
        private static readonly Dictionary<Type, Queue<Command>> Pool = new();

        public static TCommand Create<TCommand>() where TCommand : Command, new()
        {
            if (Pool.TryGetValue(typeof(TCommand), out Queue<Command> queue))
                return (TCommand)queue.Dequeue();

            return new TCommand();
        }

        internal void Release()
        {
            Type type = GetType();

            if (!Pool.TryGetValue(type, out Queue<Command> queue))
                Pool[type] = queue = new Queue<Command>();

            queue.Enqueue(this);
        }
    }
}