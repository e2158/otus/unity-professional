﻿using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace AKerp.Infrastructure.CommandSystem
{
    [UsedImplicitly]
    public sealed class CommandRunner
    {
        [NotNull] private readonly Dictionary<Type, ICommandProcessor> _processors;

        public CommandRunner([NotNull] IEnumerable<KeyValuePair<Type, ICommandProcessor>> processors)
        {
            _processors = new Dictionary<Type, ICommandProcessor>(processors);
        }

        public async UniTask RunAsync([NotNull, ItemNotNull] IEnumerable<Command> commands,
            CancellationToken cancellationToken)
        {
            foreach (Command command in commands)
                await RunAsync(command, cancellationToken);
        }

        public UniTask RunAsync<TCommand>([NotNull] TCommand command,
            CancellationToken cancellationToken) where TCommand : Command
        {
            Type type = command.GetType();
            return _processors[type].ProcessAsync(command, cancellationToken);
        }
    }
}