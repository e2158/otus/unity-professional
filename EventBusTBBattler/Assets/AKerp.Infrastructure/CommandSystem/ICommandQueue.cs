﻿using System.Collections.Generic;

namespace AKerp.Infrastructure.CommandSystem
{
    public interface ICommandQueue
    {
        IEnumerable<Command> Commands { get; }
        void Clear();
    }
}