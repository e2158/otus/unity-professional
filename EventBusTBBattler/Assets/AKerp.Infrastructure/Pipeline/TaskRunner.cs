﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace AKerp.Infrastructure.Pipeline
{
    public sealed class TaskRunner
    {
        [NotNull, ItemNotNull] private readonly Task[] _tasks;

        public TaskRunner(IEnumerable<Task> tasks)
        {
            _tasks = tasks?.ToArray() ?? throw new ArgumentNullException(nameof(tasks));
        }

        public async UniTask ProcessAsync(CancellationToken cancellationToken)
        {
            foreach (Task task in _tasks)
                await task.ProcessAsync(cancellationToken);
        }
    }
}