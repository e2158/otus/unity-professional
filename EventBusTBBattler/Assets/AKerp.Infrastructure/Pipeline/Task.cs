﻿using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace AKerp.Infrastructure.Pipeline
{
    public abstract class Task
    {
        protected readonly string LogTag;
        protected readonly ILogger Logger;

        protected Task()
        {
            LogTag = GetType().Name;
            Logger = Debug.unityLogger;
        }

        public async UniTask ProcessAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            Logger.Log(LogTag, "Processing...");
            OnStart();

            bool isCancelled = await ProcessAsyncImpl(cancellationToken)
                .SuppressCancellationThrow();

            if (isCancelled) OnCancel();
            else OnFinish();

            Logger.Log(LogTag, isCancelled ? "Cancelled" : "Processed");
            cancellationToken.ThrowIfCancellationRequested();
        }

        protected abstract UniTask ProcessAsyncImpl(CancellationToken cancellationToken);

        protected virtual void OnStart() { }
        protected virtual void OnFinish() { }
        protected virtual void OnCancel() { }
    }
}