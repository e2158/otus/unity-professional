﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace AKerp.Infrastructure.Pipeline
{
    [UsedImplicitly]
    public sealed class Installer : Zenject.Installer<IEnumerable<Task>, Installer>
    {
        [NotNull, ItemNotNull] private readonly IEnumerable<Task> _tasks;

        public Installer([NotNull] [ItemNotNull] IEnumerable<Task> tasks)
        {
            _tasks = tasks ?? throw new ArgumentNullException(nameof(tasks));
        }

        public override void InstallBindings()
        {
            Container.Bind<TaskRunner>().AsSingle().WithArguments(_tasks);
        }
    }
}