﻿namespace AKerp.Infrastructure.EventBus
{
    public interface IEventPublisher
    {
        public void Publish<TEvent>(TEvent evt) where TEvent : struct;
    }
}