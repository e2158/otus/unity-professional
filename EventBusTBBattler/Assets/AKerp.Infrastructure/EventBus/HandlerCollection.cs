﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AKerp.Infrastructure.EventBus
{
    internal sealed class HandlerCollection
    {
        private readonly Dictionary<Type, List<Delegate>> _handlers = new();

        public IEnumerable<Type> Keys => _handlers.Keys;
        public bool HasAny => _handlers.Values.Any(l => l.Count > 0);

        public void Clear()
        {
            _handlers.Clear();
        }

        public void Add<TEvent>(IEnumerable<Action<TEvent>> handlers) where TEvent : struct
        {
            Type type = typeof(TEvent);
            Add(type, handlers);
        }

        public void Add<TEvent>(Action<TEvent> handler) where TEvent : struct
        {
            Type type = typeof(TEvent);
            Add(type, handler);
        }

        public void Remove<TEvent>(IEnumerable<Action<TEvent>> handlers) where TEvent : struct
        {
            Type type = typeof(TEvent);
            Remove(type, handlers);
        }

        public void Remove<TEvent>(Action<TEvent> handler) where TEvent : struct
        {
            Type type = typeof(TEvent);
            Remove(type, handler);
        }

        public IEnumerable<Action<TEvent>> GetHandlers<TEvent>() where TEvent : struct
        {
            Type type = typeof(TEvent);
            foreach (Delegate handler in GetHandlers(type))
                yield return (Action<TEvent>)handler;
        }

        internal void Add(Type type, IEnumerable<Delegate> dlgs)
        {
            if (!_handlers.TryGetValue(type, out List<Delegate> list))
                _handlers[type] = list = new List<Delegate>();

            list.AddRange(dlgs);
        }

        internal void Add(Type type, Delegate dlg)
        {
            if (!_handlers.TryGetValue(type, out List<Delegate> list))
                _handlers[type] = list = new List<Delegate>();

            list.Add(dlg);
        }

        internal void Remove(Type type, IEnumerable<Delegate> dlgs)
        {
            if (_handlers.TryGetValue(type, out List<Delegate> list))
                list.RemoveAll(dlgs.Contains);
        }

        internal void Remove(Type type, Delegate dlg)
        {
            if (_handlers.TryGetValue(type, out List<Delegate> list))
                list.Remove(dlg);
        }

        internal IEnumerable<Delegate> GetHandlers(Type type)
        {
            return _handlers.TryGetValue(type, out List<Delegate> list)
                ? list : Enumerable.Empty<Delegate>();
        }
    }
}