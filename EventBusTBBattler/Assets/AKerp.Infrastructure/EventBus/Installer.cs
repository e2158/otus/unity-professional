﻿using Zenject;
using JetBrains.Annotations;

namespace AKerp.Infrastructure.EventBus
{
    [UsedImplicitly]
    public sealed class Installer : Installer<Installer>
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<EventBus>().AsSingle();
        }
    }
}