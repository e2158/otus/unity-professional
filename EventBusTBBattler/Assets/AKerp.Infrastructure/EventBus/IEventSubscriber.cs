﻿using System;

namespace AKerp.Infrastructure.EventBus
{
    public interface IEventSubscriber
    {
        void Subscribe<TEvent>(Action<TEvent> handler) where TEvent : struct;
        void Unsubscribe<TEvent>(Action<TEvent> handler) where TEvent : struct;
    }
}