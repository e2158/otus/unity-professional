﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace AKerp.Infrastructure.EventBus
{
    [UsedImplicitly]
    internal sealed class EventBus : IEventPublisher, IEventSubscriber
    {
        private const string LogTag = nameof(EventBus);
        private static readonly ILogger Logger = Debug.unityLogger;

        private readonly HandlerCollection _handlers = new();
        private readonly HandlerCollection _handlersToSubscribeTemp = new();
        private readonly HandlerCollection _handlersToUnsubscribeTemp = new();

        private readonly Semaphore _publishSemaphore = new();

        public void Subscribe<TEvent>(Action<TEvent> handler) where TEvent : struct
        {
            if (_publishSemaphore.IsFree) _handlers.Add(handler);
            else _handlersToSubscribeTemp.Add(handler);
            Log<TEvent>(nameof(Subscribe));
        }

        public void Unsubscribe<TEvent>(Action<TEvent> handler) where TEvent : struct
        {
            if (_publishSemaphore.IsFree) _handlers.Remove(handler);
            else _handlersToUnsubscribeTemp.Add(handler);
            Log<TEvent>(nameof(Unsubscribe));
        }

        public void Publish<TEvent>(TEvent evt) where TEvent : struct
        {
            _publishSemaphore.Acquire();

            Log<TEvent>(nameof(Publish));
            IEnumerable<Action<TEvent>> handlers = _handlers.GetHandlers<TEvent>();
            foreach (Action<TEvent> handler in handlers) handler.Invoke(evt);

            _publishSemaphore.Release();
            if (_publishSemaphore.IsFree)
                TransferFromTemp();
        }

        private void TransferFromTemp()
        {
            if (_handlersToSubscribeTemp.HasAny)
            {
                foreach (Type type in _handlersToSubscribeTemp.Keys)
                    _handlers.Add(type, _handlersToSubscribeTemp.GetHandlers(type));

                _handlersToSubscribeTemp.Clear();
            }

            if (_handlersToUnsubscribeTemp.HasAny)
            {
                foreach (Type type in _handlersToUnsubscribeTemp.Keys)
                    _handlers.Remove(type, _handlersToUnsubscribeTemp.GetHandlers(type));

                _handlersToUnsubscribeTemp.Clear();
            }
        }

        private void Log<TEvent>(string message) where TEvent : struct
        {
            Logger.Log(LogTag, $"{typeof(TEvent).Name}: {message}");
        }
    }
}