﻿namespace AKerp.Infrastructure.EventBus
{
    internal sealed class Semaphore
    {
        private uint _counter;

        public bool IsFree => _counter == 0;

        public void Acquire() => _counter++;

        public void Release() => _counter--;
    }
}