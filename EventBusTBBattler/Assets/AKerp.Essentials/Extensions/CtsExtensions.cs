﻿using System.Threading;

namespace Essentials
{
    public static class CtsExtensions
    {
        public static void Kill(this CancellationTokenSource cts)
        {
            cts.Cancel();
            cts.Dispose();
        }
    }
}