﻿using System.Collections.Generic;

namespace Essentials
{
    public static class ReadOnlyListExtensions
    {
        public static int IndexOf<T>(this IReadOnlyList<T> list, T item)
        {
            for (int index = 0; index < list.Count; index++)
                if (list[index].Equals(item))
                    return index;

            return -1;
        }
    }
}