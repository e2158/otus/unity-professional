﻿using System;
using System.Collections.Generic;
using Essentials;
using JetBrains.Annotations;
using Otus.Protected.UI;

namespace Otus.Extensions
{
    public static class UiServiceExtensions
    {
        public static HeroView FindHeroView([NotNull] this UIService uiService, int playerIndex, int heroIndex)
        {
            HeroListView heroListView = uiService.GetPlayer(playerIndex);
            return heroListView.GetView(heroIndex);
        }

        public static int FindHeroIndex([NotNull] this UIService uiService,
            int playerIndex, [NotNull] HeroView heroView)
        {
            HeroListView heroListView = uiService.GetPlayer(playerIndex);
            IReadOnlyList<HeroView> heroViews = heroListView.GetViews();

            int index = heroViews.IndexOf(heroView);
            return index >= 0 ? index : throw new KeyNotFoundException();
        }

        public static HeroListView GetPlayer([NotNull] this UIService uiService, int playerIndex)
        {
            return playerIndex switch
            {
                0 => uiService.GetRedPlayer(),
                1 => uiService.GetBluePlayer(),
                _ => throw new ArgumentOutOfRangeException()
            };
        }
    }
}