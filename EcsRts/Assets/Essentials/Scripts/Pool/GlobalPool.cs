﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace Essentials
{
    [DisallowMultipleComponent]
    public sealed class GlobalPool : MonoBehaviour
    {
        public static GlobalPool Instance { get; private set; }

        [SerializeField, CanBeNull] private Transform _poolContainer;

        [ShowInInspector, ReadOnly] private readonly Dictionary<string, Queue<PoolObject>> _poolObjects = new();

        private void Awake()
        {
            if (Instance != null)
                Destroy(gameObject);
            else
                Instance = this;
        }

        [NotNull]
        public PoolObject Provide([NotNull] PoolObject prefab)
        {
            if (_poolObjects.TryGetValue(prefab.PoolKey, out Queue<PoolObject> queue) && queue.Any())
            {
                PoolObject poolObject = queue.Dequeue();
                poolObject.gameObject.SetActive(true);
                poolObject.transform.SetParent(null);
                return poolObject;
            }

            return Instantiate(prefab);
        }

        public void Release([NotNull] PoolObject poolObject)
        {
            poolObject.gameObject.SetActive(false);
            poolObject.transform.SetParent(_poolContainer);

            if (!_poolObjects.TryGetValue(poolObject.PoolKey, out Queue<PoolObject> queue))
                _poolObjects[poolObject.PoolKey] = queue = new Queue<PoolObject>();

            queue.Enqueue(poolObject);
        }
    }
}