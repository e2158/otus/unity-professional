﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace Essentials
{
    [DisallowMultipleComponent]
    public sealed class PoolObject : MonoBehaviour
    {
        [SerializeField, Required] private string _poolKey;

        public string PoolKey => _poolKey;
    }
}