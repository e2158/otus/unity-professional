﻿using UnityEngine;

namespace Essentials
{
    public static class Vector2Extensions
    {
        public static Vector3 InsertX(this Vector2 vector2, float x) =>
            new(x, vector2.x, vector2.y);

        public static Vector3 InsertY(this Vector2 vector2, float y) =>
            new(vector2.x, y, vector2.y);
        public static Vector3 InsertZ(this Vector2 vector2, float z) =>
            new(vector2.x, vector2.y, z);

        public static bool Approximately(this Vector2 vector2, Vector2 other) =>
            vector2.x.Approximately(other.x) &&
            vector2.y.Approximately(other.y);
    }
}