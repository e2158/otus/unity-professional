﻿using UnityEngine;

namespace Essentials
{
    public static class Vector3Extensions
    {
        public static Vector2 CutX(this Vector3 vector) => new(vector.y, vector.z);
        public static Vector2 CutY(this Vector3 vector) => new(vector.x, vector.z);
        public static Vector2 CutZ(this Vector3 vector) => new(vector.x, vector.y);

        public static bool Approximately(this Vector3 vector3, Vector3 other) =>
            vector3.x.Approximately(other.x) &&
            vector3.y.Approximately(other.y) &&
            vector3.z.Approximately(other.z);
    }
}