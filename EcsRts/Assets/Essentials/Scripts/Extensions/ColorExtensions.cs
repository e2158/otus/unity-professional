﻿using UnityEngine;

namespace Essentials
{
    public static class ColorExtensions
    {
        public static string ToHtml(this Color color) =>
            ColorUtility.ToHtmlStringRGBA(color);

        public static string ToLogMessage(this Color color, string message) =>
            $"<color=#{color.ToHtml()}>{message}</color>";
    }
}