﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Essentials
{
    [DisallowMultipleComponent]
    public sealed class ParticlePlayer : MonoBehaviour
    {
        [Serializable]
        private sealed class Particle
        {
            public string Key;
            public ParticleSystem ParticleSystem;
        }

        [TableList]
        [SerializeField] private List<Particle> _particles;

        [PropertySpace]

        [Button]
        public void Play(string particleKey)
        {
            _particles
                .Single(p => p.Key == particleKey)
                .ParticleSystem
                .Play();
        }
    }
}