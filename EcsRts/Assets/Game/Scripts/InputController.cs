﻿using UnityEngine;

namespace Game
{
    public delegate void BuyUnitClickedEvent(int teamIndex, int unitIndex);

    public sealed class InputController : MonoBehaviour
    {
        private const int UnclickedUnitIndex = -1;

        public event BuyUnitClickedEvent BuyUnitClicked;

        [SerializeField] private KeyCode _altTeamKey;
        [SerializeField] private KeyCode _firstUnitBuyKey;
        [SerializeField] private KeyCode _secndUnitBuyKey;

        private void Update()
        {
            int clickedUnitIndex = DetectClickedUnitIndex();
            if (clickedUnitIndex != UnclickedUnitIndex)
            {
                int teamIndex = Input.GetKey(_altTeamKey) ? 1 : 0;
                BuyUnitClicked?.Invoke(teamIndex, clickedUnitIndex);
            }
        }

        private int DetectClickedUnitIndex()
        {
            if (Input.GetKeyDown(_firstUnitBuyKey)) return 0;
            if (Input.GetKeyDown(_secndUnitBuyKey)) return 1;
            return UnclickedUnitIndex;
        }
    }
}