﻿using System.Collections.Generic;
using Essentials;
using Game.Common.Configs;
using Game.Unit.Configs;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.TownHall.Configs
{
    [CreateAssetMenu(
        fileName = nameof(TownHallConfig),
        menuName = AssetsMenu.Configs + nameof(TownHallConfig))]
    public sealed class TownHallConfig : SerializedScriptableObject
    {
        [MinValue(0)]
        [Tooltip("Кол-во здоровья Ратуши")]
        [SerializeField] private float _hp;

        [MinValue(0), SuffixLabel("m", true)]
        [Tooltip("Радиус Ратуши для просчёта коллизий")]
        [SerializeField] private float _radius;

        [Required, AssetSelector]
        [Tooltip("Принадлежность команде")]
        [SerializeField] private TeamConfig _team;

        [Tooltip("Набор префабов юнитов для спауна")]
        [SerializeField] private Dictionary<UnitConfig, PoolObject> _producableUnits;

        public float Hp => _hp;
        public float Radius => _radius;
        [NotNull] public TeamConfig Team => _team;
        [NotNull] public IReadOnlyDictionary<UnitConfig, PoolObject> ProducableUnits => _producableUnits;
    }
}