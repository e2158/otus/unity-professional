﻿using System;
using System.Collections;
using System.Linq;
using Game.ECS;
using Game.TownHall.Components;
using Game.TownHall.Configs;
using Game.TownHall.Providers;
using Game.Unit.Components;
using Game.Unit.Configs;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.TownHall.Controllers
{
    [RequireComponent(typeof(TownHallProvider))]
    [TypeInfoBox("Компонент для управления Ратушей и, как следствие, всей игрой.\n" +
                 "Компонент позвоялет спаунить юнитов для каждой Ратуши соответственно.")]
    public sealed class TownHallRuntimeController : MonoBehaviour
    {
        [Header("Configuration")]
        [EnableIn(PrefabKind.Variant)]
        [SerializeField, Required, AssetSelector] private TownHallProvider _provider;

        [Header("Control Dependencies")]
        [InfoBox("Перемещай данный объекты на сцене, чтобы задать точку спауна для следующего юнита")]
        [SerializeField, Required] private Transform _spawnPoint;

        [InfoBox("Выбери тип юнита, который нужно заспаунить следующим")]
        [ValueDropdown(nameof(UnitConfigsDropdownValues)), DisableInEditorMode]
        [ShowInInspector] private UnitConfig _currentUnitToSpawn;

        private string LogTag { get; set; }
        private ILogger Logger => Debug.unityLogger;

        [CanBeNull] private Entity Entity => _provider?.Entity;
        [CanBeNull] private TownHallConfig Config => Entity?.GetComponent<TownHallComponent>().Config;

        private IEnumerable UnitConfigsDropdownValues =>
             Config == null ? Enumerable.Empty<ValueDropdownItem<UnitConfig>>()
                : Config.ProducableUnits.Keys.Select(c => new ValueDropdownItem<UnitConfig>(c.name, c)).ToList();

        private void OnValidate()
        {
            _provider ??= GetComponent<TownHallProvider>();
            _currentUnitToSpawn ??= Config?.ProducableUnits.Keys.FirstOrDefault();
        }

        private void Awake()
        {
            LogTag = $"{nameof(TownHallRuntimeController)} {Config?.name}";
        }

        [Button(ButtonSizes.Large, Name = "Кнопка для спауна юнита"), GUIColor(0.6f, 1, 0.6f)]
        public void SpawnUnit() =>
            SpawnUnit(_currentUnitToSpawn, _spawnPoint.position);

        public void SpawnUnit(int unitIndex)
        {
            TownHallConfig townHallConfig = Config;
            if (townHallConfig == null) throw new NullReferenceException(nameof(townHallConfig));

            UnitConfig unitConfig = townHallConfig.ProducableUnits.Keys.ElementAtOrDefault(unitIndex);
            if (unitConfig == null) throw new ArgumentNullException(nameof(unitConfig));

            SpawnUnit(unitConfig,  _spawnPoint.position);
        }

        private void SpawnUnit([NotNull] UnitConfig unitConfig, Vector3 position)
        {
            TownHallConfig townHallConfig = Config;

            if (unitConfig == null) throw new ArgumentNullException(nameof(unitConfig));
            if (townHallConfig == null) throw new NullReferenceException(nameof(townHallConfig));

            UnitSpawnRequest request = new()
            {
                Config = unitConfig,
                Team = townHallConfig.Team,
                Prefab = townHallConfig.ProducableUnits[unitConfig],
                Position = _spawnPoint.position,
                Rotation = _spawnPoint.eulerAngles
            };

            WorldRoster.SendRequest(request);

            Logger.Log(LogTag, $"Send request to spawn unit \"{unitConfig}\" at {position}");
        }
    }
}