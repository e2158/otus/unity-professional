﻿using UnityEngine;
using Game.TownHall.Systems;
using Scellecs.Morpeh;

namespace Game.TownHall
{
    public static class TownHallModule
    {
        public static SystemsGroup CreateSystemsGroup(World world)
        {
            SystemsGroup group = world.CreateSystemsGroup();

            // Initializers
            group.AddInitializer(ScriptableObject.CreateInstance<TownHallInitializer>());

            return group;
        }
    }
}