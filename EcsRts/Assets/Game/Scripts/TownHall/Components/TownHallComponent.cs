﻿using System;
using Game.TownHall.Configs;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Game.TownHall.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct TownHallComponent : IComponent
    {
        [NotNull] public TownHallConfig Config;

        public TownHallComponent([NotNull] TownHallConfig config)
        {
            Config = config ?? throw new ArgumentNullException(nameof(config));
        }
    }
}