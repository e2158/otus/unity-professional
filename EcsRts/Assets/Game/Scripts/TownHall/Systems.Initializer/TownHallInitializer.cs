﻿using Essentials;
using Game.Common.Components;
using Game.TownHall.Components;
using Game.TownHall.Configs;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.TownHall.Systems
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(TownHallInitializer))]
    public sealed class TownHallInitializer : Initializer
    {
        private const string LogTag = nameof(TownHallInitializer);
        private static readonly ILogger logger = Debug.unityLogger;

        public override void OnAwake()
        {
            Filter filter = World.Filter.With<TownHallComponent>().Build();

            foreach (Entity entity in filter)
            {
                TownHallConfig config = entity.GetComponent<TownHallComponent>().Config;

                InitializeTownHall(entity, config);

                Log(entity, config);
            }
        }

        private void InitializeTownHall(Entity entity, TownHallConfig config)
        {
            Transform transform = entity.GetComponent<TransformComponent>().Transform;

            entity.SetComponent(new TeamComponent(config.Team));
            entity.SetComponent(new HpComponent(config.Hp, config.Hp));
            entity.SetComponent(new TransformationComponent(config.Radius, transform.position, transform.eulerAngles));
        }

        private static void Log(Entity entity, TownHallConfig config)
        {
            string coloredName = config.Team.Color.ToLogMessage($"{config.name} {entity.ID}");
            logger.Log(LogTag, $"Initialized \"{coloredName}\"");
        }
    }
}