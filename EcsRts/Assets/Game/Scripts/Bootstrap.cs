﻿using Game.Common;
using Game.Common.Systems.Cleanup;
using Game.ECS;
using Game.Target;
using Game.Target.Attack;
using Game.Target.Attack.Projectile;
using Game.Target.Damage;
using Game.TownHall;
using Game.Unit;
using JetBrains.Annotations;
using UnityEngine;
using Scellecs.Morpeh;

namespace Game
{
    public sealed class Bootstrap : MonoBehaviour
    {
        private void Awake()
        {
            WorldRoster.Initialize();

            WorldRoster.Logic.AddSystemsGroup(0, CommonModule.CreateSystemsGroup(WorldRoster.Logic));
            WorldRoster.Logic.AddSystemsGroup(1, TownHallModule.CreateSystemsGroup(WorldRoster.Logic));
            WorldRoster.Logic.AddSystemsGroup(2, UnitModule.CreateSystemsGroup(WorldRoster.Logic));
            WorldRoster.Logic.AddSystemsGroup(3, TargetModule.CreateSystemsGroup(WorldRoster.Logic));
            WorldRoster.Logic.AddSystemsGroup(4, TargetAttackModule.CreateSystemsGroup(WorldRoster.Logic));
            WorldRoster.Logic.AddSystemsGroup(5, TargetAttackProjectileModule.CreateSystemsGroup(WorldRoster.Logic));
            WorldRoster.Logic.AddSystemsGroup(6, TargetDamageModule.CreateSystemsGroup(WorldRoster.Logic));

            SystemsGroup requests = CreateRequestSystemsGroup(WorldRoster.Requests);
            WorldRoster.Requests.AddSystemsGroup(default, requests);
        }

        private void OnDestroy()
        {
            WorldRoster.Deinitialize();
        }

        private SystemsGroup CreateRequestSystemsGroup([NotNull] World world)
        {
            SystemsGroup group = world.CreateSystemsGroup();

            // Cleanup Systems
            group.AddSystem(ScriptableObject.CreateInstance<OneFrameCleanupSystem>());

            return group;
        }
    }
}