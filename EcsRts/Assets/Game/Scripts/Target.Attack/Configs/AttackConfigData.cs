﻿using System;
using Game.Target.Attack.Projectile.Configs;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Target.Attack.Configs
{
    [Serializable]
    public sealed class AttackConfigData
    {
        [MinValue(0), SuffixLabel("m",true)]
        [Tooltip("Максимальная дистанция, на которую может стрелять юнит")]
        [SerializeField] private float _maxDistance;

        [MinValue(0), SuffixLabel("sec", true)]
        [Tooltip("Задержка перед спауном проджектайла после получения команды на атаку")]
        [SerializeField] private float _delay;

        [MinValue(0), SuffixLabel("sec", true)]
        [Tooltip("Время восстановления после спауна проджектайла")]
        [SerializeField] private float _cooldown;

        [BoxGroup("Projectile"), HideLabel]
        [SerializeField] private AttackProjectileConfigData _projectile;

        public float MaxDistance => _maxDistance;
        public TimeSpan Delay => TimeSpan.FromSeconds(_delay);
        public TimeSpan Cooldown => TimeSpan.FromSeconds(_cooldown);
        public AttackProjectileConfigData Projectile => _projectile;
    }
}