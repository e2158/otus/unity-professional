﻿using System;
using Game.Target.Attack.Configs;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Game.Target.Attack.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct AttackSenderComponent : IComponent
    {
        [NotNull] public AttackConfigData Data;

        public AttackSenderComponent([NotNull] AttackConfigData data)
        {
            Data = data ?? throw new ArgumentNullException(nameof(data));
        }
    }
}