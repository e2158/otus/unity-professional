﻿using System;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Game.Target.Attack.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct AttackCooldownMarker : IComponent
    {
        public float Timestamp;

        public AttackCooldownMarker(float timestamp)
        {
            Timestamp = timestamp;
        }
    }
}