﻿using Game.Common.Components;
using Game.Target.Aspects;
using Game.Target.Attack.Components;
using Game.Target.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Attack.Systems.Update
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(AttackStartSystem))]
    public sealed class AttackStartSystem : UpdateSystem
    {
        private const string LogTag = nameof(AttackStartSystem);
        private static readonly ILogger logger = Debug.unityLogger;

        private Filter _filter;

        public override void OnAwake()
        {
            _filter = World.Filter
                .Extend<TargetOwnerAspect>()
                .With<AttackSenderComponent>()
                .Without<DeadMarker>()
                .Without<AttackDelayMarker>()
                .Without<AttackCooldownMarker>()
                .Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _filter)
            {
                var targetOwner = entity.GetComponent<TargetOwnerComponent>();

                if (CanStartAttack(entity, targetOwner.TargetEntity))
                {
                    entity.AddComponent<StartAttackEvent>();
                    entity.SetComponent(new AttackDelayMarker(Time.time));

                    logger.Log(LogTag, $"Entity \"{entity.ID}\" started delay before attack");
                }
            }
        }

        private bool CanStartAttack(Entity senderEntity, EntityId receiverId)
        {
            if (!World.TryGetEntity(receiverId, out Entity receiverEntity))
                return false;

            var sTransformation = senderEntity.GetComponent<TransformationComponent>();
            var sender = senderEntity.GetComponent<AttackSenderComponent>();

            var rHp = receiverEntity.GetComponent<HpComponent>();
            var rTransformation = receiverEntity.GetComponent<TransformationComponent>();

            if (rHp.IsDead)
                return false;

            float sqrMaxDistance = Mathf.Pow(sender.Data.MaxDistance + rTransformation.Radius, 2f);
            float sqrDistance = Vector3.SqrMagnitude(rTransformation.Position - sTransformation.Position);
            if (sqrDistance > sqrMaxDistance)
                return false;

            return true;
        }
    }
}