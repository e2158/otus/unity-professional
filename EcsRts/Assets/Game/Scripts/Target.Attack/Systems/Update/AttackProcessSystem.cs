﻿using Essentials;
using Game.Common.Components;
using Game.Common.Configs;
using Game.ECS;
using Game.Target.Attack.Components;
using Game.Target.Attack.Configs;
using Game.Target.Attack.Projectile.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Attack.Systems.Update
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(AttackProcessSystem))]
    public sealed class AttackProcessSystem : UpdateSystem
    {
        private const string LogTag = nameof(AttackProcessSystem);
        private static readonly ILogger logger = Debug.unityLogger;

        private Filter _attackSenderFilter;

        public override void OnAwake()
        {
            _attackSenderFilter = World.Filter
                .With<AttackSenderComponent>()
                .With<TransformationComponent>()
                .With<AttackDelayMarker>()
                .With<TeamComponent>()
                .Without<DeadMarker>()
                .Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity senderEntity in _attackSenderFilter)
            {
                if (IsDelayElapsed(senderEntity))
                {
                    StopDelay(senderEntity);
                    StartCooldown(senderEntity);
                    SendProjectileSpawnRequest(senderEntity);
                }
            }
        }

        private static bool IsDelayElapsed(Entity senderEntity)
        {
            var delayMarker = senderEntity.GetComponent<AttackDelayMarker>();
            var senderComponent = senderEntity.GetComponent<AttackSenderComponent>();

            return Time.time - delayMarker.Timestamp >= senderComponent.Data.Delay.TotalSeconds;
        }

        private static void StopDelay(Entity senderEntity)
        {
            senderEntity.RemoveComponent<AttackDelayMarker>();
            logger.Log(LogTag, $"Entity \"{senderEntity.ID}\" stopped delay before attack");
        }

        private static void StartCooldown(Entity senderEntity)
        {
            senderEntity.SetComponent(new AttackCooldownMarker(Time.time));
            logger.Log(LogTag, $"Entity \"{senderEntity.ID}\" started cooldown after attack");
        }

        private static void SendProjectileSpawnRequest(Entity senderEntity)
        {
            var sender = senderEntity.GetComponent<AttackSenderComponent>();
            var transformation = senderEntity.GetComponent<TransformationComponent>();
            TeamConfig teamConfig = senderEntity.GetComponent<TeamComponent>().Config;

            AttackConfigData data = sender.Data;
            Vector3 localSpawnPoint = data.Projectile.LocalSpawnPoint;
            Vector3 dirVector = Quaternion.Euler(transformation.Rotation) * localSpawnPoint;
            Vector2 direction = dirVector.CutY().normalized;
            Vector3 globalSpawnPoint = transformation.Position + dirVector;

            AttackProjectileSpawnRequest spawnRequest = new(
                data.Projectile.Speed, data.Projectile.Damage, data.Projectile.Radius,
                data.MaxDistance, direction, globalSpawnPoint, teamConfig, data.Projectile.Prefab);

            WorldRoster.SendRequest(spawnRequest);

            logger.Log(LogTag, $"Entity \"{senderEntity.ID}\" sent projectile spawn request");
        }
    }
}