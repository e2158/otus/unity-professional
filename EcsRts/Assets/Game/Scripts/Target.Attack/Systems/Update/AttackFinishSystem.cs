﻿using Game.Common.Components;
using Game.Target.Attack.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Attack.Systems.Update
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(AttackFinishSystem))]
    public sealed class AttackFinishSystem : UpdateSystem
    {
        private const string LogTag = nameof(AttackProcessSystem);
        private static readonly ILogger logger = Debug.unityLogger;

        private Filter _filter;

        public override void OnAwake()
        {
            _filter = World.Filter
                .With<AttackSenderComponent>()
                .With<AttackCooldownMarker>()
                .Without<DeadMarker>()
                .Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _filter)
                if (IsCooldownElapsed(entity))
                    StopCooldown(entity);
        }

        private static bool IsCooldownElapsed(Entity senderEntity)
        {
            var cooldownMarker = senderEntity.GetComponent<AttackCooldownMarker>();
            var senderComponent = senderEntity.GetComponent<AttackSenderComponent>();

            return Time.time - cooldownMarker.Timestamp >= senderComponent.Data.Cooldown.TotalSeconds;
        }

        private static void StopCooldown(Entity senderEntity)
        {
            senderEntity.RemoveComponent<AttackCooldownMarker>();
            logger.Log(LogTag, $"Entity \"{senderEntity.ID}\" stopped cooldown after attack");
        }
    }
}