﻿using Game.Common.Components;
using Game.Target.Attack.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Attack.Systems.LateUpdate
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(ViewAttackSystem))]
    public sealed class ViewAttackSystem : LateUpdateSystem
    {
        private static readonly int attackHash = Animator.StringToHash("Attack");

        private Filter _filter;

        public override void OnAwake()
        {
            _filter = World.Filter
                .With<AnimatorComponent>()
                .With<StartAttackEvent>()
                .Without<DeadMarker>()
                .Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _filter)
            {
                ref var view = ref entity.GetComponent<AnimatorComponent>();
                view.Animator.SetTrigger(attackHash);
            }
        }
    }
}