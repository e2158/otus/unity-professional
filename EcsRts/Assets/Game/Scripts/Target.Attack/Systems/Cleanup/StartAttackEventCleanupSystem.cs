﻿using Game.Target.Attack.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;

namespace Game.Target.Attack.Systems.Cleanup
{
    public sealed class StartAttackEventCleanupSystem : CleanupSystem
    {
        private Filter _filter;

        public override void OnAwake()
        {
            _filter = World.Filter
                .With<StartAttackEvent>()
                .Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _filter)
                entity.RemoveComponent<StartAttackEvent>();
        }
    }
}