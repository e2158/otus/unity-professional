﻿using Game.Target.Attack.Systems.Cleanup;
using Game.Target.Attack.Systems.LateUpdate;
using Game.Target.Attack.Systems.Update;
using Scellecs.Morpeh;
using UnityEngine;

namespace Game.Target.Attack
{
    public static class TargetAttackModule
    {
        public static SystemsGroup CreateSystemsGroup(World world)
        {
            SystemsGroup group = world.CreateSystemsGroup();

            // Initializers

            // Update Systems
            group.AddSystem(ScriptableObject.CreateInstance<AttackStartSystem>());
            group.AddSystem(ScriptableObject.CreateInstance<AttackProcessSystem>());
            group.AddSystem(ScriptableObject.CreateInstance<AttackFinishSystem>());

            // Late Update Systems
            group.AddSystem(ScriptableObject.CreateInstance<ViewAttackSystem>());

            // Cleanup Systems
            group.AddSystem(ScriptableObject.CreateInstance<StartAttackEventCleanupSystem>());

            return group;
        }
    }
}