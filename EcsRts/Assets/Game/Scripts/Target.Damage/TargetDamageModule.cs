﻿using Game.Target.Damage.Systems.Cleanup;
using Game.Target.Damage.Systems.LateUpdate;
using Game.Target.Damage.Systems.Update;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using UnityEngine;

namespace Game.Target.Damage
{
    public static class TargetDamageModule
    {
        [NotNull]
        public static SystemsGroup CreateSystemsGroup([NotNull] World world)
        {
            SystemsGroup group = world.CreateSystemsGroup();

            // Fixed Update Systems
            group.AddSystem(ScriptableObject.CreateInstance<TakeDamageSystem>());

            // Late Update Systems
            group.AddSystem(ScriptableObject.CreateInstance<ViewTakeDamageSystem>());

            // Cleanup Systems
            group.AddSystem(ScriptableObject.CreateInstance<DamageMarkerCleanupSystem>());

            return group;

        }
    }
}