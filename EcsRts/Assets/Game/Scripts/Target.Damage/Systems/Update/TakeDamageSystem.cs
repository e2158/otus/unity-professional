﻿using Game.Common.Components;
using Game.Target.Damage.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Damage.Systems.Update
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(TakeDamageSystem))]
    public sealed class TakeDamageSystem : UpdateSystem
    {
        private const string LogTag = nameof(TakeDamageSystem);
        private static readonly ILogger logger = Debug.unityLogger;

        private Filter _filter;

        public override void OnAwake()
        {
            _filter = World.Filter
                .With<HpComponent>()
                .With<DamageMarker>()
                .Without<DeadMarker>()
                .Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _filter)
            {
                ref var hp = ref entity.GetComponent<HpComponent>();
                var damage = entity.GetComponent<DamageMarker>();

                hp.Current -= damage.Value;

                logger.Log(LogTag, $"Entity \"{entity.ID}\" got damage \"{damage.Value}\"");
            }
        }

    }
}