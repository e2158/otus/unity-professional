﻿using System;
using Game.Common.Components;
using Scellecs.Morpeh;

namespace Game.ECS
{
    public static class WorldRoster
    {
        public static World Logic { get; private set; }
        public static World Requests { get; private set; }

        public static void Initialize()
        {
            if (Logic != null || Requests != null)
                throw new InvalidOperationException(nameof(WorldRoster) + " is already initialized");

            Logic = World.Default;
            Logic!.UpdateByUnity = true;

            Requests = World.Create();
            Requests.UpdateByUnity = true;
        }

        public static void Deinitialize()
        {
            Logic?.Dispose();
            Requests?.Dispose();

            Logic = null;
            Requests = null;
        }

        public static void SendRequest<TRequest>(TRequest request) where TRequest : struct, IComponent
        {
            Entity entity = Requests.CreateEntity();
            Requests.GetStash<OneFrameComponent>().Add(entity);
            Requests.GetStash<TRequest>().Set(entity, request);
            Requests.Commit();
        }
    }
}