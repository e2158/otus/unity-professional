﻿using Essentials;
using Game.Common.Components;
using Game.Common.Configs;
using Game.Target.Aspects;
using Game.Target.Components;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Systems.FixedUpdate
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(TargetSearchSystem))]
    public sealed class TargetSearchSystem : FixedUpdateSystem
    {
        private const string LogTag = nameof(TargetSearchSystem);
        private static readonly ILogger logger = Debug.unityLogger;

        private Filter _targetFilter;
        private Filter _searcherFilter;
        private AspectFactory<TargetAspect> _targetAspects;
        private AspectFactory<TargetSearcherAspect> _searcherAspects;

        public override void OnAwake()
        {
            _targetFilter = World.Filter.Extend<TargetAspect>().Build();
            _searcherFilter = World.Filter.Extend<TargetSearcherAspect>().Build();

            _targetAspects = World.GetAspectFactory<TargetAspect>();
            _searcherAspects = World.GetAspectFactory<TargetSearcherAspect>();
        }

        public override void OnUpdate(float deltaTime)
        {
            if (_targetFilter.IsEmpty())
                return;

            foreach (Entity entity in _searcherFilter)
            {
                TargetSearcherAspect searcher = _searcherAspects.Get(entity);
                Vector3 position = searcher.Transformation.Position;
                TeamConfig searcherTeam = searcher.Team.Config;

                Entity targetEntity = FindClosestTargetForTeam(position, searcherTeam);

                if (targetEntity != null)
                {
                    TargetAspect target = _targetAspects.Get(targetEntity);
                    searcher.Entity.SetComponent(new TargetOwnerComponent(target.Entity.ID));
                    Log(searcher, target);
                }
            }
        }

        [CanBeNull]
        private Entity FindClosestTargetForTeam(Vector3 position, TeamConfig teamConfig)
        {
            Entity closestTarget = null;
            float minSqrDistance = float.MaxValue;

            foreach (Entity entity in _targetFilter)
            {
                var team = entity.GetComponent<TeamComponent>();

                if (team.Config == teamConfig)
                    continue;

                var transformation = entity.GetComponent<TransformationComponent>();
                float sqrDistance = Vector3.SqrMagnitude(transformation.Position - position);
                if (sqrDistance < minSqrDistance)
                {
                    closestTarget = entity;
                    minSqrDistance = sqrDistance;
                }
            }

            return closestTarget;
        }

        private static void Log(TargetSearcherAspect searcher, TargetAspect target)
        {
            string coloredTargetId = target.Team.Config.Color.ToLogMessage(target.Entity.ID.ToString());
            string coloredSearcherId = searcher.Team.Config.Color.ToLogMessage(searcher.Entity.ID.ToString());
            logger.Log(LogTag, $"Entity \"{coloredSearcherId}\" found target \"{coloredTargetId}\"");
        }
    }
}