﻿using Essentials;
using Game.Common.Components;
using Game.Target.Aspects;
using Game.Target.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Systems.FixedUpdate
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(TargetLoseSystem))]
    public sealed class TargetLoseSystem : FixedUpdateSystem
    {
        private const string LogTag = nameof(TargetSearchSystem);
        private static readonly ILogger logger = Debug.unityLogger;

        private Filter _ownersFilter;
        private AspectFactory<TargetAspect> _targetAspects;
        private AspectFactory<TargetOwnerAspect> _ownerAspects;

        public override void OnAwake()
        {
            _targetAspects = World.GetAspectFactory<TargetAspect>();
            _ownerAspects = World.GetAspectFactory<TargetOwnerAspect>();
            _ownersFilter = World.Filter.Extend<TargetOwnerAspect>().Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _ownersFilter)
            {
                TargetOwnerAspect owner = _ownerAspects.Get(entity);
                EntityId targetId = owner.TargetOwner.TargetEntity;
                ref MoveComponent move = ref owner.Move;

                bool shouldLoseTarget =
                    !World.TryGetEntity(targetId, out Entity targetEntity) ||
                    _targetAspects.Get(targetEntity).Hp.IsDead;

                if (shouldLoseTarget)
                {
                    move.Direction = Vector2.zero;
                    entity.RemoveComponent<TargetOwnerComponent>();
                    Log(owner, targetId);
                }
            }
        }

        private static void Log(TargetOwnerAspect owner, EntityId targetId)
        {
            string coloredOwnerId = owner.Team.Config.Color.ToLogMessage(owner.Entity.ID.ToString());
            logger.Log(LogTag, $"Entity \"{coloredOwnerId}\" lost target \"{targetId}\"");
        }
    }
}