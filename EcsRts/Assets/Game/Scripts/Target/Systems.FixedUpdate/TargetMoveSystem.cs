﻿using Essentials;
using Game.Common.Components;
using Game.Target.Aspects;
using Game.Target.Attack.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Systems.FixedUpdate
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(TargetMoveSystem))]
    public sealed class TargetMoveSystem : FixedUpdateSystem
    {
        private Filter _ownersFilter;
        private AspectFactory<TargetAspect> _targetAspects;
        private AspectFactory<TargetOwnerAspect> _ownerAspects;

        public override void OnAwake()
        {
            _targetAspects = World.GetAspectFactory<TargetAspect>();
            _ownerAspects = World.GetAspectFactory<TargetOwnerAspect>();
            _ownersFilter = World.Filter.Extend<TargetOwnerAspect>().Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _ownersFilter)
            {
                TargetOwnerAspect owner = _ownerAspects.Get(entity);
                ref var move = ref entity.GetComponent<MoveComponent>();

                if (World.TryGetEntity(owner.TargetOwner.TargetEntity, out Entity targetEntity))
                {
                    TargetAspect target = _targetAspects.Get(targetEntity);
                    move.Direction = CalcDirection(owner, target);
                }
            }
        }

        private Vector3 CalcDirection(TargetOwnerAspect owner, TargetAspect target)
        {
            AttackSenderComponent ownerAttack = owner.AttackSender;
            TransformationComponent ownerT = owner.Transformationation;
            TransformationComponent targetT = target.Transformation;

            Vector3 vecToTarget = targetT.Position - ownerT.Position;
            float distance = vecToTarget.magnitude;

            float stopDistance = ownerAttack.Data.MaxDistance + targetT.Radius;
            bool needToStop = distance.Approximately(0) || distance < stopDistance;

            return needToStop ? Vector3.zero : vecToTarget / distance;
        }
    }
}