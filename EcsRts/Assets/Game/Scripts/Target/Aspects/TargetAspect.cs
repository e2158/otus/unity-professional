﻿using Game.Common.Components;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Game.Target.Aspects
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct TargetAspect : IAspect, IFilterExtension
    {
        public Entity Entity { get; set; }

        public ref TransformationComponent Transformation => ref _transformationStash.Get(Entity);
        public ref TeamComponent Team => ref _teamStash.Get(Entity);
        public ref HpComponent Hp => ref _hpStash.Get(Entity);

        private Stash<TransformationComponent> _transformationStash;
        private Stash<TeamComponent> _teamStash;
        private Stash<HpComponent> _hpStash;

        void IAspect.OnGetAspectFactory(World world)
        {
            _transformationStash = world.GetStash<TransformationComponent>();
            _teamStash = world.GetStash<TeamComponent>();
            _hpStash = world.GetStash<HpComponent>();
        }

        FilterBuilder IFilterExtension.Extend(FilterBuilder rootFilter) =>
            rootFilter
                .With<TransformationComponent>()
                .With<TeamComponent>()
                .With<HpComponent>()
                .Without<DeadMarker>();
    }
}