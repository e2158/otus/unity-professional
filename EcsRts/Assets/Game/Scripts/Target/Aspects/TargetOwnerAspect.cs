﻿using Game.Common.Components;
using Game.Target.Attack.Components;
using Game.Target.Components;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Game.Target.Aspects
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct TargetOwnerAspect : IAspect, IFilterExtension
    {
        public Entity Entity { get; set; }

        public ref TransformationComponent Transformationation => ref _transformationStash.Get(Entity);
        public ref AttackSenderComponent AttackSender => ref _attackSenderStash.Get(Entity);
        public ref TargetOwnerComponent TargetOwner => ref _targetOwnerStash.Get(Entity);
        public ref TeamComponent Team => ref _teamStash.Get(Entity);
        public ref MoveComponent Move => ref _moveStash.Get(Entity);

        private Stash<TransformationComponent> _transformationStash;
        private Stash<AttackSenderComponent> _attackSenderStash;
        private Stash<TargetOwnerComponent> _targetOwnerStash;
        private Stash<TeamComponent> _teamStash;
        private Stash<MoveComponent> _moveStash;

        void IAspect.OnGetAspectFactory(World world)
        {
            _transformationStash = world.GetStash<TransformationComponent>();
            _attackSenderStash = world.GetStash<AttackSenderComponent>();
            _targetOwnerStash = world.GetStash<TargetOwnerComponent>();
            _teamStash = world.GetStash<TeamComponent>();
            _moveStash = world.GetStash<MoveComponent>();
        }

        FilterBuilder IFilterExtension.Extend(FilterBuilder rootFilter) =>
            rootFilter
                .With<TransformationComponent>()
                .With<AttackSenderComponent>()
                .With<TargetOwnerComponent>()
                .With<TeamComponent>()
                .With<MoveComponent>()
                .Without<DeadMarker>();
    }
}