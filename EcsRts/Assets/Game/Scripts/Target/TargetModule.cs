﻿using Game.Target.Systems.FixedUpdate;
using UnityEngine;
using Scellecs.Morpeh;

namespace Game.Target
{
    public static class TargetModule
    {
        public static SystemsGroup CreateSystemsGroup(World world)
        {
            SystemsGroup group = world.CreateSystemsGroup();

            // Fixed Update Systems
            group.AddSystem(ScriptableObject.CreateInstance<TargetSearchSystem>());
            group.AddSystem(ScriptableObject.CreateInstance<TargetLoseSystem>());
            group.AddSystem(ScriptableObject.CreateInstance<TargetMoveSystem>());

            return group;
        }
    }
}