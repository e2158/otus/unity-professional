﻿using System;
using Essentials;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Target.Attack.Projectile.Configs
{
    [Serializable]
    public sealed class AttackProjectileConfigData
    {
        [SuffixLabel("sec/m", true)]
        [Tooltip("Скорость полёта проджектайла")]
        [SerializeField] private float _speed;

        [MinValue(0), SuffixLabel("hp", true)]
        [Tooltip("Урон, который наносит проджектайл при попадании")]
        [SerializeField] private float _damage;

        [MinValue(0), SuffixLabel("m", true)]
        [Tooltip("Радиус проджектайла для просчёта коллизий")]
        [SerializeField] private float _radius;

        [MinValue(0)]
        [Tooltip("Точка спауна проджектайла относительно источника")]
        [SerializeField] private Vector3 _localSpawnPoint;

        [AssetSelector, CanBeNull]
        [Tooltip("View проджейктайла (требуется только для стрелковой атаки)")]
        [SerializeField] private PoolObject _prefab;

        public float Speed => _speed;
        public float Damage => _damage;
        public float Radius => _radius;
        [CanBeNull] public PoolObject Prefab => _prefab;
        public Vector3 LocalSpawnPoint => _localSpawnPoint;
    }
}