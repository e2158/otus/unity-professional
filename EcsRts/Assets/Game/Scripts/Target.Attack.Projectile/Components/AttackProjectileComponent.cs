﻿using System;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Attack.Projectile.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct AttackProjectileComponent : IComponent
    {
        public float Damage;
        public float MaxDistance;
        public Vector3 SpawnPoint;

        public AttackProjectileComponent(float damage, float maxDistance, Vector3 spawnPoint)
        {
            Damage = damage;
            MaxDistance = maxDistance;
            SpawnPoint = spawnPoint;
        }
    }
}