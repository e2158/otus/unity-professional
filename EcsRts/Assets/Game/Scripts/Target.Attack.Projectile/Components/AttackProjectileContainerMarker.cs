﻿using JetBrains.Annotations;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Attack.Projectile.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [System.Serializable]
    public struct AttackProjectileContainerMarker : IComponent
    {
        [NotNull] public Transform Container;

        public AttackProjectileContainerMarker([NotNull] Transform container)
        {
            Container = container ?? throw new System.ArgumentNullException(nameof(container));
        }
    }
}