﻿using System;
using Essentials;
using Game.Common.Configs;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Attack.Projectile.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct AttackProjectileSpawnRequest : IComponent
    {
        public float Speed;
        public float Damage;
        public float Radius;
        public float MaxDistance;
        public Vector2 Direction;
        public Vector3 SpawnPoint;
        [NotNull] public TeamConfig Team;
        [CanBeNull] public PoolObject ViewPrefab;

        public AttackProjectileSpawnRequest(
            float speed, float damage, float radius,
            float maxDistance, Vector2 direction, Vector3 spawnPoint,
            [NotNull] TeamConfig team, [CanBeNull] PoolObject viewPrefab)
        {
            Speed = speed;
            Damage = damage;
            Radius = radius;
            MaxDistance = maxDistance;
            Direction = direction;
            SpawnPoint = spawnPoint;
            ViewPrefab = viewPrefab;
            Team = team ?? throw new ArgumentNullException(nameof(team));
        }
    }
}