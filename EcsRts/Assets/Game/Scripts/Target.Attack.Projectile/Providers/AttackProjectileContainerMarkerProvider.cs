﻿using Game.Target.Attack.Projectile.Components;
using Scellecs.Morpeh.Providers;
using Unity.IL2CPP.CompilerServices;

namespace Game.Target.Attack.Projectile.Providers
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public sealed class AttackProjectileContainerMarkerProvider : MonoProvider<AttackProjectileContainerMarker>
    {

    }
}