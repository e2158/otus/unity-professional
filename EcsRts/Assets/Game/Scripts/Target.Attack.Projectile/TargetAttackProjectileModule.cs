﻿using Game.Target.Attack.Projectile.Systems.FixedUpdate;
using Game.Target.Attack.Projectile.Systems.Update;
using Scellecs.Morpeh;
using UnityEngine;

namespace Game.Target.Attack.Projectile
{
    public static class TargetAttackProjectileModule
    {
        public static SystemsGroup CreateSystemsGroup(World world)
        {
            SystemsGroup group = world.CreateSystemsGroup();

            // Update Systems
            group.AddSystem(ScriptableObject.CreateInstance<AttackProjectileSpawnSystem>());
            group.AddSystem(ScriptableObject.CreateInstance<AttackProjectileDistanceSystem>());
            group.AddSystem(ScriptableObject.CreateInstance<AttackProjectileDespawnSystem>());

            // Fixed Update Systems
            group.AddSystem(ScriptableObject.CreateInstance<AttackProjectileDamageSystem>());

            return group;
        }
    }
}