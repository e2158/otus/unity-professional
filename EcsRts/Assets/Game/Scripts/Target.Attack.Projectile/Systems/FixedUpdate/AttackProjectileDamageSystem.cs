﻿using Essentials;
using Game.Common.Components;
using Game.Common.Configs;
using Game.Target.Attack.Projectile.Components;
using Game.Target.Damage.Components;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Attack.Projectile.Systems.FixedUpdate
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(AttackProjectileDamageSystem))]
    public sealed class AttackProjectileDamageSystem : FixedUpdateSystem
    {
        private const string LogTag = nameof(AttackProjectileDamageSystem);
        private static readonly ILogger logger = Debug.unityLogger;

        private Filter _projectilesFilter;
        private Filter _damageReceiversFilter;

        public override void OnAwake()
        {
            _projectilesFilter = World.Filter
                .With<AttackProjectileComponent>()
                .With<TransformationComponent>()
                .With<TeamComponent>()
                .Without<AttackProjectileDespawnMarker>()
                .Build();

            _damageReceiversFilter = World.Filter
                .With<TransformationComponent>()
                .With<TeamComponent>()
                .With<HpComponent>()
                .Without<DeadMarker>()
                .Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity damageSource in _projectilesFilter)
            {
                var projectile = damageSource.GetComponent<AttackProjectileComponent>();
                var transformation = damageSource.GetComponent<TransformationComponent>();
                var team = damageSource.GetComponent<TeamComponent>();

                Entity damageReceiver = FindDamageReceiver(team.Config, transformation);
                if (damageReceiver != null)
                {
                    damageReceiver.SetComponent(new DamageMarker(projectile.Damage));
                    damageSource.AddComponent<AttackProjectileDespawnMarker>();
                    Log(damageSource, damageReceiver);
                }
            }
        }

        [CanBeNull]
        private Entity FindDamageReceiver(TeamConfig sourceTeam, TransformationComponent sourceTrans)
        {
            foreach (Entity receiver in _damageReceiversFilter)
            {
                var receiverTeam = receiver.GetComponent<TeamComponent>();
                var receiverTrans = receiver.GetComponent<TransformationComponent>();

                if (receiverTeam.Config != sourceTeam)
                {
                    float detectDistance = receiverTrans.Radius + sourceTrans.Radius;
                    float sqrDetectDistance = Mathf.Pow(detectDistance, 2);

                    Vector3 posDiff = sourceTrans.Position - receiverTrans.Position;
                    float sqrDistance = Vector2.SqrMagnitude(posDiff.CutY());

                    if (sqrDistance <= sqrDetectDistance)
                        return receiver;
                }
            }

            return null;
        }

        private void Log(Entity damageSource, Entity damageReceiver)
        {
            TeamConfig sourceTeam = damageSource.GetComponent<TeamComponent>().Config;
            TeamConfig receiverTeam = damageReceiver.GetComponent<TeamComponent>().Config;
            string coloredSource = sourceTeam.Color.ToLogMessage(damageSource.ID.ToString());
            string coloredReceiver = receiverTeam.Color.ToLogMessage(damageReceiver.ID.ToString());
            logger.Log(LogTag, $"Entity \"{coloredSource}\" damaged entity \"{coloredReceiver}\"");
        }
    }
}