﻿using Game.Common.Components;
using Game.Target.Attack.Projectile.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Attack.Projectile.Systems.Update
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(AttackProjectileDistanceSystem))]
    public sealed class AttackProjectileDistanceSystem : UpdateSystem
    {
        private const string LogTag = nameof(AttackProjectileDistanceSystem);
        private static readonly ILogger logger = Debug.unityLogger;

        private Filter _filter;

        public override void OnAwake()
        {
            _filter = World.Filter
                .With<AttackProjectileComponent>()
                .With<TransformationComponent>()
                .Without<AttackProjectileDespawnMarker>()
                .Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _filter)
            {
                var projectile = entity.GetComponent<AttackProjectileComponent>();
                var transformation = entity.GetComponent<TransformationComponent>();

                float sqrMaxDistance = Mathf.Pow(projectile.MaxDistance, 2);
                float sqrDistance = Vector3.SqrMagnitude(transformation.Position - projectile.SpawnPoint);

                if (sqrDistance >= sqrMaxDistance)
                {
                    entity.AddComponent<AttackProjectileDespawnMarker>();
                    logger.Log(LogTag, $"Entity \"{entity.ID}\" reached max distance");
                }
            }
        }
    }
}