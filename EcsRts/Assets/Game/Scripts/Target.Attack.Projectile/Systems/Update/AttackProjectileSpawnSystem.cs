﻿using Essentials;
using Game.Common.Components;
using Game.Common.Configs;
using Game.ECS;
using Game.Target.Attack.Projectile.Components;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Attack.Projectile.Systems.Update
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(AttackProjectileSpawnSystem))]
    public sealed class AttackProjectileSpawnSystem : UpdateSystem
    {
        private const string LogTag = nameof(AttackProjectileSpawnSystem);
        private static readonly ILogger logger = Debug.unityLogger;

        private Filter _requestFilter;
        private Filter _containerFilter;

        public override void OnAwake()
        {
            _requestFilter = WorldRoster.Requests.Filter.With<AttackProjectileSpawnRequest>().Build();
            _containerFilter = WorldRoster.Logic.Filter.With<AttackProjectileContainerMarker>().Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity requestEntity in _requestFilter)
            {
                var request = requestEntity.GetComponent<AttackProjectileSpawnRequest>();

                GameObject view = request.ViewPrefab == null
                    ? null : InstantiateView(request.ViewPrefab);

                Entity entity = CreateEntity(request, view);

                Log(entity);
            }
        }

        [NotNull]
        private GameObject InstantiateView(PoolObject prefab)
        {
            Transform container = _containerFilter.First().GetComponent<AttackProjectileContainerMarker>().Container;

            GameObject view = GlobalPool.Instance.Provide(prefab).gameObject;
            view.transform.SetParent(container, false);

            return view;
        }

        [NotNull]
        private Entity CreateEntity(AttackProjectileSpawnRequest request, [CanBeNull] GameObject view)
        {
            Entity entity = World.CreateEntity();

            entity.SetComponent(new AttackProjectileComponent(request.Damage, request.MaxDistance, request.SpawnPoint));
            entity.SetComponent(new TransformationComponent(request.Radius, request.SpawnPoint));
            entity.SetComponent(new MoveComponent(request.Speed, request.Direction));
            entity.SetComponent(new TeamComponent(request.Team));

            if (view)
                entity.SetComponent(new GameObjectComponent(view));

            if (view && view.TryGetComponent(out Transform transform))
                entity.SetComponent(new TransformComponent(transform));

            if (view && view.TryGetComponent(out Animator animator))
                entity.SetComponent(new AnimatorComponent(animator));

            return entity;
        }

        private static void Log(Entity entity)
        {
            TeamConfig teamConfig = entity.GetComponent<TeamComponent>().Config;
            string coloredEntityId = teamConfig.Color.ToLogMessage(entity.ID.ToString());
            logger.Log(LogTag, $"Spawned attack projectile \"{coloredEntityId}\"");
        }
    }
}