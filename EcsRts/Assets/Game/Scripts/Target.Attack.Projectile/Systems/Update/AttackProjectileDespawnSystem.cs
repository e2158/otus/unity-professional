﻿using Essentials;
using Game.Common.Components;
using Game.Target.Attack.Projectile.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Target.Attack.Projectile.Systems.Update
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(AttackProjectileDespawnSystem))]
    public sealed class AttackProjectileDespawnSystem : UpdateSystem
    {
        private const string LogTag = nameof(AttackProjectileDespawnSystem);
        private static readonly ILogger logger = Debug.unityLogger;

        private Filter _filter;
        private Stash<GameObjectComponent> _goStash;

        public override void OnAwake()
        {
            _filter = World.Filter
                .With<AttackProjectileDespawnMarker>()
                .Build();

            _goStash = World.GetStash<GameObjectComponent>();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _filter)
            {
                string entityId = entity.ID.ToString();

                if (_goStash.Has(entity))
                {
                    GameObject view = _goStash.Get(entity).GameObject;
                    GlobalPool.Instance.Release(view.GetComponent<PoolObject>());
                }

                World.RemoveEntity(entity);

                logger.Log(LogTag, $"Removed projectile \"{entityId}\"");
            }
        }
    }
}