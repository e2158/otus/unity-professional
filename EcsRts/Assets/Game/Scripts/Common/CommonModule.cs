﻿using Game.Common.Systems.Cleanup;
using Game.Common.Systems.LateUpdate;
using Game.Common.Systems.Update;
using UnityEngine;
using Scellecs.Morpeh;

namespace Game.Common
{
    public static class CommonModule
    {
        public static SystemsGroup CreateSystemsGroup(World world)
        {
            SystemsGroup group = world.CreateSystemsGroup();

            // Update Systems
            group.AddSystem(ScriptableObject.CreateInstance<MoveSystem>());

            // Late Update Systems
            group.AddSystem(ScriptableObject.CreateInstance<ViewHpSystem>());
            group.AddSystem(ScriptableObject.CreateInstance<ViewMoveSystem>());
            group.AddSystem(ScriptableObject.CreateInstance<ViewTransformSystem>());

            // Cleanup Systems
            group.AddSystem(ScriptableObject.CreateInstance<OneFrameCleanupSystem>());
            group.AddSystem(ScriptableObject.CreateInstance<DeadMarkerApplySystem>());
            group.AddSystem(ScriptableObject.CreateInstance<DeadMarkerCleanupSystem>());

            return group;
        }
    }
}