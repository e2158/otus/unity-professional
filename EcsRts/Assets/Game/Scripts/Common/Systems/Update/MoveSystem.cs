﻿using Essentials;
using Game.Common.Components;
using Game.Target.Attack.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Common.Systems.Update
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(MoveSystem))]
    public sealed class MoveSystem : UpdateSystem
    {
        private Filter _filter;

        public override void OnAwake()
        {
            _filter = World.Filter
                .With<MoveComponent>()
                .With<TransformationComponent>()
                .Without<DeadMarker>()
                .Without<AttackDelayMarker>()
                .Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _filter)
            {
                ref var transformation = ref entity.GetComponent<TransformationComponent>();
                var move = entity.GetComponent<MoveComponent>();

                transformation.Position += CalcOffset(move, deltaTime);
                if (CanRotate(move)) transformation.Rotation = CalcRotation(move);
            }
        }

        private static bool CanRotate(MoveComponent move) =>
            !move.Direction.Approximately(Vector2.zero);

        private static Vector3 CalcOffset(MoveComponent move, float deltaTime) =>
            (move.Speed * deltaTime * move.Direction).InsertY(0);

        private static Vector3 CalcRotation(MoveComponent move) =>
            Quaternion.LookRotation(move.Direction.InsertY(0)).eulerAngles;
    }
}