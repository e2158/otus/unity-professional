﻿using Game.Common.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Common.Systems.Cleanup
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(OneFrameCleanupSystem))]
    public sealed class OneFrameCleanupSystem : CleanupSystem
    {
        private const string LogTag = nameof(OneFrameCleanupSystem);
        private static readonly ILogger logger = Debug.unityLogger;

        private Filter _filter;

        public override void OnAwake()
        {
            _filter = World.Filter.With<OneFrameComponent>().Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _filter)
            {
                logger.Log(LogTag, $"Removing entity \"{entity.ID}\"");
                World.RemoveEntity(entity);
            }
        }
    }
}