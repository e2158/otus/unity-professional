﻿using Game.Common.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Common.Systems.LateUpdate
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(ViewTransformSystem))]
    public sealed class ViewTransformSystem : LateUpdateSystem
    {
        private Filter _filter;

        public override void OnAwake()
        {
            _filter = World.Filter
                .With<TransformComponent>()
                .With<TransformationComponent>()
                .Without<DeadMarker>()
                .Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _filter)
            {
                ref var view = ref entity.GetComponent<TransformComponent>();
                var transformation = entity.GetComponent<TransformationComponent>();

                view.Transform.position = transformation.Position;
                view.Transform.eulerAngles = transformation.Rotation;
            }
        }
    }
}