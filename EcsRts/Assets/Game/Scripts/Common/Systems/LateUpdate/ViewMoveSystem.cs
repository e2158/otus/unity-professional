﻿using Essentials;
using Game.Common.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Common.Systems.LateUpdate
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(ViewMoveSystem))]
    public sealed class ViewMoveSystem : LateUpdateSystem
    {
        private static readonly int speedHash = Animator.StringToHash("Speed");

        private Filter _filter;

        public override void OnAwake()
        {
            _filter = World.Filter
                .With<AnimatorComponent>()
                .With<MoveComponent>()
                .Without<DeadMarker>()
                .Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _filter)
            {
                ref var view = ref entity.GetComponent<AnimatorComponent>();
                var move = entity.GetComponent<MoveComponent>();

                bool hasDirection = !move.Direction.Approximately(Vector2.zero);
                view.Animator.SetFloat(speedHash, hasDirection ? move.Speed : 0f);
            }
        }
    }
}