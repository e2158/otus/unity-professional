﻿using Game.Common.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Common.Systems.LateUpdate
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(ViewHpSystem))]
    public sealed class ViewHpSystem : LateUpdateSystem
    {
        private static readonly int isAliveHash = Animator.StringToHash("IsAlive");

        private Filter _filter;

        public override void OnAwake()
        {
            _filter = World.Filter.With<AnimatorComponent>().With<HpComponent>().Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _filter)
            {
                ref var view = ref entity.GetComponent<AnimatorComponent>();
                var hp = entity.GetComponent<HpComponent>();

                view.Animator.SetBool(isAliveHash, hp.IsAlive);
            }
        }
    }
}