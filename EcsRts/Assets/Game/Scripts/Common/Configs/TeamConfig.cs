﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Common.Configs
{
    [CreateAssetMenu(
        fileName = nameof(TeamConfig),
        menuName = AssetsMenu.Configs + nameof(TeamConfig))]
    public sealed class TeamConfig : ScriptableObject
    {
        [SerializeField, Required] private string _name;
        [SerializeField] private Color _color;

        public string Name => _name;
        public Color Color => _color;
    }
}