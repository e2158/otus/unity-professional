﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Common.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [System.Serializable]
    public struct MoveComponent : IComponent
    {
        public float Speed;
        public Vector2 Direction;

        public MoveComponent(float speed, Vector2 direction) : this(speed)
        {
            Direction = direction;
        }

        public MoveComponent(float speed)
        {
            Speed = speed;
            Direction = Vector2.zero;
        }
    }
}