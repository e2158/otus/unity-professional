﻿using UnityEngine;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Game.Common.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [System.Serializable]
    public struct TransformationComponent : IComponent
    {
        public float Radius;
        public Vector3 Position;
        public Vector3 Rotation;

        public TransformationComponent(float radius, Vector3 position, Vector3 rotation)
        {
            Radius = radius;
            Position = position;
            Rotation = rotation;
        }

        public TransformationComponent(float radius, Vector3 position) : this()
        {
            Radius = radius;
            Position = position;
        }
    }
}