﻿using System;
using Game.Common.Configs;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Game.Common.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [System.Serializable]
    public struct TeamComponent : IComponent
    {
        [NotNull] public TeamConfig Config;

        public TeamComponent([NotNull] TeamConfig config)
        {
            Config = config ?? throw new ArgumentNullException(nameof(config));
        }
    }
}