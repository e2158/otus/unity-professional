﻿using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Common.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [System.Serializable]
    public struct HpComponent : IComponent
    {
        public float Current;
        public float Max;

        public bool IsAlive => !IsDead;
        public bool IsDead => Mathf.Approximately(Ratio, 0f) || Ratio < 0;
        public float Ratio => Max > 0 ? Current / Max : 0f;

        public HpComponent(float current, float max)
        {
            Current = current;
            Max = max;
        }
    }
}