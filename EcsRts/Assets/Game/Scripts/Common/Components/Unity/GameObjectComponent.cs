﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Game.Common.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct GameObjectComponent : IComponent
    {
        [NotNull] public GameObject GameObject;

        public GameObjectComponent([NotNull] GameObject gameObject)
        {
            GameObject = gameObject ?? throw new ArgumentNullException(nameof(gameObject));
        }
    }
}