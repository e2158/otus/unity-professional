﻿using System;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Common.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct TransformComponent : IComponent
    {
        [NotNull] public Transform Transform;

        public TransformComponent([NotNull] Transform transform)
        {
            Transform = transform ?? throw new ArgumentNullException(nameof(transform));
        }
    }
}