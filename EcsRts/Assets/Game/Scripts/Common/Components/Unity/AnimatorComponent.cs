﻿using System;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Common.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct AnimatorComponent : IComponent
    {
        [NotNull] public Animator Animator;

        public AnimatorComponent([NotNull] Animator animator)
        {
            Animator = animator ?? throw new ArgumentNullException(nameof(animator));
        }
    }
}