﻿using Game.Unit.Systems.Update;
using UnityEngine;
using Scellecs.Morpeh;

namespace Game.Unit
{
    public static class UnitModule
    {
        public static SystemsGroup CreateSystemsGroup(World world)
        {
            SystemsGroup group = world.CreateSystemsGroup();

            // Update Systems
            group.AddSystem(ScriptableObject.CreateInstance<UnitSpawnSystem>());
            group.AddSystem(ScriptableObject.CreateInstance<UnitDespawnSystem>());

            return group;

        }
    }
}