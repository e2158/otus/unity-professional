﻿using Essentials;
using Game.Common.Components;
using Game.Unit.Components;
using Game.Unit.Configs;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Unit.Systems.Update
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(UnitDespawnSystem))]
    public sealed class UnitDespawnSystem : CleanupSystem
    {
        private const float Delay = 2f;

        private const string LogTag = nameof(UnitDespawnSystem);
        private static readonly ILogger logger = Debug.unityLogger;

        private Filter _filter;
        private Stash<GameObjectComponent> _goStash;

        public override void OnAwake()
        {
            _filter = World.Filter
                .With<UnitComponent>()
                .With<DeadMarker>()
                .Build();

            _goStash = World.GetStash<GameObjectComponent>();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity entity in _filter)
            {
                float timestamp = entity.GetComponent<DeadMarker>().Timestamp;

                if (Time.time - timestamp >= Delay)
                {
                    string entityId = entity.ID.ToString();
                    UnitConfig unitConfig = entity.GetComponent<UnitComponent>().Config;
                    GameObject view = _goStash.Has(entity) ? _goStash.Get(entity).GameObject : null;

                    if (view != null)
                        GlobalPool.Instance.Release(view.GetComponent<PoolObject>());

                    logger.Log(LogTag, $"Despawned unit \"{unitConfig.name} {entityId}\"");
                }
            }
        }
    }
}