﻿using Essentials;
using Game.Common.Components;
using Game.ECS;
using Game.Target.Attack.Components;
using Game.Unit.Components;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Unit.Systems.Update
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [CreateAssetMenu(menuName = AssetsMenu.EcsSystems + nameof(UnitSpawnSystem))]
    public sealed class UnitSpawnSystem : UpdateSystem
    {
        private const string LogTag = nameof(UnitSpawnSystem);
        private static readonly ILogger logger = Debug.unityLogger;

        private Filter _requestFilter;
        private Filter _containerFilter;

        public override void OnAwake()
        {
            _requestFilter = WorldRoster.Requests.Filter.With<UnitSpawnRequest>().Build();
            _containerFilter = WorldRoster.Logic.Filter.With<UnitContainerMarker>().Build();
        }

        public override void OnUpdate(float deltaTime)
        {
            foreach (Entity requestEntity in _requestFilter)
            {
                var request = requestEntity.GetComponent<UnitSpawnRequest>();

                Entity unitEntity = InstantiateUnit(request);
                InitializeUnit(unitEntity, request);

                Log(unitEntity, request);
            }
        }

        private Entity InstantiateUnit(UnitSpawnRequest request)
        {
            Transform container = _containerFilter.First().GetComponent<UnitContainerMarker>().Container;

            var unit = GlobalPool.Instance.Provide(request.Prefab).GetComponent<EntityProvider>();
            unit.transform.SetParent(container, false);
            unit.Entity.RemoveComponent<DeadMarker>();

            return unit.Entity;
        }

        private void InitializeUnit(Entity unitEntity, UnitSpawnRequest request)
        {
            unitEntity.SetComponent(new UnitComponent(request.Config));
            unitEntity.SetComponent(new AttackSenderComponent(request.Config.AttackData));

            unitEntity.SetComponent(new HpComponent(request.Config.Hp, request.Config.Hp));
            unitEntity.SetComponent(new TeamComponent(request.Team));

            unitEntity.SetComponent(new MoveComponent(request.Config.Speed));
            unitEntity.SetComponent(new TransformationComponent(request.Config.Radius, request.Position, request.Rotation));
        }

        private static void Log(Entity unitEntity, UnitSpawnRequest request)
        {
            string coloredUnit = request.Team.Color.ToLogMessage($"{request.Config.name} {unitEntity.ID}");
            logger.Log(LogTag, $"Spawned unit \"{coloredUnit}\"");
        }
    }
}