﻿using System;
using Game.Unit.Configs;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;

namespace Game.Unit.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct UnitComponent : IComponent
    {
        [NotNull] public UnitConfig Config;

        public UnitComponent([NotNull] UnitConfig config)
        {
            Config = config ?? throw new ArgumentNullException(nameof(config));
        }
    }
}