﻿using System;
using Essentials;
using Game.Common.Configs;
using Game.Unit.Configs;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Providers;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Unit.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct UnitSpawnRequest : IComponent
    {
        [NotNull] public TeamConfig Team;
        [NotNull] public UnitConfig Config;
        [NotNull] public PoolObject Prefab;
        public Vector3 Position;
        public Vector3 Rotation;

        public UnitSpawnRequest([NotNull] TeamConfig team, [NotNull] UnitConfig config, [NotNull] PoolObject prefab, Vector3 position, Vector3 rotation)
        {
            Team = team ?? throw new ArgumentNullException(nameof(team));
            Config = config ?? throw new ArgumentNullException(nameof(config));
            Prefab = prefab ?? throw new ArgumentNullException(nameof(prefab));
            Position = position;
            Rotation = rotation;
        }
    }
}