﻿using System;
using JetBrains.Annotations;
using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

namespace Game.Unit.Components
{
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    [Serializable]
    public struct UnitContainerMarker : IComponent
    {
        [NotNull] public Transform Container;

        public UnitContainerMarker([NotNull] Transform container)
        {
            Container = container ?? throw new ArgumentNullException(nameof(container));
        }
    }
}