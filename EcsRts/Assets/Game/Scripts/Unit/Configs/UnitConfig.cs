﻿using Game.Target.Attack.Configs;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Unit.Configs
{
    [CreateAssetMenu(
        fileName = nameof(UnitConfig),
        menuName = AssetsMenu.Configs + nameof(UnitConfig))]
    public sealed class UnitConfig : ScriptableObject
    {
        [MinValue(0)]
        [Tooltip("Кол-во здоровья юнита")]
        [SerializeField] private float _hp;

        [MinValue(0), SuffixLabel("m/sec", true)]
        [Tooltip("Скорость перемещения юнита")]
        [SerializeField] private float _speed;

        [MinValue(0), SuffixLabel("m", true)]
        [Tooltip("Радиус юнита для просчёта коллизий")]
        [SerializeField] private float _radius;

        [BoxGroup("Attack Data"), HideLabel]
        [SerializeField] private AttackConfigData _attackData;


        public float Hp => _hp;
        public float Speed => _speed;
        public float Radius => _radius;
        public AttackConfigData AttackData => _attackData;
    }
}