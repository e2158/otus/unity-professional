﻿using UnityEngine;
using Sirenix.OdinInspector;
using Game.TownHall.Controllers;

namespace Game
{
    public sealed class GameController : MonoBehaviour
    {
        [SerializeField, Required] private InputController _inputController;
        [SerializeField, Required] private TownHallRuntimeController[] _townHallRuntimeControllers;

        private void OnValidate()
        {
            _inputController ??= FindObjectOfType<InputController>();
            _townHallRuntimeControllers ??= FindObjectsOfType<TownHallRuntimeController>();
        }

        private void OnEnable()
        {
            _inputController.BuyUnitClicked += OnBuyUnitClicked;
        }

        private void OnDisable()
        {
            _inputController.BuyUnitClicked -= OnBuyUnitClicked;
        }

        private void OnBuyUnitClicked(int teamindex, int unitindex)
        {
            _townHallRuntimeControllers[teamindex].SpawnUnit(unitindex);
        }
    }
}