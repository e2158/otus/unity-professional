﻿namespace Game
{
    public static class AssetsMenu
    {
        private const string Base = "Game/";
        private const string Ecs = Base + "ECS/";

        public const string Configs = Base + "Configs/";
        public const string EcsSystems = Ecs + "Systems/";
    }
}