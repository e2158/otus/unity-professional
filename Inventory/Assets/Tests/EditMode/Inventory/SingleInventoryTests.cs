﻿using System.Linq;
using Game.Inventory;
using Game.Items;
using NUnit.Framework;

namespace Tests.EditMode.Inventory
{
    [TestOf(typeof(SingleInventory))]
    public sealed class SingleInventoryTests : BaseInventoryTests
    {
        protected override IInventory CreateInventory() => new SingleInventory();

        [TestCase(new string[0], "equip_body_cool_armor")]
        [TestCase(new [] { "equip_head_cats_boots" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_head_cats_boots" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_dragon_armor", "equip_head_cats_boots" }, "equip_body_cool_armor")]
        public void WhenAddItem_ThenItemsCountIsIncremented(string[] initialItemIds, string addedItemId)
        {
            // arrange
            PopulateInventory(initialItemIds);
            Item addedItem = Items[addedItemId];
            int initialItemsCount = Inventory.Items.Count();

            // act
            Inventory.AddItem(addedItem);

            // assert
            int actualItemsCount = Inventory.Items.Count();
            Assert.That(actualItemsCount, Is.EqualTo(initialItemsCount + 1));
        }
    }
}