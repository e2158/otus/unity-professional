﻿using System.Linq;
using Game.Inventory;
using Game.Items;
using NUnit.Framework;

namespace Tests.EditMode.Inventory
{
    [TestOf(typeof(StackInventory))]
    public sealed class StackInventoryTests : BaseInventoryTests
    {
        protected override IInventory CreateInventory() => new StackInventory();

        [TestCase(new string[0], "equip_body_cool_armor")]
        [TestCase(new [] { "equip_head_cats_boots" }, "equip_body_dragon_armor")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_head_cats_boots" }, "equip_body_dragon_armor")]
        public void WhenAddItem_AndItemIsUnique_ThenItemsCountIsIncremented(string[] initialItemIds, string addedItemId)
        {
            // arrange
            PopulateInventory(initialItemIds);
            Item addedItem = Items[addedItemId];
            int initialItemsCount = Inventory.Items.Count();

            // act
            Inventory.AddItem(addedItem);

            // assert
            int actualItemsCount = Inventory.Items.Count();
            Assert.That(actualItemsCount, Is.EqualTo(initialItemsCount + 1));
        }

        [TestCase(new[] { "equip_body_cool_armor" }, "equip_body_cool_armor")]
        [TestCase(new[] { "equip_body_dragon_armor", "equip_head_cats_boots" }, "equip_body_dragon_armor")]
        public void WhenAddItem_AndItemIsDuplicating_ThenItemsCountIsTheSame(string[] initialItemIds, string addedItemId)
        {
            // arrange
            PopulateInventory(initialItemIds);
            Item addedItem = Items[addedItemId];
            int initialItemsCount = Inventory.Items.Count();

            // act
            Inventory.AddItem(addedItem);

            // assert
            int actualItemsCount = Inventory.Items.Count();
            Assert.That(actualItemsCount, Is.EqualTo(initialItemsCount));
        }
    }
}