﻿using System.Linq;
using Game.Inventory;
using Game.Items;
using NUnit.Framework;

namespace Tests.EditMode.Inventory
{
    [TestOf(typeof(CompositeInventoryDecorator))]
    public sealed class CompositeInventoryDecoratorTests : BaseInventoryTests
    {
        protected override IInventory CreateInventory() =>
            new CompositeInventoryDecorator(new SingleInventory(), new StackInventory());

        [TestCase(new string[0], "equip_body_cool_armor")]
        [TestCase(new [] { "equip_head_cats_boots" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_head_cats_boots" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_dragon_armor", "equip_head_cats_boots" }, "equip_body_cool_armor")]
        public void WhenAddItem_AndItemIsNotStackable_ThenItemsCountIsIncremented(string[] initialItemIds, string addedItemId)
        {
            // arrange
            PopulateInventory(initialItemIds);
            Item addedItem = Items[addedItemId];
            int initialItemsCount = Inventory.Items.Count();

            // act
            Inventory.AddItem(addedItem);

            // assert
            int actualItemsCount = Inventory.Items.Count();
            Assert.That(actualItemsCount, Is.EqualTo(initialItemsCount + 1));
        }

        [TestCase(new string[0], "item_gold")]
        [TestCase(new [] { "equip_head_cats_boots" }, "item_gold")]
        [TestCase(new [] { "item_heal_pack", "item_grenade" }, "item_gold")]
        [TestCase(new [] { "item_heal_pack", "equip_head_cats_boots" }, "item_gold")]
        public void WhenAddItem_AndItemIsStackable_AndItemIsUnique_ThenItemsCountIsIncremented(string[] initialItemIds, string addedItemId)
        {
            // arrange
            PopulateInventory(initialItemIds);
            Item addedItem = Items[addedItemId];
            int initialItemsCount = Inventory.Items.Count();

            // act
            Inventory.AddItem(addedItem);

            // assert
            int actualItemsCount = Inventory.Items.Count();
            Assert.That(actualItemsCount, Is.EqualTo(initialItemsCount + 1));
        }

        [TestCase(new [] { "item_gold" }, "item_gold")]
        [TestCase(new [] { "item_gold", "item_grenade" }, "item_gold")]
        [TestCase(new [] { "item_heal_pack", "item_gold" }, "item_gold")]
        public void WhenAddItem_AndItemIsStackable_AndItemIsDuplicating_ThenItemsCountIsTheSame(string[] initialItemIds, string addedItemId)
        {
            // arrange
            PopulateInventory(initialItemIds);
            Item addedItem = Items[addedItemId];
            int initialItemsCount = Inventory.Items.Count();

            // act
            Inventory.AddItem(addedItem);

            // assert
            int actualItemsCount = Inventory.Items.Count();
            Assert.That(actualItemsCount, Is.EqualTo(initialItemsCount));
        }
    }
}