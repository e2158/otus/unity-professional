﻿using System;
using System.Linq;
using System.Collections.Generic;
using Game.Items;
using Game.Inventory;
using UnityEditor;
using NUnit.Framework;

namespace Tests.EditMode.Inventory
{
    public abstract class BaseInventoryTests
    {
        protected static Dictionary<string, Item> Items { get; private set; }

        protected IInventory Inventory { get; private set; }

        [SetUp]
        public void Setup()
        {
            Inventory = CreateInventory();

            Items ??= AssetDatabase.FindAssets($"t:{nameof(Item)}")
                .Select(AssetDatabase.GUIDToAssetPath)
                .Select(AssetDatabase.LoadAssetAtPath<Item>)
                .ToDictionary(stat => stat.Id, stat => stat);
        }

        protected abstract IInventory CreateInventory();

        protected void PopulateInventory(IEnumerable<string> itemIds)
        {
            IEnumerable<Item> items = itemIds.Select(id => Items[id]);
            foreach (Item item in items) Inventory.AddItem(item);
        }

        #region void AddItem(Item item)

        [TestCase("equip_body_cool_armor")]
        public void WhenAddItem_AndInventoryIsEmpty_ThenItemsContainOnlyIt(string addedItemId)
        {
            // arrange
            Item addedItem = Items[addedItemId];

            // act
            Inventory.AddItem(addedItem);

            // assert
            Assert.That(Inventory.Items, Has.Exactly(1).EqualTo(addedItem));
        }

        [TestCase(new [] { "equip_body_cool_armor", "equip_head_cats_boots" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_dragon_armor", "equip_head_cats_boots" }, "equip_body_cool_armor")]
        public void WhenAddItem_AndInventoryHasItems_ThenItemsContainIt(string[] initialItemIds, string addedItemId)
        {
            // arrange
            PopulateInventory(initialItemIds);
            Item addedItem = Items[addedItemId];

            // act
            Inventory.AddItem(addedItem);

            // assert
            Assert.That(Inventory.Items, Has.Member(addedItem));
        }

        [TestCase("equip_body_cool_armor")]
        public void WhenAddItem_ThenEventIsRaisedWithThatItem(string addedItemId)
        {
            // arrange
            Item raisedItem = null;
            Item addedItem = Items[addedItemId];
            Inventory.ItemAdded += item => raisedItem = item;

            // act
            Inventory.AddItem(addedItem);

            // assert
            Assert.That(raisedItem, Is.Not.Null);
            Assert.That(raisedItem, Is.EqualTo(addedItem));
        }

        [Test]
        public void WhenAddItem_AndItemIsNull_ThenThrowsArgumentNullException()
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(
                () => Inventory.AddItem(null!));
        }

        #endregion // void AddItem(Item item)

        #region void RemoveItem(Item item)

        [TestCase(new [] { "equip_body_cool_armor" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_head_cats_boots" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_dragon_armor", "equip_head_cats_boots" }, "equip_head_cats_boots")]
        public void WhenRemoveItem_ThenItemsDontContainIt(string[] initialItemIds, string removedItemId)
        {
            // arrange
            PopulateInventory(initialItemIds);
            Item removedItem = Items[removedItemId];

            // act
            Inventory.RemoveItem(removedItem);

            // assert
            Assert.That(Inventory.Items, Has.No.Member(removedItem));
        }

        [TestCase(new [] { "equip_body_cool_armor" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_head_cats_boots" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_dragon_armor", "equip_head_cats_boots" }, "equip_head_cats_boots")]
        public void WhenRemoveItem_ThenItemsCountIsDecremented(string[] initialItemIds, string removedItemId)
        {
            // arrange
            PopulateInventory(initialItemIds);
            Item removedItem = Items[removedItemId];
            int initialItemsCount = Inventory.Items.Count();

            // act
            Inventory.RemoveItem(removedItem);

            // assert
            int actualItemsCount = Inventory.Items.Count();
            Assert.That(actualItemsCount, Is.EqualTo(initialItemsCount - 1));
        }

        [TestCase(new [] { "equip_body_cool_armor" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_head_cats_boots" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_dragon_armor", "equip_head_cats_boots" }, "equip_head_cats_boots")]
        public void WhenRemoveItem_ThenEventIsRaised(string[] initialItemIds, string removedItemId)
        {
            // arrange
            Item raisedItem = null;
            Item removedItem = Items[removedItemId];
            Inventory.ItemRemoved += item => raisedItem = item;

            PopulateInventory(initialItemIds);

            // act
            Inventory.RemoveItem(removedItem);

            // assert
            Assert.That(raisedItem, Is.Not.Null);
            Assert.That(raisedItem, Is.EqualTo(removedItem));
        }

        [TestCase(new string[0], "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_head_cats_boots" }, "equip_arms_playful_hand")]
        public void WhenRemoveItem_AndInventoryDoesNotContainIt_ThenThrowsArgumentException(string[] initialItemIds, string removedItemId)
        {
            // arrange
            PopulateInventory(initialItemIds);
            Item removedItem = Items[removedItemId];

            // act & assert
            Assert.Throws<ArgumentException>(
                () => Inventory.RemoveItem(removedItem));
        }

        [TestCase(arg: new string[0])]
        [TestCase(arg: new[] { "equip_body_cool_armor", "equip_head_cats_boots" })]
        public void WhenRemoveNullItem_ThenThrowsArgumentNullException(string[] initialItemIds)
        {
            // arrange
            PopulateInventory(initialItemIds);

            // act & assert
            Assert.Throws<ArgumentNullException>(
                () => Inventory.RemoveItem(null!));
        }

        #endregion // void RemoveItem(Item item)

        #region HasItem(Item item)

        [TestCase(new [] { "equip_body_cool_armor" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_body_cool_armor" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_head_cats_boots" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_dragon_armor", "equip_head_cats_boots" }, "equip_head_cats_boots")]
        public void WhenCheckHasItem_AndItemExists_ThenReturnsTrue(string[] initialItemIds, string itemId)
        {
            // arrange
            PopulateInventory(initialItemIds);
            Item item = Items[itemId];

            // act
            bool hasItem = Inventory.HasItem(item);

            // assert
            Assert.That(hasItem, Is.True);
        }

        [TestCase(new string[0], "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor" }, "equip_head_cats_boots")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_body_cool_armor" }, "equip_head_cats_boots")]
        [TestCase(new [] { "equip_body_dragon_armor", "equip_arms_playful_hand" }, "equip_head_cats_boots")]
        public void WhenCheckHasItem_AndItemDoesntExist_ThenReturnsFalse(string[] initialItemIds, string itemId)
        {
            // arrange
            PopulateInventory(initialItemIds);
            Item item = Items[itemId];

            // act
            bool hasItem = Inventory.HasItem(item);

            // assert
            Assert.That(hasItem, Is.False);
        }

        [Test]
        public void WhenCheckHasItem_AndItemIsNull_ThenThrowsArgumentNullException()
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(
                () => Inventory.HasItem(null!));
        }

        #endregion // HasItem(Item item)

        #region int GetCount(Item item)

        [TestCase(new [] { "equip_body_cool_armor" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_body_cool_armor" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_head_cats_boots" }, "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_dragon_armor", "equip_head_cats_boots" }, "equip_head_cats_boots")]
        [TestCase(new [] { "equip_head_cats_boots", "equip_body_dragon_armor", "equip_head_cats_boots" }, "equip_head_cats_boots")]
        public void WhenGetCountOfItem_AndItemExists_ThenReturnsCountOfItem(string[] initialItemIds, string itemId)
        {
            // arrange
            Item item = Items[itemId];
            PopulateInventory(initialItemIds);
            int expectedCount = initialItemIds.Count(id => id == itemId);

            // act
            int actualCount = Inventory.GetCount(item);

            // assert
            Assert.That(actualCount, Is.EqualTo(expectedCount));
        }

        [TestCase(new string[0], "equip_body_cool_armor")]
        [TestCase(new [] { "equip_body_cool_armor" }, "equip_head_cats_boots")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_body_cool_armor" }, "equip_head_cats_boots")]
        [TestCase(new [] { "equip_body_cool_armor", "equip_head_cats_boots" }, "equip_body_dragon_armor")]
        public void WhenGetCountOfItem_AndItemDoesntExist_ThenReturnsZero(string[] initialItemIds, string itemId)
        {
            // arrange
            Item item = Items[itemId];
            PopulateInventory(initialItemIds);

            // act
            int actualCount = Inventory.GetCount(item);

            // assert
            Assert.That(actualCount, Is.EqualTo(0));
        }

        [Test]
        public void WhenGetCountOfItem_AndItemIsNull_ThenThrowsArgumentNullException()
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(
                () => Inventory.GetCount(null!));
        }

        #endregion // int GetCount(Item item)

    }
}