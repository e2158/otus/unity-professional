﻿using System;
using System.Linq;
using System.Collections.Generic;
using Game.Stats;
using NUnit.Framework;

namespace Tests.EditMode.Stats
{
    [TestOf(typeof(StatRepository))]
    public sealed class StatRepositoryStringTests
    {
        private StatRepository _repository;

        [SetUp]
        public void Setup()
        {
            _repository = new StatRepository();
        }

        private void PopulateRepository(IEnumerable<string> statNames, IEnumerable<int> statValues)
        {
            IEnumerable<(string stat, int value)> tuples =
                statNames.Zip(statValues, (s, v) => (s, v));

            foreach ((string stat, int value) tuple in tuples)
                _repository.SetStat(tuple.stat, tuple.value);
        }

        #region int GetStat(string statId)

        [TestCase("hp")]
        public void WhenGetStat_AndRepoIsEmpty_ThenReturnDefaultValue(string statName)
        {
            // arrange
            const int defaultValue = 0;

            // act
            int statValue = _repository.GetStat(statName);

            // assert
            Assert.That(statValue, Is.EqualTo(defaultValue));
        }

        [TestCase("")]
        [TestCase(null)]
        public void WhenGetStat_AndStatIsNullOrEmpty_ThenThrowsArgumentNullException(string statName)
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(
                () => _repository.GetStat(statName));
        }

        #endregion // int GetStat(string statId)

        #region void SetStat(string statId, int value)

        [TestCase(new [] { "hp", "attack", "agility" }, "hp", ExpectedResult = 3)]
        [TestCase(new [] { "hp", "attack", "agility" }, "speed", ExpectedResult = 4)]
        public int WhenSetStat_ThenCountOfStatsCanBeChanged(string[] initialStatNames, string setStatName)
        {
            // arrange
            PopulateRepository(initialStatNames, initialStatNames.Select(_ => (int)default));

            // act
            _repository.SetStat(setStatName, default);

            // assert
            return _repository.Count();
        }

        [TestCase("hp", 0)]
        [TestCase("attack", 10)]
        [TestCase("123gdsa", -2)]
        public void WhenSetStat_AndRepoIsEmpty_ThenGetTheSameStat(string statName, int value)
        {
            // act
            _repository.SetStat(statName, value);

            // assert
            int actualValue = _repository.GetStat(statName);
            Assert.That(actualValue, Is.EqualTo(value));
        }

        [TestCase("hp", 10, 0)]
        [TestCase("attack", 10, 10)]
        [TestCase("123gdsa", 3, -2)]
        public void WhenSetStat_AndRepoHasIt_ThenReturnUpdatedStat(string statName, int initialValue, int setValue)
        {
            // arrange
            _repository.SetStat(statName, initialValue);

            // act
            _repository.SetStat(statName, setValue);

            // assert
            int actualValue = _repository.GetStat(statName);
            Assert.That(actualValue, Is.EqualTo(setValue));
        }

        [TestCase("")]
        [TestCase(null)]
        public void WhenSetStat_AndStatsIsNullOrEmpty_ThenThrowsArgumentNullException(string statName)
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(
                () => _repository.SetStat(statName, default));
        }

        #endregion // void SetStat(string statId, int value)

        #region void SetStats(IEnumerable<KeyValuePair<string, int>> stats)

        [TestCase(new [] { "hp", "attack", "agility" }, new [] { "speed" }, ExpectedResult = 4)]
        [TestCase(new [] { "hp", "attack", "agility" }, new [] { "speed", "intelligence"}, ExpectedResult = 5)]
        [TestCase(new [] { "hp", "attack", "agility" }, new [] { "speed", "skill", "attack" }, ExpectedResult = 5)]
        public int WhenSetStats_ThenCountOfStatsCanBeChanged(
            string[] initialStatNames, string[] setStatNames)
        {
            // arrange
            PopulateRepository(initialStatNames, initialStatNames.Select(_ => (int)default));

            IEnumerable<KeyValuePair<string,int>> setStatsAndValues = setStatNames
                .Select(s => new KeyValuePair<string, int>(s, default));

            // act
            _repository.SetStats(setStatsAndValues);

            // assert
            return _repository.Count();
        }

        [TestCase(new[] { "hp", "attack", "123gdsa" }, new[] { 0, 10, -2 })]
        public void WhenSetStats_AndRepoIsEmpty_ThenGetTheSameStats(string[] stats, int[] values)
        {
            // arrange
            IEnumerable<KeyValuePair<string, int>> statsAndValues = stats
                .Zip(values, (s, v) => (s, v))
                .Select(x => new KeyValuePair<string, int>(x.s, x.v));

            // act
            _repository.SetStats(statsAndValues);

            // assert
            IEnumerable<int> actualValues = stats.Select(_repository.GetStat);
            Assert.That(actualValues, Is.EquivalentTo(values));
        }

        [TestCase(new[] { "hp", "attack", "123gdsa" }, new[] { 10, 10, 3 },
            new[] { 0, 10, -2 })]
        public void WhenSetStats_AndRepoHasThem_ThenReturnUpdatedStats(
            string[] stats, int[] initialValues, int[] setValues)
        {
            // arrange
            PopulateRepository(stats, initialValues);

            IEnumerable<KeyValuePair<string, int>> setStatsAndValues = stats
                .Zip(setValues, (s, v) => (s, v))
                .Select(x => new KeyValuePair<string, int>(x.s, x.v));

            // act
            _repository.SetStats(setStatsAndValues);

            // assert
            IEnumerable<int> actualValues = stats.Select(_repository.GetStat);
            Assert.That(actualValues, Is.EquivalentTo(setValues));
        }

        [Test]
        public void WhenSetStats_AndStatsAreNull_ThenThrowsArgumentNullException()
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(
                () => _repository.SetStats(((IEnumerable<KeyValuePair<string, int>>)null)!));
        }

        #endregion // void SetStats(IEnumerable<KeyValuePair<string, int>> stats)
    }
}