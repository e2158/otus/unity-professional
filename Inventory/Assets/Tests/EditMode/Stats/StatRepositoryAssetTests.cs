﻿using System;
using System.Linq;
using System.Collections.Generic;
using Game.Stats;
using UnityEditor;
using NUnit.Framework;

namespace Tests.EditMode.Stats
{
    [TestOf(typeof(StatRepository))]
    public sealed class StatRepositoryAssetTests
    {
        private static Dictionary<string, Stat> statAssets;

        private StatRepository _repository;

        [SetUp]
        public void Setup()
        {
            _repository = new StatRepository();

            statAssets ??= AssetDatabase.FindAssets($"t:{nameof(Stat)}")
                .Select(AssetDatabase.GUIDToAssetPath)
                .Select(AssetDatabase.LoadAssetAtPath<Stat>)
                .ToDictionary(stat => stat.Id, stat => stat);
        }

        private void PopulateRepository(IEnumerable<string> statIds, IEnumerable<int> statValues)
        {
            IEnumerable<Stat> stats = statIds.Select(statId => statAssets[statId]);
            PopulateRepository(stats, statValues);
        }

        private void PopulateRepository(IEnumerable<Stat> stats, IEnumerable<int> statValues)
        {
            IEnumerable<(Stat stat, int value)> tuples =
                stats.Zip(statValues, (s, v) => (s, v));

            foreach ((Stat stat, int value) tuple in tuples)
                _repository.SetStat(tuple.stat, tuple.value);
        }

        #region int GetStat(Stat stat)

        [TestCase("stat_hp")]
        public void WhenGetStat_AndRepoIsEmpty_ThenReturnDefaultValue(string statId)
        {
            // arrange
            const int defaultValue = 0;
            Stat statAsset = statAssets[statId];

            // act
            int statValue = _repository.GetStat(statAsset);

            // assert
            Assert.That(statValue, Is.EqualTo(defaultValue));
        }

        [Test]
        public void WhenGetNullStat_ThenThrowsArgumentNullException()
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(
                () => _repository.GetStat(null!));
        }

        #endregion // int GetStat(Stat stat)

        #region void SetStat(Stat stat, int value)

        [TestCase(new [] { "stat_hp", "stat_damage", "stat_agility" }, "stat_hp", ExpectedResult = 3)]
        [TestCase(new [] { "stat_hp", "stat_damage", "stat_agility" }, "stat_speed", ExpectedResult = 4)]
        public int WhenSetStat_ThenCountOfStatsCanBeChanged(string[] initialStatIds, string setStatIds)
        {
            // arrange
            PopulateRepository(initialStatIds, initialStatIds.Select(_ => (int)default));

            // act
            Stat statAsset = statAssets[setStatIds];
            _repository.SetStat(statAsset, default);

            // assert
            return _repository.Count();
        }

        [TestCase("stat_hp", 0)]
        [TestCase("stat_damage", 10)]
        [TestCase("stat_agility", -2 )]
        public void WhenSetStat_AndRepoIsEmpty_ThenGetTheSameStat(string statId, int value)
        {
            // arrange
            Stat statAsset = statAssets[statId];

            // act
            _repository.SetStat(statAsset, value);

            // assert
            int actualValue = _repository.GetStat(statAsset);
            Assert.That(actualValue, Is.EqualTo(value));
        }

        [TestCase("stat_hp", 10, 0)]
        [TestCase("stat_damage", 10, 10)]
        [TestCase("stat_agility", 3, -2)]
        public void WhenSetStat_AndRepoHasIt_ThenReturnUpdatedStat(string statId, int initialValue, int setValue)
        {
            // arrange
            Stat statAsset = statAssets[statId];
            _repository.SetStat(statAsset, initialValue);

            // act
            _repository.SetStat(statAsset, setValue);

            // assert
            int actualValue = _repository.GetStat(statAsset);
            Assert.That(actualValue, Is.EqualTo(setValue));
        }

        [Test]
        public void WhenSetNullStat_ThenThrowsArgumentNullException()
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(
                () => _repository.SetStat(null!, default));
        }

        #endregion // void SetStat(Stat stat, int value)

        #region void SetStats(IEnumerable<KeyValuePair<Stat, int>> stats)

        [TestCase(new [] { "stat_hp", "stat_damage", "stat_agility" }, new [] { "stat_speed" }, ExpectedResult = 4)]
        [TestCase(new [] { "stat_hp", "stat_damage", "stat_agility" }, new [] { "stat_speed", "stat_intelligence"}, ExpectedResult = 5)]
        [TestCase(new [] { "stat_hp", "stat_damage", "stat_agility" }, new [] { "stat_speed", "stat_stamina", "stat_damage" }, ExpectedResult = 5)]
        public int WhenSetStats_ThenCountOfStatsCanBeChanged(
            string[] initialStatIds, string[] setStatIds)
        {
            // arrange
            PopulateRepository(initialStatIds, initialStatIds.Select(_ => (int)default));

            IEnumerable<KeyValuePair<Stat, int>> setStatsAndValues = setStatIds
                .Select(s => new KeyValuePair<Stat, int>(statAssets[s], default));

            // act
            _repository.SetStats(setStatsAndValues);

            // assert
            return _repository.Count();
        }

        [TestCase(new[] { "stat_hp", "stat_damage", "stat_agility" }, new[] { 0, 10, -2 })]
        public void WhenSetStats_AndRepoIsEmpty_ThenGetTheSameStats(string[] statIds, int[] values)
        {
            // arrange
            Stat[] stats = statIds.Select(statId => statAssets[statId]).ToArray();
            IEnumerable<KeyValuePair<Stat, int>> statsAndValues = stats
                .Zip(values, (s, v) => (s, v))
                .Select(x => new KeyValuePair<Stat, int>(x.s, x.v));

            // act
            _repository.SetStats(statsAndValues);

            // assert
            IEnumerable<int> actualValues = stats.Select(_repository.GetStat);
            Assert.That(actualValues, Is.EquivalentTo(values));
        }

        [TestCase(new[] { "stat_hp", "stat_damage", "stat_agility" }, new[] { 10, 10, 3 }, new[] { 0, 10, -2 })]
        public void WhenSetStats_AndRepoHasThem_ThenReturnUpdatedStats(string[] statIds, int[] initialValues, int[] setValues)
        {
            // arrange
            Stat[] stats = statIds.Select(statId => statAssets[statId]).ToArray();
            PopulateRepository(stats, initialValues);

            IEnumerable<KeyValuePair<Stat, int>> statsAndValues = stats
                .Zip(setValues, (s, v) => (s, v))
                .Select(x => new KeyValuePair<Stat, int>(x.s, x.v));

            // act
            _repository.SetStats(statsAndValues);

            // assert
            IEnumerable<int> actualValues = stats.Select(_repository.GetStat);
            Assert.That(actualValues, Is.EquivalentTo(setValues));
        }

        [Test]
        public void WhenSetStats_AndStatsAreNull_ThenThrowsArgumentNullException()
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(
                () => _repository.SetStats(((IEnumerable<KeyValuePair<Stat, int>>)null)!));
        }

        #endregion // void SetStats(IEnumerable<KeyValuePair<Stat, int>> stats)
    }
}