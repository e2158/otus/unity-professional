﻿using System;
using System.Linq;
using System.Collections.Generic;
using Game.Heroes;
using Game.Items;
using Game.Items.Components;
using UnityEditor;
using NSubstitute;
using NUnit.Framework;
using JetBrains.Annotations;

namespace Tests.EditMode.Heroes
{
    [TestOf(typeof(HeroModel))]
    public sealed class HeroModelTests
    {
        private static Dictionary<string, Item> items;

        private IHeroStats _heroStats;
        private IReadOnlyHeroEquipment _equipment;
        [UsedImplicitly] private HeroModel _heroModel;

        [SetUp]
        public void Setup()
        {
            _heroStats = Substitute.For<IHeroStats>();
            _equipment = Substitute.For<IReadOnlyHeroEquipment>();
            _heroModel = new HeroModel(_equipment, _heroStats);

            items ??= AssetDatabase.FindAssets($"t:{nameof(Item)}")
                .Select(AssetDatabase.GUIDToAssetPath)
                .Select(AssetDatabase.LoadAssetAtPath<Item>)
                .ToDictionary(stat => stat.Id, stat => stat);
        }

        [TestCase("equip_body_cool_armor")]
        public void WhenEquipmentIsEquipped_ThenStatsAreAffected(string itemId)
        {
            // arrange
            Item item = items[itemId];
            EquipmentType type = item.Components.OfType<EquipmentComponent>().Single().Type;

            int affectStatCallsCount = 0;
            _heroStats
                .When(x => x.AffectStat(Arg.Any<StatAffectComponent>()))
                .Do(_ => affectStatCallsCount++);

            // act
            _equipment.Equipped += Raise.Event<Action<EquipmentType, Item>>(type, item);

            // assert
            Assert.That(affectStatCallsCount, Is.Positive);
        }

        [TestCase("equip_body_cool_armor")]
        public void WhenEquipmentIsUnequipped_ThenStatsAreUnaffected(string itemId)
        {
            // arrange
            Item item = items[itemId];
            EquipmentType type = item.Components.OfType<EquipmentComponent>().Single().Type;

            int unaffectStatCallsCount = 0;
            _heroStats
                .When(x => x.UnaffectStat(Arg.Any<StatAffectComponent>()))
                .Do(_ => unaffectStatCallsCount++);

            // act
            _equipment.Unequipped += Raise.Event<Action<EquipmentType, Item>>(type, item);

            // assert
            Assert.That(unaffectStatCallsCount, Is.Positive);
        }
    }
}