﻿using System;
using Game.Items;
using Game.Heroes;
using Game.Inventory;
using UnityEditor;
using NUnit.Framework;
using System.Linq;
using System.Collections.Generic;
using Game.Items.Components;

namespace Tests.EditMode.Heroes
{
    [TestOf(typeof(HeroEquipment))]
    public sealed class HeroEquipmentTests
    {
        private static Dictionary<string, Item> items;

        private IInventory _inventory;
        private HeroEquipment _equipment;

        [SetUp]
        public void Setup()
        {
            _inventory = new SingleInventory();
            _equipment = new HeroEquipment(_inventory);

            items ??= AssetDatabase.FindAssets($"t:{nameof(Item)}")
                .Select(AssetDatabase.GUIDToAssetPath)
                .Select(AssetDatabase.LoadAssetAtPath<Item>)
                .ToDictionary(stat => stat.Id, stat => stat);
        }

        private void PreEquipItems(IEnumerable<string> itemIds)
        {
            foreach (string itemId in itemIds)
                PreEquipItem(itemId);
        }

        private void PreEquipItem(string itemId)
        {
            Item item = items[itemId];
            _inventory.AddItem(item);
            _equipment.Equip(item);
        }

        #region Equip

        [Test]
        public void WhenEquip_AndItemIsNull_ThenThrowsArgumentNullException()
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(
                () => _equipment.Equip(null!));
        }

        [TestCase("item_gold")]
        public void WhenEquip_AndItemIsNotEquipment_ThenThrowsArgumentException(string itemId)
        {
            // arrange
            Item item = items[itemId];

            // act & assert
            Assert.Throws<ArgumentException>(
                () => _equipment.Equip(item));
        }

        [TestCase("equip_body_cool_armor" , "equip_body_cool_armor")]
        [TestCase("equip_body_dragon_armor" , "equip_body_cool_armor")]
        public void WhenEquip_AndTypeIsAlreadyEquipped_ThenThrowsArgumentException(string initialItemId, string itemId)
        {
            // arrange
            Item item = items[itemId];
            PreEquipItem(initialItemId);

            // act & assert
            Assert.Throws<ArgumentException>(
                () => _equipment.Equip(item));
        }

        [TestCase("equip_body_cool_armor")]
        public void WhenEquip_AndItemIsNotInInventory_ThenThrowsArgumentException(string itemId)
        {
            // arrange
            Item item = items[itemId];

            // act & assert
            Assert.Throws<ArgumentException>(
                () => _equipment.Equip(item));
        }

        [TestCase("equip_body_cool_armor")]
        public void WhenEquip_ThenEventIsRaised(string itemId)
        {
            // arrange
            Item equippedItem = null;
            Item item = items[itemId];
            _inventory.AddItem(item);
            _equipment.Equipped += (_, i) => equippedItem = i;

            // act
            _equipment.Equip(item);

            // assert
            Assert.That(equippedItem, Is.Not.Null);
            Assert.That(equippedItem, Is.EqualTo(item));
        }

        [TestCase("equip_body_cool_armor")]
        public void WhenEquip_ThenEquipmentListContainsIt(string itemId)
        {
            // arrange
            Item item = items[itemId];
            _inventory.AddItem(item);

            // act
            _equipment.Equip(item);

            // assert
            Assert.That(_equipment.Equipment, Contains.Item(item));
        }

        #endregion // Equip

        #region Unequip

        [Test]
        public void WhenUnequip_AndItemIsNull_ThenThrowsArgumentNullException()
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(
                () => _equipment.Unequip(null!));
        }

        [TestCase("item_gold")]
        public void WhenUnequip_AndItemIsNotEquipment_ThenThrowsArgumentException(string itemId)
        {
            // arrange
            Item item = items[itemId];

            // act & assert
            Assert.Throws<ArgumentException>(
                () => _equipment.Unequip(item));
        }

        [TestCase("equip_body_cool_armor")]
        public void WhenUnequip_AndItemIsNotEquipped_ThenThrowsArgumentException(string itemId)
        {
            // arrange
            Item item = items[itemId];
            _inventory.AddItem(item);

            // act & assert
            Assert.Throws<ArgumentException>(
                () => _equipment.Unequip(item));
        }

        [TestCase("equip_body_cool_armor")]
        public void WhenUnequip_ThenEventIsRaised(string itemId)
        {
            // arrange
            Item item = items[itemId];
            Item unequippedItem = null;
            _equipment.Unequipped += (_, i) => unequippedItem = i;
            PreEquipItem(itemId);

            // act
            _equipment.Unequip(item);

            // assert
            Assert.That(unequippedItem, Is.Not.Null);
            Assert.That(unequippedItem, Is.EqualTo(item));
        }

        [TestCase("equip_body_cool_armor")]
        public void WhenUnequip_ThenEquipmentListDoesntContainIt(string itemId)
        {
            // arrange
            Item item = items[itemId];
            PreEquipItem(itemId);

            // act
            _equipment.Unequip(item);

            // assert
            Assert.That(_equipment.Equipment, Is.Not.Contain(item));
        }

        #endregion // Unequip

        #region Is Equipped

        [TestCase(new string[0], "equip_body_cool_armor", ExpectedResult = false)]
        [TestCase(new [] { "equip_body_cool_armor" }, "equip_body_cool_armor", ExpectedResult = true)]
        [TestCase(new [] { "equip_body_dragon_armor" }, "equip_body_cool_armor", ExpectedResult = false)]
        [TestCase(new [] { "equip_head_power_helmet", "equip_body_cool_armor" }, "equip_body_cool_armor", ExpectedResult = true)]
        public bool WhenCheckIsEquipped_ThenReturnsAsExpected(string[] initialItemIds, string itemId)
        {
            // arrange
            Item item = items[itemId];
            PreEquipItems(initialItemIds);

            // act & assert
            return _equipment.IsEquipped(item);
        }

        [TestCase("equip_body_cool_armor")]
        public void WhenRemoveFromInventory_AndItemIsEquipped_ThenUnequipsThisItem(string itemId)
        {
            // arrange
            Item item = items[itemId];
            PreEquipItem(itemId);

            // act
            _inventory.RemoveItem(item);
            bool isEquipped = _equipment.IsEquipped(item);

            // assert
            Assert.That(isEquipped, Is.False);
        }

        #endregion // Is Equipped

        #region Get

        [TestCase("equip_body_cool_armor")]
        public void WhenGetEquipment_AndHeroHasThisEquipment_ThenReturnsIt(string itemId)
        {
            // arrange
            Item item = items[itemId];
            EquipmentType equipmentType = item.Components.OfType<EquipmentComponent>().Single().Type;
            PreEquipItem(itemId);

            // act
            Item result = _equipment.GetEquipment(equipmentType);

            // assert
            Assert.That(result, Is.EqualTo(item));
        }

        [TestCase("equip_body_cool_armor")]
        public void WhenGetEquipment_AndHeroDoesntHaveThisEquipment_ThenReturnsNull(string itemId)
        {
            // arrange
            Item item = items[itemId];
            EquipmentType equipmentType = item.Components.OfType<EquipmentComponent>().Single().Type;

            // act
            Item result = _equipment.GetEquipment(equipmentType);

            // assert
            Assert.That(result, Is.Null);
        }

        #endregion // Get
    }
}