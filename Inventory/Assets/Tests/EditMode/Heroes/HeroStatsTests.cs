﻿using System;
using System.Linq;
using System.Collections.Generic;
using Game.Stats;
using Game.Heroes;
using Game.Items.Components;
using UnityEditor;
using NUnit.Framework;

namespace Tests.EditMode.Heroes
{
    [TestOf(typeof(HeroStats))]
    public sealed class HeroStatsTests
    {
        private static Dictionary<string, Stat> stats;

        private HeroStats _heroStats;

        [SetUp]
        public void Setup()
        {
            stats ??= AssetDatabase.FindAssets($"t:{nameof(Stat)}")
                .Select(AssetDatabase.GUIDToAssetPath)
                .Select(AssetDatabase.LoadAssetAtPath<Stat>)
                .ToDictionary(stat => stat.Id, stat => stat);
        }

        private static IReadOnlyStatRepository CreateBaseStats(IEnumerable<string> statNames, IEnumerable<int> statValues)
        {
            StatRepository repository = new();

            IEnumerable<(string stat, int value)> tuples =
                statNames.Zip(statValues, (s, v) => (s, v));

            foreach ((string stat, int value) tuple in tuples)
                repository.SetStat(tuple.stat, tuple.value);

            return repository;
        }

        #region Init

        [TestCase(new string[0], new int[0])]
        [TestCase(new[] { "hp" }, new[] { 0 })]
        [TestCase(new[] { "hp", "attack" }, new[] { 0, 10 })]
        [TestCase(new[] { "hp", "attack", "speed" }, new[] { 0, 10, -2 })]
        public void WhenInitStats_ThenEnumerateThatStats(string[] initialStatNames, int[] initialStatValues)
        {
            // arrange
            HeroStats heroStats = new(CreateBaseStats(initialStatNames, initialStatValues));
            IEnumerable<KeyValuePair<string,int>> expectedEnumerable = initialStatNames
                .Zip(initialStatValues, (s, v) => new KeyValuePair<string, int>(s, v));

            // act
            IEnumerable<KeyValuePair<string,int>> actualEnumerable = heroStats;

            // assert
            Assert.That(actualEnumerable, Is.EquivalentTo(expectedEnumerable));
        }

        #endregion // Init

        #region GetStat

        [TestCase(new[] { "hp" }, new[] { 0 }, "hp")]
        [TestCase(new[] { "hp", "attack", "speed" }, new[] { 0, 10, -2 }, "attack")]
        public void WhenGetStat_AndHeroHasThisStat_ThenReturnsIt(string[] initialStatNames, int[] initialStatValues, string getStatName)
        {
            // arrange
            HeroStats heroStats = new(CreateBaseStats(initialStatNames, initialStatValues));
            int index = Array.IndexOf(initialStatNames, getStatName);
            int expectedValue = initialStatValues[index];

            // act
            int actualValue = heroStats.GetStat(getStatName);

            // assert
            Assert.That(actualValue, Is.EqualTo(expectedValue));
        }

        [TestCase(new string[0], "hp")]
        [TestCase(new[] { "hp" }, "speed")]
        [TestCase(new[] { "hp", "attack", "speed" }, "agility")]
        public void WhenGetStat_AndHeroDoesntHaveThisStat_ThenDefaultValue(string[] initialStatNames, string getStatName)
        {
            // arrange
            IEnumerable<int> initialStatValues = Enumerable.Repeat(default(int), initialStatNames.Length);
            HeroStats heroStats = new(CreateBaseStats(initialStatNames, initialStatValues));

            // act
            int actualValue = heroStats.GetStat(getStatName);

            // assert
            Assert.That(actualValue, Is.EqualTo(default(int)));
        }

        #endregion // GetStat

        #region AffectStat

        [TestCase(new[] { "stat_damage" }, new[] { 10 }, AffectType.AddValue, "stat_hp", 5, ExpectedResult = 5)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddValue, "stat_hp", 5, ExpectedResult = 5)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddValue, "stat_hp", -4, ExpectedResult = -4)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddValue, "stat_damage", 2, ExpectedResult = 12)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddValue, "stat_damage", -7, ExpectedResult = 3)]
        [TestCase(new[] { "stat_damage" }, new[] { 10 }, AffectType.AddPercent, "stat_hp", 25, ExpectedResult = 0)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddPercent, "stat_hp", 25, ExpectedResult = 0)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddPercent, "stat_hp", -10, ExpectedResult = 0)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddPercent, "stat_damage", 50, ExpectedResult = 15)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddPercent, "stat_damage", -30, ExpectedResult = 7)]
        public int WhenAffectStat_ThenGetAsExpected(
            string[] initialStatIds, int[] initialStatValues,
            AffectType affectType, string affectStatId, int affectValue)
        {
            // arrange
            HeroStats heroStats = new(CreateBaseStats(initialStatIds, initialStatValues));
            StatAffectComponent statAffect = new(stats[affectStatId], affectType, affectValue);

            // act
            heroStats.AffectStat(statAffect);

            // assert
            return heroStats.GetStat(affectStatId);
        }

        [Test]
        public void WhenAffectStat_AndAffectIsNull_ThenThrowsArgumentNullException()
        {
            // arrange
            HeroStats heroStats = new(
                CreateBaseStats(Enumerable.Empty<string>(), Enumerable.Empty<int>()));

            // act && assert
            Assert.Throws<ArgumentNullException>(
                () => heroStats.AffectStat(null!));
        }

        #endregion // AffectStat

        #region UnaffectStat

        [TestCase(new[] { "stat_damage" }, new[] { 10 }, AffectType.AddValue, "stat_hp", 5, ExpectedResult = -5)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddValue, "stat_hp", 5, ExpectedResult = -5)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddValue, "stat_hp", -4, ExpectedResult = 4)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddValue, "stat_damage", 2, ExpectedResult = 8)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddValue, "stat_damage", -7, ExpectedResult = 17)]
        [TestCase(new[] { "stat_damage" }, new[] { 10 }, AffectType.AddPercent, "stat_hp", 25, ExpectedResult = 0)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddPercent, "stat_hp", 25, ExpectedResult = 0)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddPercent, "stat_hp", -10, ExpectedResult = 0)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddPercent, "stat_damage", 50, ExpectedResult = 5)]
        [TestCase(new[] { "stat_hp", "stat_damage" }, new[] { 0, 10 }, AffectType.AddPercent, "stat_damage", -30, ExpectedResult = 13)]
        public int WhenUnaffectStat_ThenGetAsExpected(
            string[] initialStatIds, int[] initialStatValues,
            AffectType affectType, string affectStatId, int affectValue)
        {
            // arrange
            HeroStats heroStats = new(CreateBaseStats(initialStatIds, initialStatValues));
            StatAffectComponent statAffect = new(stats[affectStatId], affectType, affectValue);

            // act
            heroStats.UnaffectStat(statAffect);

            // assert
            return heroStats.GetStat(affectStatId);
        }

        [Test]
        public void WhenUnaffectStat_AndAffectIsNull_ThenThrowsArgumentNullException()
        {
            // arrange
            HeroStats heroStats = new(
                CreateBaseStats(Enumerable.Empty<string>(), Enumerable.Empty<int>()));

            // act && assert
            Assert.Throws<ArgumentNullException>(
                () => heroStats.UnaffectStat(null!));
        }

        #endregion // UnaffectStat
    }
}