﻿using System;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Essentials.Runtime
{
    [Serializable]
    public sealed class MetaData
    {
        [SerializeField, Required] private string _name;
        [SerializeField] private string _description;
        [SerializeField, AssetSelector] private Sprite _icon;

        [NotNull] public string Name => _name;
        [CanBeNull] public Sprite Icon => _icon;
        [CanBeNull] public string Description => _description;
    }
}