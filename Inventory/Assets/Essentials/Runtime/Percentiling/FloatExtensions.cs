﻿namespace Essentials.Runtime.Percentiling
{
    public static class FloatExtensions
    {
        public static float ToPercents(this float proportion) => Percentage.Convert(proportion);
        public static float ToProportion(this float percents) => Proportion.Convert(percents);
    }
}