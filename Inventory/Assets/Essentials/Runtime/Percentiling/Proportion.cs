﻿using UnityEngine;

namespace Essentials.Runtime.Percentiling
{
    public static class Proportion
    {
        public const float Min = 0;
        public const float Max = 1f;
        public const string Suffix = "[0..1]";

        public static float Convert(float percents) =>
            percents / Percentage.Max;

        public static float Calc(float currentValue, float maxValue) =>
            Mathf.Approximately(maxValue, 0) ? 0 : currentValue / maxValue;
    }
}