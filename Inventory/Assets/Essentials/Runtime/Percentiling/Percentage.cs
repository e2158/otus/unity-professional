﻿using UnityEngine;

namespace Essentials.Runtime.Percentiling
{
    public static class Percentage
    {
        public const float Min = 0f;
        public const float Max = 100f;
        public const string Suffix = "%";

        public static float Convert(float proportion) =>
            proportion * Max;

        public static float Calc(float currentValue, float maxValue) =>
            Mathf.Approximately(maxValue, 0) ? 0 : currentValue / maxValue * Max;
    }
}