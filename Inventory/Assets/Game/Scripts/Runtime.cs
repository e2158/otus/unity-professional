﻿using Game.Items;
using Game.Stats;
using Game.Heroes;
using Game.Inventory;
using UnityEngine;
using Sirenix.Utilities;
using Sirenix.OdinInspector;

namespace Game
{
    [DisallowMultipleComponent]
    public sealed class Runtime : MonoBehaviour
    {
        [Title("Configuration")]
        [SerializeField, Required, AssetSelector] private Hero _hero;
        [SerializeField, Required] private Item[] _initialInventory;

        [Title("Dependencies")]
        [PropertySpace, ShowInInspector] private CompositeInventoryDecorator _inventory;
        [PropertySpace, ShowInInspector] private HeroEquipment _heroEquipment;
        [PropertySpace, ShowInInspector] private HeroStats _heroStats;

        private HeroModel _heroModel;

        private void Start()
        {
            StackInventory stackInventory = new();
            SingleInventory singleInventory = new();
            _inventory = new CompositeInventoryDecorator(singleInventory, stackInventory);
            _initialInventory.ForEach(_inventory.AddItem);

            _heroEquipment = new HeroEquipment(_inventory);

            StatRepository baseStats = new();
            baseStats.SetStats(_hero.BaseStats);
            _heroStats = new HeroStats(baseStats);

            _heroModel = new HeroModel(_heroEquipment, _heroStats);
        }

        private void OnDestroy()
        {
            _heroEquipment?.Dispose();
            _heroModel?.Dispose();
        }
    }
}