﻿using System;
using System.Collections.Generic;
using Game.Items;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace Game.Inventory
{
    public sealed class StackInventory : IInventory
    {
        public event Action<Item> ItemAdded;
        public event Action<Item> ItemRemoved;

        [ShowInInspector, ReadOnly, NotNull] private readonly Dictionary<Item, int> _stackItems = new();

        public IEnumerable<Item> Items => _stackItems.Keys;

        public void AddItem(Item item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            int count = _stackItems.GetValueOrDefault(item, default);
            _stackItems[item] = count + 1;

            ItemAdded?.Invoke(item);
        }

        public void RemoveItem(Item item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            if (!_stackItems.TryGetValue(item, out int count))
                throw new ArgumentException($"Item {item.Id} not found", nameof(item));

            int newCount = count - 1;

            if (newCount == 0) _stackItems.Remove(item);
            else _stackItems[item] = newCount;

            ItemRemoved?.Invoke(item);
        }

        public bool HasItem(Item item) => item != null
            ? _stackItems.ContainsKey(item)
            : throw new ArgumentNullException(nameof(item));

        public int GetCount(Item item) =>
            item != null
                ? _stackItems.GetValueOrDefault(item, default)
                : throw new ArgumentNullException(nameof(item));
    }
}