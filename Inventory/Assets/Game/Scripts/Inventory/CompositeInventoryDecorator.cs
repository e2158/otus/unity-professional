﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Items;
using Game.Items.Components;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace Game.Inventory
{
    public sealed class CompositeInventoryDecorator : IInventory, IDisposable
    {
        public event Action<Item> ItemAdded;
        public event Action<Item> ItemRemoved;

        [ShowInInspector, ReadOnly, NotNull] private readonly SingleInventory _singleInventory;
        [ShowInInspector, ReadOnly, NotNull] private readonly StackInventory _stackInventory;

        public IEnumerable<Item> Items => _singleInventory.Items.Concat(_stackInventory.Items);

        public CompositeInventoryDecorator([NotNull] SingleInventory singleInventory, [NotNull] StackInventory stackInventory)
        {
            _singleInventory = singleInventory ?? throw new ArgumentNullException(nameof(singleInventory));
            _stackInventory = stackInventory ?? throw new ArgumentNullException(nameof(stackInventory));

            _singleInventory.ItemAdded += OnItemAdded;
            _singleInventory.ItemRemoved += OnItemRemoved;
            _stackInventory.ItemAdded += OnItemAdded;
            _stackInventory.ItemRemoved += OnItemRemoved;
        }

        public void Dispose()
        {
            _singleInventory.ItemAdded -= OnItemAdded;
            _singleInventory.ItemRemoved -= OnItemRemoved;
            _stackInventory.ItemAdded -= OnItemAdded;
            _stackInventory.ItemRemoved -= OnItemRemoved;
        }

        private void OnItemAdded(Item item) => ItemAdded?.Invoke(item);
        private void OnItemRemoved(Item item) => ItemRemoved?.Invoke(item);

        [Button(Expanded = true, Style = ButtonStyle.CompactBox)]
        public void AddItem(Item item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            if (item.Components.OfType<StackComponent>().Any())
                _stackInventory.AddItem(item);
            else
                _singleInventory.AddItem(item);
        }

        [Button(Expanded = true, Style = ButtonStyle.CompactBox)]
        public void RemoveItem(Item item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            if (_stackInventory.HasItem(item))
                _stackInventory.RemoveItem(item);

            else if (_singleInventory.HasItem(item))
                _singleInventory.RemoveItem(item);

            else
                throw new ArgumentException($"Item {item.Id} not found", nameof(item));
        }

        [Button(Expanded = true, Style = ButtonStyle.CompactBox)]
        public bool HasItem(Item item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            return item.Components.OfType<StackComponent>().Any()
                ? _stackInventory.HasItem(item)
                : _singleInventory.HasItem(item);
        }

        [Button(Expanded = true, Style = ButtonStyle.CompactBox)]
        public int GetCount(Item item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            return item.Components.OfType<StackComponent>().Any()
                ? _stackInventory.GetCount(item)
                : _singleInventory.GetCount(item);
        }
    }
}