﻿using System;
using System.Linq;
using System.Collections.Generic;
using Game.Items;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace Game.Inventory
{
    public sealed class SingleInventory : IInventory
    {
        public event Action<Item> ItemAdded;
        public event Action<Item> ItemRemoved;

        [ShowInInspector, ReadOnly, NotNull, ItemNotNull] private readonly List<Item> _items = new();

        public IEnumerable<Item> Items => _items;

        public void AddItem(Item item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            _items.Add(item);
            ItemAdded?.Invoke(item);
        }

        public void RemoveItem(Item item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            if (!_items.Remove(item))
                throw new ArgumentException($"Item {item.Id} not found", nameof(item));

            ItemRemoved?.Invoke(item);
        }

        public bool HasItem(Item item) =>
            item != null
                ? _items.Contains(item)
                : throw new ArgumentNullException(nameof(item));

        public int GetCount(Item item) =>
            item != null
                ? _items.Count(i => i == item)
                : throw new ArgumentNullException(nameof(item));
    }
}