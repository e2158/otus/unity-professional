﻿using System;
using System.Collections.Generic;
using Game.Items;
using JetBrains.Annotations;

namespace Game.Inventory
{
    public interface IReadOnlyInventory
    {
        event Action<Item> ItemAdded;
        event Action<Item> ItemRemoved;

        [NotNull, ItemNotNull] IEnumerable<Item> Items { get; }

        bool HasItem([NotNull] Item item);
        int GetCount([NotNull] Item item);
    }

    public interface IInventory : IReadOnlyInventory
    {
        void AddItem([NotNull] Item item);
        void RemoveItem([NotNull] Item item);
    }
}