﻿using System;
using System.Linq;
using System.Collections.Generic;
using Essentials.Runtime;
using Game.Items.Components;
using UnityEngine;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace Game.Items
{
    [CreateAssetMenu(fileName = nameof(Item), menuName = "Configs/" + nameof(Item))]
    public sealed class Item : IdentifiedScriptableObject
    {
        [BoxGroup("Meta Data"), HideLabel]
        [SerializeField] private MetaData _meta;

        [Required, ListDrawerSettings(Expanded = true)]
        [SerializeField] private ItemComponent[] _components;

        [NotNull] public MetaData Meta => _meta;
        [NotNull, ItemNotNull] public IReadOnlyList<ItemComponent> Components => _components;

        #region INSPECTOR

        private readonly List<ItemComponent> _componentsToRemove = new();

        private void OnValidate()
        {
            IEnumerable<IGrouping<Type,ItemComponent>> singleComponentsByTypes =
                _components
                    .Where(c => c.IsSingleComponent())
                    .GroupBy(x => x.GetType());

            foreach (IGrouping<Type,ItemComponent> group in singleComponentsByTypes)
                if (group.Count() > 1)
                {
                    IEnumerable<ItemComponent> extraComponents = group.TakeLast(group.Count() - 1);
                    _componentsToRemove.AddRange(extraComponents);
                }

            if (_componentsToRemove.Count > 0)
            {
                _components = _components.Except(_componentsToRemove).ToArray();
                _componentsToRemove.Clear();
            }
        }

        #endregion // INSPECTOR
    }
}