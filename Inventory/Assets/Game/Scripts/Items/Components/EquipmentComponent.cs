﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Game.Items.Components
{
    public enum EquipmentType
    {
        Legs = 0,
        Body = 1,
        Head = 2,
        LeftHand = 3,
        RightHand = 4
    }

    [Serializable, SingleComponent]
    public sealed class EquipmentComponent : ItemComponent
    {
        [SerializeField, HideLabel] private EquipmentType _type;

        public EquipmentType Type => _type;
    }
}