﻿using System;
using Game.Stats;
using UnityEngine;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Essentials.Runtime.Percentiling;

namespace Game.Items.Components
{
    public enum AffectType
    {
        None = 0,
        AddValue = 1,
        AddPercent = 2,
    }

    [Serializable, InlineEditor]
    public sealed class StatAffectComponent : ItemComponent
    {
        [HorizontalGroup, Required, AssetSelector, HideLabel]
        [SerializeField] private Stat _stat;

        [HorizontalGroup, HideLabel]
        [SerializeField] private AffectType _affectType;

        [GUIColor(nameof(ValueColor))]
        [HorizontalGroup, SuffixLabel("@" + nameof(ValueSuffix), true)]
        [MinValue(nameof(ValueMin)), MaxValue(nameof(ValueMax))]
        [HideLabel, HideIf(nameof(_affectType), AffectType.None)]
        [SerializeField] private float _value;

        public AffectType AffectType => _affectType;
        public Stat Stat => _stat;
        public float Value => _value;

        public StatAffectComponent([NotNull] Stat stat, AffectType affectType, float value)
        {
            _stat = stat ?? throw new ArgumentNullException(nameof(stat));
            _value = value;
            _affectType = affectType;
        }

        #region INSPECTOR

        private Color ValueColor => _value switch
        {
            > 0 => new Color(0.5f, 1f, 0.5f),
            < 0 => new Color(1f, 0.6f, 0.6f),
            _ => new Color(1f, 1f, 0.64f)
        };

        private string ValueSuffix => _affectType switch
        {
            AffectType.None => string.Empty,
            AffectType.AddValue => string.Empty,
            AffectType.AddPercent => Percentage.Suffix,
            _ => throw new ArgumentOutOfRangeException()
        };

        private int ValueMin => _affectType switch
        {
            AffectType.None => default,
            AffectType.AddValue => int.MinValue,
            AffectType.AddPercent => -(int)Percentage.Max,
            _ => throw new ArgumentOutOfRangeException()
        };

        private int ValueMax => _affectType switch
        {
            AffectType.None => default,
            AffectType.AddValue => int.MaxValue,
            AffectType.AddPercent => (int)Percentage.Max,
            _ => throw new ArgumentOutOfRangeException()
        };

        #endregion // INSPECTOR
    }
}