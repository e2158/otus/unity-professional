﻿using System;

namespace Game.Items.Components
{
    internal sealed class SingleComponentAttribute : Attribute
    {
    }

    internal static class ItemComponentExtensions
    {
        internal static bool IsSingleComponent(this ItemComponent component) =>
            component.GetType().GetCustomAttributes(typeof(SingleComponentAttribute), false).Length > 0;
    }
}