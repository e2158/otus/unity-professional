﻿using System;
using System.Linq;
using System.Collections.Generic;
using Game.Items;
using Game.Items.Components;
using Game.Inventory;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace Game.Heroes
{
    public interface IReadOnlyHeroEquipment
    {
        event Action<EquipmentType, Item> Equipped;
        event Action<EquipmentType, Item> Unequipped;

        [NotNull, ItemNotNull] IEnumerable<Item> Equipment { get; }

        bool IsEquipped(EquipmentType type);
        [CanBeNull] Item GetEquipment(EquipmentType type);
    }

    public sealed class HeroEquipment : IDisposable, IReadOnlyHeroEquipment
    {
        public event Action<EquipmentType, Item> Equipped;
        public event Action<EquipmentType, Item> Unequipped;

        [NotNull, ShowInInspector, ReadOnly] private readonly Dictionary<EquipmentType, Item> _equipment = new();
        [NotNull] private readonly IReadOnlyInventory _inventory;

        public IEnumerable<Item> Equipment => _equipment.Values;

        public HeroEquipment([NotNull] IReadOnlyInventory inventory)
        {
            _inventory = inventory ?? throw new ArgumentNullException(nameof(inventory));
            _inventory.ItemRemoved += OnItemRemoved;
        }

        public void Dispose()
        {
            _inventory.ItemRemoved -= OnItemRemoved;
        }

        private void OnItemRemoved(Item item)
        {
            if (IsEquipped(item) && !_inventory.HasItem(item))
                Unequip(item);
        }

        [Button(Expanded = true, Style = ButtonStyle.CompactBox)]
        public bool IsEquipped(EquipmentType type) =>
            _equipment.ContainsKey(type);

        [Button(Expanded = true, Style = ButtonStyle.CompactBox)]
        public bool IsEquipped(Item item) =>
            _equipment.Values.Contains(item);

        [Button(Expanded = true, Style = ButtonStyle.CompactBox)]
        public Item GetEquipment(EquipmentType type) =>
            _equipment.GetValueOrDefault(type);

        [Button(Expanded = true, Style = ButtonStyle.CompactBox)]
        public void Equip([NotNull] Item item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            EquipmentComponent equipmentComponent = item.Components
                .OfType<EquipmentComponent>().SingleOrDefault();

            if (equipmentComponent == null)
                throw new ArgumentException($"Item must have an {nameof(EquipmentComponent)}", nameof(item));

            if (IsEquipped(equipmentComponent.Type))
                throw new ArgumentException($"Item of type \"{equipmentComponent.Type}\" is already equipped", nameof(item));

            if (!_inventory.HasItem(item))
                throw new ArgumentException($"Item \"{item.Id}\" is not in inventory", nameof(item));

            _equipment[equipmentComponent.Type] = item;
            Equipped?.Invoke(equipmentComponent.Type, item);
        }

        [Button(Expanded = true, Style = ButtonStyle.CompactBox)]
        public void Unequip([NotNull] Item item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            EquipmentComponent equipmentComponent = item.Components
                .OfType<EquipmentComponent>().SingleOrDefault();

            if (equipmentComponent == null)
                throw new ArgumentException($"Item must have an {nameof(EquipmentComponent)}", nameof(item));

            if (!_equipment.TryGetValue(equipmentComponent.Type, out Item equippedItem))
                throw new ArgumentException($"Item of type \"{equipmentComponent.Type}\" is not equipped", nameof(item));

            if (equippedItem != item)
                throw new ArgumentException($"Item \"{item.Id}\" is not equipped", nameof(item));

            _equipment.Remove(equipmentComponent.Type);
            Unequipped?.Invoke(equipmentComponent.Type, equippedItem);
        }
    }
}