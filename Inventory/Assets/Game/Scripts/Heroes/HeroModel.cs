﻿using System;
using System.Linq;
using Game.Items;
using Game.Items.Components;
using Game.Stats;
using JetBrains.Annotations;
using Sirenix.Utilities;

namespace Game.Heroes
{
    [Serializable]
    public sealed class HeroModel : IDisposable
    {
        [NotNull] private readonly IHeroStats _stats;
        [NotNull] private readonly IReadOnlyHeroEquipment _equipment;

        public IReadOnlyStatRepository Stats => _stats;

        public HeroModel([NotNull] IReadOnlyHeroEquipment equipment, [NotNull] IHeroStats stats)
        {
            _stats = stats ?? throw new ArgumentNullException(nameof(stats));
            _equipment = equipment ?? throw new ArgumentNullException(nameof(equipment));

            _equipment.Equipped += OnEquipped;
            _equipment.Unequipped += OnUnequipped;
        }

        public void Dispose()
        {
            _equipment.Equipped -= OnEquipped;
            _equipment.Unequipped -= OnUnequipped;
        }

        private void OnEquipped(EquipmentType type, Item item)
        {
            item.Components.OfType<StatAffectComponent>().ForEach(_stats.AffectStat);
        }

        private void OnUnequipped(EquipmentType type, Item item)
        {
            item.Components.OfType<StatAffectComponent>().ForEach(_stats.UnaffectStat);
        }
    }
}