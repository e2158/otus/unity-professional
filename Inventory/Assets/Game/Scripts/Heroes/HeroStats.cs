﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.Stats;
using Game.Items.Components;
using Essentials.Runtime.Percentiling;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Game.Heroes
{
    public interface IHeroStats : IReadOnlyStatRepository
    {
        void AffectStat([NotNull] StatAffectComponent statComponent);
        void UnaffectStat([NotNull] StatAffectComponent statComponent);
    }

    public sealed class HeroStats : IHeroStats
    {
        [ShowInInspector, ReadOnly, NotNull] private readonly IReadOnlyStatRepository _baseStats;
        [ShowInInspector, ReadOnly, NotNull] private readonly StatRepository _actualStats = new();

        public HeroStats([NotNull] IReadOnlyStatRepository baseStats)
        {
            _baseStats = baseStats ?? throw new ArgumentNullException(nameof(baseStats));
            _actualStats.SetStats(_baseStats);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public IEnumerator<KeyValuePair<string, int>> GetEnumerator() => _actualStats.GetEnumerator();

        public int GetStat(Stat stat) => _actualStats.GetStat(stat);
        public int GetStat(string statId) => _actualStats.GetStat(statId);

        public void AffectStat([NotNull] StatAffectComponent statComponent)
        {
            if (statComponent == null)
                throw new ArgumentNullException(nameof(statComponent));

            int statValue = _actualStats.GetStat(statComponent.Stat);
            int addStatValue = CalcStatValueToAdd(statComponent);
            _actualStats.SetStat(statComponent.Stat, statValue + addStatValue);
        }

        public void UnaffectStat([NotNull] StatAffectComponent statComponent)
        {
            if (statComponent == null)
                throw new ArgumentNullException(nameof(statComponent));

            int statValue = _actualStats.GetStat(statComponent.Stat);
            int addStatValue = CalcStatValueToAdd(statComponent);
            _actualStats.SetStat(statComponent.Stat, statValue - addStatValue);
        }

        private int CalcStatValueToAdd(StatAffectComponent statComponent) =>
            statComponent.AffectType switch
            {
                AffectType.None => default,
                AffectType.AddValue => (int)statComponent.Value,
                AffectType.AddPercent => (int)(_baseStats.GetStat(statComponent.Stat) * statComponent.Value.ToProportion()),
                _ => throw new ArgumentOutOfRangeException()
            };
    }
}