﻿using Game.Stats;
using Essentials.Runtime;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace Game.Heroes
{
    [CreateAssetMenu(fileName = nameof(Hero), menuName = "Configs/" + nameof(Hero))]
    public sealed class Hero : IdentifiedScriptableObject
    {
        [BoxGroup("Meta Data"), HideLabel]
        [SerializeField] private MetaData _meta;

        [SerializeField, Required] private Dictionary<Stat, int> _baseStats;

        public MetaData Meta => _meta;
        public IReadOnlyDictionary<Stat, int> BaseStats => _baseStats;
    }
}