﻿using UnityEngine;
using Essentials.Runtime;
using Sirenix.OdinInspector;

namespace Game.Stats
{
    [CreateAssetMenu(fileName = nameof(Stat), menuName = "Configs/" + nameof(Stat))]
    public sealed class Stat : IdentifiedScriptableObject
    {
        [BoxGroup("Meta Data"), HideLabel]
        [SerializeField] private MetaData _meta;

        public MetaData Meta => _meta;
    }
}