﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace Game.Stats
{
    public interface IReadOnlyStatRepository : IEnumerable<KeyValuePair<string, int>>
    {
        int GetStat([NotNull] Stat stat);
        int GetStat([NotNull] string statId);
    }

    [Serializable]
    public sealed class StatRepository : IReadOnlyStatRepository
    {
        private const int DefaultStatValue = 0;

        [ShowInInspector, ReadOnly] private readonly Dictionary<string, int> _stats = new();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public IEnumerator<KeyValuePair<string, int>> GetEnumerator() => _stats.GetEnumerator();

        public void SetStats([NotNull] IEnumerable<KeyValuePair<Stat, int>> stats)
        {
            if (stats == null) throw new ArgumentNullException(nameof(stats));
            foreach (KeyValuePair<Stat, int> stat in stats) SetStat(stat.Key, stat.Value);
        }

        public void SetStats([NotNull] IEnumerable<KeyValuePair<string, int>> stats)
        {
            if (stats == null) throw new ArgumentNullException(nameof(stats));
            foreach (KeyValuePair<string, int> stat in stats) SetStat(stat.Key, stat.Value);
        }

        public void SetStat([NotNull] Stat stat, int value)
        {
            if (stat == null) throw new ArgumentNullException(nameof(stat));
            SetStat(stat.Id, value);
        }

        public void SetStat([NotNull] string statId, int value)
        {
            if (string.IsNullOrEmpty(statId)) throw new ArgumentNullException(nameof(statId));
            _stats[statId] = value;
        }

        public int GetStat(Stat stat)
        {
            if (stat == null) throw new ArgumentNullException(nameof(stat));
            return GetStat(stat.Id);
        }

        public int GetStat(string statId)
        {
            if (string.IsNullOrEmpty(statId)) throw new ArgumentNullException(nameof(statId));
            return _stats.GetValueOrDefault(statId, DefaultStatValue);
        }
    }
}