**Страница курса**: https://otus.ru/lessons/unity-professional/

**Период обучения**: 31 октября 2023 года — 24 мая 2024 года

## Задания:
1. [Шаблоны GRASP](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/ShootEmUp/Assets)
2. [Игровой цикл](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/ShootEmUp/Assets)
3. [Dependecy Injection](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/ShootEmUp/Assets)
4. [Интерфейс игры (Presentation Model)](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/ModelViewPresenter/Assets)
5. [Сохранение данных (Repository)](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/SaveSystem/Assets)
6. [Взаимодействие персонажа с миром](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/ZombieShooter/Assets)
7. [Entity Component System](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/EcsRts/Assets)
8. [Пошаговые механики](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/EventBusTBBattler/Assets)
9. [Менеджер апгрейдов](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/UpgradeSystem/Assets)
10. [Инвентарь (Компонентный подход)](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/Inventory/Assets)
11. [Крафтинг (Test Driven Development)](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/Inventory/Assets)
12. [Механика реального времени](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/LiveOps/Assets)
13. [Behaviour Tree](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/BehaviourTree/Assets)
14. [Adressables](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/Addressables/Assets)
15. [Туториал](https://gitlab.com/e2158/otus/unity-professional/-/tree/main/Tutorial/Assets)
