﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Lessons.Architecture.PM.Game.UI
{
    public sealed class UiProgressBar : MonoBehaviour
    {
        [Title("Components")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private Slider _slider;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private Image _fillImage;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private TMP_Text _valueName;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private TMP_Text _currentValue;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private TMP_Text _targetValue;

        [Title("Assets")]
        [Required, AssetSelector]
        [SerializeField] private Sprite _fillImageWhenSliderIsFull;

        [Required, AssetSelector]
        [SerializeField] private Sprite _fillImageWhenSliderIsInProgress;

        private void OnValidate()
        {
            _slider ??= GetComponentInChildren<Slider>();
            _fillImage ??= GetComponentInChildren<Image>();
        }

        [Title("Controls")]
        [Button]
        public void SetData(uint currentValue, uint targetValue) =>
            SetData(_valueName.text, currentValue, targetValue);


        [Button]
        public void SetData([CanBeNull] string valueName, uint currentValue, uint targetValue)
        {
            _valueName.text = valueName;
            _currentValue.text = currentValue.ToString();
            _targetValue.text = targetValue.ToString();

            _slider.value = targetValue > 0 ? (float)currentValue / targetValue : 0;
            _fillImage.sprite = SelectActualFillImage(_slider);
        }

        private Sprite SelectActualFillImage([NotNull] Slider slider)
        {
            if (_slider == null)
                throw new ArgumentNullException(nameof(slider));

            return IsSliderFull(slider)
                ? _fillImageWhenSliderIsFull
                : _fillImageWhenSliderIsInProgress;
        }

        private bool IsSliderFull([NotNull] Slider slider)
        {
            if (_slider == null)
                throw new ArgumentNullException(nameof(slider));

            const uint fullSliderValue = 1;
            return Mathf.Approximately(slider.value, fullSliderValue);
        }
    }
}