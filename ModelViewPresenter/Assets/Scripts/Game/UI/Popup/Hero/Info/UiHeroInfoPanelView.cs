﻿using TMPro;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Lessons.Architecture.PM.Framework.UI.MVP;

namespace Lessons.Architecture.PM.Game.UI.Popup
{
    [DisallowMultipleComponent]
    internal sealed class UiHeroInfoPanelView : View
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private Image _avatar;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private TMP_Text _name;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private TMP_Text _description;

        private void OnValidate()
        {
            _avatar ??= GetComponentInChildren<Image>();
            _name ??= GetComponentsInChildren<TMP_Text>().FirstOrDefault();
            _description ??= GetComponentsInChildren<TMP_Text>().LastOrDefault();
        }

        public void SetName(string name)
        {
            _name.text = name;
        }

        public void SetAvatar([CanBeNull] Sprite avatar)
        {
            _avatar.sprite = avatar;
        }

        public void SetDescription([CanBeNull] string description)
        {
            _description.text = description;
        }
    }
}