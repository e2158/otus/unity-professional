﻿using System;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Lessons.Architecture.PM.Game.Hero;
using Lessons.Architecture.PM.Framework.UI.MVP;

namespace Lessons.Architecture.PM.Game.UI.Popup
{
    internal sealed class UiHeroInfoPanelPresenter : IPresenter
    {
        [NotNull] private readonly HeroConfig _model;
        [NotNull] private readonly UiHeroInfoPanelView _view;

        public UiHeroInfoPanelPresenter([NotNull] HeroConfig model, [NotNull] UiHeroInfoPanelView view)
        {
            _view = view != null ? view : throw new ArgumentNullException(nameof(view));
            _model = model != null ? model : throw new ArgumentNullException(nameof(model));
        }

        public UniTask InitViewAsync()
        {
            _view.SetName(_model.Name);
            _view.SetAvatar(_model.Icon);
            _view.SetDescription(_model.Description);

            return UniTask.CompletedTask;
        }

        public void SubscribeView() { }
        public void SubscribeModel() { }

        public void UnsubscribeView() { }
        public void UnsubscribeModel() { }
    }
}