﻿using JetBrains.Annotations;
using Lessons.Architecture.PM.Game.Hero;
using Lessons.Architecture.PM.Framework.UI.MVP;

namespace Lessons.Architecture.PM.Game.UI.Popup
{
    internal sealed class UiHeroPopupPresenter : CompositePresenter
    {
        public UiHeroPopupPresenter([NotNull] UiHeroPopupView view, [NotNull] HeroModel model)
            : base(CreateSubPresenters(view, model)) {}

        private static IPresenter[] CreateSubPresenters([NotNull] UiHeroPopupView view, [NotNull] HeroModel model) =>
            new IPresenter[]
            {
                new UiHeroInfoPanelPresenter(model.Config, view.InfoPanel),
                new UiHeroLevelPanelPresenter(model, view.LevelPanel),
                new UiHeroInfoStatsPanelPresenter(model, view.StatsPanel),
            };
    }
}