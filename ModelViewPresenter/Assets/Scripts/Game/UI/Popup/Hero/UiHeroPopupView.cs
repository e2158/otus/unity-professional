﻿using UnityEngine;
using Sirenix.OdinInspector;
using Lessons.Architecture.PM.Framework.UI.Popup;

namespace Lessons.Architecture.PM.Game.UI.Popup
{
    [DisallowMultipleComponent]
    public sealed class UiHeroPopupView : UiPopupView
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private UiHeroInfoPanelView _infoPanel;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private UiHeroLevelPanelView _levelPanel;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private UiHeroInfoStatsPanelView _statsPanel;

        internal UiHeroInfoPanelView InfoPanel => _infoPanel;
        internal UiHeroLevelPanelView LevelPanel => _levelPanel;
        internal UiHeroInfoStatsPanelView StatsPanel => _statsPanel;

        protected override void OnValidateInner()
        {
            _infoPanel ??= GetComponentInChildren<UiHeroInfoPanelView>();
            _levelPanel ??= GetComponentInChildren<UiHeroLevelPanelView>();
            _statsPanel ??= GetComponentInChildren<UiHeroInfoStatsPanelView>();
        }
    }
}