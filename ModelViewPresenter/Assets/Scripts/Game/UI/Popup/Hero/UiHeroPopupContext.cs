﻿using System;
using Zenject;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Lessons.Architecture.PM.Game.Hero;
using Lessons.Architecture.PM.Framework.UI.MVP;
using Lessons.Architecture.PM.Framework.UI.Popup;
using Object = UnityEngine.Object;

namespace Lessons.Architecture.PM.Game.UI.Popup
{
    public sealed class UiHeroPopupContext : UiPopupContext
    {
        [NotNull] private readonly HeroModel _heroModel;
        [NotNull] private readonly Transform _viewContainer;
        [NotNull] private readonly IInstantiator _instantiator;
        [NotNull] private readonly UiHeroPopupView _viewPrefab;

        internal UiHeroPopupContext([NotNull] HeroModel heroModel, [NotNull] IInstantiator instantiator,
            [NotNull] UiHeroPopupView viewPrefab, [NotNull] Transform viewContainer)
        {
            _heroModel = heroModel ?? throw new ArgumentNullException(nameof(heroModel));
            _instantiator = instantiator ?? throw new ArgumentNullException(nameof(instantiator));

            _viewPrefab = viewPrefab ? viewPrefab : throw new ArgumentNullException(nameof(viewPrefab));
            _viewContainer = viewContainer ? viewContainer : throw new ArgumentNullException(nameof(viewContainer));
        }

        protected override UniTask<UiPopupView> LoadViewAsync()
        {
            GameObject popupObject = _instantiator.InstantiatePrefab(_viewPrefab, _viewContainer);
            return UniTask.FromResult(popupObject.GetComponent<UiPopupView>());
        }

        protected override UniTask UnloadViewAsync(UiPopupView view)
        {
            Object.Destroy(view.gameObject);
            return UniTask.CompletedTask;
        }

        protected override UniTask<IPresenter> CreatePresenterAsync(UiPopupView view)
        {
            UiHeroPopupPresenter presenter = new((UiHeroPopupView)view, _heroModel);
            return UniTask.FromResult<IPresenter>(presenter);
        }
    }
}