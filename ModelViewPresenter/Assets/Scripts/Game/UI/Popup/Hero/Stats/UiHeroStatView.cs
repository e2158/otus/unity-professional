﻿using TMPro;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Lessons.Architecture.PM.Game.UI.Popup
{
    [DisallowMultipleComponent]
    internal sealed class UiHeroStatView : MonoBehaviour
    {
        [Title("Components")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private TMP_Text _name;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private TMP_Text _value;

        private void OnValidate()
        {
            _name ??= GetComponentsInChildren<TMP_Text>().FirstOrDefault();
            _value ??= GetComponentsInChildren<TMP_Text>().LastOrDefault();
        }

        [Title("Controls")]
        [Button]
        public void SetData([CanBeNull] string name, uint value)
        {
            _name.text = name;
            _value.text = value.ToString();
        }
    }
}