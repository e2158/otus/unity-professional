﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Lessons.Architecture.PM.Framework.UI.MVP;

namespace Lessons.Architecture.PM.Game.UI.Popup
{
    [DisallowMultipleComponent]
    internal sealed class UiHeroInfoStatsPanelView : View
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private UiHeroStatViewPool _statViewPool;

        private readonly Dictionary<string, UiHeroStatView> _statViews = new();

        private void OnValidate()
        {
            _statViewPool ??= GetComponentInChildren<UiHeroStatViewPool>();
        }

        [Title("Controls")]

        [Button]
        public void ClearStats()
        {
            foreach (UiHeroStatView statView in _statViews.Values)
                _statViewPool.Release(statView);

            _statViews.Clear();
        }

        [Button]
        public void AddStat(string statName, uint statValue)
        {
            if (_statViews.ContainsKey(statName))
                throw new ArgumentException($"Stat {statName} already exists");

            UiHeroStatView statView = _statViewPool.Get();
            statView.SetData(statName, statValue);

            _statViews[statName] = statView;
        }

        [Button]
        public void UpdateStat(string statName, uint statValue)
        {
            if (!_statViews.ContainsKey(statName))
                throw new ArgumentException($"Stat {statName} doesn't exist");

            UiHeroStatView statView = _statViews[statName];
            statView.SetData(statName, statValue);
        }

        [Button]
        public void RemoveStat(string statName)
        {
            if (!_statViews.ContainsKey(statName))
                throw new ArgumentException($"Stat {statName} doesn't exist");

            UiHeroStatView statView = _statViews[statName];
            _statViewPool.Release(statView);

            _statViews.Remove(statName);
        }
    }
}