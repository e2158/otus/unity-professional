﻿using System;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Lessons.Architecture.PM.Game.Hero;
using Lessons.Architecture.PM.Framework.UI.MVP;

namespace Lessons.Architecture.PM.Game.UI.Popup
{
    internal sealed class UiHeroInfoStatsPanelPresenter : IPresenter
    {
        [NotNull] private readonly IHeroStatsModel _model;
        [NotNull] private readonly UiHeroInfoStatsPanelView _view;

        public UiHeroInfoStatsPanelPresenter([NotNull] IHeroStatsModel model, [NotNull] UiHeroInfoStatsPanelView view)
        {
            _view = view != null ? view : throw new ArgumentNullException(nameof(view));
            _model = model != null ? model : throw new ArgumentNullException(nameof(model));
        }

        public UniTask InitViewAsync()
        {
            _view.ClearStats();

            foreach (var kv in _model.Stats)
                _view.AddStat(kv.Key, kv.Value);

            return UniTask.CompletedTask;
        }

        public void SubscribeView() { }
        public void UnsubscribeView() { }

        public void SubscribeModel()
        {
            _model.StatAdded += OnStatAdded;
            _model.StatRemoved += OnStatRemoved;
            _model.StatUpdated += OnStatUpdated;
        }

        public void UnsubscribeModel()
        {
            _model.StatAdded += OnStatAdded;
            _model.StatRemoved += OnStatRemoved;
            _model.StatUpdated += OnStatUpdated;
        }

        private void OnStatAdded(string statName, uint initialValue)
        {
            _view.AddStat(statName, initialValue);
        }

        private void OnStatRemoved(string statName)
        {
            _view.RemoveStat(statName);
        }

        private void OnStatUpdated(string statName, uint statValue)
        {
            _view.UpdateStat(statName, statValue);
        }
    }
}