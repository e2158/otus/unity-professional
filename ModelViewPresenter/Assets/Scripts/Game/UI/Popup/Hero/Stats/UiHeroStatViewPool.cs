﻿using UnityEngine;
using Sirenix.OdinInspector;
using Lessons.Architecture.PM.Framework;

namespace Lessons.Architecture.PM.Game.UI.Popup
{
    [DisallowMultipleComponent]
    internal sealed class UiHeroStatViewPool : MonoPool<UiHeroStatView>
    {
        [Required, AssetSelector]
        [SerializeField] private UiHeroStatView _prefab;

        protected override UiHeroStatView CreateObject()
        {
            return Instantiate(_prefab);
        }

        protected override void OnGet(UiHeroStatView obj)
        {
            obj.gameObject.SetActive(true);
        }

        protected override void OnRelease(UiHeroStatView obj)
        {
            obj.gameObject.SetActive(false);
        }
    }
}