﻿using System;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Lessons.Architecture.PM.Game.Hero;
using Lessons.Architecture.PM.Framework.UI.MVP;

namespace Lessons.Architecture.PM.Game.UI.Popup
{
    internal sealed class UiHeroLevelPanelPresenter : IPresenter
    {
        [NotNull] private readonly IHeroLevelModel _model;
        [NotNull] private readonly UiHeroLevelPanelView _view;

        public UiHeroLevelPanelPresenter([NotNull] IHeroLevelModel model, [NotNull] UiHeroLevelPanelView view)
        {
            _view = view != null ? view : throw new ArgumentNullException(nameof(view));
            _model = model != null ? model : throw new ArgumentNullException(nameof(model));
        }

        public UniTask InitViewAsync()
        {
            _view.SetLevelUpButtonEnabled(_model.CanLevelUp);
            _view.XpProgressBar.SetData(_model.CurrentXp, _model.TargetXpOnCurrentLevel);

            return UniTask.CompletedTask;
        }

        public void SubscribeView()
        {
            _view.LevelUpButtonClicked.AddListener(OnLevelUpButtonClicked);
        }

        public void UnsubscribeView()
        {
            _view.LevelUpButtonClicked.RemoveListener(OnLevelUpButtonClicked);
        }

        public void SubscribeModel()
        {
            _model.CurrentXpChanged += OnCurrentXpChanged;
            _model.CurrentLevelUpped += OnCurrentLevelUpped;
        }

        public void UnsubscribeModel()
        {
            _model.CurrentXpChanged -= OnCurrentXpChanged;
            _model.CurrentLevelUpped -= OnCurrentLevelUpped;
        }

        private void OnLevelUpButtonClicked()
        {
            _model.LevelUp();
        }

        private void OnCurrentXpChanged()
        {
            _view.SetLevelUpButtonEnabled(_model.CanLevelUp);
            _view.XpProgressBar.SetData(_model.CurrentXp, _model.TargetXpOnCurrentLevel);
        }

        private void OnCurrentLevelUpped()
        {
            _view.SetLevel(_model.CurrentLevel);
            _view.SetLevelUpButtonEnabled(_model.CanLevelUp);
        }
    }
}