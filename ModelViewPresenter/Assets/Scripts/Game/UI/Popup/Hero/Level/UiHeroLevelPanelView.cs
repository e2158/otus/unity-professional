﻿using TMPro;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using Lessons.Architecture.PM.Framework.UI.MVP;

namespace Lessons.Architecture.PM.Game.UI.Popup
{
    [DisallowMultipleComponent]
    internal sealed class UiHeroLevelPanelView : View
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private TMP_Text _levelValue;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private Button _levelUpButton;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private UiProgressBar _xpProgressBar;

        public UiProgressBar XpProgressBar => _xpProgressBar;
        public Button.ButtonClickedEvent LevelUpButtonClicked => _levelUpButton.onClick;

        private void OnValidate()
        {
            _levelValue ??= GetComponentInChildren<TMP_Text>();
            _xpProgressBar ??= GetComponentInChildren<UiProgressBar>();
            _levelUpButton ??= GetComponentsInChildren<Button>().LastOrDefault();
        }

        public void SetLevel(uint level)
        {
            _levelValue.text = level.ToString();
        }

        public void SetLevelUpButtonEnabled(bool enabled)
        {
            _levelUpButton.interactable = enabled;
        }
    }
}