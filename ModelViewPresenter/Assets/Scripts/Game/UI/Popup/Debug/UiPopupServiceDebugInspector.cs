﻿using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Cysharp.Threading.Tasks;
using Lessons.Architecture.PM.Framework.UI.Popup;

namespace Lessons.Architecture.PM.Game.UI.Popup
{
    [DisallowMultipleComponent]
    internal sealed class UiPopupServiceDebugInspector : MonoBehaviour
    {
        private UiPopupService _uiPopupService;

        [Inject]
        private void Construct(UiPopupService uiPopupService)
        {
            _uiPopupService = uiPopupService;
        }

        [Title("Controls")]

        [Button]
        public void HideCurrentPopup() =>
            _uiPopupService.HideCurrentPopup().Forget();

        [Button]
        public void ShowHeroInfoPopup() =>
            _uiPopupService.ShowPopup<UiHeroPopupView>().Forget();
    }
}