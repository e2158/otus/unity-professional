﻿using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Lessons.Architecture.PM.Game.Hero
{
    [DisallowMultipleComponent]
    internal sealed class HeroModelDebugInspector : MonoBehaviour
    {
        private HeroModel _heroModel;

        [Inject]
        private void Construct(HeroModel heroModel) =>
            _heroModel = heroModel;

        [Title("Controls")]

        [Button(ButtonSizes.Medium)]
        public void LevelUp() =>
            _heroModel.LevelUp();

        [Button]
        public void AddXp(uint addingXp) =>
            _heroModel.AddXp(addingXp);

        [Button]
        public void AddStat(string statName, uint initialValue) =>
            _heroModel.AddStat(statName, initialValue);

        [Button]
        public void RemoveStat(string statName) =>
            _heroModel.RemoveStat(statName);

        [Button]
        public void UpdateStat(string statName, uint statValue) =>
            _heroModel.UpdateStat(statName, statValue);
    }
}