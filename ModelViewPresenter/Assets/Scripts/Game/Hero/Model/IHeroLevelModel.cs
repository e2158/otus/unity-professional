﻿using System;

namespace Lessons.Architecture.PM.Game.Hero
{
    public interface IHeroLevelModel
    {
        public event Action CurrentXpChanged;
        public event Action CurrentLevelUpped;

        uint CurrentXp { get; }
        uint LastLevel { get; }
        uint CurrentLevel { get; }
        bool CanLevelUp { get; }
        uint TargetXpOnCurrentLevel { get; }

        void AddXp(uint addingXp);
        void LevelUp();
    }
}
