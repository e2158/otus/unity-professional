﻿using System;
using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Lessons.Architecture.PM.Game.Hero
{
    public sealed class HeroModel : IHeroStatsModel, IHeroLevelModel
    {
        public event Action CurrentXpChanged;
        public event Action CurrentLevelUpped;
        public event Action<string, uint> StatAdded;
        public event Action<string, uint> StatUpdated;
        public event Action<string> StatRemoved;

        private readonly HeroData _heroData;
        private readonly Dictionary<string, uint> _statsCache;

        public HeroConfig Config => _heroData.Config;
        public uint CurrentXp => _heroData.CurrentXp;
        public uint CurrentLevel => _heroData.CurrentLevel;
        public uint LastLevel => Config.LastLevel;
        public IReadOnlyDictionary<string, uint> Stats => _statsCache;

        public bool CanLevelUp => CurrentXp >= TargetXpOnCurrentLevel && CurrentLevel < LastLevel;
        public uint TargetXpOnCurrentLevel => _heroData.Config.TargetXpOnLevel[(int)CurrentLevel];

        public HeroModel([NotNull] HeroData heroData)
        {
            _heroData = heroData ?? throw new ArgumentNullException(nameof(heroData));
            _statsCache = CreateStatsCache(heroData);
        }

        private static Dictionary<string, uint> CreateStatsCache([NotNull] HeroData heroData)
        {
            Dictionary<string, uint> statsCache = heroData.Config.BaseStats
                .ToDictionary(kv => kv.Name, kv => kv.Value);

            foreach (var kv in heroData.OverriddenStats)
                statsCache[kv.Key] = kv.Value;

            return statsCache;
        }

        public void AddXp(uint addingXp)
        {
            _heroData.CurrentXp += addingXp;
            CurrentXpChanged?.Invoke();
        }

        public void LevelUp()
        {
            if (!CanLevelUp)
                throw new InvalidOperationException(
                    $"Can't level up from level {_heroData.CurrentLevel}. "
                    + $"Exp state: {_heroData.CurrentXp}/{TargetXpOnCurrentLevel}");

            _heroData.CurrentXp -= TargetXpOnCurrentLevel;
            _heroData.CurrentLevel++;

            CurrentLevelUpped?.Invoke();
            CurrentXpChanged?.Invoke();
        }

        public void AddStat(string statName, uint initialValue)
        {
            if (_statsCache.ContainsKey(statName))
                throw new ArgumentException($"Stat {statName} already exists");

            _statsCache[statName] = initialValue;
            _heroData.OverriddenStats[statName] = initialValue;
            StatAdded?.Invoke(statName, initialValue);
        }

        public void RemoveStat(string statName)
        {
            if (!_statsCache.ContainsKey(statName))
                throw new ArgumentException($"Stat {statName} doesn't exist");

            _statsCache.Remove(statName);
            _heroData.OverriddenStats.Remove(statName);
            StatRemoved?.Invoke(statName);
        }

        public void UpdateStat(string statName, uint statValue)
        {
            if (!_statsCache.ContainsKey(statName))
                throw new ArgumentException($"Stat {statName} doesn't exist");

            _statsCache[statName] = statValue;
            _heroData.OverriddenStats[statName] = statValue;
            StatUpdated?.Invoke(statName, statValue);
        }
    }
}