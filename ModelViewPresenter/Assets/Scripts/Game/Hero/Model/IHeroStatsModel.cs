﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Lessons.Architecture.PM.Game.Hero
{
    public interface IHeroStatsModel
    {
        event Action<string, uint> StatAdded;
        event Action<string, uint> StatUpdated;
        event Action<string> StatRemoved;

        [NotNull] IReadOnlyDictionary<string, uint> Stats { get; }

        void AddStat([NotNull] string statName, uint initialValue);
        void UpdateStat([NotNull] string statName, uint statValue);
        void RemoveStat([NotNull] string statName);
    }
}
