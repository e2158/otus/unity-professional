﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Lessons.Architecture.PM.Game.Hero
{
    [Serializable, InlineProperty]
    public sealed class HeroStatConfigData
    {
        [Required, HorizontalGroup, HideLabel]
        [SerializeField] private string _name;

        [MinValue(0), HorizontalGroup, HideLabel]
        [SerializeField] private uint _value;

        public string Name => _name;
        public uint Value => _value;
    }
}