using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace Lessons.Architecture.PM.Game.Hero
{
    [CreateAssetMenu(fileName = nameof(HeroConfig), menuName = "Configs/" + nameof(HeroConfig))]
    public sealed class HeroConfig : ScriptableObject
    {
        [Required]
        [SerializeField] private string _name;

        [Required]
        [SerializeField] private string _description;

        [Required, AssetSelector]
        [SerializeField] private Sprite _icon;

        [Required, ListDrawerSettings(Expanded = true)]
        [SerializeField] private HeroStatConfigData[] _baseStats;

        [ListDrawerSettings(Expanded = true, ShowIndexLabels = true)]
        [SerializeField] private uint[] _targetXpOnLevel;

        public string Name => _name;
        public string Description => _description;
        public Sprite Icon => _icon;
        public IReadOnlyList<HeroStatConfigData> BaseStats => _baseStats;
        public IReadOnlyList<uint> TargetXpOnLevel => _targetXpOnLevel;
        public uint LastLevel => (uint)Mathf.Max(TargetXpOnLevel.Count - 1, 0);
    }
}