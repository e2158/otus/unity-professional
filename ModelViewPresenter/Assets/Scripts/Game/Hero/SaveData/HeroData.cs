﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace Lessons.Architecture.PM.Game.Hero
{
    [Serializable]
    public sealed class HeroData : ICloneable
    {
        [Required, AssetSelector]
        public HeroConfig Config;

        [MinValue(0)]
        public uint CurrentLevel;

        [MinValue(0)]
        public uint CurrentXp;

        [Required]
        public Dictionary<string, uint> OverriddenStats = new();

        public object Clone() =>
            new HeroData()
            {
                Config = Config,
                CurrentLevel = CurrentLevel,
                CurrentXp = CurrentXp,
                OverriddenStats = new Dictionary<string, uint>(OverriddenStats)
            };
    }
}