﻿using System;
using System.Collections.Generic;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Lessons.Architecture.PM.Game.UI.Popup;
using Lessons.Architecture.PM.Framework.UI.Popup;

namespace Lessons.Architecture.PM.Game
{
    [DisallowMultipleComponent]
    internal sealed class PopupContextInstaller : MonoInstaller
    {
        [Required, AssetSelector]
        [SerializeField] private Transform _viewContainer;

        [Required, AssetSelector]
        [SerializeField] private UiHeroPopupView _viewPrefab;

        public override void InstallBindings()
        {
            Container.Bind<UiHeroPopupContext>().AsTransient()
                .WithArguments(new object[] { _viewPrefab, _viewContainer });

            Container.Bind<UiPopupService>().FromMethod(CreateUiPopupService).AsSingle();
        }

        private UiPopupService CreateUiPopupService()
        {
            var contexts = new Dictionary<Type, UiPopupContext>()
            {
                { typeof(UiHeroPopupView), Container.Resolve<UiHeroPopupContext>() }
            };

            return new UiPopupService(contexts);
        }
    }
}