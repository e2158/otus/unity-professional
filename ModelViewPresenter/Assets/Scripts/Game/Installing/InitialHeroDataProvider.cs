﻿using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Lessons.Architecture.PM.Game.Hero;

namespace Lessons.Architecture.PM.Game
{
    [CreateAssetMenu(fileName = nameof(InitialHeroDataProvider),
        menuName = "Configs/" + nameof(InitialHeroDataProvider))]
    internal sealed class InitialHeroDataProvider : ScriptableObject
    {
        [Required, HideLabel]
        [SerializeField] private HeroData _heroData;

        [NotNull] public HeroData Provide() => (HeroData)_heroData.Clone();
    }
}