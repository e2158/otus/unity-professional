﻿using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Lessons.Architecture.PM.Game.Hero;

namespace Lessons.Architecture.PM.Game
{
    [DisallowMultipleComponent]
    internal sealed class HeroContextInstaller : MonoInstaller
    {
        [Required, AssetSelector]
        [SerializeField] private InitialHeroDataProvider _initialHeroDataProvider;

        public override void InstallBindings()
        {
            Container.Bind<HeroData>().FromMethod(_initialHeroDataProvider.Provide).AsSingle();
            Container.Bind<HeroModel>().AsSingle();
        }
    }
}