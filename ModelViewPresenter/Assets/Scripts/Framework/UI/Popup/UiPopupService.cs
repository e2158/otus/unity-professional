﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace Lessons.Architecture.PM.Framework.UI.Popup
{
    public sealed class UiPopupService : IDisposable
    {
        [NotNull] private readonly IReadOnlyDictionary<Type, UiPopupContext> _popupContexts;

        [CanBeNull] private UiPopupContext _currentPopup;

        public UiPopupService([NotNull] IReadOnlyDictionary<Type, UiPopupContext> popupContexts)
        {
            _popupContexts = popupContexts ?? throw new ArgumentNullException(nameof(popupContexts));
        }

        public void Dispose()
        {
            HideCurrentPopup().Forget();
        }

        public async UniTask ShowPopup<TView>() where TView : UiPopupView
        {
            await HideCurrentPopup();

            _currentPopup = _popupContexts[typeof(TView)];
            await _currentPopup!.InitializeAsync();

            _currentPopup.CloseButtonClicked?.AddListener(OnCloseButtonClicked);
        }

        public async UniTask HideCurrentPopup()
        {
            if (_currentPopup != null)
            {
                _currentPopup.CloseButtonClicked?.RemoveListener(OnCloseButtonClicked);
                await _currentPopup.DeinitializeAsync();
                _currentPopup = null;
            }
        }

        private void OnCloseButtonClicked()
        {
            HideCurrentPopup().Forget();
        }
    }
}