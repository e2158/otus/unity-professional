﻿using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using Lessons.Architecture.PM.Framework.UI.MVP;

namespace Lessons.Architecture.PM.Framework.UI.Popup
{
    public abstract class UiPopupView : View
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private Button _closeButton;

        public Button.ButtonClickedEvent CloseButtonClicked => _closeButton.onClick;

        private void OnValidate()
        {
            _closeButton ??= GetComponentInChildren<Button>();
            OnValidateInner();
        }

        protected virtual void OnValidateInner() { }
    }
}