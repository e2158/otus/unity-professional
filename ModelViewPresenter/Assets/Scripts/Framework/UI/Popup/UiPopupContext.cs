﻿using UnityEngine.UI;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Lessons.Architecture.PM.Framework.UI.MVP;

namespace Lessons.Architecture.PM.Framework.UI.Popup
{
    public abstract class UiPopupContext
    {
        [CanBeNull] private UiPopupView _view;
        [CanBeNull] private IPresenter _presenter;

        [CanBeNull] public Button.ButtonClickedEvent CloseButtonClicked =>
            _view != null ? _view.CloseButtonClicked : null;

        public async UniTask InitializeAsync()
        {
            _view = await LoadViewAsync();
            _presenter = await CreatePresenterAsync(_view!);

            _presenter!.SubscribeView();
            _presenter!.SubscribeModel();
            await _presenter!.InitViewAsync();
        }

        public async UniTask DeinitializeAsync()
        {
            if (_presenter != null)
            {
                _presenter.UnsubscribeView();
                _presenter.UnsubscribeModel();
            }

            if (_view != null)
            {
                await UnloadViewAsync(_view);
            }
        }

        protected abstract UniTask<UiPopupView> LoadViewAsync();
        protected abstract UniTask UnloadViewAsync([NotNull] UiPopupView view);
        protected abstract UniTask<IPresenter> CreatePresenterAsync([NotNull] UiPopupView view);
    }
}