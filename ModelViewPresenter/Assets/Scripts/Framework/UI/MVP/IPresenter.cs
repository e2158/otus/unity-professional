﻿using Cysharp.Threading.Tasks;

namespace Lessons.Architecture.PM.Framework.UI.MVP
{
    public interface IPresenter
    {
        UniTask InitViewAsync();

        void SubscribeModel();
        void UnsubscribeModel();

        void SubscribeView();
        void UnsubscribeView();
    }
}