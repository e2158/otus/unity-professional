﻿using System;
using System.Linq;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using System.Collections.Generic;

namespace Lessons.Architecture.PM.Framework.UI.MVP
{
    public abstract class CompositePresenter : IPresenter
    {
        [NotNull, ItemNotNull] private readonly IPresenter[] _subPresenters;

        protected CompositePresenter([NotNull, ItemNotNull] IPresenter[] subPresenters)
        {
            _subPresenters = subPresenters ?? throw new ArgumentNullException(nameof(subPresenters));
            if (_subPresenters.Any(p => p.Equals(null))) throw new ArgumentNullException(nameof(subPresenters));
        }

        public UniTask InitViewAsync()
        {
            IEnumerable<UniTask> tasks = _subPresenters.Select(p => p.InitViewAsync());
            return UniTask.WhenAll(tasks);
        }

        public void SubscribeModel()
        {
            foreach (IPresenter subPresenter in _subPresenters)
                subPresenter.SubscribeModel();
        }

        public void UnsubscribeModel()
        {
            foreach (IPresenter subPresenter in _subPresenters)
                subPresenter.UnsubscribeModel();
        }

        public void SubscribeView()
        {
            foreach (IPresenter subPresenter in _subPresenters)
                subPresenter.SubscribeView();
        }

        public void UnsubscribeView()
        {
            foreach (IPresenter subPresenter in _subPresenters)
                subPresenter.UnsubscribeView();
        }
    }
}