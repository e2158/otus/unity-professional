﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using JetBrains.Annotations;

namespace Lessons.Architecture.PM.Framework
{
    public abstract class MonoPool<TObject> : MonoBehaviour where TObject : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private int _initialCount = 6;

        [Header("References")]
        [SerializeField] private Transform _poolContainer;
        [SerializeField] private Transform _worldContainer;

        private readonly Queue<TObject> _pool = new();

        private void Awake()
        {
            IEnumerable<TObject> existingObjects =
                _poolContainer.GetComponentsInChildren<TObject>().Union(
                    _worldContainer.GetComponentsInChildren<TObject>());

            foreach (TObject obj in existingObjects)
                Release(obj);

            for (int i = _pool.Count; i < _initialCount; i++)
            {
                TObject obj = CreateObject();
                Release(obj);
            }
        }

        [NotNull]
        public TObject Get()
        {
            if (!_pool.TryDequeue(out TObject obj))
                obj = CreateObject();

            obj.transform.SetParent(_worldContainer);
            OnGet(obj);

            return obj;
        }

        public void Release([NotNull] TObject obj)
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj), "Can't be null");

            obj.transform.SetParent(_poolContainer);
            _pool.Enqueue(obj);
            OnRelease(obj);
        }

        [NotNull] protected abstract TObject CreateObject();

        // Точки расширения обработки методов пула.
        // Шаблонные методы, которые не треубют вызова внутри наследников.
        protected virtual void OnGet([NotNull] TObject obj) { }
        protected virtual void OnRelease([NotNull] TObject obj) { }
    }
}