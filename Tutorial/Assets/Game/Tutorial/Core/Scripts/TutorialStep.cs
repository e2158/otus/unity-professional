namespace Game.Tutorial
{
    public enum TutorialStep
    {
        WELCOME = 0,
        HARVEST_RESOURCE = 1,
        BUY_PRODUCT = 2,
        SELL_RESOURCE = 3,
        UPGRADE_HERO = 4,
        DESTROY_ENEMY = 5,
        GET_REWARD = 6,
    }
}