using Game.GameEngine;
using Game.GameEngine.Products;
using Game.Localization;
using Game.Meta;
using UnityEngine;

namespace Game.Tutorial
{
    [CreateAssetMenu(
        fileName = "Config «Buy Product»",
        menuName = "Tutorial/Config «Buy Product»"
    )]
    public sealed class BuyProductConfig : ScriptableObject
    {
        [Header("Quest")]
        [SerializeField]
        public ProductConfig productConfig;

        [SerializeField]
        public WorldPlaceType worldPlaceType =  WorldPlaceType.MARKET;

        [SerializeField]
        public PopupName requiredPopupName = PopupName.MARKET;

        [Header("Meta")]
        [TranslationKey]
        [SerializeField]
        public string title = "BUY PRODUCT";

        [SerializeField]
        public Sprite icon;
    }
}