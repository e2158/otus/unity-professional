using System;
using Game.GameEngine.Products;

namespace Game.Tutorial
{
    public sealed class BuyProductInspector
    {
        private BuyProductConfig config;

        private ProductBuyer productBuyer;

        private Product targetProduct;

        private Action callback;

        public void Construct(ProductBuyer productBuyer, BuyProductConfig config)
        {
            this.productBuyer = productBuyer;
            this.config = config;
        }

        public void Inspect(Action callback)
        {
            this.callback = callback;
            this.targetProduct = this.config.productConfig.Prototype;
            this.productBuyer.OnBuyCompleted += this.OnBought;
        }

        private void OnBought(Product product)
        {
            if (product != this.targetProduct)
            {
                return;
            }

            this.productBuyer.OnBuyCompleted -= this.OnBought;
            this.callback?.Invoke();
        }
    }
}