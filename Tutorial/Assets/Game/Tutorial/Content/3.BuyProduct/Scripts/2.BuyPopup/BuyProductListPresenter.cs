using System;
using System.Collections.Generic;
using System.Linq;
using Game.GameEngine.GameResources;
using Game.GameEngine.Products;
using Game.Gameplay.Player;
using Game.Meta;
using GameSystem;
using UnityEngine;

namespace Game.Tutorial
{
    public sealed class BuyProductListPresenter : MonoBehaviour, IGameConstructElement
    {
        [SerializeField]
        private ProductCatalog productCatalog;

        [SerializeField]
        private ResourceInfoCatalog resourceCatalog;

        [SerializeField]
        private Sprite moneyIcon;

        [SerializeField]
        private BuyProductConfig config;

        [SerializeField]
        private ProductView targetView;

        [SerializeField]
        private ProductView[] otherViews;

        private ProductBuyer productBuyer;

        private MoneyStorage moneyStorage;

        private ResourceStorage resourceStorage;

        private readonly List<ProductPresenter> presenters;

        public BuyProductListPresenter()
        {
            this.presenters = new List<ProductPresenter>();
        }

        void IGameConstructElement.ConstructGame(GameContext context)
        {
            this.productBuyer = context.GetService<ProductBuyer>();
            this.moneyStorage = context.GetService<MoneyStorage>();
            this.resourceStorage = context.GetService<ResourceStorage>();
        }

        public void Show()
        {
            this.InitProducts();
            this.ShowProducts();
        }

        public void Hide()
        {
            this.HideProducts();
            this.presenters.Clear();
        }

        private void InitProducts()
        {
            var targetId = this.config.productConfig.Id;
            var targetProduct = this.config.productConfig.Prototype;
            this.CreatePresenter(targetProduct, this.targetView);

            var otherProducts = this.productCatalog
                .GetAllProducts()
                .Where(it => it.Id != targetId)
                .Select(it => it.Prototype)
                .ToArray();

            var otherCount = Math.Min(this.otherViews.Length, otherProducts.Length);

            for (var i = 0 ; i < otherCount; i++)
            {
                var product = otherProducts[i];
                var view = this.otherViews[i];
                this.CreatePresenter(product, view);
            }
        }

        private void CreatePresenter(Product targetProduct, ProductView view)
        {
            var targetPresenter = new ProductPresenter(view);
            targetPresenter.Construct(this.productBuyer, this.moneyStorage,
                this.resourceStorage, this.resourceCatalog, this.moneyIcon);
            targetPresenter.SetProduct(targetProduct);
            this.presenters.Add(targetPresenter);
        }

        private void ShowProducts()
        {
            for (int i = 0, count = this.presenters.Count; i < count; i++)
            {
                var presenter = this.presenters[i];
                presenter.Start();
            }
        }

        private void HideProducts()
        {
            for (int i = 0, count = this.presenters.Count; i < count; i++)
            {
                var presenter = this.presenters[i];
                presenter.Stop();
            }
        }
    }
}