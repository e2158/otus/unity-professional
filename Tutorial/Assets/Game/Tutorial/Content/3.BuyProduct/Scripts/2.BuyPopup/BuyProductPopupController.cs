using Game.GameEngine.Products;
using Game.Tutorial.Gameplay;
using GameSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Tutorial
{
    [AddComponentMenu("Tutorial/Step «Buy Product Popup»")]
    public sealed class BuyProductPopupController : TutorialStepController
    {
        private readonly BuyProductInspector buyProductInspector = new();

        [SerializeField]
        private BuyProductConfig config;

        [SerializeField]
        private GameObject questCursor;

        [SerializeField]
        private Transform fading;

        [Header("Close")]
        [SerializeField]
        private Button closeButton;

        [SerializeField]
        private GameObject closeCursor;

        private void Awake()
        {
            this.questCursor.SetActive(false);
            this.closeCursor.SetActive(false);
            this.closeButton.interactable = false;
        }

        public override void ConstructGame(GameContext context)
        {
            var productBuyer = context.GetService<ProductBuyer>();
            this.buyProductInspector.Construct(productBuyer, this.config);

            base.ConstructGame(context);
        }

        public void Show()
        {
            //Ждем выполнение квеста покупки:
            this.buyProductInspector.Inspect(this.OnQuestFinished);

            //Включаем курсор на покупке:
            this.questCursor.SetActive(true);
        }

        private void OnQuestFinished()
        {
            //Выключаем курсор на покупке:
            this.questCursor.SetActive(false);

            //Включаем курсор на кнопке закрыть:
            this.closeCursor.SetActive(true);

            //Делаем затемнение на покупке:
            this.fading.SetAsLastSibling();

            //Активируем кнопку закрыть:
            this.closeButton.interactable = true;
            this.closeButton.onClick.AddListener(this.OnCloseClicked);

            //Завершаем шаг туториала:
            this.NotifyAboutComplete();
        }

        private void OnCloseClicked()
        {
            this.closeButton.onClick.RemoveListener(this.OnCloseClicked);

            //Выключаем курсор на кнопке закрыть:
            this.closeCursor.SetActive(false);

            //Переходим к следующему шагу туториала:
            this.NotifyAboutMoveNext();
        }
    }
}