using Game.Meta;
using Game.Tutorial.Gameplay;
using GameSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Tutorial
{
    [AddComponentMenu("Tutorial/Step «Missions Popup»")]
    public sealed class MissionPopupController : TutorialStepController
    {
        private readonly MissionInspector missionInspector = new();

        [SerializeField]
        private GetRewardConfig config;

        [SerializeField]
        private GameObject missionCursor;

        [SerializeField]
        private Transform fading;

        [Header("Close")]
        [SerializeField]
        private Button closeButton;

        [SerializeField]
        private GameObject closeCursor;

        private void Awake()
        {
            this.missionCursor.SetActive(false);
            this.closeCursor.SetActive(false);
            this.closeButton.interactable = false;
        }

        public override void ConstructGame(GameContext context)
        {
            var missionsManager = context.GetService<MissionsManager>();
            this.missionInspector.Construct(missionsManager, this.config);

            base.ConstructGame(context);
        }

        public void Show()
        {
            //Ждем выполнение квеста:
            this.missionInspector.Inspect(this.OnQuestFinished);

            //Включаем курсор на задании:
            this.missionCursor.SetActive(true);
        }

        private void OnQuestFinished()
        {
            //Выключаем курсор на задании:
            this.missionCursor.SetActive(false);

            //Включаем курсор на кнопке закрыть:
            this.closeCursor.SetActive(true);

            //Делаем затемнение на миссиях:
            this.fading.SetAsLastSibling();

            //Активируем кнопку закрыть:
            this.closeButton.interactable = true;
            this.closeButton.onClick.AddListener(this.OnCloseClicked);

            //Завершаем шаг туториала:
            this.NotifyAboutComplete();
        }

        private void OnCloseClicked()
        {
            this.closeButton.onClick.RemoveListener(this.OnCloseClicked);

            //Выключаем курсор на кнопке закрыть:
            this.closeCursor.SetActive(false);

            //Переходим к следующему шагу туториала:
            this.NotifyAboutMoveNext();
        }
    }
}