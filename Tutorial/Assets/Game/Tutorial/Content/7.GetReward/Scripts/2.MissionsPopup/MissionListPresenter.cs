using System;
using System.Collections.Generic;
using Game.Gameplay.Player;
using Game.Meta;
using GameSystem;
using UnityEngine;

namespace Game.Tutorial
{
    public sealed class MissionListPresenter : MonoBehaviour, IGameConstructElement
    {
        [SerializeField]
        private GetRewardConfig config;

        [SerializeField]
        private MissionItem targetItem;

        [SerializeField]
        private MissionItem[] otherItems;

        private MoneyPanelAnimator_AddMoney moneyPanelAnimator;

        private MissionsManager missionsManager;

        private readonly List<MissionItem> allItems;

        public MissionListPresenter()
        {
            this.allItems = new List<MissionItem>();
        }

        void IGameConstructElement.ConstructGame(GameContext context)
        {
            this.moneyPanelAnimator = context.GetService<MoneyPanelAnimator_AddMoney>();
            this.missionsManager = context.GetService<MissionsManager>();
        }

        public void Show()
        {
            this.InitMissions();
            this.ShowMissions();
        }

        public void Hide()
        {
            this.HideMissions();
            this.allItems.Clear();
        }

        private void InitMissions()
        {
            this.targetItem.presenter.Construct(this.missionsManager, this.moneyPanelAnimator);
            this.allItems.Add(this.targetItem);

            foreach (MissionItem item in this.otherItems)
            {
                item.presenter.Construct(this.missionsManager, this.moneyPanelAnimator);
                this.allItems.Add(item);
            }
        }

        private void ShowMissions()
        {
            this.missionsManager.OnMissionChanged += this.OnMissionChanged;

            var missions = this.missionsManager.GetMissions();
            for (int i = 0, count = missions.Length; i < count; i++)
            {
                var mission = missions[i];
                var presenter = this.GetPresenter(mission.Difficulty);
                presenter.Start(mission);
            }
        }

        private void HideMissions()
        {
            this.missionsManager.OnMissionChanged -= this.OnMissionChanged;

            for (int i = 0, count = this.allItems.Count; i < count; i++)
            {
                var presenter = this.allItems[i].presenter;
                presenter.Stop();
            }
        }

        private void OnMissionChanged(Mission mission)
        {
            var presenter = this.GetPresenter(mission.Difficulty);
            if (presenter.IsShown)
            {
                presenter.Stop();
            }

            presenter.Start(mission);
        }

        private MissionPresenter GetPresenter(MissionDifficulty difficulty)
        {
            for (int i = 0, count = this.allItems.Count; i < count; i++)
            {
                var missionItem = this.allItems[i];
                if (missionItem.difficulty == difficulty)
                {
                    return missionItem.presenter;
                }
            }

            throw new Exception($"Mission with difficulty {difficulty} is not found"!);
        }
    }
}