using System;
using Game.Meta;

namespace Game.Tutorial
{
    public sealed class MissionInspector
    {
        private GetRewardConfig config;

        private MissionsManager missionsManager;

        private Mission targetMission;

        private Action callback;

        public void Construct(MissionsManager missionsManager, GetRewardConfig targetConfig)
        {
            this.missionsManager = missionsManager;
            this.config = targetConfig;
        }

        public void Inspect(Action callback)
        {
            this.callback = callback;
            this.targetMission = this.missionsManager.GetMission(this.config.missionConfig.Difficulty);
            this.missionsManager.OnRewardReceived += this.OnRewardReceived;
        }

        private void OnRewardReceived(Mission mission)
        {
            if (mission != this.targetMission)
            {
                return;
            }

            this.missionsManager.OnRewardReceived -= this.OnRewardReceived;
            this.targetMission = null;
            this.callback?.Invoke();
        }
    }
}