using Game.GameEngine;
using Game.Localization;
using Game.Meta;
using UnityEngine;

namespace Game.Tutorial
{
    [CreateAssetMenu(
        fileName = "Config «Get Reward»",
        menuName = "Tutorial/Config «Get Reward»"
    )]
    public sealed class GetRewardConfig : ScriptableObject
    {
        [Header("Quest")]
        [SerializeField]
        public MissionConfig missionConfig;

        [SerializeField]
        public WorldPlaceType worldPlaceType =  WorldPlaceType.TAVERN;

        [SerializeField]
        public PopupName requiredPopupName = PopupName.MISSIONS;

        [Header("Meta")]
        [TranslationKey]
        [SerializeField]
        public string title = "GET REWARD";

        [SerializeField]
        public Sprite icon;
    }
}