﻿using System;
using JetBrains.Annotations;
using ShootEmUp.UI;
using ShootEmUp.Framework;

namespace ShootEmUp
{
    internal sealed class UiGameManagerController : IDisposable
    {
        [NotNull] private readonly UiGameManager m_uiGameManager;
        [NotNull] private readonly IReadOnlyGameRunner m_gameRunner;

        public UiGameManagerController(
            [NotNull] IReadOnlyGameRunner gameRunner, [NotNull] UiGameManager uiGameManager)
        {
            this.m_gameRunner = gameRunner;
            this.m_uiGameManager = uiGameManager;

            this.UpdateUI(this.m_gameRunner.CurrentStatus);

            this.Subscribe(this.m_gameRunner);
        }

        public void Dispose()
        {
            this.Unsubscribe(this.m_gameRunner);
        }

        private void Subscribe(IReadOnlyGameRunner gameRunner)
        {
            gameRunner.OnStatusUpdated += this.UpdateUI;
        }

        private void Unsubscribe(IReadOnlyGameRunner gameRunner)
        {
            gameRunner.OnStatusUpdated -= this.UpdateUI;
        }

        private void UpdateUI(GameStatus status)
        {
            switch (status)
            {
                case GameStatus.None:
                    this.m_uiGameManager.ShowMainMenu();
                    break;

                case GameStatus.Play:
                    this.m_uiGameManager.ShowPlayingGameMenu();
                    break;

                case GameStatus.Pause:
                    this.m_uiGameManager.ShowPausedGameMenu();
                    break;

                case GameStatus.Finish:
                    this.m_uiGameManager.ShowFinishMenu();
                    break;

                default: throw new ArgumentOutOfRangeException(nameof(status), status, null);
            }
        }
    }
}