﻿using System;
using VContainer;
using UnityEngine;
using System.Collections.Generic;
using JetBrains.Annotations;
using ShootEmUp.UI;
using ShootEmUp.Gameplay;
using ShootEmUp.Framework;

namespace ShootEmUp
{
    internal sealed class GameRunnerController : IDisposable
    {
        [NotNull] private readonly IUiEvents m_uiEvents;
        [NotNull] private readonly GameRunner m_gameRunner;
        [NotNull] private readonly IReadOnlyList<IGameOverTrigger> m_gameOverTriggers;

        [Inject]
        public GameRunnerController(
            [NotNull] IUiEvents uiEvents, [NotNull] GameRunner gameRunner,
            [NotNull] IReadOnlyList<IGameOverTrigger> gameOverTriggers)
        {
            this.m_uiEvents = uiEvents;
            this.m_gameRunner = gameRunner;
            this.m_gameOverTriggers = gameOverTriggers;

            this.Subscribe(this.m_uiEvents);
            this.Subscribe(this.m_gameOverTriggers);
        }

        public void Dispose()
        {
            this.Unsubscribe(this.m_uiEvents);
            this.Unsubscribe(this.m_gameOverTriggers);
        }

        private void Subscribe(IUiEvents uiEvents)
        {
            uiEvents.OnCountdownCompleted.AddListener(this.m_gameRunner.StartGame);
            uiEvents.OnPauseResumeButtonClicked.AddListener(this.m_gameRunner.PauseOnResumeGame);
        }

        private void Unsubscribe(IUiEvents uiEvents)
        {
            uiEvents.OnCountdownCompleted.RemoveListener(this.m_gameRunner.StartGame);
            uiEvents.OnPauseResumeButtonClicked.RemoveListener(this.m_gameRunner.PauseOnResumeGame);
        }

        private void Subscribe(IReadOnlyList<IGameOverTrigger> gameOverTriggers)
        {
            foreach (IGameOverTrigger trigger in gameOverTriggers)
            {
                trigger.OnTriggered += this.m_gameRunner.FinishGame;
            }
        }

        private void Unsubscribe(IReadOnlyList<IGameOverTrigger> gameOverTriggers)
        {
            foreach (IGameOverTrigger trigger in gameOverTriggers)
            {
                trigger.OnTriggered -= this.m_gameRunner.FinishGame;
            }
        }
    }
}