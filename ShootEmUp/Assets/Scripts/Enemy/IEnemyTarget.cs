﻿using UnityEngine;

namespace ShootEmUp.Enemy
{
    public interface IEnemyTarget
    {
        bool IsDead { get; }
        Vector2 Position { get; }
    }
}