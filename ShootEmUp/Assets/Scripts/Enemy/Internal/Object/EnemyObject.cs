﻿using System;
using UnityEngine;

namespace ShootEmUp.Enemy
{
    internal sealed class EnemyObject : MonoBehaviour
    {
        public event Action<GameObject> OnDead
        {
            add { this.hitPointsComponent.OnHitPointsEmpty += value; }
            remove { this.hitPointsComponent.OnHitPointsEmpty -= value; }
        }

        [SerializeField]
        private EnemyMoveAgent moveAgent;

        [SerializeField]
        private EnemyAttackAgent attackAgent;

        [SerializeField]
        private HitPointsComponent hitPointsComponent;

        private void OnValidate()
        {
            this.moveAgent ??= this.GetComponentInChildren<EnemyMoveAgent>();
            this.attackAgent ??= this.GetComponentInChildren<EnemyAttackAgent>();
            this.hitPointsComponent ??= this.GetComponentInChildren<HitPointsComponent>();
        }

        public void Init(Vector3 spawnPosition, Vector3 attackPosition)
        {
            this.transform.position = spawnPosition;
            this.moveAgent.SetDestination(attackPosition);
        }
    }
}