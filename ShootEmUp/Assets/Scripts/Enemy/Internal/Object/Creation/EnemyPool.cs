﻿using UnityEngine;

namespace ShootEmUp.Enemy
{
    internal sealed class EnemyPool : MonoPool<EnemyObject>
    {
        [SerializeField]
        private EnemyObject prefab;

        protected override EnemyObject CreateObject()
        {
            return Instantiate(this.prefab);
        }
    }
}