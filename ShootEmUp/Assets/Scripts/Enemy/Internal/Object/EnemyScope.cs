﻿using UnityEngine;
using VContainer;
using VContainer.Unity;
using ShootEmUp.Framework;

namespace ShootEmUp.Enemy
{
    public sealed class EnemyScope : LifetimeScope
    {
        [SerializeField]
        private EnemyObject enemyObject;

        private void OnValidate()
        {
            this.enemyObject ??= this.GetComponentInChildren<EnemyObject>();
        }

        private void OnEnable()
        {
            this.RegisterInGameEvents();
        }

        private void OnDisable()
        {
            this.UnregisterInGameEvents();
        }

        protected override void Configure(IContainerBuilder builder)
        {
            Transform parent = this.enemyObject.transform;

            builder.RegisterInstance(this.enemyObject);

            builder.RegisterComponentInHierarchy<EnemyMoveAgent>().UnderTransform(parent);
            builder.RegisterComponentInHierarchy<EnemyAttackAgent>().UnderTransform(parent);
        }

        private void RegisterInGameEvents()
        {
            var gameEventsSubscriber = this.Container.Resolve<GameEventsSubscriber>();
            gameEventsSubscriber.AddListener(this.enemyObject.gameObject);
        }

        private void UnregisterInGameEvents()
        {
            var gameEventsSubscriber = this.Container.Resolve<GameEventsSubscriber>();
            gameEventsSubscriber.RemoveListener(this.enemyObject.gameObject);
        }
    }
}