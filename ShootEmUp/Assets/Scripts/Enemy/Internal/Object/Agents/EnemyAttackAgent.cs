﻿using System;
using VContainer;
using UnityEngine;
using ShootEmUp.Bullet;
using ShootEmUp.Framework;

namespace ShootEmUp.Enemy
{
    internal sealed class EnemyAttackAgent : MonoBehaviour, IStartGameListener, IFinishGameListener, ITickPhysicsListener
    {
        [Header("Settings")]
        [SerializeField, Min(0)]
        private float shootIntervalSec;

        [Header("References")]
        [SerializeField]
        private TeamComponent teamComponent;

        [SerializeField]
        private BulletShootComponent shootComponent;

        private IEnemyTarget m_target;
        private TickableTimer m_shootTimer;
        private EnemyMoveAgent m_moveAgent;

        private void OnValidate()
        {
            this.m_moveAgent ??= this.GetComponentInChildren<EnemyMoveAgent>();
            this.teamComponent ??= this.GetComponentInChildren<TeamComponent>();
            this.shootComponent ??= this.GetComponentInChildren<BulletShootComponent>();
        }

        [Inject]
        private void Construct(IEnemyTarget target, EnemyMoveAgent moveAgent,
            Func<float, TickableTimer> timerFactory)
        {
            this.m_target = target;
            this.m_moveAgent = moveAgent;
            this.m_shootTimer = timerFactory.Invoke(this.shootIntervalSec);
        }

        void IStartGameListener.OnStartGame()
        {
            this.m_shootTimer.OnElapsed += this.OnTimerElapsed;
        }

        void IFinishGameListener.OnFinishGame()
        {
            this.m_shootTimer.OnElapsed -= this.OnTimerElapsed;
        }

        void ITickPhysicsListener.OnTickPhysics(float deltaTime)
        {
            if (this.CanTickLogic())
            {
                this.m_shootTimer.Tick(deltaTime);
            }
        }

        private bool CanTickLogic()
        {
            return this.m_moveAgent.IsReached
                   && this.m_target is { IsDead: false };
        }

        private void OnTimerElapsed()
        {
            this.m_shootTimer.Reset();

            this.shootComponent.ShootToPosition(
                team: this.teamComponent.Team,
                position: this.m_target!.Position);
        }
    }
}