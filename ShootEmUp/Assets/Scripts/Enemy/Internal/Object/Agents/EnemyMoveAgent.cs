using UnityEngine;
using ShootEmUp.Framework;

namespace ShootEmUp.Enemy
{
    internal sealed class EnemyMoveAgent : MonoBehaviour, ITickPhysicsListener
    {
        public bool IsReached { get; private set; }

        [Header("Settings")]
        [SerializeField, Min(0)]
        private float accuracy = 0.25f;

        [Header("References")]
        [SerializeField]
        private MoveComponent moveComponent;

        private Vector2 m_destination;

        private void OnValidate()
        {
            this.moveComponent ??= this.GetComponentInChildren<MoveComponent>();
        }

        public void SetDestination(Vector2 endPoint)
        {
            this.m_destination = endPoint;
            this.IsReached = false;
        }

        void ITickPhysicsListener.OnTickPhysics(float deltaTime)
        {
            if (this.IsReached)
            {
                return;
            }

            var distVector = this.m_destination - (Vector2) this.transform.position;
            var distance = distVector.magnitude;

            if (distance <= this.accuracy)
            {
                this.IsReached = true;
                return;
            }

            var direction = distVector / distance;
            var velocity = direction * deltaTime;
            this.moveComponent.MoveWithVelocity(velocity);
        }
    }
}