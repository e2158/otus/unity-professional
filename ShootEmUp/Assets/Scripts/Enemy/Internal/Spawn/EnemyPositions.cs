using System.Linq;
using UnityEngine;

namespace ShootEmUp.Enemy
{
    internal sealed class EnemyPositions : MonoBehaviour
    {
        [SerializeField]
        private Transform[] positions;

        private void OnValidate()
        {
            if (this.positions.IsNullOrEmpty())
            {
                this.PopulateArray();
            }
        }

        public Vector3 PickRandom()
        {
            return this.positions.RandomElement().position;
        }

        [ContextMenu(nameof(PopulateArray))]
        private void PopulateArray()
        {
            this.positions = this.transform.GetAllChildren().ToArray();
        }
    }
}