using System;
using UnityEngine;
using JetBrains.Annotations;
using System.Collections.Generic;

namespace ShootEmUp.Enemy
{
    internal sealed class EnemySpawner
    {
        public event Action Spawned;
        public event Action Despawned;

        public int TotalSpawnedCount { get; private set; }

        private readonly EnemyPool m_enemyPool;
        private readonly EnemyPositions m_spawnPositions;
        private readonly EnemyPositions m_attackPositions;

        private readonly HashSet<EnemyObject> m_activeEnemies = new();

        public EnemySpawner(EnemyPool pool, EnemyPositions spawnPositions, EnemyPositions attackPositions)
        {
            this.m_enemyPool = pool;
            this.m_spawnPositions = spawnPositions;
            this.m_attackPositions = attackPositions;
        }

        public void Spawn()
        {
            var enemy = this.m_enemyPool.Get();

            if (this.m_activeEnemies.Add(enemy))
            {
                enemy.OnDead += this.Despawn;
                enemy.Init(
                    this.m_spawnPositions.PickRandom(),
                    this.m_attackPositions.PickRandom());

                this.TotalSpawnedCount++;
                this.Spawned?.Invoke();
            }
        }

        private void Despawn([NotNull] GameObject enemy)
        {
            this.Despawn(enemy.GetComponent<EnemyObject>());
        }

        private void Despawn([NotNull] EnemyObject enemy)
        {
            if (this.m_activeEnemies.Remove(enemy))
            {
                enemy.OnDead -= this.Despawn;
                this.m_enemyPool.Release(enemy);
                this.Despawned?.Invoke();
            }
        }
    }
}