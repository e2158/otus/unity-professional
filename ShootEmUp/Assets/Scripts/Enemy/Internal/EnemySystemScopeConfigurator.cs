﻿using VContainer;
using UnityEngine;

namespace ShootEmUp.Enemy
{
    internal sealed class EnemySystemScopeConfigurator : ScopeConfigurator
    {
        [SerializeField]
        private EnemyPool enemyPool;

        [SerializeField]
        private EnemySystem enemySystem;

        [SerializeField]
        private EnemyPositions spawnPositions;

        [SerializeField]
        private EnemyPositions attackPositions;

        private void OnValidate()
        {
            this.enemyPool ??= FindObjectOfType<EnemyPool>();
            this.enemySystem ??= FindObjectOfType<EnemySystem>();
        }

        public override void Configure(IContainerBuilder builder)
        {
            builder.RegisterFactory<float, TickableTimer>(interval => new TickableTimer(interval));

            // VContainer не умеет регистрировать инстансы одного типа с разным Id
            builder.RegisterInstance(
                new EnemySpawner(this.enemyPool, this.spawnPositions, this.attackPositions));

            builder.RegisterInstance(this.enemySystem).AsImplementedInterfaces().AsSelf();
        }
    }
}