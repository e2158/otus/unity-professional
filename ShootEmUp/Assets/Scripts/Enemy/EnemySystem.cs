using System;
using VContainer;
using UnityEngine;
using System.Collections;
using ShootEmUp.Framework;

namespace ShootEmUp.Enemy
{
    public sealed class EnemySystem : MonoBehaviour, IStartGameListener, IFinishGameListener
    {
        public event Action OnAllEnemiesDestroyed;

        [Header("Settings")]
        [SerializeField, Min(0)]
        private int enemiesCount = 7;

        [SerializeField, Min(0)]
        private float spawnCooldownSec = 1;

        private EnemySpawner m_enemySpawner;
        private int m_destroyedEnemiesCounter;

        [Inject]
        private void Construct(EnemySpawner enemySpawner)
        {
            this.m_enemySpawner = enemySpawner;
        }

        void IStartGameListener.OnStartGame()
        {
            this.m_destroyedEnemiesCounter = default;
            this.m_enemySpawner.Despawned += this.IncreaseDestroyedEnemiesCounter;

            this.StartCoroutine(this.SpawningRoutine(this.enemiesCount));
        }

        void IFinishGameListener.OnFinishGame()
        {
            this.m_enemySpawner.Despawned -= this.IncreaseDestroyedEnemiesCounter;

            this.StopAllCoroutines();
        }

        private IEnumerator SpawningRoutine(int spawnCount)
        {
            var cooldownWait = new WaitForSeconds(this.spawnCooldownSec);

            while (this.m_enemySpawner.TotalSpawnedCount < spawnCount)
            {
                yield return cooldownWait;
                this.m_enemySpawner.Spawn();
            }
        }

        private void IncreaseDestroyedEnemiesCounter()
        {
            this.m_destroyedEnemiesCounter++;

            if (this.m_destroyedEnemiesCounter == this.enemiesCount)
            {
                this.OnAllEnemiesDestroyed?.Invoke();
            }
        }
    }
}