﻿using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace ShootEmUp
{
    internal sealed class GameScope : LifetimeScope
    {
        [SerializeField]
        private ScopeConfigurator[] configurators;

        private void OnValidate()
        {
            if (this.configurators.IsNullOrEmpty())
                this.UpdateConfiguratorsList();
        }

        protected override void Configure(IContainerBuilder builder)
        {
            this.RegisterControllers(builder);
            this.RegisterConfigurators(builder);

            builder.RegisterBuildCallback(this.OnBuilt);
        }

        private void RegisterControllers(IContainerBuilder builder)
        {
            builder.Register<GameRunnerController>(Lifetime.Singleton)
                .AsImplementedInterfaces().AsSelf();

            builder.Register<UiGameManagerController>(Lifetime.Singleton)
                .AsImplementedInterfaces().AsSelf();
        }

        private void RegisterConfigurators(IContainerBuilder builder)
        {
            foreach (var configurator in this.configurators)
            {
                configurator.Configure(builder);
            }
        }

        private void OnBuilt(IObjectResolver resolver)
        {
            resolver.Resolve<GameRunnerController>();
            resolver.Resolve<UiGameManagerController>();
        }

        [ContextMenu(nameof(UpdateConfiguratorsList))]
        private void UpdateConfiguratorsList()
        {
            this.configurators = FindObjectsOfType<ScopeConfigurator>();
        }
    }
}