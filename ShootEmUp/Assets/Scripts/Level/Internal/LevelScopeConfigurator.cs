﻿using VContainer;
using UnityEngine;

namespace ShootEmUp.Level
{
    internal sealed class LevelScopeConfigurator : ScopeConfigurator
    {
        [SerializeField]
        private LevelBounds levelBounds;

        [SerializeField]
        private LevelBackground levelBackground;

        private void OnValidate()
        {
            this.levelBounds ??= FindObjectOfType<LevelBounds>();
            this.levelBackground ??= FindObjectOfType<LevelBackground>();
        }

        public override void Configure(IContainerBuilder builder)
        {
            builder.RegisterInstance(this.levelBounds);
            builder.RegisterInstance(this.levelBackground).AsImplementedInterfaces();
        }
    }
}