using UnityEngine;
using ShootEmUp.Framework;

namespace ShootEmUp.Level
{
    internal sealed class LevelBackground : MonoBehaviour, IStartGameListener, ITickGameListener
    {
        [SerializeField]
        private float startPositionY;

        [SerializeField]
        private float endPositionY;

        [SerializeField]
        private float movingSpeedY;

        private float m_positionX;
        private float m_positionZ;
        private Transform m_myTransform;

        void IStartGameListener.OnStartGame()
        {
            this.m_myTransform = this.transform;

            var position = this.m_myTransform.position;
            this.m_positionX = position.x;
            this.m_positionZ = position.z;
        }

        void ITickGameListener.OnTickGame(float deltaTime)
        {
            if (this.HasReachedEndPosition())
            {
                this.TeleportToStartPosition();
            }

            var shiftValue = this.movingSpeedY * deltaTime;
            this.ShiftDown(shiftValue);
        }

        private bool HasReachedEndPosition()
        {
            return this.m_myTransform.position.y <= this.endPositionY;
        }

        private void ShiftDown(float shiftValue)
        {
            this.SetPosition(this.m_myTransform.position.y - shiftValue);
        }

        private void TeleportToStartPosition()
        {
            this.SetPosition(this.startPositionY);
        }

        private void SetPosition(float y)
        {
            this.m_myTransform.position = new Vector3(this.m_positionX, y, this.m_positionZ);
        }
    }
}