using UnityEngine;

namespace ShootEmUp.Level
{
    public sealed class LevelBounds : MonoBehaviour
    {
        [SerializeField]
        private Transform leftBorder;

        [SerializeField]
        private Transform rightBorder;

        [SerializeField]
        private Transform downBorder;

        [SerializeField]
        private Transform topBorder;

        public bool Contains(Vector3 position)
        {
            return position.x > this.leftBorder.position.x
                   && position.x < this.rightBorder.position.x
                   && position.y > this.downBorder.position.y
                   && position.y < this.topBorder.position.y;
        }
    }
}