﻿using System;
using UnityEngine;
using JetBrains.Annotations;
using ShootEmUp.Character;

namespace ShootEmUp.Gameplay
{
    internal sealed class CharacterDeadGameOverTrigger : IGameOverTrigger, IDisposable
    {
        public event Action OnTriggered;

        [NotNull] private readonly CharacterObject m_character;

        public CharacterDeadGameOverTrigger([NotNull] CharacterObject character)
        {
            this.m_character = character;
            this.m_character.OnDead += this.OnCharacterDead;
        }

        public void Dispose()
        {
            this.m_character.OnDead -= this.OnCharacterDead;
        }

        private void OnCharacterDead(GameObject _)
        {
            this.OnTriggered?.Invoke();
        }
    }
}