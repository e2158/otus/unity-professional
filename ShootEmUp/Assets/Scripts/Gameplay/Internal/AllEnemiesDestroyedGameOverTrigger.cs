﻿using System;
using JetBrains.Annotations;
using ShootEmUp.Enemy;

namespace ShootEmUp.Gameplay
{
    internal sealed class AllEnemiesDestroyedGameOverTrigger : IGameOverTrigger, IDisposable
    {
        public event Action OnTriggered;

        [NotNull] private readonly EnemySystem m_enemySystem;

        public AllEnemiesDestroyedGameOverTrigger([NotNull] EnemySystem enemySystem)
        {
            this.m_enemySystem = enemySystem;
            this.m_enemySystem.OnAllEnemiesDestroyed += this.OnAllEnemiesDestroyed;
        }

        public void Dispose()
        {
            this.m_enemySystem.OnAllEnemiesDestroyed -= this.OnAllEnemiesDestroyed;
        }

        private void OnAllEnemiesDestroyed()
        {
            this.OnTriggered?.Invoke();
        }
    }
}