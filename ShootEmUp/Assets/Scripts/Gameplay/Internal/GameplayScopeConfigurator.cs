﻿using VContainer;
using UnityEngine;

namespace ShootEmUp.Gameplay
{
    internal sealed class GameplayScopeConfigurator : ScopeConfigurator
    {
        [SerializeField]
        private TimeScaleSystem timeScaleSystem;

        private void OnValidate()
        {
            this.timeScaleSystem ??= FindObjectOfType<TimeScaleSystem>();
        }

        public override void Configure(IContainerBuilder builder)
        {
            builder.RegisterInstance(this.timeScaleSystem).AsImplementedInterfaces();
            builder.Register<CharacterDeadGameOverTrigger>(Lifetime.Singleton).AsImplementedInterfaces();
            builder.Register<AllEnemiesDestroyedGameOverTrigger>(Lifetime.Singleton).AsImplementedInterfaces();
        }
    }
}