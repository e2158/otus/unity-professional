﻿using System;

namespace ShootEmUp.Gameplay
{
    public interface IGameOverTrigger
    {
        event Action OnTriggered;
    }
}