﻿using UnityEngine;
using ShootEmUp.Framework;

namespace ShootEmUp.Gameplay
{
    public sealed class TimeScaleSystem : MonoBehaviour, IStartGameListener, IFinishGameListener, IPauseGameListener, IResumeGameListener
    {
        [SerializeField, Min(0)]
        private float initialTimeScale = 0;

        [SerializeField, Min(0)]
        private float gameTimeScale = 1;

        [SerializeField, Min(0)]
        private float pauseTimeScale = 0;

        private void Awake()
        {
            Time.timeScale = this.initialTimeScale;
        }

        void IStartGameListener.OnStartGame()
        {
            Time.timeScale = this.gameTimeScale;
        }

        void IFinishGameListener.OnFinishGame()
        {
            Time.timeScale = this.pauseTimeScale;
        }

        void IPauseGameListener.OnPauseGame()
        {
            Time.timeScale = this.pauseTimeScale;
        }

        void IResumeGameListener.OnResumeGame()
        {
            Time.timeScale = this.gameTimeScale;
        }
    }
}