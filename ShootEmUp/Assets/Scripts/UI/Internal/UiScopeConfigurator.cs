﻿using VContainer;
using UnityEngine;

namespace ShootEmUp.UI
{
    internal sealed class UiScopeConfigurator : ScopeConfigurator
    {
        [SerializeField]
        private UiMainMenu mainMenu;

        [SerializeField]
        private UiGameMenu gameMenu;

        [SerializeField]
        private UiFinishMenu finishMenu;

        private void OnValidate()
        {
            this.mainMenu ??= FindObjectOfType<UiMainMenu>();
            this.gameMenu ??= FindObjectOfType<UiGameMenu>();
            this.finishMenu ??= FindObjectOfType<UiFinishMenu>();
        }

        public override void Configure(IContainerBuilder builder)
        {
            builder.RegisterInstance(this.mainMenu);
            builder.RegisterInstance(this.gameMenu);
            builder.RegisterInstance(this.finishMenu);

            builder.Register<UiGameManager>(Lifetime.Singleton)
                .AsImplementedInterfaces().AsSelf();
        }
    }
}