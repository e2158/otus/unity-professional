﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace ShootEmUp.UI
{
    internal sealed class UiGameMenu : MonoBehaviour
    {
        public UnityEvent OnButtonClicked
        {
            get { return this.button.onClick; }
        }

        [Header("Settings")]
        [SerializeField]
        private Sprite pauseIcon;

        [SerializeField]
        private Sprite resumeIcon;

        [Header("References")]
        [SerializeField]
        private Button button;

        [SerializeField]
        private Image icon;

        private void OnValidate()
        {
            this.icon ??= this.GetComponentInChildren<Image>();
            this.button ??= this.GetComponentInChildren<Button>();
        }

        public void SetResumeIcon()
        {
            this.icon.sprite = this.resumeIcon;
        }

        public void SetPauseIcon()
        {
            this.icon.sprite = this.pauseIcon;
        }
    }
}