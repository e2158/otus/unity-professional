﻿using TMPro;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace ShootEmUp.UI
{
    internal sealed class UiCountdownLayout : MonoBehaviour
    {
        public UnityEvent OnCompleted
        {
            get { return this.completed; }
        }

        [Header("Settings")]
        [SerializeField]
        private string[] countElements;

        [SerializeField, Min(0)]
        private float countIterationDurationSec = 1;

        [Header("References")]
        [SerializeField]
        private TMP_Text countTextComponent;

        [Header("Events")]
        [SerializeField]
        private UnityEvent completed;

        private void OnValidate()
        {
            this.countTextComponent ??= this.GetComponentInChildren<TMP_Text>();
        }

        private void OnEnable()
        {
            this.StartCoroutine(this.CountdownRoutine());
        }

        private void OnDisable()
        {
            this.StopAllCoroutines();
        }

        private IEnumerator CountdownRoutine()
        {
            var countdownWait = new WaitForSecondsRealtime(this.countIterationDurationSec);

            foreach (var countElement in this.countElements)
            {
                this.countTextComponent.text = countElement;
                yield return countdownWait;
            }

            this.completed?.Invoke();
        }
    }
}