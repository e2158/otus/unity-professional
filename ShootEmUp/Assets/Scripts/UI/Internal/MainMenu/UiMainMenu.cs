﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace ShootEmUp.UI
{
    internal sealed class UiMainMenu : MonoBehaviour
    {
        public UnityEvent OnCountdownCompleted
        {
            get { return this.countdownLayout.OnCompleted; }
        }

        [SerializeField]
        private UiStartLayout startLayout;

        [SerializeField]
        private UiCountdownLayout countdownLayout;

        private void OnValidate()
        {
            this.startLayout ??= this.GetComponentInChildren<UiStartLayout>();
            this.countdownLayout ??= this.GetComponentInChildren<UiCountdownLayout>();
        }

        private void OnEnable()
        {
            this.startLayout.OnStartButtonClicked.AddListener(this.OnStartButtonClicked);

            this.startLayout.gameObject.SetActive(true);
            this.countdownLayout.gameObject.SetActive(false);
        }

        private void OnDisable()
        {
            this.startLayout.OnStartButtonClicked.AddListener(this.OnStartButtonClicked);

            this.startLayout.gameObject.SetActive(false);
            this.countdownLayout.gameObject.SetActive(false);
        }

        private void OnStartButtonClicked()
        {
            this.startLayout.gameObject.SetActive(false);
            this.countdownLayout.gameObject.SetActive(true);
        }
    }
}