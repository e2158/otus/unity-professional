﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace ShootEmUp.UI
{
    internal sealed class UiStartLayout : MonoBehaviour
    {
        public UnityEvent OnStartButtonClicked
        {
            get { return this.startButton.onClick; }
        }

        [SerializeField]
        private Button startButton;

        private void OnValidate()
        {
            this.startButton ??= this.GetComponentInChildren<Button>();
        }
    }
}