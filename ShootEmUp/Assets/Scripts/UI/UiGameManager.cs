﻿using UnityEngine.Events;
using JetBrains.Annotations;

namespace ShootEmUp.UI
{
    public interface IUiEvents
    {
        UnityEvent OnCountdownCompleted { get; }
        UnityEvent OnPauseResumeButtonClicked { get; }
    }

    public sealed class UiGameManager : IUiEvents
    {
        public UnityEvent OnCountdownCompleted
        {
            get { return this.m_mainMenu.OnCountdownCompleted; }
        }

        public UnityEvent OnPauseResumeButtonClicked
        {
            get { return this.m_gameMenu.OnButtonClicked; }
        }

        [NotNull] private readonly UiMainMenu m_mainMenu;
        [NotNull] private readonly UiGameMenu m_gameMenu;
        [NotNull] private readonly UiFinishMenu m_finishMenu;

        internal UiGameManager([NotNull] UiMainMenu mainMenu,
            [NotNull] UiGameMenu gameMenu, [NotNull] UiFinishMenu finishMenu)
        {
            this.m_mainMenu = mainMenu;
            this.m_gameMenu = gameMenu;
            this.m_finishMenu = finishMenu;
        }

        public void ShowMainMenu()
        {
            this.m_mainMenu.gameObject.SetActive(true);
            this.m_gameMenu.gameObject.SetActive(false);
            this.m_finishMenu.gameObject.SetActive(false);
        }

        public void ShowPlayingGameMenu()
        {
            this.ShowGameMenu();
            this.m_gameMenu.SetPauseIcon();
        }

        public void ShowPausedGameMenu()
        {
            this.ShowGameMenu();
            this.m_gameMenu.SetResumeIcon();
        }

        public void ShowFinishMenu()
        {
            this.m_mainMenu.gameObject.SetActive(false);
            this.m_gameMenu.gameObject.SetActive(false);
            this.m_finishMenu.gameObject.SetActive(true);
        }

        private void ShowGameMenu()
        {
            this.m_mainMenu.gameObject.SetActive(false);
            this.m_gameMenu.gameObject.SetActive(true);
            this.m_finishMenu.gameObject.SetActive(false);
        }
    }
}