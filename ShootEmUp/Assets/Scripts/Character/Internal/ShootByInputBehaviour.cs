﻿using JetBrains.Annotations;
using ShootEmUp.Bullet;
using ShootEmUp.Input;
using ShootEmUp.Framework;

namespace ShootEmUp.Character
{
    internal sealed class ShootByInputBehaviour : IStartGameListener, IFinishGameListener
    {
        private readonly TeamComponent m_teamComponent;
        private readonly BulletShootComponent m_shootComponent;
        private readonly IInputShootSource m_inputShootSource;

        public ShootByInputBehaviour(
            [NotNull] TeamComponent teamComponent,
            [NotNull] BulletShootComponent shootComponent,
            [NotNull] IInputShootSource inputShootSource)
        {
            this.m_teamComponent = teamComponent;
            this.m_shootComponent = shootComponent;
            this.m_inputShootSource = inputShootSource;
        }

        void IStartGameListener.OnStartGame()
        {
            this.m_inputShootSource.OnShootTriggered += this.OnShootTriggered;
        }

        void IFinishGameListener.OnFinishGame()
        {
            this.m_inputShootSource.OnShootTriggered -= this.OnShootTriggered;
        }

        private void OnShootTriggered()
        {
            this.m_shootComponent.ShootForward(this.m_teamComponent.Team);
        }
    }
}