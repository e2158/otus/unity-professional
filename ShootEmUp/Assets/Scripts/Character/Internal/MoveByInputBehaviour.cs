﻿using UnityEngine;
using JetBrains.Annotations;
using ShootEmUp.Input;
using ShootEmUp.Framework;

namespace ShootEmUp.Character
{
    internal sealed class MoveByInputBehaviour : ITickPhysicsListener
    {
        private readonly MoveComponent m_moveComponent;
        private readonly IInputDirectionSource m_inputDirectionSource;

        public MoveByInputBehaviour(
            [NotNull] MoveComponent moveComponent,
            [NotNull] IInputDirectionSource inputDirectionSource)
        {
            this.m_moveComponent = moveComponent;
            this.m_inputDirectionSource = inputDirectionSource;
        }

        void ITickPhysicsListener.OnTickPhysics(float deltaTime)
        {
            var velocity = new Vector2(this.m_inputDirectionSource.HorizontalDirection, 0);
            this.m_moveComponent.MoveWithVelocity(velocity * deltaTime);
        }
    }
}