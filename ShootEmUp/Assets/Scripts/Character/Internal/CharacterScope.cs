﻿using UnityEngine;
using VContainer;
using VContainer.Unity;
using ShootEmUp.Bullet;
using ShootEmUp.Framework;

namespace ShootEmUp.Character
{
    internal sealed class CharacterScope : LifetimeScope
    {
        [SerializeField] private CharacterObject character;

        private void OnValidate()
        {
            this.character ??= this.GetComponentInChildren<CharacterObject>();
        }

        private void Start()
        {
            this.RegisterInGameEvents();
        }

        protected override void OnDestroy()
        {
            this.UnregisterInGameEvents();
            base.OnDestroy();
        }

        protected override void Configure(IContainerBuilder builder)
        {
            this.RegisterComponents(builder);
            this.RegisterBehaviours(builder);
        }

        private void RegisterComponents(IContainerBuilder builder)
        {
            Transform transform = this.transform;
            builder.RegisterComponentInHierarchy<TeamComponent>().UnderTransform(transform);
            builder.RegisterComponentInHierarchy<MoveComponent>().UnderTransform(transform);
            builder.RegisterComponentInHierarchy<BulletShootComponent>().UnderTransform(transform);
            builder.RegisterComponentInHierarchy<HitPointsComponent>().UnderTransform(transform);
        }

        private void RegisterBehaviours(IContainerBuilder builder)
        {
            builder.Register<MoveByInputBehaviour>(Lifetime.Scoped);
            builder.Register<ShootByInputBehaviour>(Lifetime.Scoped);
        }

        private void RegisterInGameEvents()
        {
            var gameEventsSubscriber = this.Container.Resolve<GameEventsSubscriber>();
            gameEventsSubscriber.AddListener(this.Container.Resolve<MoveByInputBehaviour>());
            gameEventsSubscriber.AddListener(this.Container.Resolve<ShootByInputBehaviour>());
        }

        private void UnregisterInGameEvents()
        {
            var gameEventsSubscriber = this.Container.Resolve<GameEventsSubscriber>();
            gameEventsSubscriber.RemoveListener(this.Container.Resolve<MoveByInputBehaviour>());
            gameEventsSubscriber.RemoveListener(this.Container.Resolve<ShootByInputBehaviour>());
        }
    }
}