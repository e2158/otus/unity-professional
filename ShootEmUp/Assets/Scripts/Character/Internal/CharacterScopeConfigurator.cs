﻿using UnityEngine;
using VContainer;

namespace ShootEmUp.Character
{
    internal sealed class CharacterScopeConfigurator : ScopeConfigurator
    {
        [SerializeField]
        private CharacterObject character;

        private void OnValidate()
        {
            this.character ??= FindObjectOfType<CharacterObject>();
        }

        public override void Configure(IContainerBuilder builder)
        {
            builder.RegisterInstance(this.character).AsImplementedInterfaces().AsSelf();
        }
    }
}