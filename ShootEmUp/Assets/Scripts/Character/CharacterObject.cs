﻿using System;
using UnityEngine;
using ShootEmUp.Enemy;
using ShootEmUp.Bullet;

namespace ShootEmUp.Character
{
    public sealed class CharacterObject : MonoBehaviour, IEnemyTarget
    {
        public event Action<GameObject> OnDead
        {
            add { this.hitPointsComponent.OnHitPointsEmpty += value; }
            remove { this.hitPointsComponent.OnHitPointsEmpty -= value; }
        }

        public bool IsDead
        {
            get { return this.hitPointsComponent.IsHitPointsEmpty; }
        }

        public Vector2 Position
        {
            get { return this.transform.position; }
        }

        [SerializeField]
        private MoveComponent moveComponent;

        [SerializeField]
        private TeamComponent teamComponent;

        [SerializeField]
        private BulletShootComponent shootComponent;

        [SerializeField]
        private HitPointsComponent hitPointsComponent;

        private void OnValidate()
        {
            this.moveComponent ??= this.GetComponentInChildren<MoveComponent>();
            this.teamComponent ??= this.GetComponentInChildren<TeamComponent>();
            this.shootComponent ??= this.GetComponentInChildren<BulletShootComponent>();
            this.hitPointsComponent ??= this.GetComponentInChildren<HitPointsComponent>();
        }
    }
}