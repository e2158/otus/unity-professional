using System;
using UnityEngine;
using ShootEmUp.Framework;

namespace ShootEmUp.Input
{
    internal sealed class InputManager : IInputShootSource, IInputDirectionSource, ITickGameListener
    {
        public event Action OnShootTriggered;

        public float HorizontalDirection { get; private set; }

        void ITickGameListener.OnTickGame(float _)
        {
            this.HandleFireInput();
            this.HandleDirectionInput();
        }

        private void HandleFireInput()
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.Space))
            {
                this.OnShootTriggered?.Invoke();
            }
        }

        private void HandleDirectionInput()
        {
            if (UnityEngine.Input.GetKey(KeyCode.LeftArrow))
            {
                this.HorizontalDirection = -1;
            }
            else if (UnityEngine.Input.GetKey(KeyCode.RightArrow))
            {
                this.HorizontalDirection = 1;
            }
            else
            {
                this.HorizontalDirection = 0;
            }
        }
    }
}