﻿using VContainer;

namespace ShootEmUp.Input
{
    internal sealed class InputScopeConfigurator : ScopeConfigurator
    {
        public override void Configure(IContainerBuilder builder)
        {
            builder.Register<InputManager>(Lifetime.Singleton).AsImplementedInterfaces();
        }
    }
}