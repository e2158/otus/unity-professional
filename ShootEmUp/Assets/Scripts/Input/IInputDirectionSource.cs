﻿namespace ShootEmUp.Input
{
    public interface IInputDirectionSource
    {
        float HorizontalDirection { get; }
    }
}