﻿using System;

namespace ShootEmUp.Input
{
    public interface IInputShootSource
    {
        event Action OnShootTriggered;
    }
}