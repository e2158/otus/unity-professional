﻿using UnityEngine;
using System.Collections.Generic;

namespace ShootEmUp
{
    public static class ReadOnlyListExtensions
    {
        public static T RandomElement<T>(this IReadOnlyList<T> list)
        {
            var index = Random.Range(0, list.Count);
            return list[index];
        }

        public static bool IsNullOrEmpty<T>(this IReadOnlyList<T> list)
        {
            return list == null || list.Count == 0;
        }
    }
}