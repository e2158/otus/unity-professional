﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace ShootEmUp
{
    public static class TransformExtensions
    {
        public static IEnumerable<Transform> GetAllChildren(this Transform transform)
        {
            return Enumerable.Range(0, transform.childCount).Select(transform.GetChild);
        }
    }
}