﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace ShootEmUp
{
    public static class EnumerableExtensions
    {
        public static void ForEach<T>([NotNull] this IEnumerable<T> enumerable, [NotNull] Action<T> action)
        {
            foreach (T element in enumerable)
            {
                action(element);
            }
        }
    }
}