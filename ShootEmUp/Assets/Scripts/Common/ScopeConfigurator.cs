﻿using UnityEngine;
using VContainer;

namespace ShootEmUp
{
    public abstract class ScopeConfigurator : MonoBehaviour
    {
        public abstract void Configure(IContainerBuilder builder);
    }
}