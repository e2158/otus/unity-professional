﻿using UnityEngine;
using System.Collections.Generic;

namespace ShootEmUp
{
    public abstract class MonoPool<TObject> : MonoBehaviour
        where TObject : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField]
        private int initialCount = 7;

        [Header("References")]
        [SerializeField]
        private Transform poolContainer;

        [SerializeField]
        private Transform worldContainer;

        private readonly Queue<TObject> m_pool = new();

        private void Awake()
        {
            for (var i = 0; i < this.initialCount; i++)
            {
                var obj = this.CreateObject();
                this.Release(obj);
            }
        }

        public TObject Get()
        {
            if (!this.m_pool.TryDequeue(out var obj))
            {
                obj = this.CreateObject();
            }

            obj.transform.SetParent(this.worldContainer);
            this.OnGet(obj);

            return obj;
        }

        public void Release(TObject obj)
        {
            obj.transform.SetParent(this.poolContainer);
            this.m_pool.Enqueue(obj);
            this.OnRelease(obj);
        }

        protected abstract TObject CreateObject();

        protected virtual void OnGet(TObject obj) { }
        protected virtual void OnRelease(TObject obj) { }
    }
}