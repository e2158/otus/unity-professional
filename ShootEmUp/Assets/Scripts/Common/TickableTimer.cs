﻿using System;

namespace ShootEmUp
{
    public sealed class TickableTimer
    {
        public event Action OnElapsed;

        private readonly float m_interval;
        private float m_elapsedTime;

        public bool IsElapsed
        {
            get { return this.m_elapsedTime >= this.m_interval; }
        }

        public TickableTimer(float interval)
        {
            this.m_interval = interval;
        }

        public void Tick(float deltaTime)
        {
            if (this.IsElapsed)
            {
                return;
            }

            this.m_elapsedTime += deltaTime;

            if (this.IsElapsed)
            {
                this.OnElapsed?.Invoke();
            }
        }

        public void Reset()
        {
            this.m_elapsedTime = default;
        }
    }
}