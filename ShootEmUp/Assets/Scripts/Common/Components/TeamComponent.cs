using UnityEngine;

namespace ShootEmUp
{
    public enum Team
    {
        Player = 0,
        Enemy = 1
    }

    public sealed class TeamComponent : MonoBehaviour
    {
        public Team Team
        {
            get { return this.team; }
        }

        [SerializeField]
        private Team team;
    }
}