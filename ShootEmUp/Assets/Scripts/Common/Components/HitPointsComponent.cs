using System;
using UnityEngine;

namespace ShootEmUp
{
    public sealed class HitPointsComponent : MonoBehaviour
    {
        private const int MIN_HIT_POINTS = 0;

        public event Action<GameObject> OnHitPointsEmpty;

        [SerializeField]
        private int hitPoints;

        public bool IsHitPointsEmpty
        {
            get { return this.hitPoints == MIN_HIT_POINTS; }
        }

        public void TakeDamage(int damage)
        {
            if (this.IsHitPointsEmpty)
            {
                throw new InvalidOperationException("Can't damage with empty hitPoints");
            }

            this.hitPoints =
                Mathf.Max(this.hitPoints - damage, MIN_HIT_POINTS);

            if (this.IsHitPointsEmpty)
            {
                this.OnHitPointsEmpty?.Invoke(this.gameObject);
            }
        }
    }
}