using UnityEngine;

namespace ShootEmUp
{
    public sealed class MoveComponent : MonoBehaviour
    {
        [Header("References")]
        [SerializeField]
        private new Rigidbody2D rigidbody2D;

        [Header("Settings")]
        [SerializeField]
        private float speed = 5.0f;

        private void OnValidate()
        {
            this.rigidbody2D ??= this.GetComponentInChildren<Rigidbody2D>();
        }

        public void MoveWithVelocity(Vector2 velocity)
        {
            var nextPosition = this.rigidbody2D.position + velocity * this.speed;
            this.rigidbody2D.MovePosition(nextPosition);
        }
    }
}