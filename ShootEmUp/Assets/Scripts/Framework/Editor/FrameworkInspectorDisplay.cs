﻿using System;
using System.Linq;
using System.Collections.Generic;
using VContainer;
using UnityEngine;
// ReSharper disable NotAccessedField.Local

namespace ShootEmUp.Framework.Editor
{
    internal sealed class FrameworkInspectorDisplay : MonoBehaviour
    {
        [Header("Info")]

        [SerializeField]
        private GameStatus currentStatus;

        [SerializeField]
        private string[] tickGameListeners = Array.Empty<string>();

        [SerializeField]
        private string[] tickPhysicsListeners = Array.Empty<string>();

        [SerializeField]
        private string[] startGameListeners = Array.Empty<string>();

        [SerializeField]
        private string[] finishGameListeners = Array.Empty<string>();

        [SerializeField]
        private string[] pauseGameListeners = Array.Empty<string>();

        [SerializeField]
        private string[] resumeGameListeners = Array.Empty<string>();

        private IReadOnlyGameRunner m_gameRunner;
        private IReadOnlyGameEventsListeners m_gameEventsListeners;

        [Inject]
        private void Construct(IReadOnlyGameRunner gameRunner, IReadOnlyGameEventsListeners gameEventsListeners)
        {
            this.m_gameRunner = gameRunner;
            this.m_gameEventsListeners = gameEventsListeners;
        }

#if UNITY_EDITOR // т.к. это компонент для редактора. если в виде окна реализовать, то можно на уровне asmdef порешать
        private void Update()
        {
            this.currentStatus = this.m_gameRunner?.CurrentStatus ?? GameStatus.None;

            this.UpdateListenersInfo(ref this.tickGameListeners, this.m_gameEventsListeners.TickGameListeners);
            this.UpdateListenersInfo(ref this.tickPhysicsListeners, this.m_gameEventsListeners.TickPhysicsListeners);

            this.UpdateListenersInfo(ref this.startGameListeners, this.m_gameEventsListeners.StartGameListeners);
            this.UpdateListenersInfo(ref this.finishGameListeners, this.m_gameEventsListeners.FinishGameListeners);

            this.UpdateListenersInfo(ref this.pauseGameListeners, this.m_gameEventsListeners.PauseGameListeners);
            this.UpdateListenersInfo(ref this.resumeGameListeners, this.m_gameEventsListeners.ResumeGameListeners);
        }
#endif

        private void UpdateListenersInfo<TListener>(ref string[] infoArray, IReadOnlyList<TListener> listeners)
        {
            if (infoArray.Length != listeners.Count)
            {
                infoArray = listeners.Select(t => t.GetType().Name).ToArray();
            }
        }
    }
}