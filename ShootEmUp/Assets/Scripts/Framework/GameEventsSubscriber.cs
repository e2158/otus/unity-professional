﻿using System;
using UnityEngine;
using JetBrains.Annotations;

namespace ShootEmUp.Framework
{
    public sealed class GameEventsSubscriber
    {
        public event Action<ITickGameListener> OnTickGameListenerAdded;
        public event Action<ITickPhysicsListener> OnTickPhysicsListenerAdded;

        public event Action<IStartGameListener> OnStartGameListenerAdded;
        public event Action<IFinishGameListener> OnFinishGameListenerAdded;

        public event Action<IPauseGameListener> OnPauseGameListenerAdded;
        public event Action<IResumeGameListener> OnResumeGameListenerAdded;

        [NotNull] private readonly GameEventsListeners m_listeners;

        internal GameEventsSubscriber([NotNull] GameEventsListeners listeners)
        {
            this.m_listeners = listeners;
        }

        public void AddListener([NotNull] GameObject gameObject)
        {
            var behaviours = gameObject.GetComponents<MonoBehaviour>();
            foreach (var behaviour in behaviours)
            {
                this.AddListener(behaviour);
            }
        }

        public void RemoveListener([NotNull] GameObject gameObject)
        {
            var behaviours = gameObject.GetComponents<MonoBehaviour>();
            foreach (var behaviour in behaviours)
            {
                this.RemoveListener(behaviour);
            }
        }

        public void AddListener([NotNull] object listener)
        {
            if (listener is ITickGameListener tickGameListener)
            {
                this.m_listeners.TickGameListeners.Add(tickGameListener);
                this.OnTickGameListenerAdded?.Invoke(tickGameListener);
            }

            if (listener is ITickPhysicsListener tickPhysicsListener)
            {
                this.m_listeners.TickPhysicsListeners.Add(tickPhysicsListener);
                this.OnTickPhysicsListenerAdded?.Invoke(tickPhysicsListener);
            }


            if (listener is IStartGameListener startGameListener)
            {
                this.m_listeners.StartGameListeners.Add(startGameListener);
                this.OnStartGameListenerAdded?.Invoke(startGameListener);
            }

            if (listener is IFinishGameListener finishGameListener)
            {
                this.m_listeners.FinishGameListeners.Add(finishGameListener);
                this.OnFinishGameListenerAdded?.Invoke(finishGameListener);
            }


            if (listener is IPauseGameListener pauseGameListener)
            {
                this.m_listeners.PauseGameListeners.Add(pauseGameListener);
                this.OnPauseGameListenerAdded?.Invoke(pauseGameListener);
            }

            if (listener is IResumeGameListener resumeGameListener)
            {
                this.m_listeners.ResumeGameListeners.Add(resumeGameListener);
                this.OnResumeGameListenerAdded?.Invoke(resumeGameListener);
            }
        }

        public void RemoveListener([NotNull] object listener)
        {
            if (listener is ITickGameListener tickGameListener)
                this.m_listeners.TickGameListeners.Remove(tickGameListener);

            if (listener is ITickPhysicsListener tickPhysicsListener)
                this.m_listeners.TickPhysicsListeners.Remove(tickPhysicsListener);


            if (listener is IStartGameListener startGameListener)
                this.m_listeners.StartGameListeners.Remove(startGameListener);

            if (listener is IFinishGameListener finishGameListener)
                this.m_listeners.FinishGameListeners.Remove(finishGameListener);


            if (listener is IPauseGameListener pauseGameListener)
                this.m_listeners.PauseGameListeners.Remove(pauseGameListener);

            if (listener is IResumeGameListener resumeGameListener)
                this.m_listeners.ResumeGameListeners.Remove(resumeGameListener);
        }
    }
}