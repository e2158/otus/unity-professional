﻿using System.Collections.Generic;

namespace ShootEmUp.Framework
{
    internal sealed class GameEventsListeners : IReadOnlyGameEventsListeners
    {
        public List<ITickGameListener> TickGameListeners { get; } = new();
        public List<ITickPhysicsListener> TickPhysicsListeners { get; } = new();

        public List<IStartGameListener> StartGameListeners { get; } = new();
        public List<IFinishGameListener> FinishGameListeners { get; } = new();

        public List<IPauseGameListener> PauseGameListeners { get; } = new();
        public List<IResumeGameListener> ResumeGameListeners { get; } = new();

        IReadOnlyList<ITickGameListener> IReadOnlyGameEventsListeners.TickGameListeners
        {
            get { return this.TickGameListeners; }
        }
        IReadOnlyList<ITickPhysicsListener> IReadOnlyGameEventsListeners.TickPhysicsListeners
        {
            get { return this.TickPhysicsListeners; }
        }

        IReadOnlyList<IStartGameListener> IReadOnlyGameEventsListeners.StartGameListeners
        {
            get { return this.StartGameListeners; }
        }
        IReadOnlyList<IFinishGameListener> IReadOnlyGameEventsListeners.FinishGameListeners
        {
            get { return this.FinishGameListeners; }
        }

        IReadOnlyList<IPauseGameListener> IReadOnlyGameEventsListeners.PauseGameListeners
        {
            get { return this.PauseGameListeners; }
        }
        IReadOnlyList<IResumeGameListener> IReadOnlyGameEventsListeners.ResumeGameListeners
        {
            get { return this.ResumeGameListeners; }
        }
    }
}