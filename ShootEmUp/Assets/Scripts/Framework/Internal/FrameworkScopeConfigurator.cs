﻿using VContainer;

namespace ShootEmUp.Framework
{
    internal sealed class FrameworkScopeConfigurator : ScopeConfigurator
    {
        public override void Configure(IContainerBuilder builder)
        {
            builder.Register<GameRunner>(Lifetime.Singleton)
                .AsImplementedInterfaces().AsSelf();

            builder.Register<GameEventsListeners>(Lifetime.Singleton)
                .AsImplementedInterfaces().AsSelf();

            builder.Register<GameEventsListenersAddProcessor>(Lifetime.Singleton)
                .AsImplementedInterfaces().AsSelf();

            builder.Register<GameEventsInvoker>(Lifetime.Singleton);
            builder.Register<GameEventsSubscriber>(Lifetime.Singleton);
            builder.Register<GameEventsListenersRegistrar>(Lifetime.Singleton);

            builder.RegisterBuildCallback(this.OnBuilt);
        }

        private void OnBuilt(IObjectResolver resolver)
        {
            resolver.Resolve<GameEventsListenersAddProcessor>();
            resolver.Resolve<GameEventsListenersRegistrar>().Register();
        }
    }
}