﻿using System.Linq;
using JetBrains.Annotations;
using System.Collections.Generic;

namespace ShootEmUp.Framework
{
    internal sealed class GameEventsListenersRegistrar
    {
        [NotNull] private readonly GameEventsSubscriber m_subscriber;
        [NotNull, ItemNotNull] private readonly IEnumerable<object> m_listeners;

        public GameEventsListenersRegistrar(
            [NotNull] GameEventsSubscriber subscriber,
            [NotNull, ItemNotNull] IEnumerable<ITickGameListener> tickGameListeners,
            [NotNull, ItemNotNull] IEnumerable<ITickPhysicsListener> tickPhysicsListeners,
            [NotNull, ItemNotNull] IEnumerable<IStartGameListener> startGameListeners,
            [NotNull, ItemNotNull] IEnumerable<IFinishGameListener> finishGameListeners,
            [NotNull, ItemNotNull] IEnumerable<IPauseGameListener> pauseGameListeners,
            [NotNull, ItemNotNull] IEnumerable<IResumeGameListener> resumeGameListeners)
        {
            this.m_subscriber = subscriber;

            this.m_listeners = tickGameListeners.OfType<object>()
                .Union(tickPhysicsListeners)
                .Union(startGameListeners)
                .Union(finishGameListeners)
                .Union(pauseGameListeners)
                .Union(resumeGameListeners);
        }

        public void Register()
        {
            foreach (var listener in this.m_listeners)
            {
                this.m_subscriber.AddListener(listener);
            }
        }
    }
}