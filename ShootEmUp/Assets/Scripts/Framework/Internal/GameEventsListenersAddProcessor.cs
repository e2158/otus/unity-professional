﻿using System;
using JetBrains.Annotations;

namespace ShootEmUp.Framework
{
    internal sealed class GameEventsListenersAddProcessor : IDisposable
    {
        [NotNull] private readonly IReadOnlyGameRunner m_gameRunner;
        [NotNull] private readonly GameEventsSubscriber m_eventsSubscriber;

        public GameEventsListenersAddProcessor(
            [NotNull] IReadOnlyGameRunner gameRunner,
            [NotNull] GameEventsSubscriber eventsSubscriber)
        {
            this.m_eventsSubscriber = eventsSubscriber;
            this.m_gameRunner = gameRunner;

            this.m_eventsSubscriber.OnStartGameListenerAdded += this.OnStartGameListenerAdded;
            this.m_eventsSubscriber.OnFinishGameListenerAdded += this.OnFinishGameListenerAdded;

            this.m_eventsSubscriber.OnPauseGameListenerAdded += this.OnPauseGameListenerAdded;
            this.m_eventsSubscriber.OnResumeGameListenerAdded += this.OnResumeGameListenerAdded;
        }

        public void Dispose()
        {
            this.m_eventsSubscriber.OnStartGameListenerAdded -= this.OnStartGameListenerAdded;
            this.m_eventsSubscriber.OnFinishGameListenerAdded -= this.OnFinishGameListenerAdded;

            this.m_eventsSubscriber.OnPauseGameListenerAdded -= this.OnPauseGameListenerAdded;
            this.m_eventsSubscriber.OnResumeGameListenerAdded -= this.OnResumeGameListenerAdded;
        }

        private void OnStartGameListenerAdded(IStartGameListener listener)
        {
            if (this.m_gameRunner.CurrentStatus == GameStatus.Play)
                listener.OnStartGame();
        }

        private void OnFinishGameListenerAdded(IFinishGameListener listener)
        {
            if (this.m_gameRunner.CurrentStatus == GameStatus.Finish)
                listener.OnFinishGame();
        }

        private void OnPauseGameListenerAdded(IPauseGameListener listener)
        {
            if (this.m_gameRunner.CurrentStatus == GameStatus.Pause)
                listener.OnPauseGame();
        }

        private void OnResumeGameListenerAdded(IResumeGameListener listener)
        {
            if (this.m_gameRunner.CurrentStatus == GameStatus.Play)
                listener.OnResumeGame();
        }
    }
}