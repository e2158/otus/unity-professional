﻿using JetBrains.Annotations;

namespace ShootEmUp.Framework
{
    internal sealed class GameEventsInvoker
    {
        [NotNull] private readonly IReadOnlyGameEventsListeners m_listeners;

        public GameEventsInvoker([NotNull] IReadOnlyGameEventsListeners listeners)
        {
            this.m_listeners = listeners;
        }

        public void InvokeTickGameListeners(float deltaTime)
        {
            foreach (var listener in this.m_listeners.TickGameListeners)
            {
                listener.OnTickGame(deltaTime);
            }
        }

        public void InvokeTickPhysicsListeners(float deltaTime)
        {
            foreach (var listener in this.m_listeners.TickPhysicsListeners)
            {
                listener.OnTickPhysics(deltaTime);
            }
        }

        public void InvokeStartGameListeners()
        {
            foreach (var listener in this.m_listeners.StartGameListeners)
            {
                listener.OnStartGame();
            }
        }

        public void InvokeFinishGameListeners()
        {
            foreach (var listener in this.m_listeners.FinishGameListeners)
            {
                listener.OnFinishGame();
            }
        }

        public void InvokePauseGameListeners()
        {
            foreach (var listener in this.m_listeners.PauseGameListeners)
            {
                listener.OnPauseGame();
            }
        }

        public void InvokeResumeGameListeners()
        {
            foreach (var listener in this.m_listeners.ResumeGameListeners)
            {
                listener.OnResumeGame();
            }
        }
    }
}