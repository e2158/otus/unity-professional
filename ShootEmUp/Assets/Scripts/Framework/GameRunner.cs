﻿using System;
using UnityEngine;
using VContainer.Unity;
using JetBrains.Annotations;

namespace ShootEmUp.Framework
{
    public interface IReadOnlyGameRunner
    {
        event Action<GameStatus> OnStatusUpdated;
        GameStatus CurrentStatus { get; }
    }

    public sealed class GameRunner : ITickable, IFixedTickable, IReadOnlyGameRunner
    {
        public event Action<GameStatus> OnStatusUpdated;

        public GameStatus CurrentStatus { get; private set; }

        [NotNull] private readonly GameEventsInvoker m_eventsInvoker;

        internal GameRunner([NotNull] GameEventsInvoker eventsInvoker)
        {
            this.m_eventsInvoker = eventsInvoker;
        }

        void ITickable.Tick()
        {
            if (this.CurrentStatus == GameStatus.Play)
            {
                var deltaTime = Time.deltaTime;
                this.m_eventsInvoker.InvokeTickGameListeners(deltaTime);
            }
        }

        void IFixedTickable.FixedTick()
        {
            if (this.CurrentStatus == GameStatus.Play)
            {
                var deltaTime = Time.fixedDeltaTime;
                this.m_eventsInvoker.InvokeTickPhysicsListeners(deltaTime);
            }
        }

        [ContextMenu(nameof(StartGame))]
        public void StartGame()
        {
            if (this.CurrentStatus != GameStatus.None)
                throw new InvalidOperationException($"Can't {nameof(this.StartGame)}");

            this.CurrentStatus = GameStatus.Play;
            this.OnStatusUpdated?.Invoke(this.CurrentStatus);

            this.m_eventsInvoker.InvokeStartGameListeners();
        }

        [ContextMenu(nameof(FinishGame))]
        public void FinishGame()
        {
            if (this.CurrentStatus == GameStatus.Finish)
                throw new InvalidOperationException($"Can't {nameof(this.FinishGame)}");

            this.CurrentStatus = GameStatus.Finish;
            this.OnStatusUpdated?.Invoke(this.CurrentStatus);

            this.m_eventsInvoker.InvokeFinishGameListeners();
        }

        [ContextMenu(nameof(PauseGame))]
        public void PauseGame()
        {
            if (this.CurrentStatus != GameStatus.Play)
                throw new InvalidOperationException($"Can't {nameof(this.PauseGame)}");

            this.CurrentStatus = GameStatus.Pause;
            this.OnStatusUpdated?.Invoke(this.CurrentStatus);

            this.m_eventsInvoker.InvokePauseGameListeners();
        }

        [ContextMenu(nameof(ResumeGame))]
        public void ResumeGame()
        {
            if (this.CurrentStatus != GameStatus.Pause)
                throw new InvalidOperationException($"Can't {nameof(this.ResumeGame)}");

            this.CurrentStatus = GameStatus.Play;
            this.OnStatusUpdated?.Invoke(this.CurrentStatus);

            this.m_eventsInvoker.InvokeResumeGameListeners();
        }

        [ContextMenu(nameof(PauseOnResumeGame))]
        public void PauseOnResumeGame()
        {
            switch (this.CurrentStatus)
            {
                case GameStatus.Play:
                    this.PauseGame();
                    break;

                case GameStatus.Pause:
                    this.ResumeGame();
                    break;

                default:
                    throw new ArgumentException();
            }
        }
    }
}