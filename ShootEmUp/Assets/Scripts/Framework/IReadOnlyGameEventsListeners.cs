﻿using System.Collections.Generic;

namespace ShootEmUp.Framework
{
    public interface IReadOnlyGameEventsListeners
    {
        IReadOnlyList<ITickGameListener> TickGameListeners { get; }
        IReadOnlyList<ITickPhysicsListener> TickPhysicsListeners { get; }

        IReadOnlyList<IStartGameListener> StartGameListeners { get; }
        IReadOnlyList<IFinishGameListener> FinishGameListeners { get; }

        IReadOnlyList<IPauseGameListener> PauseGameListeners { get; }
        IReadOnlyList<IResumeGameListener> ResumeGameListeners { get; }
    }
}
