﻿namespace ShootEmUp.Framework
{
    public enum GameStatus : byte
    {
        None = 0,
        Play = 1,
        Pause = 2,
        Finish = 3,
    }
}