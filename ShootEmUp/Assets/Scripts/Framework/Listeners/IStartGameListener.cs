﻿namespace ShootEmUp.Framework
{
    public interface IStartGameListener
    {
        void OnStartGame();
    }
}