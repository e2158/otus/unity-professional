﻿namespace ShootEmUp.Framework
{
    public interface IResumeGameListener
    {
        void OnResumeGame();
    }
}