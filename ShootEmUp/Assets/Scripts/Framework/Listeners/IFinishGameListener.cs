﻿namespace ShootEmUp.Framework
{
    public interface IFinishGameListener
    {
        void OnFinishGame();
    }
}