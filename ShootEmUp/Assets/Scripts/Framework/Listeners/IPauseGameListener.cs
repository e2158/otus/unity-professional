﻿namespace ShootEmUp.Framework
{
    public interface IPauseGameListener
    {
        void OnPauseGame();
    }
}