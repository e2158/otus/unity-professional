﻿namespace ShootEmUp.Framework
{
    public interface ITickPhysicsListener
    {
        void OnTickPhysics(float deltaTime);
    }
}