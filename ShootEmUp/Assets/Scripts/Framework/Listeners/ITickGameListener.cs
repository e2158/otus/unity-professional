﻿namespace ShootEmUp.Framework
{
    public interface ITickGameListener
    {
        void OnTickGame(float deltaTime);
    }
}