using UnityEngine;

namespace ShootEmUp.Bullet
{
    [CreateAssetMenu(
        fileName = "BulletConfig",
        menuName = "Bullets/New BulletConfig"
    )]
    public sealed class BulletConfig : ScriptableObject
    {
        public int PhysicsLayer
        {
            get { return (int) this.physicsLayer; }
        }

        public Color Color
        {
            get { return this.color; }
        }

        public int Damage
        {
            get { return this.damage; }
        }

        public float Speed
        {
            get { return this.speed; }
        }

        [SerializeField]
        private PhysicsLayer physicsLayer;

        [SerializeField]
        private Color color;

        [SerializeField]
        private int damage;

        [SerializeField]
        private float speed;
    }
}