﻿using VContainer;
using UnityEngine;

namespace ShootEmUp.Bullet
{
    internal sealed class BulletSystemScopeConfigurator : ScopeConfigurator
    {
        [SerializeField]
        private BulletPool pool;

        private void OnValidate()
        {
            this.pool ??= FindObjectOfType<BulletPool>();
        }

        public override void Configure(IContainerBuilder builder)
        {
            builder.RegisterInstance(this.pool);

            builder.Register<BulletsFilter, HittedBulletsFilter>(Lifetime.Singleton);
            builder.Register<BulletsFilter, OutOfBoundsBulletsFilter>(Lifetime.Singleton);

            builder.Register<BulletSystem>(Lifetime.Singleton).AsImplementedInterfaces();
        }
    }
}