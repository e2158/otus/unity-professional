﻿using UnityEngine;

namespace ShootEmUp.Bullet
{
    internal sealed class BulletPool : MonoPool<BulletObject>
    {
        [SerializeField]
        private BulletObject prefab;

        protected override BulletObject CreateObject()
        {
            return Instantiate(this.prefab);
        }

    }
}