﻿using ShootEmUp.Level;
using JetBrains.Annotations;

namespace ShootEmUp.Bullet
{
    internal sealed class OutOfBoundsBulletsFilter : BulletsFilter
    {
        private readonly LevelBounds m_bounds;

        public OutOfBoundsBulletsFilter([NotNull] LevelBounds bounds)
        {
            this.m_bounds = bounds;
        }

        protected override bool CheckFilterCondition(BulletObject bullet)
        {
            return !this.m_bounds.Contains(bullet.transform.position);
        }
    }
}