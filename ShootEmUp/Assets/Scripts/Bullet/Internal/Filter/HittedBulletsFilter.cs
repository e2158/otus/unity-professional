﻿namespace ShootEmUp.Bullet
{
    internal sealed class HittedBulletsFilter : BulletsFilter
    {
        protected override bool CheckFilterCondition(BulletObject bullet)
        {
            return bullet.IsHitted;
        }
    }
}