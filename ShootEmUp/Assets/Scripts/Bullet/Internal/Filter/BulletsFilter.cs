﻿using JetBrains.Annotations;
using System.Collections.Generic;

namespace ShootEmUp.Bullet
{
    internal abstract class BulletsFilter
    {
        private const int INITIAL_CACHE_CAPACITY = 20;

        private readonly List<BulletObject> m_returnCache = new(INITIAL_CACHE_CAPACITY);

        public IReadOnlyList<BulletObject> Filter([NotNull, ItemNotNull] IReadOnlyCollection<BulletObject> collection)
        {
            this.m_returnCache.Clear();

            foreach (BulletObject bullet in collection)
            {
                if (this.CheckFilterCondition(bullet))
                {
                    this.m_returnCache.Add(bullet);
                }
            }

            return this.m_returnCache;
        }

        protected abstract bool CheckFilterCondition(BulletObject bullet);
    }
}