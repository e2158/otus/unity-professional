﻿using UnityEngine;
using VContainer;
using VContainer.Unity;
using ShootEmUp.Framework;

namespace ShootEmUp.Bullet
{
    internal sealed class BulletScope : LifetimeScope
    {
        [SerializeField] private BulletObject bulletObject;

        private void OnValidate()
        {
            this.bulletObject ??= this.GetComponentInChildren<BulletObject>();
        }

        private void OnEnable()
        {
            this.RegisterInGameEvents();
        }

        private void OnDisable()
        {
            this.UnregisterInGameEvents();
        }

        protected override void Configure(IContainerBuilder builder)
        {
            this.RegisterComponents(builder);
            this.RegisterBehaviours(builder);
        }

        private void RegisterComponents(IContainerBuilder builder)
        {
            builder.RegisterInstance(this.bulletObject);
        }

        private void RegisterBehaviours(IContainerBuilder builder)
        {
            builder.Register<BulletDamageBehaviour>(Lifetime.Scoped);
        }

        private void RegisterInGameEvents()
        {
            var gameEventsSubscriber = this.Container.Resolve<GameEventsSubscriber>();
            gameEventsSubscriber.AddListener(this.Container.Resolve<BulletDamageBehaviour>());
        }

        private void UnregisterInGameEvents()
        {
            var gameEventsSubscriber = this.Container.Resolve<GameEventsSubscriber>();
            gameEventsSubscriber.RemoveListener(this.Container.Resolve<BulletDamageBehaviour>());
        }
    }
}