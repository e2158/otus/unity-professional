using System;
using UnityEngine;
using JetBrains.Annotations;

namespace ShootEmUp.Bullet
{
    internal sealed class BulletDamageBehaviour : IDisposable
    {
        [NotNull] private readonly BulletObject m_bulletObject;

        public BulletDamageBehaviour([NotNull] BulletObject bulletObject)
        {
            this.m_bulletObject = bulletObject;
            this.m_bulletObject.OnCollisionEntered += this.OnCollisionEntered;
        }

        public void Dispose()
        {
            this.m_bulletObject.OnCollisionEntered -= this.OnCollisionEntered;
        }

        private void OnCollisionEntered(BulletObject bullet, Collision2D collision)
        {
            if (this.m_bulletObject == bullet && this.CanDamage(collision.gameObject))
                this.TakeDamage(collision.gameObject);
        }

        private bool CanDamage([NotNull] GameObject other)
        {
            return other.TryGetComponent<HitPointsComponent>(out var hitPointsComponent)
                   && !hitPointsComponent.IsHitPointsEmpty
                   && other.TryGetComponent(out TeamComponent teamComponent)
                   && this.m_bulletObject.Team != teamComponent.Team;
        }

        private void TakeDamage([NotNull] GameObject target)
        {
            target
                .GetComponent<HitPointsComponent>()
                .TakeDamage(this.m_bulletObject.Damage);
        }
    }
}