using System;
using UnityEngine;

namespace ShootEmUp.Bullet
{
    internal sealed class BulletObject : MonoBehaviour
    {
        public event Action<BulletObject, Collision2D> OnCollisionEntered;

        public Team Team { get; private set; }
        public int Damage { get; private set; }
        public bool IsHitted { get; private set; }

        [SerializeField]
        private new Rigidbody2D rigidbody2D;

        [SerializeField]
        private SpriteRenderer spriteRenderer;

        private void OnValidate()
        {
            this.rigidbody2D ??= this.GetComponentInChildren<Rigidbody2D>();
            this.spriteRenderer ??= this.GetComponentInChildren<SpriteRenderer>();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            this.OnCollisionEntered?.Invoke(this, collision);
            this.IsHitted = true;
        }

        public void Initialize(BulletArgs args)
        {
            this.IsHitted = false;
            this.Team = args.Team;
            this.Damage = args.Config.Damage;

            this.spriteRenderer.color = args.Config.Color;
            this.gameObject.layer = args.Config.PhysicsLayer;

            this.transform.position = args.StartPosition;
            this.rigidbody2D.velocity = args.Velocity;
        }
    }
}