using ShootEmUp.Framework;
using JetBrains.Annotations;
using System.Collections.Generic;

namespace ShootEmUp.Bullet
{
    internal sealed class BulletSystem : IBulletShooter, ITickPhysicsListener
    {
        private readonly BulletPool m_pool;
        private readonly IReadOnlyList<BulletsFilter> m_removeBulletFilters;

        private readonly HashSet<BulletObject> m_collection = new();

        private BulletSystem([NotNull] BulletPool pool, [NotNull] IReadOnlyList<BulletsFilter> bulletsFilters)
        {
            this.m_pool = pool;
            this.m_removeBulletFilters = bulletsFilters;
        }

        void ITickPhysicsListener.OnTickPhysics(float _)
        {
            foreach (BulletsFilter removeBulletFilter in this.m_removeBulletFilters)
            {
                var bulletsToRemove = removeBulletFilter.Filter(this.m_collection);
                bulletsToRemove.ForEach(this.RemoveBullet);
            }
        }

        public void Shoot(BulletArgs args)
        {
            var bullet = this.ProduceBullet();
            bullet.Initialize(args);
        }

        private BulletObject ProduceBullet()
        {
            var bullet = this.m_pool.Get();
            this.m_collection.Add(bullet);

            return bullet;
        }

        private void RemoveBullet(BulletObject bullet)
        {
            this.m_collection.Remove(bullet);
            this.m_pool.Release(bullet);
        }
    }
}