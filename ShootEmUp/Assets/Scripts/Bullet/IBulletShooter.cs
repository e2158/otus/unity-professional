﻿namespace ShootEmUp.Bullet
{
    public interface IBulletShooter
    {
        void Shoot(BulletArgs args);
    }
}