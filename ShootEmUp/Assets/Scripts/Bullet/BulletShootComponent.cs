﻿using VContainer;
using UnityEngine;
using JetBrains.Annotations;

namespace ShootEmUp.Bullet
{
    public sealed class BulletShootComponent : MonoBehaviour
    {
        [SerializeField]
        private BulletConfig bulletConfig;

        [SerializeField]
        private WeaponComponent weaponComponent;

        private IBulletShooter m_bulletShooter;

        private void OnValidate()
        {
            this.weaponComponent ??= this.GetComponentInChildren<WeaponComponent>();
        }

        [Inject]
        private void Construct([NotNull] IBulletShooter bulletShooter)
        {
            this.m_bulletShooter = bulletShooter;
        }

        public void ShootForward(Team team)
        {
            this.ShootInDirection(team, this.weaponComponent.Rotation * Vector3.up);
        }

        public void ShootToPosition(Team team, Vector2 position)
        {
            var direction = position - this.weaponComponent.Position;
            this.ShootInDirection(team, direction.normalized);
        }

        private void ShootInDirection(Team team, Vector3 direction)
        {
            var args = new BulletArgs(
                team: team,
                config: this.bulletConfig,
                startPosition: this.weaponComponent.Position,
                direction: direction
            );

            this.m_bulletShooter.Shoot(args);
        }
    }
}