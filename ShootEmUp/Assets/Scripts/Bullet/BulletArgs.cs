﻿using UnityEngine;

namespace ShootEmUp.Bullet
{
    public readonly struct BulletArgs
    {
        public Vector2 Velocity
        {
            get { return this.m_direction * this.Config.Speed; }
        }

        public readonly Team Team;
        public readonly BulletConfig Config;
        public readonly Vector3 StartPosition;

        private readonly Vector2 m_direction;


        public BulletArgs(Team team, BulletConfig config, Vector3 startPosition, Vector2 direction)
        {
            this.Team = team;
            this.Config = config;
            this.StartPosition = startPosition;
            this.m_direction = direction;
        }
    }
}