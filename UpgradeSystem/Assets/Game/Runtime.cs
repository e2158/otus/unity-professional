﻿using System;
using System.Linq;
using System.Collections.Generic;
using AKerp.Stats;
using AKerp.Upgrades;
using UnityEngine;
using Sirenix.OdinInspector;
using AKerp.Upgrades.Configuration;
using AKerp.Upgrades.Impl;
using JetBrains.Annotations;
using Otus.Environment;

namespace Game
{
    [DisallowMultipleComponent]
    public sealed class Runtime : MonoBehaviour
    {
        [Title("Dependencies")]
        [SerializeField, Required, AssetSelector] private UpgradeConfig[] _upgradeConfigs;

        [Title("Debug")]
        [ShowInInspector, DisableInEditorMode] private PlayerStats _playerStats;
        [ShowInInspector, DisableInEditorMode] private MoneyStorage _moneyStorage;
        [ShowInInspector, DisableInEditorMode] private UpgradesManager _upgradesManager;

        private void Start()
        {
            _playerStats = new PlayerStats();
            _moneyStorage = new MoneyStorage();
            _upgradesManager = new UpgradesManager(_moneyStorage);

            IEnumerable<Upgrade> upgrades = _upgradeConfigs.Select(CreateUpgrade);
            _upgradesManager.Fill(upgrades);
        }

        private Upgrade CreateUpgrade([NotNull] UpgradeConfig config) =>
            new(config, CreateStatProvider(config));

        private StatProvider CreateStatProvider([NotNull] UpgradeConfig config) =>
            new(_playerStats, FindStatsName(config.StatType));

        private static string FindStatsName([NotNull] Type statType)
        {
            if (statType == typeof(UpgradeHpTable)) return "hp";
            if (statType == typeof(UpgradeSpeedTable)) return "speed";
            if (statType == typeof(UpgradeDamageTable)) return "damage";
            throw new ArgumentOutOfRangeException(nameof(statType), statType, "Unknown stat type");
        }
    }
}