﻿using System;
using System.Linq;
using System.Collections.Generic;
using AKerp.Upgrades.Configuration;
using UnityEngine;
using Sirenix.OdinInspector;

namespace AKerp.Upgrades.Impl
{
    [Serializable]
    public sealed class UpgradeHpTable : UpgradeStatTable
    {
        [SerializeField, MinValue(0)] private int _baseHp;
        [SerializeField, MinValue(0)] private int _hpPerLevelStep;

        protected override string DataName => "Hp";

        protected override void Fill(Dictionary<int, int> dataOnLevel)
        {
            int[] levels = dataOnLevel.Keys.ToArray();
            foreach (int level in levels)
            {
                int hp = _baseHp + _hpPerLevelStep * (level - 1);
                dataOnLevel[level] = hp;
            }
        }
    }
}