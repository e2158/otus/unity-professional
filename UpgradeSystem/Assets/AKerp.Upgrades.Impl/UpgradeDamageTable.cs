﻿using System;
using System.Linq;
using System.Collections.Generic;
using AKerp.Upgrades.Configuration;
using UnityEngine;
using Sirenix.OdinInspector;

namespace AKerp.Upgrades.Impl
{
    [Serializable]
    public sealed class UpgradeDamageTable : UpgradeStatTable
    {
        [SerializeField, MinValue(0)] private int _baseDamage;
        [SerializeField, MinValue(0)] private int _damagePerLevelStep;

        protected override string DataName => "Damage";

        protected override void Fill(Dictionary<int, int> dataOnLevel)
        {
            int[] levels = dataOnLevel.Keys.ToArray();

            foreach (int level in levels)
            {
                int damage = _baseDamage + _damagePerLevelStep * (level - 1);
                dataOnLevel[level] = damage;
            }
        }
    }
}