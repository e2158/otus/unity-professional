﻿using System;
using System.Linq;
using System.Collections.Generic;
using AKerp.Upgrades.Configuration;
using UnityEngine;
using Sirenix.OdinInspector;

namespace AKerp.Upgrades.Impl
{
    [Serializable]
    public sealed class UpgradeSpeedTable : UpgradeStatTable
    {
        [SerializeField, MinValue(0)] private int _minSpeed;
        [SerializeField, MinValue(0)] private int _maxSpeed;

        protected override string DataName => "Speed";

        protected override void Fill(Dictionary<int, int> dataOnLevel)
        {
            int maxLevel = dataOnLevel.Keys.Max();
            int[] levels = dataOnLevel.Keys.ToArray();

            foreach (int level in levels)
            {
                float ratio = level / (float)maxLevel;
                int speed = (int)Mathf.Lerp(_minSpeed, _maxSpeed, ratio);
                dataOnLevel[level] = speed;
            }
        }
    }
}