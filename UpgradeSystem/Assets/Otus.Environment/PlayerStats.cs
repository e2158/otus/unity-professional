using AKerp.Stats;
using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace Otus.Environment
{
    public sealed class PlayerStats : IStatsModel
    {
        [ShowInInspector, ReadOnly]
        private readonly Dictionary<string, int> _stats = new();

        public IReadOnlyDictionary<string, int> Stats => _stats;

        public void RemoveStat(string name) => _stats.Remove(name);
        public void SetStat(string name, int value) => _stats[name] = value;
        public int GetStat(string name) => _stats.GetValueOrDefault(name, 0);
    }
}