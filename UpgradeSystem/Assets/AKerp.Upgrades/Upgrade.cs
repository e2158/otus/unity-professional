using System;
using System.Collections.Generic;
using AKerp.Stats;
using AKerp.Upgrades.Requirements;
using AKerp.Upgrades.Configuration;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace AKerp.Upgrades
{
    public sealed class Upgrade
    {
        private const int MinLevel = UpgradeConfig.MinLevel;

        public event Action<int> OnLevelUp;

        [NotNull, ShowInInspector, ReadOnly] private readonly UpgradeConfig _config;
        [ShowInInspector, ReadOnly] public string Id => _config.Id;
        [ShowInInspector, ReadOnly] public int MaxLevel => _config.MaxLevel;
        [ShowInInspector, ReadOnly] public int Stat => _statProvider.GetStat();
        [ShowInInspector, ReadOnly] public int UpgradePrice => _config.GetPriceForLevel(NextLevel);
        [ShowInInspector, ReadOnly, PropertyRange(MinLevel, nameof(MaxLevel))] public int Level { get; private set; } = MinLevel;
        [NotNull, ItemNotNull] public IEnumerable<UpgradeRequirement> UpgradeRequirements => _config.GetRequirementsForLevel(NextLevel);
        public bool IsMaxLevel => Level == _config.MaxLevel;

        private int NextLevel => Level + 1;
        [NotNull] private readonly StatProvider _statProvider;

        public Upgrade([NotNull] UpgradeConfig config, [NotNull] StatProvider statProvider)
        {
            _config = config ?? throw new ArgumentNullException(nameof(config));
            _statProvider = statProvider ?? throw new ArgumentNullException(nameof(statProvider));

            UpdateStat(Level);
        }

        public void LevelUp()
        {
            if (Level >= MaxLevel)
                throw new InvalidOperationException($"Level of upgrade \"{_config.Id}\" is already max: {_config.MaxLevel}");

            int nextLevel = Level + 1;

            Level = nextLevel;
            UpdateStat(nextLevel);

            OnLevelUp?.Invoke(nextLevel);
        }

        private void UpdateStat(int level)
        {
            int stat = _config.GetStatForLevel(level);
            _statProvider.SetStat(stat);
        }
    }
}