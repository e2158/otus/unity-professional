using System;
using System.Collections.Generic;
using AKerp.Upgrades.Requirements;
using Otus.Environment;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace AKerp.Upgrades
{
    public sealed class UpgradesManager
    {
        public event Action<Upgrade> OnLevelUp;

        [ShowInInspector, ReadOnly] private Dictionary<string, Upgrade> _upgrades = new();

        [NotNull] private readonly MoneyStorage _moneyStorage;
        [NotNull] private readonly UpgradeRequirementResolver _requirementResolver;

        public UpgradesManager([NotNull] MoneyStorage moneyStorage)
        {
            _moneyStorage = moneyStorage ?? throw new ArgumentNullException(nameof(moneyStorage));
            _requirementResolver = new UpgradeRequirementResolver(_upgrades);
        }

        [Button(Expanded = true)] public bool CanLevelUp(string id) => CanLevelUp(GetUpgrade(id));
        [Button(Expanded = true)] public void LevelUp(string id) => LevelUp(GetUpgrade(id));

        public Upgrade GetUpgrade(string id) => _upgrades[id];
        public IEnumerable<Upgrade> GetAllUpgrades() => _upgrades.Values;

        public void Fill([NotNull, ItemNotNull] IEnumerable<Upgrade> upgrades)
        {
            if (upgrades == null)
                throw new ArgumentNullException(nameof(upgrades));

            _upgrades.Clear();

            foreach (Upgrade upgrade in upgrades)
                _upgrades[upgrade.Id] = upgrade;
        }

        public bool CanLevelUp([NotNull] Upgrade upgrade)
        {
            if (upgrade == null) throw new ArgumentNullException(nameof(upgrade));
            return !upgrade.IsMaxLevel &&
                   _moneyStorage.CanSpendMoney(upgrade.UpgradePrice) &&
                   _requirementResolver.IsResolved(upgrade.UpgradeRequirements);
        }

        public void LevelUp([NotNull] Upgrade upgrade)
        {
            if (!CanLevelUp(upgrade))
                throw new InvalidOperationException($"Can not level up {upgrade.Id}");

            int price = upgrade.UpgradePrice;
            _moneyStorage.SpendMoney(price);

            upgrade.LevelUp();
            OnLevelUp?.Invoke(upgrade);
        }
    }
}