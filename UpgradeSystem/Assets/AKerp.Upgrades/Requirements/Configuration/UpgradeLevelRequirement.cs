﻿using System;
using System.Linq;
using System.Collections.Generic;
using AKerp.Upgrades.Configuration;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace AKerp.Upgrades.Requirements
{
    [Serializable]
    public sealed class UpgradeLevelRequirement : UpgradeRequirement
    {
        [TableList(AlwaysExpanded = true), ValidateInput(nameof(ValidateDuplicates), "You have duplicated upgrades")]
        [SerializeReference] private List<UpgradeLevelData> _lockingUpgradeLevels = new();

        [NotNull, ItemNotNull] public IReadOnlyList<UpgradeLevelData> LockingUpgradeLevels => _lockingUpgradeLevels;

        private bool ValidateDuplicates(List<UpgradeLevelData> upgradeLevels)
        {
            int actualCount = upgradeLevels.Count;
            int uniqueCount = upgradeLevels.Select(l => l.Upgrade).Distinct().Count();

            bool hasDuplicates = uniqueCount != actualCount;
            return !hasDuplicates;
        }
    }

    [Serializable, InlineProperty]
    public sealed class UpgradeLevelData
    {
        [Required, AssetSelector]
        [SerializeField] private UpgradeConfig _upgrade;

        [MinValue(0), MaxValue(nameof(MaxLevel))]
        [SerializeField] private int _level;

        private int MaxLevel => _upgrade?.MaxLevel ?? 0;

        [NotNull] public UpgradeConfig Upgrade => _upgrade;
        public int Level => _level;
    }
}