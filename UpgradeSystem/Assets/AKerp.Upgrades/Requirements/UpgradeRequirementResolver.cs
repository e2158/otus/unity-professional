﻿using System;
using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace AKerp.Upgrades.Requirements
{
    internal sealed class UpgradeRequirementResolver
    {
        [NotNull] private readonly IReadOnlyDictionary<string, Upgrade> _upgrades;

        public UpgradeRequirementResolver([NotNull] IReadOnlyDictionary<string, Upgrade> upgrades)
        {
            _upgrades = upgrades ?? throw new ArgumentNullException(nameof(upgrades));
        }

        public bool IsResolved([NotNull, ItemNotNull] IEnumerable<UpgradeRequirement> requirements)
        {
            if (requirements == null) throw new ArgumentNullException(nameof(requirements));
            return requirements.All(IsResolved);
        }

        public bool IsResolved([NotNull] UpgradeRequirement requirement)
        {
            if (requirement == null)
                throw new ArgumentNullException(nameof(requirement));

            switch (requirement)
            {
                case UpgradeLevelRequirement levelRequirement:
                    return levelRequirement.LockingUpgradeLevels.All(locking =>
                    {
                        string id = locking.Upgrade.Id;
                        return _upgrades.TryGetValue(id, out Upgrade upgrade) && upgrade.Level >= locking.Level;
                    });

                default:
                    throw new ArgumentOutOfRangeException(nameof(requirement),
                        $"Can't resolve requirement of type {requirement.GetType().Name}");
            }
        }
    }
}