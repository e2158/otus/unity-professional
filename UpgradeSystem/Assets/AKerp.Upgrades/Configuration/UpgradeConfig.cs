using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using AKerp.Upgrades.Requirements;
using JetBrains.Annotations;

namespace AKerp.Upgrades.Configuration
{
    [CreateAssetMenu(menuName = nameof(UpgradeConfig), fileName = "Configs/" + nameof(UpgradeConfig))]
    public sealed class UpgradeConfig : SerializedScriptableObject
    {
        public const int MinLevel = 1;

        [SerializeField, Required] private string _id;

        [MinValue(MinLevel)]
        [SerializeField] private int _maxLevel = MinLevel;

        [HideLabel, BoxGroup("Price Table")]
        [SerializeField] private UpgradePriceTable _priceTable;

        [HideLabel, BoxGroup("Stat Table")]
        [SerializeField] private UpgradeStatTable _statTable;

        [LabelText("Level & Requirements for this level")]
        [SerializeField] private Dictionary<int, UpgradeRequirement[]> _requirementsPerLevel;

        public string Id => _id;
        public int MaxLevel => _maxLevel;
        public Type StatType => _statTable.GetType();

        private void OnValidate()
        {
            _priceTable.Fill(_maxLevel);
            _statTable?.Fill(_maxLevel);

            if (_requirementsPerLevel != null && _requirementsPerLevel.Keys.Any(lvl => !IsLevelCorrect(lvl)))
                _requirementsPerLevel = _requirementsPerLevel.Where(kv => IsLevelCorrect(kv.Key))
                    .ToDictionary(kv => kv.Key, kv => kv.Value);
        }

        private bool IsLevelCorrect(int level) =>
            level >= MinLevel && level <= MaxLevel;

        public int GetPriceForLevel(int level) =>
            _priceTable.GetDataForLevel(level);

        public int GetStatForLevel(int level) =>
            _statTable.GetDataForLevel(level);

        [NotNull, ItemNotNull]
        public IEnumerable<UpgradeRequirement> GetRequirementsForLevel(int level) =>
            _requirementsPerLevel != null && _requirementsPerLevel.TryGetValue(level, out UpgradeRequirement[] requirements)
                ? requirements : Enumerable.Empty<UpgradeRequirement>();
    }
}