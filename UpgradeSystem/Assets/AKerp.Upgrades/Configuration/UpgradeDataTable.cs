﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace AKerp.Upgrades.Configuration
{
    [Serializable]
    public abstract class UpgradeDataTable
    {
        [PropertyOrder(10)]
        [LabelText("@" + nameof(DataLabel))]
        [TableList(IsReadOnly = true, AlwaysExpanded = true, HideToolbar = true)]
        [SerializeReference, ReadOnly] private Dictionary<int, int> _dataOnLevel;

        protected abstract string DataName { get; }

        private int MaxLevel => _dataOnLevel.Count;
        private string DataLabel => $"Level & {DataName} on this Level";

        public void Fill(int maxLevel)
        {
            if (_dataOnLevel == null || _dataOnLevel.Count != maxLevel)
                _dataOnLevel = Enumerable.Range(1, maxLevel).ToDictionary(x => x);

            Fill(_dataOnLevel);
        }

        public int GetDataForLevel(int level)
        {
            int clampedLevel = Mathf.Clamp(level, UpgradeConfig.MinLevel, MaxLevel);
            return _dataOnLevel[clampedLevel];
        }

        protected abstract void Fill([NotNull] Dictionary<int, int> dataOnLevel);
    }
}