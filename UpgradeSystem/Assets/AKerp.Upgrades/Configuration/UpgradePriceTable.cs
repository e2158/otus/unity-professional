﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace AKerp.Upgrades.Configuration
{
    [Serializable]
    internal sealed class UpgradePriceTable : UpgradeDataTable
    {
        [SerializeField, MinValue(0)] private int _basePrice;
        [SerializeField, MinValue(0)] private int _pricePower;

        protected override string DataName => "Price";

        protected override void Fill(Dictionary<int, int> dataOnLevel)
        {
            int[] levels = dataOnLevel.Keys.ToArray();

            foreach (int level in levels)
            {
                int price = (int)(_basePrice * Mathf.Pow(_pricePower, level - 2));
                dataOnLevel[level] = price;
            }
        }
    }
}