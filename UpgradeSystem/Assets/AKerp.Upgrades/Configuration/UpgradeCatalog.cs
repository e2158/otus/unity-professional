using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using System.Collections.Generic;

namespace AKerp.Upgrades.Configuration
{
    [CreateAssetMenu(fileName = nameof(UpgradeCatalog), menuName = "Configs/" + nameof(UpgradeCatalog))]
    public sealed class UpgradeCatalog : ScriptableObject
    {
        [Required, AssetSelector]
        [SerializeField] private UpgradeConfig[] _configs;

        [NotNull, ItemNotNull] public IReadOnlyList<UpgradeConfig> Configs => _configs;
    }
}