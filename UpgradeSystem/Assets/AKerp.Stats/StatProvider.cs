﻿using System;
using JetBrains.Annotations;

namespace AKerp.Stats
{
    public sealed class StatProvider
    {
        [NotNull] private readonly string _statName;
        [NotNull] private readonly IStatsModel _statsModel;

        public StatProvider([NotNull] IStatsModel statsModel, [NotNull] string statName)
        {
            _statName = statName?? throw new ArgumentNullException(nameof(statName));
            _statsModel = statsModel ?? throw new ArgumentNullException(nameof(statsModel));
        }

        public int GetStat() => _statsModel.GetStat(_statName);
        public void SetStat(int value) => _statsModel.SetStat(_statName, value);
    }
}