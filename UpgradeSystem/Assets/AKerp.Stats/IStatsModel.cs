﻿using System.Collections.Generic;

namespace AKerp.Stats
{
    public interface IStatsModel
    {
        IReadOnlyDictionary<string, int> Stats { get; }

        void SetStat(string name, int value);
        void RemoveStat(string name);

        int GetStat(string name);
    }
}