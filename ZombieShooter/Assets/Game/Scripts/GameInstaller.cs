﻿using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Essentials;
using AtomicFramework;
using ZombieShooter.Systems;
using ZombieShooter.Controllers;
using ZombieShooter.Features.Unit;

namespace ZombieShooter
{
    internal sealed class GameInstaller : MonoInstaller
    {
        [Required, SceneObjectsOnly]
        [SerializeField] private UnitObject _player;

        private void OnValidate()
        {
            this._player ??= FindObjectOfType<UnitObject>();
        }

        public override void Start()
        {
            base.Start();
            this.SetupGame();
        }

        private void OnDestroy()
        {
            this.UndoGameSetup();
        }

        public override void InstallBindings()
        {
            this.Container.BindInterfacesAndSelfTo<GameInputAction>().AsSingle();
            this.Container.BindInterfacesAndSelfTo<Collection<IReadOnlyAccessEntity>>().AsSingle();
            this.Container.Bind<IReadOnlyAccessEntity>().FromInstance(this._player.AccessEntity);
            this.Container.BindInterfacesAndSelfTo<AttackController>().AsSingle();
        }

        private void SetupGame()
        {
            this.Container.Resolve<CameraSystem>().SetTarget(this._player.transform);
            this.Container.Resolve<SpawnSystem>().StartSpawn();
        }

        private void UndoGameSetup()
        {
            this.Container.Resolve<CameraSystem>().SetTarget(null);
            this.Container.Resolve<SpawnSystem>().StopSpawn();
        }
    }
}