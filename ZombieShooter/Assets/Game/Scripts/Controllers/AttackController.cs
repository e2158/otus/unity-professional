﻿using System;
using AtomicFramework;
using AtomicFramework.Impl;
using Essentials;
using JetBrains.Annotations;
using UnityEngine;

namespace ZombieShooter.Controllers
{
    public sealed class AttackController : IDisposable
    {
        private static readonly RaycastHit[] raycastBuffer = new RaycastHit[16];

        [NotNull] private readonly IReadOnlyCollection<IReadOnlyAccessEntity> _entities;

        public AttackController([NotNull] IReadOnlyCollection<IReadOnlyAccessEntity> entities)
        {
            this._entities = entities ?? throw new ArgumentNullException(nameof(entities));

            this._entities.Added += this.OnEntityAdded;
            this._entities.Removed += this.OnEntityRemoved;

            foreach (IReadOnlyAccessEntity entity in this._entities)
                this.OnEntityAdded(entity);
        }

        public void Dispose()
        {
            this._entities.Added -= this.OnEntityAdded;
            this._entities.Removed -= this.OnEntityRemoved;

            foreach (IReadOnlyAccessEntity entity in this._entities)
                this.OnEntityRemoved(entity);
        }

        private void OnEntityAdded(IReadOnlyAccessEntity entity)
        {
            if (entity.TryGet(out ITakeDamageReactionAccessor attackReactionAccessor))
                attackReactionAccessor.Get().Invoked += this.OnAttackReaction;
        }

        private void OnEntityRemoved(IReadOnlyAccessEntity entity)
        {
            if (entity.TryGet(out ITakeDamageReactionAccessor attackReactionAccessor))
                attackReactionAccessor.Get().Invoked -= this.OnAttackReaction;
        }

        private void OnAttackReaction(DamageDto dto)
        {
            Ray ray = new(dto.Position, dto.Direction);
            int count = Physics.RaycastNonAlloc(
                ray, AttackController.raycastBuffer, dto.MaxDistance, dto.Layer);

            if (count > 0)
            {
                RaycastHit hit = AttackController.raycastBuffer[0];
                Collider collider = hit.collider;

                var entity = collider.GetComponent<IReadOnlyAccessEntity>();
                if (entity.TryGet(out HpAccessor hpAccessor))
                    hpAccessor.Decrease(dto.Damage);
            }
        }
    }
}