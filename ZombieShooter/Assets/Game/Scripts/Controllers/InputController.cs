﻿using System;
using AtomicFramework;
using AtomicFramework.Impl;
using Essentials;
using JetBrains.Annotations;
using UnityEngine;
using ZombieShooter.Systems;

namespace ZombieShooter.Controllers
{
    public sealed class InputController : IDisposable
    {
        [NotNull] private readonly Camera _camera;
        [NotNull] private readonly IInputSystem _inputSystem;
        [NotNull] private readonly IReadOnlyAccessEntity _accessEntity;

        public InputController(
            [NotNull] Camera camera,
            [NotNull] IInputSystem inputSystem,
            [NotNull] IReadOnlyAccessEntity accessEntity)
        {
            this._camera = camera ? camera : throw new ArgumentNullException(nameof(camera));
            this._inputSystem = inputSystem ?? throw new ArgumentNullException(nameof(inputSystem));
            this._accessEntity = accessEntity ?? throw new ArgumentNullException(nameof(accessEntity));

            this.Subscribe(this._inputSystem);
        }

        public void Dispose()
        {
            this.Unsubscribe(this._inputSystem);
        }

        private void Subscribe(IInputSystem inputSystem)
        {
            inputSystem.Moving += this.OnMoving;
            inputSystem.Looking += this.OnLooking;
            inputSystem.FireTriggered += this.OnFireTriggered;
        }

        private void Unsubscribe(IInputSystem inputSystem)
        {
            inputSystem.Moving -= this.OnMoving;
            inputSystem.Looking -= this.OnLooking;
            inputSystem.FireTriggered -= this.OnFireTriggered;
        }

        private void OnMoving(Vector2 direction)
        {
            if (this._accessEntity.TryGet(out MoveAccessor moveAccessor))
                moveAccessor.Write(direction);
        }

        private void OnLooking(Vector2 lookScreenPoint)
        {
            if (this._accessEntity.TryGet(out RotateAccessor rotateAccessor))
            {
                Vector3 rotationVector = this.ConvertScreenLookPointToToRotationVector(lookScreenPoint);
                rotateAccessor.Write(rotationVector.y);
            }
        }

        private void OnFireTriggered()
        {
            if (this._accessEntity.TryGet(out IAttackTriggerAccessor attackAccessor))
                attackAccessor.Invoke();
        }

        private Vector3 ConvertScreenLookPointToToRotationVector(Vector2 lookScreenPoint)
        {
            if (!this._accessEntity.TryGet(out PositionAccessor positionAccessor))
                return default;

            float cameraDepth = this._camera.transform.position.y;
            Vector3 lookWorldPoint = this._camera.ScreenToWorldPoint(lookScreenPoint.InsertZ(cameraDepth));
            Vector3 lookPointDirection = lookWorldPoint - positionAccessor.Get();

            return Quaternion.LookRotation(lookPointDirection.SetY(0)).eulerAngles;
        }
    }
}