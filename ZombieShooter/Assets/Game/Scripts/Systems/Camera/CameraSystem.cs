﻿using System;
using UnityEngine;
using Cinemachine;
using JetBrains.Annotations;

namespace ZombieShooter.Systems
{
    public sealed class CameraSystem
    {
        [NotNull] private readonly CinemachineVirtualCamera _virtualCamera;

        public CameraSystem([NotNull] CinemachineVirtualCamera virtualCamera)
        {
            this._virtualCamera = virtualCamera
                ? virtualCamera : throw new ArgumentNullException(nameof(virtualCamera));
        }

        public void SetTarget([CanBeNull] Transform target)
        {
            this._virtualCamera.Follow = target;
        }
    }
}