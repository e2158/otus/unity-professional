﻿using Zenject;
using UnityEngine;
using Cinemachine;
using Sirenix.OdinInspector;

namespace ZombieShooter.Systems
{
    [DisallowMultipleComponent]
    internal sealed class CameraInstaller : MonoInstaller
    {
        [Required, SceneObjectsOnly]
        [SerializeField] private Camera _camera;

        [Required, SceneObjectsOnly]
        [SerializeField] private CinemachineVirtualCamera _virtualCamera;

        private void OnValidate()
        {
            this._camera ??= FindObjectOfType<Camera>();
            this._virtualCamera ??= FindObjectOfType<CinemachineVirtualCamera>();
        }

        public override void InstallBindings()
        {
            this.Container.Bind<Camera>().FromInstance(this._camera);
            this.Container.Bind<CinemachineVirtualCamera>().FromInstance(this._virtualCamera);
            this.Container.Bind<CameraSystem>().AsSingle();
        }
    }
}