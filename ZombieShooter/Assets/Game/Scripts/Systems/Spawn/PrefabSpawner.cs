﻿using System;
using Zenject;
using UnityEngine;
using JetBrains.Annotations;

namespace ZombieShooter.Systems
{
    internal sealed class PrefabSpawner
    {
        [NotNull] private readonly GameObject _prefab;
        [NotNull] private readonly Transform _container;
        [NotNull] private readonly IInstantiator _instantiator;

        public PrefabSpawner([NotNull] GameObject prefab, [NotNull] Transform container, [NotNull] IInstantiator instantiator)
        {
            this._prefab = prefab ? prefab : throw new ArgumentNullException(nameof(prefab));
            this._instantiator = instantiator ?? throw new ArgumentNullException(nameof(instantiator));
            this._container = container ? container : throw new ArgumentNullException(nameof(container));
        }

        public void Spawn(Vector3 position)
        {
            this._instantiator.InstantiatePrefab(this._prefab, position, Quaternion.identity, this._container);
        }
    }
}