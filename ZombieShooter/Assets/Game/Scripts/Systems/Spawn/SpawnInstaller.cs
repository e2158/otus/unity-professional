﻿using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;

namespace ZombieShooter.Systems
{
    [DisallowMultipleComponent]
    internal sealed class SpawnInstaller : MonoInstaller
    {
        [Required, AssetSelector]
        [SerializeField] private LevelSpawnConfig _levelSpawnConfig;

        [Required, AssetSelector]
        [SerializeField] private GameObject _prefab;

        [Required, SceneObjectsOnly]
        [SerializeField] private Transform _container;

        public override void InstallBindings()
        {
            this.Container.Bind<GameObject>().FromInstance(this._prefab).WhenInjectedInto<PrefabSpawner>();
            this.Container.Bind<Transform>().FromInstance(this._container).WhenInjectedInto<PrefabSpawner>();
            this.Container.Bind<PrefabSpawner>().AsSingle();

            this.Container.Bind<LevelSpawnConfig>().FromInstance(this._levelSpawnConfig).WhenInjectedInto<SpawnSystem>();
            this.Container.Bind<SpawnSystem>().AsSingle();
        }
    }
}