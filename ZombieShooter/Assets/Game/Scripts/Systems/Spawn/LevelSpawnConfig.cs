﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace ZombieShooter.Systems
{
    [CreateAssetMenu(fileName = nameof(LevelSpawnConfig), menuName = "Configs/" + nameof(LevelSpawnConfig))]
    internal sealed class LevelSpawnConfig : ScriptableObject
    {
        [ListDrawerSettings(Expanded = true)]
        [SerializeField] private Vector3[] _spawnPoints;

        [SuffixLabel("sec", true)]
        [SerializeField] private float _cooldown;

        public IReadOnlyList<Vector3> SpawnPoints => this._spawnPoints;
        public TimeSpan Cooldown => TimeSpan.FromSeconds(this._cooldown);
    }
}