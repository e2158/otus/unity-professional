﻿using System;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Random = UnityEngine.Random;

namespace ZombieShooter.Systems
{
    public sealed class SpawnSystem
    {
        [NotNull] private readonly PrefabSpawner _prefabSpawner;
        [NotNull] private readonly LevelSpawnConfig _levelSpawnConfig;

        [CanBeNull] private CancellationTokenSource _cts;

        internal SpawnSystem([NotNull] PrefabSpawner prefabSpawner, [NotNull] LevelSpawnConfig levelSpawnConfig)
        {
            this._prefabSpawner = prefabSpawner ?? throw new ArgumentNullException(nameof(prefabSpawner));
            this._levelSpawnConfig = levelSpawnConfig ? levelSpawnConfig : throw new ArgumentNullException(nameof(levelSpawnConfig));
        }

        public void StartSpawn()
        {
            this.StopSpawn();
            this._cts = new CancellationTokenSource();
            this.SpawnRoutine(this._cts.Token).SuppressCancellationThrow().Forget();
        }

        public void StopSpawn()
        {
            if (this._cts != null)
            {
                this._cts.Cancel();
                this._cts.Dispose();
                this._cts = null;
            }
        }

        private async UniTask SpawnRoutine(CancellationToken cancellationToken)
        {
            IReadOnlyList<Vector3> spawnPoints = this._levelSpawnConfig.SpawnPoints;

            while (!cancellationToken.IsCancellationRequested && spawnPoints.Count > 0)
            {
                int index = Random.Range(0, spawnPoints.Count);
                Vector3 spawnPoint = spawnPoints[index];
                this._prefabSpawner.Spawn(spawnPoint);

                TimeSpan cooldown = this._levelSpawnConfig.Cooldown;
                await UniTask.Delay(cooldown, cancellationToken: cancellationToken);
            }
        }
    }
}