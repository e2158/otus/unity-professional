﻿using System;
using Essentials;
using UnityEngine;

namespace ZombieShooter.Systems
{
    public interface IInputSystem : IEnablable
    {
        event Action<Vector2> Moving;
        event Action<Vector2> Looking;
        event Action FireTriggered;
    }
}