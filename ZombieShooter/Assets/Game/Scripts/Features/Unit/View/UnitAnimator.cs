﻿using System;
using UnityEngine;
using JetBrains.Annotations;
using Essentials;
using ZombieShooter.Features.Unit.Configuration;

namespace ZombieShooter.Features.Unit.View
{
    internal sealed class UnitAnimator
    {
        [NotNull] private readonly Animator _animator;

        private readonly int _attackKeyHash;
        private readonly int _deathKeyHash;
        private readonly int _movingKeyHash;
        private readonly int _moveVerticalKeyHash;
        private readonly int _moveHorizontalKeyHash;

        public UnitAnimator([NotNull] Animator animator, [NotNull] UnitViewConfigData viewData)
        {
            if (viewData == null) throw new ArgumentNullException(nameof(viewData));
            this._animator = animator ? animator : throw new ArgumentNullException(nameof(animator));

            this._attackKeyHash = Animator.StringToHash(viewData.AttackAnimationKey);
            this._deathKeyHash = Animator.StringToHash(viewData.DeathAnimationKey);
            this._movingKeyHash = Animator.StringToHash(viewData.MovingAnimationKey);
            this._moveVerticalKeyHash = Animator.StringToHash(viewData.MoveVerticalAnimationKey);
            this._moveHorizontalKeyHash = Animator.StringToHash(viewData.MoveHorizontalAnimationKey);
        }

        public void PlayDeath()
        {
            this._animator.SetTrigger(this._deathKeyHash);
        }

        public void PlayAttack()
        {
            this._animator.SetTrigger(this._attackKeyHash);
        }

        public void SetMovingData(Vector2 direction, float rotation)
        {
            Vector2 localDirection = direction.RotateDeg(rotation);
            this._animator.SetFloat(this._moveVerticalKeyHash, localDirection.y);
            this._animator.SetFloat(this._moveHorizontalKeyHash, localDirection.x);
            this._animator.SetBool(this._movingKeyHash, !localDirection.EqualsApproximately(Vector2.zero));
        }
    }
}