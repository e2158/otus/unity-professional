﻿using System;
using UnityEngine;
using JetBrains.Annotations;
using Essentials;
using AtomicFramework;

namespace ZombieShooter.Features.Unit.View
{
    internal sealed class UnitView : IEnablable
    {
        [NotNull] private readonly UnitAnimator _animator;
        [CanBeNull] private readonly ParticleSystemEmitter _damageVfx;

        [NotNull] private readonly IReadOnlyAtomicValue<bool> _isDead;
        [NotNull] private readonly IReadOnlyAtomicValue<float> _rotation;
        [NotNull] private readonly IReadOnlyAtomicValue<Vector2> _movementDirection;
        [NotNull] private readonly IReadOnlyAtomicEvent<int> _damageReactionEvent;
        [NotNull] private readonly IReadOnlyAtomicEvent _attackReactionEvent;

        public bool IsEnabled { get; private set; }

        public UnitView(
            [NotNull] UnitAnimator animator,
            [CanBeNull] ParticleSystemEmitter damageVfx,
            [NotNull] IReadOnlyAtomicValue<bool> isDead,
            [NotNull] IReadOnlyAtomicValue<float> rotation,
            [NotNull] IReadOnlyAtomicValue<Vector2> movementDirection,
            [NotNull] IReadOnlyAtomicEvent<int> damageReactionEvent,
            [NotNull] IReadOnlyAtomicEvent attackReactionEvent)
        {
            this._damageVfx = damageVfx;
            this._animator = animator ?? throw new ArgumentNullException(nameof(animator));

            this._isDead = isDead ?? throw new ArgumentNullException(nameof(isDead));
            this._rotation = rotation ?? throw new ArgumentNullException(nameof(rotation));
            this._movementDirection = movementDirection ?? throw new ArgumentNullException(nameof(movementDirection));
            this._damageReactionEvent = damageReactionEvent ?? throw new ArgumentNullException(nameof(damageReactionEvent));
            this._attackReactionEvent = attackReactionEvent ?? throw new ArgumentNullException(nameof(attackReactionEvent));
        }

        public void Enable()
        {
            if (this.IsEnabled)
                throw new InvalidOperationException("It's already enabled. Can't enable twice");

            this.IsEnabled = true;
            this._isDead.Changed += this.OnIsDeadChanged;
            this._rotation.Changed += this.OnRotationChanged;
            this._movementDirection.Changed += this.OnMovementDirectionChanged;
            this._damageReactionEvent.Invoked += this.OnDamage;
            this._attackReactionEvent.Invoked += this.OnAttack;
        }

        public void Disable()
        {
            if (!this.IsEnabled)
                throw new InvalidOperationException("It's already disabled. Can't disable twice");

            this.IsEnabled = false;
            this._isDead.Changed -= this.OnIsDeadChanged;
            this._rotation.Changed -= this.OnRotationChanged;
            this._movementDirection.Changed -= this.OnMovementDirectionChanged;
            this._damageReactionEvent.Invoked -= this.OnDamage;
            this._attackReactionEvent.Invoked -= this.OnAttack;
        }

        private void OnIsDeadChanged(bool isDead)
        {
            if (isDead)
                this._animator.PlayDeath();
        }

        private void OnAttack()
        {
            this._animator.PlayAttack();
        }

        private void OnDamage(int damage)
        {
            this._damageVfx?.Play();
        }

        private void OnMovementDirectionChanged(Vector2 direction)
        {
            this._animator.SetMovingData(direction, this._rotation.Value);
        }

        private void OnRotationChanged(float rotation)
        {
            this._animator.SetMovingData(this._movementDirection.Value, rotation);
        }
    }
}