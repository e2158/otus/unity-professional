﻿using UnityEngine;
using AtomicFramework;

namespace ZombieShooter.Features.Unit.Model
{
    [DisallowMultipleComponent]
    internal sealed class UnitReactionEvents : MonoBehaviour
    {
        [SerializeField] private AtomicEvent<int> _damage;

        public AtomicEvent<int> Damage => this._damage;
    }
}