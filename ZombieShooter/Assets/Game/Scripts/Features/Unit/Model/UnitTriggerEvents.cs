﻿using UnityEngine;
using AtomicFramework;

namespace ZombieShooter.Features.Unit.Model
{
    [DisallowMultipleComponent]
    internal sealed class UnitTriggerEvents : MonoBehaviour
    {
        [SerializeField] private AtomicEvent _death;

        public AtomicEvent Death => this._death;
    }
}