﻿using UnityEngine;
using AtomicFramework;

namespace ZombieShooter.Features.Unit.Model
{
    public interface IReadOnlyUnitState
    {
        IReadOnlyAtomicValue<int> Hp { get; }
        IReadOnlyAtomicValue<bool> IsDead { get; }
        IReadOnlyAtomicValue<bool> CanMove { get; }
        IReadOnlyAtomicValue<float> Rotation { get; }
        IReadOnlyAtomicValue<Vector2> MoveDirection { get; }
    }

    [DisallowMultipleComponent]
    internal sealed class UnitState : MonoBehaviour, IReadOnlyUnitState
    {
        [SerializeField] private AtomicValue<int> _hp;
        [SerializeField] private AtomicValue<bool> _isDead;
        [SerializeField] private AtomicValue<bool> _canMove;
        [SerializeField] private AtomicValue<float> _rotation;
        [SerializeField] private AtomicValue<Vector2> _moveDirection;

        public AtomicValue<int> Hp => this._hp;
        public AtomicValue<bool> IsDead => this._isDead;
        public AtomicValue<bool> CanMove => this._canMove;
        public AtomicValue<float> Rotation => this._rotation;
        public AtomicValue<Vector2> MoveDirection => this._moveDirection;

        IReadOnlyAtomicValue<int> IReadOnlyUnitState.Hp => this._hp;
        IReadOnlyAtomicValue<bool> IReadOnlyUnitState.IsDead => this._isDead;
        IReadOnlyAtomicValue<bool> IReadOnlyUnitState.CanMove => this._canMove;
        IReadOnlyAtomicValue<float> IReadOnlyUnitState.Rotation => this._rotation;
        IReadOnlyAtomicValue<Vector2> IReadOnlyUnitState.MoveDirection => this._moveDirection;
    }
}