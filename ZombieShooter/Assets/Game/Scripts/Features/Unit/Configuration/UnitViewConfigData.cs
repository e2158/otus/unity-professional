﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace ZombieShooter.Features.Unit.Configuration
{
    [Serializable]
    public sealed class UnitViewConfigData
    {
        [SerializeField, Required] private string _attackAnimationKey;
        [SerializeField, Required] private string _deathAnimationKey;
        [SerializeField, Required] private string _movingAnimationKey;
        [SerializeField, Required] private string _moveVerticalAnimationKey;
        [SerializeField, Required] private string _moveHorizontalAnimationKey;

        public string AttackAnimationKey => this._attackAnimationKey;
        public string DeathAnimationKey => this._deathAnimationKey;
        public string MovingAnimationKey => this._movingAnimationKey;
        public string MoveVerticalAnimationKey => this._moveVerticalAnimationKey;
        public string MoveHorizontalAnimationKey => this._moveHorizontalAnimationKey;
    }
}