﻿using System;
using UnityEngine;
using Essentials;
using AtomicFramework;

namespace ZombieShooter.Features.Unit.Configuration
{
    [Serializable]
    public sealed class UnitModelConfigData : IValidatable
    {
        [SerializeField] private AtomicValue<int> _maxHp = new();
        [SerializeField] private AtomicValue<float> _movementSpeed = new();
        [SerializeField] private AtomicValue<float> _rotationSpeed = new();

        public IReadOnlyAtomicValue<int> MaxHp => this._maxHp;
        public IReadOnlyAtomicValue<float> MovementSpeed => this._movementSpeed;
        public IReadOnlyAtomicValue<float> RotationSpeed => this._rotationSpeed;

        void IValidatable.Validate()
        {
            this._maxHp.Value = Mathf.Max(0, this._maxHp.Value);
            this._movementSpeed.Value = Mathf.Max(0, this._movementSpeed.Value);
            this._rotationSpeed.Value = Mathf.Max(0, this._rotationSpeed.Value);
        }
    }
}