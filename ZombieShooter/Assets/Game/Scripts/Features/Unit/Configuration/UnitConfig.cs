﻿using UnityEngine;
using Sirenix.OdinInspector;
using Essentials;

namespace ZombieShooter.Features.Unit.Configuration
{
    [CreateAssetMenu(fileName = nameof(UnitConfig), menuName = "Configs/" + nameof(UnitConfig))]
    public sealed class UnitConfig : ScriptableObject
    {
        [SerializeField, HideLabel, BoxGroup("Model Data")]
        private UnitModelConfigData _modelData;

        [SerializeField, HideLabel, BoxGroup("View Data")]
        private UnitViewConfigData _viewData;

        public UnitModelConfigData ModelData => this._modelData;
        public UnitViewConfigData ViewData => this._viewData;

        private void OnValidate()
        {
            ((IValidatable)this._modelData).Validate();
        }
    }
}