﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;
using Essentials;
using AtomicFramework;
using AtomicFramework.Impl;
using JetBrains.Annotations;
using ZombieShooter.Features.Unit.Model;
using ZombieShooter.Features.Unit.View;
using ZombieShooter.Features.Unit.Configuration;
using ZombieShooter.Features.Weapon;
using ZombieShooter.Features.Weapon.Configuration;

namespace ZombieShooter.Features.Unit
{
    [DisallowMultipleComponent]
    public sealed class UnitObject : MonoBehaviour
    {
        [Header("Components")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private Animator _animator;

        [ChildGameObjectsOnly]
        [SerializeField] private ParticleSystemEmitter _damageVfx;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private WeaponObject _weaponObject;

        [Header("Model")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private UnitState _state;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private UnitTriggerEvents _triggerEvents;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private UnitReactionEvents _reactionEvents;

        [Header("Logic")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private MechanicsRunner _mechanicsRunner;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private AccessEntity _accessEntity;

        private UnitView _unitView;

        public IReadOnlyAccessEntity AccessEntity => this._accessEntity;

        private void OnValidate()
        {
            this._animator ??= this.GetComponentInChildren<Animator>();
            this._weaponObject ??= this.GetComponentInChildren<WeaponObject>();
            this._damageVfx ??= this.GetComponentInChildren<ParticleSystemEmitter>();

            this._state ??= this.GetComponentInChildren<UnitState>();
            this._triggerEvents ??= this.GetComponentInChildren<UnitTriggerEvents>();
            this._reactionEvents ??= this.GetComponentInChildren<UnitReactionEvents>();

            this._mechanicsRunner ??= this.GetComponentInChildren<MechanicsRunner>();
            this._accessEntity ??= this.GetComponentInChildren<AccessEntity>();
        }

        public void Initialize([NotNull] UnitConfig unitConfig, [NotNull] WeaponConfig weaponConfig)
        {
            if (unitConfig == null) throw new ArgumentNullException(nameof(unitConfig));
            if (weaponConfig == null) throw new ArgumentNullException(nameof(weaponConfig));

            this._weaponObject.Initialize(weaponConfig);

            this.InitAccessors();
            this.InitView(unitConfig);
            this.InitMechanics(unitConfig);

            this._state.Hp.Value = unitConfig.ModelData.MaxHp.Value;
            this._unitView?.Enable();
        }

        private void OnDestroy()
        {
            this._unitView?.Disable();
        }

        private void InitAccessors()
        {
            UnitState state = this._state;

            this._accessEntity.Add(new HpAccessor(state.Hp));
            this._accessEntity.Add(new RotateAccessor(state.Rotation));
            this._accessEntity.Add(new MoveAccessor(state.MoveDirection));
            this._accessEntity.Add(new PositionAccessor(this.transform));
            this._accessEntity.Add(new DeathReactionAccessor(this._triggerEvents.Death));

            var weaponAttackTrigger = this._weaponObject.AccessEntity.Get<IAttackTriggerAccessor>();
            var attackTriggerRoutingAccessor = new AttackTriggerRoutingAccessor(weaponAttackTrigger);
            this._accessEntity.Add<IAttackTriggerAccessor>(attackTriggerRoutingAccessor);
            this._accessEntity.Add(attackTriggerRoutingAccessor);
            attackTriggerRoutingAccessor.Enable();

            var weaponTakeDamageReaction = this._weaponObject.AccessEntity.Get<ITakeDamageReactionAccessor>();
            var takeDamageReactionRoutingAccessor = new TakeDamageReactionRoutingAccessor(weaponTakeDamageReaction);
            this._accessEntity.Add<ITakeDamageReactionAccessor>(takeDamageReactionRoutingAccessor);
            this._accessEntity.Add(takeDamageReactionRoutingAccessor);
            takeDamageReactionRoutingAccessor.Enable();
        }

        private void InitView(UnitConfig unitConfig)
        {
            UnitState state = this._state;
            UnitViewConfigData viewData = unitConfig.ViewData;
            AttackReactionAccessor attackReactionAccessor =
                this._weaponObject.AccessEntity.Get<AttackReactionAccessor>();

            UnitAnimator unitAnimator = new(this._animator, viewData);

            this._unitView = new UnitView(unitAnimator, this._damageVfx,
                state.IsDead, state.Rotation, state.MoveDirection,
                this._reactionEvents.Damage, attackReactionAccessor.Get());
        }

        private void InitMechanics(UnitConfig unitConfig)
        {
            UnitState state = this._state;
            Transform transform = this.transform;
            UnitModelConfigData modelData = unitConfig.ModelData;
            UnitTriggerEvents triggerEvents = this._triggerEvents;
            UnitReactionEvents reactionEvents = this._reactionEvents;

            var disableWhenDead = new IEnablable[]
            {
                this._accessEntity.Get<AttackTriggerRoutingAccessor>(),
                this._accessEntity.Get<TakeDamageReactionRoutingAccessor>(),
            };

            this._mechanicsRunner.Add(
                new LifeMechanics(state.Hp, triggerEvents.Death),
                new DeathMechanics(state.IsDead, triggerEvents.Death, disableWhenDead),

                new CanMoveMechanics(state.CanMove, state.IsDead),
                new RotationMechanics(transform, state.CanMove, modelData.RotationSpeed, state.Rotation),
                new MovementMechanics(transform, state.CanMove, modelData.MovementSpeed, state.MoveDirection),

                new DamageReceivingMechanics(state.Hp, reactionEvents.Damage));
        }
    }
}