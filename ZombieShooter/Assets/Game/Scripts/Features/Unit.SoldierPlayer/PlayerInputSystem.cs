﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;
using ZombieShooter.Systems;

namespace ZombieShooter.Features.Unit.SoldierPlayer
{
    internal sealed class PlayerInputSystem : IInputSystem
    {
        public event Action<Vector2> Moving;
        public event Action<Vector2> Looking;
        public event Action FireTriggered;

        [NotNull] private readonly GameInputAction _input;

        public bool IsEnabled => this._input.Player.enabled;

        public PlayerInputSystem([NotNull] GameInputAction input)
        {
            this._input = input ?? throw new ArgumentNullException(nameof(input));
        }

        public void Enable()
        {
            if (this.IsEnabled)
                throw new InvalidOperationException("It's already enabled. Can't enable twice");

            GameInputAction.PlayerActions playerActions = this._input.Player;
            this.Subscribe(playerActions);
            playerActions.Enable();
        }

        public void Disable()
        {
            if (!this.IsEnabled)
                throw new InvalidOperationException("It's already disabled. Can't disable twice");

            GameInputAction.PlayerActions playerActions = this._input.Player;
            this.Unsubscribe(playerActions);
            playerActions.Disable();
        }

        private void Subscribe(GameInputAction.PlayerActions playerActions)
        {
            playerActions.Move.performed += this.OnMovePerformed;
            playerActions.Move.canceled += this.OnMoveCanceled;
            playerActions.Look.performed += this.OnLookPerformed;
            playerActions.Look.canceled += this.OnLookCanceled;
            playerActions.Fire.performed += this.OnFirePerformed;
        }

        private void Unsubscribe(GameInputAction.PlayerActions playerActions)
        {
            playerActions.Move.performed -= this.OnMovePerformed;
            playerActions.Move.canceled -= this.OnMoveCanceled;
            playerActions.Look.performed -= this.OnLookPerformed;
            playerActions.Look.canceled -= this.OnLookCanceled;
            playerActions.Fire.performed -= this.OnFirePerformed;
        }

        private void OnMovePerformed(InputAction.CallbackContext context)
        {
            Vector2 direction = context.ReadValue<Vector2>();
            this.Moving?.Invoke(direction);
        }

        private void OnMoveCanceled(InputAction.CallbackContext context)
        {
            this.Moving?.Invoke(Vector2.zero);
        }

        private void OnLookPerformed(InputAction.CallbackContext context)
        {
            Vector2 direction = context.ReadValue<Vector2>();
            this.Looking?.Invoke(direction);
        }

        private void OnLookCanceled(InputAction.CallbackContext context)
        {
            this.Looking?.Invoke(Vector2.zero);
        }

        private void OnFirePerformed(InputAction.CallbackContext context)
        {
            this.FireTriggered?.Invoke();
        }
    }
}