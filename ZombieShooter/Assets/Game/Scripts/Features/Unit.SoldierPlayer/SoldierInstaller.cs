﻿using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Essentials;
using AtomicFramework;
using ZombieShooter.Systems;
using ZombieShooter.Controllers;
using ZombieShooter.Features.Unit.Configuration;
using ZombieShooter.Features.Weapon.Configuration;

namespace ZombieShooter.Features.Unit.SoldierPlayer
{
    [DisallowMultipleComponent]
    internal sealed class SoldierInstaller : MonoInstaller
    {
        [Required]
        [SerializeField] private UnitObject _unitObject;

        [Required, AssetSelector]
        [SerializeField] private UnitConfig _unitConfig;

        [Required, AssetSelector]
        [SerializeField] private WeaponConfig _weaponConfig;

        private void OnValidate()
        {
            this._unitObject ??= FindObjectOfType<UnitObject>();
        }

        public override void Start()
        {
            base.Start();
            this.SetupGame();
        }

        private void OnDestroy()
        {
            this.UndoGameSetup();
        }

        public override void InstallBindings()
        {
            this.Container.Bind<IReadOnlyAccessEntity>().FromInstance(this._unitObject.AccessEntity);

            this.Container.BindInterfacesTo<PlayerInputSystem>().AsSingle();
            this.Container.BindInterfacesAndSelfTo<InputController>().AsSingle();
        }

        private void SetupGame()
        {
            this._unitObject.Initialize(this._unitConfig, this._weaponConfig);

            this.Container.Resolve<IInputSystem>().Enable();
            this.Container.Resolve<Collection<IReadOnlyAccessEntity>>().Add(this._unitObject.AccessEntity);
        }

        private void UndoGameSetup()
        {
            this.Container.Resolve<IInputSystem>().Disable();
            this.Container.Resolve<Collection<IReadOnlyAccessEntity>>().Remove(this._unitObject.AccessEntity);
        }
    }
}