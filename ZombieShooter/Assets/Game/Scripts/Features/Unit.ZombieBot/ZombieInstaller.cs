﻿using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Essentials;
using AtomicFramework;
using ZombieShooter.Systems;
using ZombieShooter.Controllers;
using ZombieShooter.Features.Unit.Configuration;
using ZombieShooter.Features.Weapon.Configuration;

namespace ZombieShooter.Features.Unit.ZombieBot
{
    [DisallowMultipleComponent]
    internal sealed class ZombieInstaller : MonoInstaller
    {
        [Required]
        [SerializeField] private UnitObject _unitObject;

        [Required, AssetSelector]
        [SerializeField] private UnitConfig _unitConfig;

        [Required, AssetSelector]
        [SerializeField] private WeaponConfig _weaponConfig;

        [Required, AssetSelector]
        [SerializeField] private AiBrainConfig _aiBrainConfig;

        private void OnValidate()
        {
            this._unitObject ??= FindObjectOfType<UnitObject>();
        }

        public override void Start()
        {
            base.Start();
            this.SetupGame();
        }

        private void OnDestroy()
        {
            this.UndoGameSetup();
        }

        public override void InstallBindings()
        {
            Vector3 startPoint = this.transform.position;
            IReadOnlyAccessEntity zombieEntity = this._unitObject.AccessEntity;

            this.Container.Bind<AiBrainConfig>().FromInstance(this._aiBrainConfig);
            this.Container.Bind<AiBrain>().AsSingle().WithArguments(zombieEntity, startPoint);

            this.Container.BindInterfacesTo<AiInputSystem>().AsSingle();
            this.Container.BindInterfacesAndSelfTo<InputController>().AsSingle().WithArguments(zombieEntity);
        }

        private void SetupGame()
        {
            this._unitObject.Initialize(this._unitConfig, this._weaponConfig);

            this.Container.Resolve<IInputSystem>().Enable();
            this.Container.Resolve<Collection<IReadOnlyAccessEntity>>().Add(this._unitObject.AccessEntity);

            IReadOnlyAccessEntity soldierEntity = this.Container.Resolve<IReadOnlyAccessEntity>();
            this.Container.Resolve<AiBrain>().SetTarget(soldierEntity);

            this._unitObject.GetComponent<AutoDestroyerOnDeath>().enabled = true;
        }

        private void UndoGameSetup()
        {
            this.Container.Resolve<AiBrain>().Stop();
            this.Container.Resolve<IInputSystem>().Disable();
            this.Container.Resolve<Collection<IReadOnlyAccessEntity>>().Remove(this._unitObject.AccessEntity);

            this._unitObject.GetComponent<AutoDestroyerOnDeath>().enabled = false;
        }
    }
}