﻿using UnityEngine;

namespace ZombieShooter.Features.Unit.ZombieBot
{
    public readonly struct AiBrainData
    {
        public bool IsAttacking { get; }
        public Vector3 LookPoint { get; }
        public Vector2 MovingDirection { get; }

        public AiBrainData(bool isAttacking, Vector3 lookPoint, Vector2 movingDirection)
        {
            this.IsAttacking = isAttacking;
            this.LookPoint = lookPoint;
            this.MovingDirection = movingDirection;
        }
    }
}