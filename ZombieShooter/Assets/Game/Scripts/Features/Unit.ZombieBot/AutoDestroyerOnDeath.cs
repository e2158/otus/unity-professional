﻿using System;
using System.Threading;
using UnityEngine;
using AtomicFramework;
using AtomicFramework.Impl;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;

namespace ZombieShooter.Features.Unit.ZombieBot
{
    [DisallowMultipleComponent]
    internal sealed class AutoDestroyerOnDeath : MonoBehaviour
    {
        [Header("Settings")]
        [MinValue(0), SuffixLabel("sec", true)]
        [SerializeField] private float _delay;

        [Header("Components")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private AccessEntity _accessEntity;

        private void OnValidate()
        {
            this._accessEntity ??= this.GetComponentInChildren<AccessEntity>();
            this.enabled = false;
        }

        private void OnEnable()
        {
            this._accessEntity.Get<DeathReactionAccessor>().Get().Invoked += this.OnDeath;
        }

        private void OnDisable()
        {
            this._accessEntity.Get<DeathReactionAccessor>().Get().Invoked -= this.OnDeath;
        }

        private async void OnDeath()
        {
            if (this._delay > 0)
            {
                CancellationToken cancellationToken = this.GetCancellationTokenOnDestroy();
                await UniTask.Delay(TimeSpan.FromSeconds(this._delay), cancellationToken: cancellationToken);
            }

            Destroy(this.gameObject);
        }
    }
}