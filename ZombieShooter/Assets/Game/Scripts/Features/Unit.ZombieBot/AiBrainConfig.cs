﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace ZombieShooter.Features.Unit.ZombieBot
{
    [CreateAssetMenu(fileName = nameof(AiBrainConfig), menuName = "Configs/" + nameof(AiBrainConfig))]
    public sealed class AiBrainConfig : ScriptableObject
    {
        [MinValue(0), SuffixLabel("m", true)]
        [SerializeField] private float _attackDistance;

        [MinValue(0), SuffixLabel("m", true)]
        [SerializeField] private float _minTargetDistance;

        public float AttackDistance => this._attackDistance;
        public float MinTargetDistance => this._minTargetDistance;
    }
}