﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using ZombieShooter.Systems;

namespace ZombieShooter.Features.Unit.ZombieBot
{
    public sealed class AiInputSystem : IInputSystem
    {
        public event Action<Vector2> Moving;
        public event Action<Vector2> Looking;
        public event Action FireTriggered;

        [NotNull] private readonly AiBrain _brain;
        [NotNull] private readonly Camera _camera;

        public bool IsEnabled { get; private set; }

        public AiInputSystem([NotNull] AiBrain brain, [NotNull] Camera camera)
        {
            this._brain = brain ?? throw new ArgumentNullException(nameof(brain));
            this._camera = camera ? camera : throw new ArgumentNullException(nameof(camera));
        }

        public void Enable()
        {
            if (this.IsEnabled)
                throw new InvalidOperationException("It's already enabled. Can't enable twice");

            this.IsEnabled = true;
            this.Subscribe(this._brain);
        }

        public void Disable()
        {
            if (!this.IsEnabled)
                throw new InvalidOperationException("It's already disabled. Can't disable twice");

            this.IsEnabled = false;
            this.Unsubscribe(this._brain);
        }

        private void Subscribe(AiBrain brain)
        {
            brain.DataUpdated += this.OnBrainDataUpdated;
        }

        private void Unsubscribe(AiBrain brain)
        {
            brain.DataUpdated -= this.OnBrainDataUpdated;
        }

        private void OnBrainDataUpdated(AiBrainData data)
        {
            Vector3 screenPoint = this._camera.WorldToScreenPoint(data.LookPoint);

            this.Moving?.Invoke(data.MovingDirection);
            this.Looking?.Invoke(screenPoint);

            if (data.IsAttacking) this.FireTriggered?.Invoke();
        }
    }
}