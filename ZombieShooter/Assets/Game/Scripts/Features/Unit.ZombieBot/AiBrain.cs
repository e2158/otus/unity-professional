﻿using System;
using System.Threading;
using AtomicFramework;
using AtomicFramework.Impl;
using Cysharp.Threading.Tasks;
using Essentials;
using JetBrains.Annotations;
using UnityEngine;

namespace ZombieShooter.Features.Unit.ZombieBot
{
    public sealed class AiBrain : IDisposable
    {
        public event Action<AiBrainData> DataUpdated;

        private readonly Vector3 _startPoint;
        [NotNull] private readonly AiBrainConfig _config;
        [NotNull] private readonly IReadOnlyAccessEntity _entity;

        public AiBrainData Data { get; private set; }

        [CanBeNull] private CancellationTokenSource _cts;

        public AiBrain([NotNull] IReadOnlyAccessEntity entity,
            [NotNull] AiBrainConfig config, Vector3 startPoint)
        {
            this._config = config ? config : throw new ArgumentNullException(nameof(config));
            this._entity = entity ?? throw new ArgumentNullException(nameof(entity));
            this._startPoint = startPoint;
        }

        public void Dispose()
        {
            this.Stop();
        }

        public void Stop()
        {
            FinalizeCts(ref this._cts);
        }

        public void SetTarget([CanBeNull] IReadOnlyAccessEntity targetEntity)
        {
            FinalizeCts(ref this._cts);

            if (targetEntity != null)
            {
                this._cts = new CancellationTokenSource();
                this.TargetHandlingRoutine(targetEntity, this._cts.Token).Forget();
            }
            else
            {
                this.DirectToStartPoint();
            }
        }

        private async UniTask TargetHandlingRoutine(
            [NotNull] IReadOnlyAccessEntity target, CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                var data = this.CalcDataForTarget(target);
                this.SetData(data);
                await UniTask.Yield();
            }
        }

        private void DirectToStartPoint()
        {
            AiBrainData data = this.CalcDataForPoint(this._startPoint);
            this.SetData(data);
        }

        private AiBrainData CalcDataForTarget([NotNull] IReadOnlyAccessEntity target)
        {
            if (target == null) throw new ArgumentNullException(nameof(target));

            Vector3 lookPoint = default;
            Vector2 movingDirection = default;

            if (target.TryGet(out PositionAccessor targetPosition))
            {
                lookPoint = targetPosition.Get();
                movingDirection = CalcMovingDirection(
                    this._entity, targetPosition.Get(), this._config.MinTargetDistance);
            }

            bool isAttacking = CalcIsAttacking(this._entity, target, this._config.AttackDistance);

            return new AiBrainData(isAttacking, lookPoint, movingDirection);
        }

        private AiBrainData CalcDataForPoint(Vector3 point)
        {
            return new AiBrainData(
                isAttacking: false,
                lookPoint: point,
                movingDirection: CalcMovingDirection(this._entity, point));
        }

        private void SetData(AiBrainData data)
        {
            this.Data = data;
            this.DataUpdated?.Invoke(data);
        }

        private static Vector2 CalcMovingDirection([NotNull] IReadOnlyAccessEntity movingEntity, Vector3 targetPosition, float minDistance = 0)
        {
            if (movingEntity == null) throw new ArgumentNullException(nameof(movingEntity));

            if (movingEntity.TryGet(out PositionAccessor positionAccessor))
            {
                Vector2 distanceVector = (targetPosition - positionAccessor.Get()).CutY();
                float distance = distanceVector.magnitude;

                if (distance > minDistance && !Mathf.Approximately(distance, minDistance))
                    return distanceVector / distance;
            }

            return Vector2.zero;
        }

        private static bool CalcIsAttacking(
            [NotNull] IReadOnlyAccessEntity attacking, [NotNull] IReadOnlyAccessEntity attacked, float attackDistance)
        {
            if (attacking == null) throw new ArgumentNullException(nameof(attacking));
            if (attacked == null) throw new ArgumentNullException(nameof(attacked));

            if (!attacking.TryGet(out PositionAccessor attackingPosition)) return false;
            if (!attacked.TryGet(out PositionAccessor attackedPosition)) return false;

            float sqrAttackDistance = attackDistance * attackDistance;
            float sqrDistance = (attackedPosition.Get() - attackingPosition.Get()).sqrMagnitude;

            return sqrDistance <= sqrAttackDistance;
        }

        private static void FinalizeCts([CanBeNull] ref CancellationTokenSource cts)
        {
            if (cts != null)
            {
                cts.Cancel();
                cts.Dispose();
                cts = null;
            }
        }
    }
}