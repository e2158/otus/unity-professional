﻿using Essentials;
using UnityEngine;
using Sirenix.OdinInspector;

namespace ZombieShooter.Features.Weapon.Configuration
{
    [CreateAssetMenu(fileName = nameof(WeaponConfig), menuName = "Configs/" + nameof(WeaponConfig))]
    public sealed class WeaponConfig : ScriptableObject
    {
        [SerializeField, HideLabel, BoxGroup("Model Data")]
        private WeaponModelConfigData _modelData;

        [SerializeField, HideLabel, BoxGroup("View Data")]
        private WeaponViewConfigData _viewData;

        public WeaponModelConfigData ModelData => this._modelData;
        public WeaponViewConfigData ViewData => this._viewData;

        private void OnValidate()
        {
            ((IValidatable)this._modelData).Validate();
        }
    }
}