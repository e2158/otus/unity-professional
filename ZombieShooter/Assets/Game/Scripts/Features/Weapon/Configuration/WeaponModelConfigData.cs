﻿using System;
using UnityEngine;
using Essentials;
using AtomicFramework;

namespace ZombieShooter.Features.Weapon.Configuration
{
    [Serializable]
    public sealed class WeaponModelConfigData : IValidatable
    {
        [SerializeField] private AtomicValue<LayerMask> _targetLayer;
        [SerializeField] private AtomicValue<int> _bulletsCount = new();
        [SerializeField] private AtomicValue<int> _bulletDamage = new();
        [SerializeField] private AtomicValue<float> _bulletDistance = new();
        [SerializeField] private AtomicValue<float> _damageDelay = new();
        [SerializeField] private AtomicValue<float> _shootCooldown = new();
        [SerializeField] private AtomicValue<float> _refillCooldown = new();

        public IReadOnlyAtomicValue<LayerMask> TargetLayer => this._targetLayer;
        public IReadOnlyAtomicValue<int> BulletsCount => this._bulletsCount;
        public IReadOnlyAtomicValue<int> BulletDamage => this._bulletDamage;
        public IReadOnlyAtomicValue<float> BulletDistance => this._bulletDistance;
        public IReadOnlyAtomicValue<float> DamageDelay => this._damageDelay;
        public IReadOnlyAtomicValue<float> ShootCooldown => this._shootCooldown;
        public IReadOnlyAtomicValue<float> RefillCooldown => this._refillCooldown;

        void IValidatable.Validate()
        {
            this._bulletsCount.Value = Mathf.Max(0, this._bulletsCount.Value);
            this._bulletDamage.Value = Mathf.Max(0, this._bulletDamage.Value);
            this._bulletDistance.Value = Mathf.Max(0, this._bulletDistance.Value);
            this._damageDelay.Value = Mathf.Max(0, this._damageDelay.Value);
            this._shootCooldown.Value = Mathf.Max(0, this._shootCooldown.Value);
            this._refillCooldown.Value = Mathf.Max(0, this._refillCooldown.Value);
        }
    }
}