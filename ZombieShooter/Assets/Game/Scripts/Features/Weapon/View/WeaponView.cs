﻿using System;
using Essentials;
using AtomicFramework;
using AtomicFramework.Impl;
using JetBrains.Annotations;

namespace ZombieShooter.Features.Weapon.View
{
    internal sealed class WeaponView : IEnablable
    {
        [CanBeNull] private readonly ParticleSystemEmitter _shootVfx;
        [NotNull] private readonly IReadOnlyAtomicEvent<DamageDto> _takeDamageReactionEvent;

        public bool IsEnabled { get; private set; }

        public WeaponView([CanBeNull] ParticleSystemEmitter shootVfx, [NotNull] IReadOnlyAtomicEvent<DamageDto> takeDamageReactionEvent)
        {
            this._shootVfx = shootVfx;
            this._takeDamageReactionEvent = takeDamageReactionEvent ?? throw new ArgumentNullException(nameof(takeDamageReactionEvent));
        }

        public void Enable()
        {
            if (this.IsEnabled)
                throw new InvalidOperationException("It's already enabled. Can't enable twice");

            this.IsEnabled = true;
            this._takeDamageReactionEvent.Invoked += this.OnTakeDamage;
        }

        public void Disable()
        {
            if (!this.IsEnabled)
                throw new InvalidOperationException("It's already disabled. Can't disable twice");

            this.IsEnabled = false;
            this._takeDamageReactionEvent.Invoked -= this.OnTakeDamage;
        }

        private void OnTakeDamage(DamageDto dto)
        {
            this._shootVfx?.Play();
        }
    }
}