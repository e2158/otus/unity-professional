﻿using AtomicFramework;
using UnityEngine;

namespace ZombieShooter.Features.Weapon.Model
{
    public interface IReadOnlyWeaponState
    {
        IReadOnlyAtomicValue<bool> CanShoot { get; }
        IReadOnlyAtomicValue<bool> CanRefill { get; }
        IReadOnlyAtomicValue<int> LoadedBulletsCount { get; }
        IReadOnlyAtomicValue<float> LastShootTime { get; }
        IReadOnlyAtomicValue<float> LastRefillingTime { get; }
    }

    [DisallowMultipleComponent]
    internal sealed class WeaponState : MonoBehaviour, IReadOnlyWeaponState
    {
        [SerializeField] private AtomicValue<bool> _canShoot;
        [SerializeField] private AtomicValue<bool> _canRefill;
        [SerializeField] private AtomicValue<int> _loadedBulletsCount;
        [SerializeField] private AtomicValue<float> _lastShootTime;
        [SerializeField] private AtomicValue<float> _lastRefillingTime;

        public AtomicValue<bool> CanShoot => this._canShoot;
        public AtomicValue<bool> CanRefill => this._canRefill;
        public AtomicValue<int> LoadedBulletsCount => this._loadedBulletsCount;
        public AtomicValue<float> LastShootTime => this._lastShootTime;
        public AtomicValue<float> LastRefillingTime => this._lastRefillingTime;

        IReadOnlyAtomicValue<bool> IReadOnlyWeaponState.CanShoot => this._canShoot;
        IReadOnlyAtomicValue<bool> IReadOnlyWeaponState.CanRefill => this._canRefill;
        IReadOnlyAtomicValue<int> IReadOnlyWeaponState.LoadedBulletsCount => this._loadedBulletsCount;
        IReadOnlyAtomicValue<float> IReadOnlyWeaponState.LastShootTime => this._lastShootTime;
        IReadOnlyAtomicValue<float> IReadOnlyWeaponState.LastRefillingTime => this._lastRefillingTime;
    }
}