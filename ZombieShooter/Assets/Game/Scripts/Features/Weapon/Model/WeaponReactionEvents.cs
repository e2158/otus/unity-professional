﻿using UnityEngine;
using AtomicFramework;
using AtomicFramework.Impl;

namespace ZombieShooter.Features.Weapon.Model
{
    [DisallowMultipleComponent]
    internal sealed class WeaponReactionEvents : MonoBehaviour
    {
        [SerializeField] private AtomicEvent _shoot;
        [SerializeField] private AtomicEvent<DamageDto> _takeDamage;

        public AtomicEvent Shoot => this._shoot;
        public AtomicEvent<DamageDto> TakeDamage => this._takeDamage;
    }
}