﻿using UnityEngine;
using AtomicFramework;

namespace ZombieShooter.Features.Weapon.Model
{
    [DisallowMultipleComponent]
    internal sealed class WeaponTriggerEvents : MonoBehaviour
    {
        [SerializeField] private AtomicEvent _shoot;

        public AtomicEvent Shoot => this._shoot;
    }
}