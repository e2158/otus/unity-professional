﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using AtomicFramework;
using AtomicFramework.Impl;
using Essentials;
using JetBrains.Annotations;
using ZombieShooter.Features.Weapon.View;
using ZombieShooter.Features.Weapon.Model;
using ZombieShooter.Features.Weapon.Configuration;

namespace ZombieShooter.Features.Weapon
{
    [DisallowMultipleComponent]
    public sealed class WeaponObject : MonoBehaviour
    {
        [Header("Components")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private Transform _shootPoint;

        [ChildGameObjectsOnly]
        [SerializeField] private ParticleSystemEmitter _shootVfx;

        [Header("Model")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private WeaponState _state;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private WeaponTriggerEvents _triggerEvents;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private WeaponReactionEvents _reactionEvents;

        [Header("Logic")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private MechanicsRunner _mechanicsRunner;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private AccessEntity _accessEntity;

        private WeaponView _weaponView;

        public IReadOnlyAccessEntity AccessEntity => this._accessEntity;

        private void OnValidate()
        {
            this._shootVfx ??= this.GetComponentInChildren<ParticleSystemEmitter>();

            this._state ??= this.GetComponentInChildren<WeaponState>();
            this._triggerEvents ??= this.GetComponentInChildren<WeaponTriggerEvents>();
            this._reactionEvents ??= this.GetComponentInChildren<WeaponReactionEvents>();

            this._mechanicsRunner ??= this.GetComponentInChildren<MechanicsRunner>();
            this._accessEntity ??= this.GetComponentInChildren<AccessEntity>();
        }

        public void Initialize([NotNull] WeaponConfig weaponConfig)
        {
            if (weaponConfig == null) throw new ArgumentNullException(nameof(weaponConfig));

            this.InitView();
            this.InitMechanics(weaponConfig);
            this.InitAccessors();

            this._state.LoadedBulletsCount.Value = weaponConfig.ModelData.BulletsCount.Value;
            this._weaponView.Enable();
        }

        private void OnDestroy()
        {
            this._weaponView?.Disable();
        }

        private void InitView()
        {
            this._weaponView = new WeaponView(this._shootVfx, this._reactionEvents.TakeDamage);
        }

        private void InitMechanics(WeaponConfig weaponConfig)
        {
            WeaponState state = this._state;
            WeaponModelConfigData modelData = weaponConfig.ModelData;
            DamageDtoFactory damageDtoFactory = new(this._shootPoint, modelData.TargetLayer,
                modelData.BulletDamage, modelData.DamageDelay, modelData.BulletDistance);

            this._mechanicsRunner.Add(
                new CanRefillMechanics(state.CanRefill, modelData.BulletsCount, state.LoadedBulletsCount),
                new CanShootMechanics(state.CanShoot, modelData.ShootCooldown, state.LastShootTime, state.LoadedBulletsCount),

                new RefillMechanics(state.LoadedBulletsCount, state.LastRefillingTime, state.CanRefill, modelData.RefillCooldown),
                new ShootMechanics(damageDtoFactory, state.LastShootTime, state.LoadedBulletsCount, state.CanShoot,
                    this._reactionEvents.Shoot, this._reactionEvents.TakeDamage, this._triggerEvents.Shoot)
            );
        }

        private void InitAccessors()
        {
            this._accessEntity.Add<IAttackTriggerAccessor>(new AttackTriggerAccessor(this._triggerEvents.Shoot));
            this._accessEntity.Add<ITakeDamageReactionAccessor>(new TakeDamageReactionAccessor(this._reactionEvents.TakeDamage));

            this._accessEntity.Add(new AttackReactionAccessor(this._reactionEvents.Shoot));
            this._accessEntity.Add(new PositionAccessor(this.transform));
        }
    }
}