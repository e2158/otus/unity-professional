﻿using UnityEngine;

namespace AtomicFramework.Impl
{
    public readonly struct DamageDto
    {
        public readonly int Damage;
        public readonly float Delay;
        public readonly float MaxDistance;

        public readonly LayerMask Layer;
        public readonly Vector3 Position;
        public readonly Vector3 Direction;

        public DamageDto(int damage, float delay, float maxDistance, LayerMask layer, Vector3 position, Vector3 direction)
        {
            this.Delay = delay;
            this.Damage = damage;
            this.MaxDistance = maxDistance;

            this.Layer = layer;
            this.Position = position;
            this.Direction = direction;
        }
    }
}