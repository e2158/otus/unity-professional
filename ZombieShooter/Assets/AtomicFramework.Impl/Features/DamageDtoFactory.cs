﻿using UnityEngine;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class DamageDtoFactory
    {
        [NotNull] private readonly Transform _shootPoint;
        [NotNull] private readonly IReadOnlyAtomicValue<LayerMask> _layer;
        [NotNull] private readonly IReadOnlyAtomicValue<int> _damage;
        [NotNull] private readonly IReadOnlyAtomicValue<float> _delay;
        [NotNull] private readonly IReadOnlyAtomicValue<float> _maxDistance;

        public DamageDtoFactory([NotNull] Transform shootPoint, [NotNull] IReadOnlyAtomicValue<LayerMask> layer,
            [NotNull] IReadOnlyAtomicValue<int> damage, [NotNull] IReadOnlyAtomicValue<float> delay,
            [NotNull] IReadOnlyAtomicValue<float> maxDistance)
        {
            this._shootPoint = shootPoint;

            this._layer = layer;
            this._delay = delay;
            this._damage = damage;
            this._maxDistance = maxDistance;
        }

        public DamageDto Create()
        {
            return new DamageDto(
                this._damage.Value, this._delay.Value, this._maxDistance.Value,
                this._layer.Value, this._shootPoint.position, this._shootPoint.forward);
        }
    }
}