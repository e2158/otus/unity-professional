﻿using System;
using UnityEngine;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class HpAccessor : IAccessor
    {
        [NotNull] private readonly AtomicValue<int> _hp;

        public HpAccessor([NotNull] AtomicValue<int> hp)
        {
            this._hp = hp ?? throw new ArgumentNullException(nameof(hp));
        }

        public void Increase(int hp)
        {
            this.Set(this._hp + hp);
        }

        public void Decrease(int hp)
        {
            this.Set(this._hp - hp);
        }

        public void Set(int hp)
        {
            this._hp.Value = Mathf.Max(hp, 0);
        }
    }
}