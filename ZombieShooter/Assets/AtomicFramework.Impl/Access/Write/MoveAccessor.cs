﻿using System;
using UnityEngine;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class MoveAccessor : IAccessor
    {
        [NotNull] private readonly AtomicValue<Vector2> _movingDirection;

        public MoveAccessor(
            [NotNull] AtomicValue<Vector2> movingDirection)
        {
            this._movingDirection = movingDirection ?? throw new ArgumentNullException(nameof(movingDirection));
        }

        public void Write(Vector2 moveDirection)
        {
            this._movingDirection.Value = moveDirection;
        }
    }
}