﻿using System;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class RotateAccessor : IAccessor
    {
        [NotNull] private readonly AtomicValue<float> _rotation;

        public RotateAccessor(
            [NotNull] AtomicValue<float> rotation)
        {
            this._rotation = rotation ?? throw new ArgumentNullException(nameof(rotation));
        }

        public void Write(float rotation)
        {
            this._rotation.Value = rotation;
        }
    }
}