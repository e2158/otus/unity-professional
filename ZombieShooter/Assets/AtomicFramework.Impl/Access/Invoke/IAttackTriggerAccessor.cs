﻿namespace AtomicFramework.Impl
{
    public interface IAttackTriggerAccessor : IAccessor
    {
        void Invoke();
    }
}