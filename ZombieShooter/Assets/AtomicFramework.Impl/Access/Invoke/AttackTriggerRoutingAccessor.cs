﻿using System;
using Essentials;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class AttackTriggerRoutingAccessor : IAttackTriggerAccessor, IEnablable
    {
        private static readonly IAttackTriggerAccessor stub = new AttackTriggerAccessor(new AtomicEvent());

        [NotNull] private readonly IAttackTriggerAccessor _nextAttackTriggerAccessor;

        public bool IsEnabled { get; private set; }

        public AttackTriggerRoutingAccessor([NotNull] IAttackTriggerAccessor nextAttackTriggerAccessor)
        {
            this._nextAttackTriggerAccessor =
                nextAttackTriggerAccessor ?? throw new ArgumentNullException(nameof(nextAttackTriggerAccessor));
        }

        public void Invoke()
        {
            IAttackTriggerAccessor accessor = this.IsEnabled
                ? this._nextAttackTriggerAccessor
                : AttackTriggerRoutingAccessor.stub;

            accessor.Invoke();
        }

        public void Enable()
        {
            if (this.IsEnabled)
                throw new InvalidOperationException("It's already enabled. Can't enable twice");

            this.IsEnabled = true;
        }

        public void Disable()
        {
            if (!this.IsEnabled)
                throw new InvalidOperationException("It's already disabled. Can't disable twice");

            this.IsEnabled = false;
        }
    }
}