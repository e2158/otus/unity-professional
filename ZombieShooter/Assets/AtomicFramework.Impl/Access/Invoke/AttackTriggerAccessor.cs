﻿using System;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class AttackTriggerAccessor : IAttackTriggerAccessor
    {
        [NotNull] private readonly AtomicEvent _attackTriggerEvent;

        public AttackTriggerAccessor(
            [NotNull] AtomicEvent attackTriggerEvent)
        {
            this._attackTriggerEvent = attackTriggerEvent ?? throw new ArgumentNullException(nameof(attackTriggerEvent));
        }

        public void Invoke()
        {
            this._attackTriggerEvent.Invoke();
        }
    }
}