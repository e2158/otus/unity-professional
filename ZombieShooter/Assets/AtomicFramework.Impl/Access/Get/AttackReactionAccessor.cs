﻿using System;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class AttackReactionAccessor : IAccessor
    {
        [NotNull] private readonly IReadOnlyAtomicEvent _attackReactionEvent;

        public AttackReactionAccessor([NotNull] IReadOnlyAtomicEvent attackReactionEvent)
        {
            this._attackReactionEvent =
                attackReactionEvent ?? throw new ArgumentNullException(nameof(attackReactionEvent));
        }

        public IReadOnlyAtomicEvent Get()
        {
            return this._attackReactionEvent;
        }
    }
}