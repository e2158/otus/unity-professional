﻿using System;
using UnityEngine;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class PositionAccessor : IAccessor
    {
        [NotNull] private readonly Transform _transform;

        public PositionAccessor([NotNull] Transform transform)
        {
            this._transform = transform
                ? transform : throw new ArgumentNullException(nameof(transform));
        }

        public Vector3 Get()
        {
            return this._transform.position;
        }
    }
}