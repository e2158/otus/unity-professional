﻿using System;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class TakeDamageReactionAccessor : ITakeDamageReactionAccessor
    {
        [NotNull] private readonly IReadOnlyAtomicEvent<DamageDto> _takeDamageReactionEvent;

        public TakeDamageReactionAccessor([NotNull] IReadOnlyAtomicEvent<DamageDto> takeDamageReactionEvent)
        {
            this._takeDamageReactionEvent =
                takeDamageReactionEvent ?? throw new ArgumentNullException(nameof(takeDamageReactionEvent));
        }

        public IReadOnlyAtomicEvent<DamageDto> Get()
        {
            return this._takeDamageReactionEvent;
        }
    }
}