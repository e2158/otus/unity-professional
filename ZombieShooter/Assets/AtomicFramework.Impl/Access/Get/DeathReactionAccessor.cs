﻿using System;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class DeathReactionAccessor : IAccessor
    {
        [NotNull] private readonly IReadOnlyAtomicEvent _deathReactionEvent;

        public DeathReactionAccessor([NotNull] IReadOnlyAtomicEvent deathReactionEvent)
        {
            this._deathReactionEvent =
                deathReactionEvent ?? throw new ArgumentNullException(nameof(deathReactionEvent));
        }

        public IReadOnlyAtomicEvent Get()
        {
            return this._deathReactionEvent;
        }
    }
}