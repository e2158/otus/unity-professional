﻿namespace AtomicFramework.Impl
{
    public interface ITakeDamageReactionAccessor : IAccessor
    {
        IReadOnlyAtomicEvent<DamageDto> Get();
    }
}