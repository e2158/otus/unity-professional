﻿using System;
using Essentials;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class TakeDamageReactionRoutingAccessor : ITakeDamageReactionAccessor, IEnablable
    {
        private static readonly IReadOnlyAtomicEvent<DamageDto> stub = new AtomicEvent<DamageDto>();

        [NotNull] private readonly ITakeDamageReactionAccessor _nextTakeDamageReactionAccessor;

        public bool IsEnabled { get; private set; }

        public TakeDamageReactionRoutingAccessor([NotNull] ITakeDamageReactionAccessor nextTakeDamageReactionAccessor)
        {
            this._nextTakeDamageReactionAccessor =
                nextTakeDamageReactionAccessor ?? throw new ArgumentNullException(nameof(nextTakeDamageReactionAccessor));
        }

        public IReadOnlyAtomicEvent<DamageDto> Get()
        {
            return this.IsEnabled
                ? this._nextTakeDamageReactionAccessor.Get()
                : TakeDamageReactionRoutingAccessor.stub;
        }

        public void Enable()
        {
            if (this.IsEnabled)
                throw new InvalidOperationException("It's already enabled. Can't enable twice");

            this.IsEnabled = true;
        }

        public void Disable()
        {
            if (!this.IsEnabled)
                throw new InvalidOperationException("It's already disabled. Can't disable twice");

            this.IsEnabled = false;
        }
    }
}