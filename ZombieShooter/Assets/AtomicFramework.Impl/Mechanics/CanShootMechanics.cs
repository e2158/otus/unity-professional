﻿using System;
using Essentials;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class CanShootMechanics : IMechanics, ITickable
    {
        [NotNull] private readonly AtomicValue<bool> _canShoot;
        [NotNull] private readonly IReadOnlyAtomicValue<float> _shootCooldown;
        [NotNull] private readonly IReadOnlyAtomicValue<float> _lastShootTime;
        [NotNull] private readonly IReadOnlyAtomicValue<int> _loadedBulletsCount;

        public CanShootMechanics(
            [NotNull] AtomicValue<bool> canShoot,
            [NotNull] IReadOnlyAtomicValue<float> shootCooldown,
            [NotNull] IReadOnlyAtomicValue<float> lastShootTime,
            [NotNull] IReadOnlyAtomicValue<int> loadedBulletsCount)
        {
            this._canShoot = canShoot ?? throw new ArgumentNullException(nameof(canShoot));
            this._shootCooldown = shootCooldown ?? throw new ArgumentNullException(nameof(shootCooldown));
            this._lastShootTime = lastShootTime ?? throw new ArgumentNullException(nameof(lastShootTime));
            this._loadedBulletsCount = loadedBulletsCount ?? throw new ArgumentNullException(nameof(loadedBulletsCount));
        }

        public void Tick(float deltaTime)
        {
            this._canShoot.Value =
                this._loadedBulletsCount.Value > 0 &&
                UnityEngine.Time.time - this._lastShootTime.Value >= this._shootCooldown.Value;
        }
    }
}