﻿using System;
using Essentials;
using UnityEngine;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class RotationMechanics : IMechanics, ITickable
    {
        [NotNull] private readonly Transform _target;
        [NotNull] private readonly IReadOnlyAtomicValue<bool> _canMove;
        [NotNull] private readonly IReadOnlyAtomicValue<float> _rotation;
        [NotNull] private readonly IReadOnlyAtomicValue<float> _rotationSpeed;

        public RotationMechanics(
            [NotNull] Transform target,
            [NotNull] IReadOnlyAtomicValue<bool> canMove,
            [NotNull] IReadOnlyAtomicValue<float> rotationSpeed,
            [NotNull] IReadOnlyAtomicValue<float> rotation)
        {
            this._target = target ? target : throw new ArgumentNullException(nameof(target));
            this._canMove = canMove ?? throw new ArgumentNullException(nameof(canMove));
            this._rotation = rotation ?? throw new ArgumentNullException(nameof(rotation));
            this._rotationSpeed = rotationSpeed ?? throw new ArgumentNullException(nameof(rotationSpeed));
        }

        public void Tick(float deltaTime)
        {
            if (this._canMove.Value)
            {
                Quaternion startRotation = this._target.rotation;
                Vector3 targetEuler = this._target.transform.eulerAngles.SetY(this._rotation.Value);
                Quaternion targetRotation = Quaternion.Euler(targetEuler);

                float lerpValue = Time.deltaTime * this._rotationSpeed.Value;
                this._target.rotation = Quaternion.Lerp(startRotation, targetRotation, lerpValue);
            }
        }
    }
}