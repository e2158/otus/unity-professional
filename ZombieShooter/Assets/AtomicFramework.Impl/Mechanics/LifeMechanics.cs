﻿using System;
using Essentials;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class LifeMechanics : IMechanics, IEnablable
    {
        [NotNull] private readonly AtomicEvent _deathTriggerEvent;
        [NotNull] private readonly IReadOnlyAtomicValue<int> _hp;

        public bool IsEnabled { get; private set; }

        public LifeMechanics([NotNull] IReadOnlyAtomicValue<int> hp, [NotNull] AtomicEvent deathTriggerEvent)
        {
            this._hp = hp ?? throw new ArgumentNullException(nameof(hp));
            this._deathTriggerEvent = deathTriggerEvent ?? throw new ArgumentNullException(nameof(deathTriggerEvent));
        }

        public void Enable()
        {
            if (this.IsEnabled)
                throw new InvalidOperationException("It's already enabled. Can't enable twice");

            this.IsEnabled = true;
            this.OnHpChanged(this._hp.Value);
            this._hp.Changed += this.OnHpChanged;
        }

        public void Disable()
        {
            if (!this.IsEnabled)
                throw new InvalidOperationException("It's already disabled. Can't disable twice");

            this.IsEnabled = false;
            this._hp.Changed -= this.OnHpChanged;
        }

        private void OnHpChanged(int hp)
        {
            if (hp <= 0)
                this._deathTriggerEvent.Invoke();
        }
    }
}