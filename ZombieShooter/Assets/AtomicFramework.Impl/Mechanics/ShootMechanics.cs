﻿using System;
using Cysharp.Threading.Tasks;
using Essentials;
using UnityEngine;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class ShootMechanics: IMechanics, IEnablable
    {
        [NotNull] private readonly DamageDtoFactory _damageDtoFactory;

        [NotNull] private readonly AtomicValue<float> _lastShootTime;
        [NotNull] private readonly AtomicValue<int> _loadedBulletsCount;
        [NotNull] private readonly AtomicEvent _shootReactionEvent;
        [NotNull] private readonly AtomicEvent<DamageDto> _takeDamageReactionEvent;

        [NotNull] private readonly IReadOnlyAtomicValue<bool> _canShoot;
        [NotNull] private readonly IReadOnlyAtomicEvent _shootTriggerEvent;

        public bool IsEnabled { get; private set; }

        public ShootMechanics(
            [NotNull] DamageDtoFactory damageDtoFactory, [NotNull] AtomicValue<float> lastShootTime,
            [NotNull] AtomicValue<int> loadedBulletsCount, [NotNull] IReadOnlyAtomicValue<bool> canShoot,
            [NotNull] AtomicEvent shootReactionEvent, [NotNull] AtomicEvent<DamageDto> takeDamageReactionEvent,
            [NotNull] IReadOnlyAtomicEvent shootTriggerEvent)
        {
            this._damageDtoFactory = damageDtoFactory;

            this._lastShootTime = lastShootTime;
            this._loadedBulletsCount = loadedBulletsCount;
            this._shootReactionEvent = shootReactionEvent;
            this._takeDamageReactionEvent = takeDamageReactionEvent;

            this._canShoot = canShoot;
            this._shootTriggerEvent = shootTriggerEvent;
        }

        public void Enable()
        {
            if (this.IsEnabled)
                throw new InvalidOperationException("It's already enabled. Can't enable twice");

            this.IsEnabled = true;
            this._shootTriggerEvent.Invoked += this.OnShootTriggered;
        }

        public void Disable()
        {
            if (!this.IsEnabled)
                throw new InvalidOperationException("It's already disabled. Can't disable twice");

            this.IsEnabled = false;
            this._shootTriggerEvent.Invoked -= this.OnShootTriggered;
        }

        private async void OnShootTriggered()
        {
            if (!this._canShoot.Value)
                return;

            this._lastShootTime.Value = Time.time;
            DamageDto dto = this._damageDtoFactory.Create();
            this._shootReactionEvent.Invoke();

            TimeSpan delay = TimeSpan.FromSeconds(dto.Delay);
            await UniTask.Delay(delay);

            this._loadedBulletsCount.Value--;
            this._takeDamageReactionEvent.Invoke(dto);
        }
    }
}