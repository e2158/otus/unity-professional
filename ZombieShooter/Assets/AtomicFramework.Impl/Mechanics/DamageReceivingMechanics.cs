﻿using System;
using Essentials;
using UnityEngine;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class DamageReceivingMechanics : IMechanics, IEnablable
    {
        [NotNull] private readonly AtomicValue<int> _hp;
        [NotNull] private readonly IReadOnlyAtomicEvent<int> _damageReactionEvent;

        public bool IsEnabled { get; private set; }

        public DamageReceivingMechanics(
            [NotNull] AtomicValue<int> hp,
            [NotNull] IReadOnlyAtomicEvent<int> damageReactionEvent)
        {
            this._hp = hp ?? throw new ArgumentNullException(nameof(hp));
            this._damageReactionEvent = damageReactionEvent ?? throw new ArgumentNullException(nameof(damageReactionEvent));
        }

        public void Enable()
        {
            if (this.IsEnabled)
                throw new InvalidOperationException("It's already enabled. Can't enable twice");

            this.IsEnabled = true;
            this._damageReactionEvent.Invoked += this.OnDamageReactionInvoked;
        }

        public void Disable()
        {
            if (!this.IsEnabled)
                throw new InvalidOperationException("It's already disabled. Can't disable twice");

            this.IsEnabled = false;
            this._damageReactionEvent.Invoked -= this.OnDamageReactionInvoked;
        }

        private void OnDamageReactionInvoked(int damage)
        {
            int actualHp = this._hp - damage;
            this._hp.Value = Mathf.Max(0, actualHp);
        }
    }
}