﻿using System;
using Essentials;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class RefillMechanics : IMechanics, ITickable, IEnablable
    {
        [NotNull] private readonly AtomicValue<int> _loadedBulletsCount;
        [NotNull] private readonly AtomicValue<float> _lastRefillingTime;
        [NotNull] private readonly IReadOnlyAtomicValue<bool> _canRefill;
        [NotNull] private readonly IReadOnlyAtomicValue<float> _refillCooldown;

        public bool IsEnabled { get; private set; }

        public RefillMechanics(
            [NotNull] AtomicValue<int> loadedBulletsCount, [NotNull] AtomicValue<float> lastRefillingTime,
            [NotNull] IReadOnlyAtomicValue<bool> canRefill, [NotNull] IReadOnlyAtomicValue<float> refillCooldown)
        {
            this._loadedBulletsCount = loadedBulletsCount;
            this._lastRefillingTime = lastRefillingTime;
            this._canRefill = canRefill;
            this._refillCooldown = refillCooldown;
        }

        public void Enable()
        {
            if (this.IsEnabled)
                throw new InvalidOperationException("It's already enabled. Can't enable twice");

            this.IsEnabled = true;
            this._canRefill.Changed += this.OnCanRefillChanged;
        }

        public void Disable()
        {
            if (!this.IsEnabled)
                throw new InvalidOperationException("It's already disabled. Can't disable twice");

            this.IsEnabled = false;
            this._canRefill.Changed -= this.OnCanRefillChanged;
        }

        public void Tick(float deltaTime)
        {
            if (!this._canRefill.Value)
                return;

            float time = UnityEngine.Time.time;
            if (time - this._lastRefillingTime.Value >= this._refillCooldown.Value)
            {
                this._loadedBulletsCount.Value++;
                this._lastRefillingTime.Value = time;
            }
        }

        private void OnCanRefillChanged(bool canRefill)
        {
            if (canRefill)
                this._lastRefillingTime.Value = UnityEngine.Time.time;
        }
    }
}