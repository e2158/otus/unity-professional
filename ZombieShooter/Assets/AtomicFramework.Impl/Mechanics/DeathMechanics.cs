﻿using System;
using System.Linq;
using System.Collections.Generic;
using Essentials;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class DeathMechanics : IMechanics, IEnablable
    {
        [NotNull] private readonly AtomicValue<bool> _isDead;
        [NotNull] private readonly IReadOnlyAtomicEvent _deathTriggerEvent;
        [NotNull, ItemNotNull] private readonly IEnablable[] _disableWhenDead;

        public bool IsEnabled { get; private set; }

        public DeathMechanics(
            [NotNull] AtomicValue<bool> isDead,
            [NotNull] IReadOnlyAtomicEvent deathTriggerEvent,
            [NotNull, ItemNotNull] IEnumerable<IEnablable> disableWhenDead)
        {
            this._isDead = isDead ?? throw new ArgumentNullException(nameof(isDead));
            this._deathTriggerEvent = deathTriggerEvent ?? throw new ArgumentNullException(nameof(deathTriggerEvent));
            this._disableWhenDead = (disableWhenDead ?? throw new ArgumentNullException(nameof(disableWhenDead))).ToArray();
        }

        public void Enable()
        {
            if (this.IsEnabled)
                throw new InvalidOperationException("It's already enabled. Can't enable twice");

            this.IsEnabled = true;
            this._deathTriggerEvent.Invoked += this.OnDeathTriggered;
        }

        public void Disable()
        {
            if (!this.IsEnabled)
                throw new InvalidOperationException("It's already disabled. Can't disable twice");

            this.IsEnabled = false;
            this._deathTriggerEvent.Invoked -= this.OnDeathTriggered;
        }

        private void OnDeathTriggered()
        {
            if (!this._isDead)
            {
                this._isDead.Value = true;

                foreach (IEnablable enablable in this._disableWhenDead)
                    enablable.Disable();
            }
        }
    }
}