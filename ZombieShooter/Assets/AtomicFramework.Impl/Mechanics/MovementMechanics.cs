﻿using System;
using Essentials;
using UnityEngine;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class MovementMechanics : IMechanics, ITickable
    {
        [NotNull] private readonly Transform _movableObject;
        [NotNull] private readonly IReadOnlyAtomicValue<bool> _canMove;
        [NotNull] private readonly IReadOnlyAtomicValue<float> _movementSpeed;
        [NotNull] private readonly IReadOnlyAtomicValue<Vector2> _moveDirection;

        public MovementMechanics(
            [NotNull] Transform movableObject,
            [NotNull] IReadOnlyAtomicValue<bool> canMove,
            [NotNull] IReadOnlyAtomicValue<float> movementSpeed,
            [NotNull] IReadOnlyAtomicValue<Vector2> moveDirection)
        {
            this._canMove = canMove ?? throw new ArgumentNullException(nameof(canMove));
            this._movementSpeed = movementSpeed ?? throw new ArgumentNullException(nameof(movementSpeed));
            this._moveDirection = moveDirection ?? throw new ArgumentNullException(nameof(moveDirection));
            this._movableObject = movableObject ? movableObject : throw new ArgumentNullException(nameof(movableObject));
        }

        public void Tick(float deltaTime)
        {
            if (this._canMove.Value)
            {
                Vector3 startPosition = this._movableObject.position;
                Vector3 direction = this._moveDirection.Value.InsertY(startPosition.y);
                float frameOffset = this._movementSpeed.Value * deltaTime;

                this._movableObject.position = startPosition + frameOffset * direction;
            }
        }
    }
}