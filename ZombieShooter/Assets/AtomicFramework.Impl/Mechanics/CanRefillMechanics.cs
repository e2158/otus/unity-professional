﻿using System;
using Essentials;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class CanRefillMechanics : IMechanics, ITickable
    {
        [NotNull] private readonly AtomicValue<bool> _canRefill;
        [NotNull] private readonly IReadOnlyAtomicValue<int> _maxBulletsCount;
        [NotNull] private readonly IReadOnlyAtomicValue<int> _loadedBulletsCount;

        public CanRefillMechanics([NotNull] AtomicValue<bool> canRefill,
            [NotNull] IReadOnlyAtomicValue<int> maxBulletsCount, [NotNull] IReadOnlyAtomicValue<int> loadedBulletsCount)
        {
            this._canRefill = canRefill ?? throw new ArgumentNullException(nameof(canRefill));
            this._maxBulletsCount = maxBulletsCount ?? throw new ArgumentNullException(nameof(maxBulletsCount));
            this._loadedBulletsCount = loadedBulletsCount ?? throw new ArgumentNullException(nameof(loadedBulletsCount));
        }

        public void Tick(float deltaTime)
        {
            this._canRefill.Value = this._loadedBulletsCount.Value < this._maxBulletsCount.Value;
        }
    }
}