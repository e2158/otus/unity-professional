﻿using System;
using Essentials;
using JetBrains.Annotations;

namespace AtomicFramework.Impl
{
    public sealed class CanMoveMechanics : IMechanics, ITickable
    {
        [NotNull] private readonly AtomicValue<bool> _canMove;
        [NotNull] private readonly IReadOnlyAtomicValue<bool> _isDead;

        public CanMoveMechanics(
            [NotNull] AtomicValue<bool> canMove,
            [NotNull] IReadOnlyAtomicValue<bool> isDead)
        {
            this._isDead = isDead ?? throw new ArgumentNullException(nameof(isDead));
            this._canMove = canMove ?? throw new ArgumentNullException(nameof(canMove));
        }

        public void Tick(float deltaTime)
        {
            this._canMove.Value = !this._isDead.Value;
        }
    }
}