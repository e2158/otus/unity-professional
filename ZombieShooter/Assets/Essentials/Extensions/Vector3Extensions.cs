﻿using UnityEngine;

namespace Essentials
{
    public static class Vector3Extensions
    {
        public static bool EqualsApproximately(this Vector3 a, Vector3 b) =>
            Mathf.Approximately(a.x, b.x) &&
            Mathf.Approximately(a.y, b.y) &&
            Mathf.Approximately(a.z, b.z);

        public static Vector3 SetY(this Vector3 vec3, float y) => new(vec3.x, y, vec3.z);
        public static Vector2 CutY(this Vector3 vec3) => new(vec3.x, vec3.z);
    }
}