﻿using UnityEngine;

namespace Essentials
{
    public static class Vector2Extensions
    {
        public static bool EqualsApproximately(this Vector2 a, Vector2 b) =>
            Mathf.Approximately(a.x, b.x) &&
            Mathf.Approximately(a.y, b.y);

        public static Vector3 InsertY(this Vector2 vec2, float y) => new(vec2.x, y, vec2.y);
        public static Vector3 InsertZ(this Vector2 vec2, float z) => new(vec2.x, vec2.y, z);

        public static Vector2 RotateDeg(this Vector2 vec2, float degrees) =>
            vec2.RotateRad(degrees * Mathf.Deg2Rad);

        public static Vector2 RotateRad(this Vector2 vec2, float radians) =>
            new(Mathf.Cos(radians) * vec2.x - Mathf.Sin(radians) * vec2.y,
                Mathf.Sin(radians) * vec2.x + Mathf.Cos(radians) * vec2.y);
    }
}