﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Essentials
{
    public interface IReadOnlyCollection<out TEntity> : IEnumerable<TEntity>
    {
        public event Action<TEntity> Added;
        public event Action<TEntity> Removed;
    }

    public sealed class Collection<TEntity> : IReadOnlyCollection<TEntity>
    {
        public event Action<TEntity> Added;
        public event Action<TEntity> Removed;

        private readonly HashSet<TEntity> _entities = new();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
        public IEnumerator<TEntity> GetEnumerator() => this._entities.GetEnumerator();

        public void AddRange([NotNull, ItemNotNull] IEnumerable<TEntity> entities)
        {
            if (entities == null) throw new ArgumentNullException(nameof(entities));
            foreach (TEntity entity in entities)
                this.Add(entity);
        }

        public void Add([NotNull] TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            this._entities.Add(entity);
            this.Added?.Invoke(entity);
        }

        public void Remove([NotNull] TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            this._entities.Remove(entity);
            this.Removed?.Invoke(entity);
        }
    }
}