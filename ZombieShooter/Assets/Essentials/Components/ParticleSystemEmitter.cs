﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace Essentials
{
    [DisallowMultipleComponent]
    public sealed class ParticleSystemEmitter : MonoBehaviour
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private ParticleSystem[] _particleSystems;

        private void OnValidate()
        {
            this._particleSystems ??= this.GetComponentsInChildren<ParticleSystem>();
        }

        [Button]
        public void Play()
        {
            foreach (ParticleSystem system in this._particleSystems)
                system.Play();
        }

        [Button(Expanded = true)]
        public void Emit(int count = 1)
        {
            foreach (ParticleSystem system in this._particleSystems)
                system.Emit(count);
        }
    }
}