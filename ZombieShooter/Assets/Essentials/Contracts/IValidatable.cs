﻿namespace Essentials
{
    public interface IValidatable
    {
        void Validate();
    }
}