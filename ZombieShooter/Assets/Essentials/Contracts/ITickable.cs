﻿namespace Essentials
{
    public interface ITickable
    {
        void Tick(float deltaTime);
    }
}