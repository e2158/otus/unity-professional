﻿using System;
using System.Collections.Generic;
using Essentials;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace AtomicFramework
{
    [DisallowMultipleComponent]
    public sealed class MechanicsRunner : MonoBehaviour
    {
        [Title("Info")]
        [ShowInInspector, ReadOnly] private readonly List<ITickable> _tickables = new();
        [ShowInInspector, ReadOnly] private readonly List<IEnablable> _enablables = new();

        private void OnEnable()
        {
            foreach (IEnablable enablable in this._enablables)
                if (!enablable.IsEnabled)
                    enablable.Enable();
        }

        private void OnDisable()
        {
            foreach (IEnablable enablable in this._enablables)
                if (enablable.IsEnabled)
                    enablable.Disable();
        }

        private void Update()
        {
            foreach (ITickable tickable in this._tickables)
                tickable.Tick(Time.deltaTime);
        }

        public void Add([NotNull, ItemNotNull] params IMechanics[] mechanics)
        {
            foreach (IMechanics mec in mechanics)
                this.Add(mec);
        }

        public void Add([NotNull] IMechanics mechanics)
        {
            if (mechanics == null)
                throw new ArgumentNullException(nameof(mechanics));

            if (mechanics is IEnablable enablable)
            {
                this._enablables.Add(enablable);
                if (this.enabled && !enablable.IsEnabled) enablable.Enable();
                if (!this.enabled && enablable.IsEnabled) enablable.Disable();
            }

            if (mechanics is ITickable tickable)
            {
                this._tickables.Add(tickable);
            }
        }
    }
}