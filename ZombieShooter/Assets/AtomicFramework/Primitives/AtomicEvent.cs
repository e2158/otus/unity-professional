﻿using System;
using Sirenix.OdinInspector;

namespace AtomicFramework
{
    public interface IReadOnlyAtomicEvent
    {
        event Action Invoked;
    }

    public interface IReadOnlyAtomicEvent<out T>
    {
        event Action<T> Invoked;
    }

    [Serializable, InlineProperty]
    public sealed class AtomicEvent : IReadOnlyAtomicEvent
    {
        public event Action Invoked;

        [Button]
        public void Invoke() => this.Invoked?.Invoke();
    }

    [Serializable, InlineProperty]
    public sealed class AtomicEvent<T> : IReadOnlyAtomicEvent<T>
    {
        public event Action<T> Invoked;

        [Button]
        public void Invoke(T value) => this.Invoked?.Invoke(value);
    }
}