﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace AtomicFramework
{
    public interface IReadOnlyAtomicValue<out T>
    {
        event Action<T> Changed;
        T Value { get; }
    }

    [Serializable, InlineProperty]
    public sealed class AtomicValue<T> : IReadOnlyAtomicValue<T>
    {
        public event Action<T> Changed;

        [HideLabel, OnValueChanged(nameof(OnValueChanged))]
        [SerializeField] private T _value;

        public T Value
        {
            get => this._value;
            set
            {
                if (!this._value.Equals(value))
                {
                    this._value = value;
                    this.Changed?.Invoke(this._value);
                }
            }
        }

        private void OnValueChanged() =>
            this.Changed?.Invoke(this._value);

        public static implicit operator T(AtomicValue<T> value) => value.Value;
    }
}