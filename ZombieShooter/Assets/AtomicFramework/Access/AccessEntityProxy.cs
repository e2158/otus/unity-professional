﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace AtomicFramework
{
    [DisallowMultipleComponent]
    public sealed class AccessEntityProxy : MonoBehaviour, IReadOnlyAccessEntity
    {
        [SerializeField, Required] private AccessEntity _accessEntity;

        private void OnValidate()
        {
            this._accessEntity ??= this.GetComponentInParent<AccessEntity>();
        }

        public TAccessor Get<TAccessor>() where TAccessor : IAccessor =>
            this._accessEntity.Get<TAccessor>();

        public bool TryGet<TAccessor>(out TAccessor accessor) where TAccessor : IAccessor =>
            this._accessEntity.TryGet(out accessor);
    }
}