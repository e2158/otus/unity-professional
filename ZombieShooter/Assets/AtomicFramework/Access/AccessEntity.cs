﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace AtomicFramework
{
    public interface IReadOnlyAccessEntity
    {
        [NotNull] TAccessor Get<TAccessor>() where TAccessor : IAccessor;
        bool TryGet<TAccessor>(out TAccessor accessor) where TAccessor : IAccessor;
    }

    [DisallowMultipleComponent]
    public sealed class AccessEntity : MonoBehaviour, IReadOnlyAccessEntity
    {
        [Title("Info"), PropertyOrder(100)]
        [ShowInInspector, ReadOnly] private readonly Dictionary<Type, IAccessor> _accessors = new();

        public void Add<TAccessor>([NotNull] TAccessor accessor) where TAccessor : IAccessor
        {
            this.Add<TAccessor>((IAccessor)accessor);
        }

        public void Add<TAccessor>([NotNull] IAccessor accessor) where TAccessor : IAccessor
        {
            if (accessor == null)
                throw new ArgumentNullException(nameof(accessor));

            if (accessor is not TAccessor)
                throw new ArgumentException($"Is not \"{typeof(TAccessor).Name}\"", nameof(accessor));

            Type type = typeof(TAccessor);

            if (!this._accessors.TryAdd(type, accessor))
                throw new ArgumentException($"Already contains type {type.Name}", nameof(accessor));
        }

        public void Remove<TAccessor>() where TAccessor : IAccessor
        {
            Type type = typeof(TAccessor);
            this._accessors.Remove(type);
        }

        public TAccessor Get<TAccessor>() where TAccessor : IAccessor
        {
            Type type = typeof(TAccessor);
            return (TAccessor)this._accessors[type];
        }

        public bool TryGet<TAccessor>(out TAccessor accessor) where TAccessor : IAccessor
        {
            Type type = typeof(TAccessor);

            accessor = this._accessors.TryGetValue(type, out IAccessor baseAccessor)
                ? (TAccessor)baseAccessor : default;

            return accessor != null;
        }
    }
}