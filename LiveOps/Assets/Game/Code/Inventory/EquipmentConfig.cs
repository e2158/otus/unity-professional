﻿using UnityEngine;

namespace Game.Code.Inventory
{
    [CreateAssetMenu(fileName = nameof(EquipmentConfig), menuName = "Configs/" + nameof(EquipmentConfig))]
    public sealed class EquipmentConfig : SingleInventoryItemConfig
    {

    }
}