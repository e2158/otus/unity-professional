﻿using UnityEngine;
using Essentials.Runtime;
using Sirenix.OdinInspector;

namespace Game.Code.Inventory
{
    public abstract class InventoryItemConfig : IdentifiedScriptableObject
    {
        [HideLabel, BoxGroup("Meta Data")]
        [SerializeField] private MetaData _metaData;

        public MetaData Meta => _metaData;
    }
}