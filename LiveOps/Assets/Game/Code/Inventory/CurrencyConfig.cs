﻿using UnityEngine;

namespace Game.Code.Inventory
{
    [CreateAssetMenu(fileName = nameof(CurrencyConfig), menuName = "Configs/" + nameof(CurrencyConfig))]
    public sealed class CurrencyConfig : MultipleInventoryItemConfig
    {

    }
}