﻿using System.Collections.Generic;
using Game.Code.Chest;
using Game.Code.Systems.Save;
using Sirenix.OdinInspector;

namespace Game.Code
{
    [Searchable]
    public sealed class GameState : ISaveData
    {
        public readonly List<ChestState> ChestStates = new();
    }
}