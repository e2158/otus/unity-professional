﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Game.Code.Reward;
using Essentials.Runtime;
using Timer = Essentials.Runtime.Timer;

namespace Game.Code.Chest
{
    public sealed class ChestModel : IDisposable
    {
        private const string LogTag = nameof(ChestModel);
        private static readonly ILogger Logger = Debug.unityLogger;

        public event Action<ChestModel> StatusChanged;

        [NotNull] private readonly ChestState _state;
        [NotNull] private readonly ChestConfig _config;
        [NotNull] private readonly TimeProvider _timeProvider;
        [NotNull] private readonly RewardService _rewardService;

        [NotNull] private readonly Timer _unlockingTimer;
        [NotNull] private readonly Timer _lockingTimer;

        [ShowInInspector, ReadOnly] public ChestStatus Status => _state.Status;
        [ShowInInspector, ReadOnly] public int OpenedCount => _state.OpenedCount;
        [ShowInInspector, ReadOnly] public IReadOnlyTimer UnlockingTimer => _unlockingTimer;
        [ShowInInspector, ReadOnly] public IReadOnlyTimer LockingTimer => _lockingTimer;

        public ChestModel([NotNull] ChestState state, [NotNull] ChestConfig config,
            [NotNull] TimeProvider timeProvider, [NotNull] RewardService rewardService)
        {
            _state = state ?? throw new ArgumentNullException(nameof(state));
            _config = config ?? throw new ArgumentNullException(nameof(config));
            _timeProvider = timeProvider ?? throw new ArgumentNullException(nameof(timeProvider));
            _rewardService = rewardService ?? throw new ArgumentNullException(nameof(rewardService));

            _unlockingTimer = new Timer(_config.UnlockingDuration, true);
            _lockingTimer = new Timer(_config.LockingDuration, true);

            _unlockingTimer.TimerElapsed += Unlock;
            _lockingTimer.TimerElapsed += ResetStatus;

            HandleActualStatus(_state.Status, _state.StatusTimestamp);
        }

        public void Dispose()
        {
            _unlockingTimer.TimerElapsed -= Unlock;
            _lockingTimer.TimerElapsed -= ResetStatus;

            _unlockingTimer.Dispose();
            _lockingTimer.Dispose();
        }

        [Button, EnableIf(nameof(Status), ChestStatus.Locked)]
        public void StartUnlocking()
        {
            if (Status is not ChestStatus.Locked)
                throw new InvalidOperationException($"Can't Start Opening when Chest is in status \"{Status}\"");

            SetStatus(ChestStatus.Unlocking);
        }

        [Button, EnableIf(nameof(Status), ChestStatus.Unlocked)]
        public void Open()
        {
            if (Status is not ChestStatus.Unlocked)
                throw new InvalidOperationException($"Can't Open when Chest is in status \"{Status}\"");

            Logger.Log(LogTag, "Collecting rewards...");
            _rewardService.Collect(_config.PossibleRewardRoster.TakeRandom(_config.RewardsCount));
            _state.OpenedCount++;

            SetStatus(ChestStatus.Opened);
        }

        private void ResetStatus()
        {
            SetStatus(ChestStatus.None);
        }

        private void Lock()
        {
            if (Status is not ChestStatus.None)
                throw new InvalidOperationException($"Can't Activate New when Chest is in status \"{Status}\"");

            SetStatus(ChestStatus.Locked);
        }

        private void Unlock()
        {
            if (Status is not ChestStatus.Unlocking)
                throw new InvalidOperationException($"Can't Activate New when Chest is in status \"{Status}\"");

            SetStatus(ChestStatus.Unlocked);
        }

        private void SetStatus(ChestStatus status)
        {
            _state.Status = status;
            _state.StatusTimestamp = _timeProvider.Now;
            Logger.Log(LogTag, $"Set status to \"{status}\"");

            HandleActualStatus(_state.Status, _state.StatusTimestamp);
            StatusChanged?.Invoke(this);
        }

        private void HandleActualStatus(ChestStatus status, DateTime timestamp)
        {
            TimeSpan elapsed = _timeProvider.Now - timestamp;

            switch (status)
            {
                case ChestStatus.None:
                    Lock();
                    break;

                case ChestStatus.Locked: break;
                case ChestStatus.Unlocking:
                    _unlockingTimer.Start(elapsed);
                    break;

                case ChestStatus.Unlocked: break;
                case ChestStatus.Opened:
                    _lockingTimer.Start(elapsed);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(
                        nameof(status), status, $"Unknown {nameof(ChestStatus)}");
            }
        }
    }
}