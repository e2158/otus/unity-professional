﻿using System;
using Game.Code.Reward;
using Essentials.Runtime;
using JetBrains.Annotations;

namespace Game.Code.Chest
{
    [UsedImplicitly]
    public sealed class ChestModelFactory
    {
        [NotNull] private readonly TimeProvider _timeProvider;
        [NotNull] private readonly RewardService _rewardService;

        public ChestModelFactory([NotNull] TimeProvider timeProvider, [NotNull] RewardService rewardService)
        {
            _timeProvider = timeProvider ?? throw new ArgumentNullException(nameof(timeProvider));
            _rewardService = rewardService ?? throw new ArgumentNullException(nameof(rewardService));
        }

        public ChestModel Create((ChestState state, ChestConfig config) tuple) =>
            Create(tuple.state, tuple.config);

        public ChestModel Create([NotNull] ChestState state, [NotNull] ChestConfig config)
        {
            if (state == null) throw new ArgumentNullException(nameof(state));
            if (config == null) throw new ArgumentNullException(nameof(config));

            return new ChestModel(state, config, _timeProvider, _rewardService);
        }
    }
}