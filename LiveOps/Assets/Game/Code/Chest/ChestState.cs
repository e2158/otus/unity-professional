﻿using System;

namespace Game.Code.Chest
{
    public enum ChestStatus
    {
        None = 0,
        Locked = 1,
        Unlocking = 2,
        Unlocked = 3,
        Opened = 4,
    }

    [Serializable]
    public sealed class ChestState
    {
        public string Id;
        public int OpenedCount;
        public ChestStatus Status;
        public DateTime StatusTimestamp;
    }
}