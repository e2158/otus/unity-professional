﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Game.Code.Reward;
using Essentials.Runtime;

namespace Game.Code.Chest
{
    [CreateAssetMenu(fileName = nameof(ChestConfig), menuName = "Configs/" + nameof(ChestConfig))]
    public sealed class ChestConfig : IdentifiedScriptableObject
    {
        [HideLabel, BoxGroup("Meta Data")]
        [SerializeField] private MetaData _metaData;

        [Tooltip("Время разблокировки сундука для открытия")]
        [BoxGroup("Durations"), SuffixLabel("sec", true)]
        [SerializeField] private float _unlockingDuration;

        [Tooltip("Время блокировки сундука после открытия для повторного использования")]
        [BoxGroup("Durations"), SuffixLabel("sec", true)]
        [SerializeField] private float _lockingDuration;

        [Tooltip("Количество наград из списка за открытие")]
        [BoxGroup("Rewards"), MinValue(0)]
        [SerializeField] private int _rewardsCount;

        [Tooltip("Список возможных наград")]
        [BoxGroup("Rewards"), TableList(AlwaysExpanded = true)]
        [SerializeField] private List<RewardConfigData> _possibleRewardRoster;

        public MetaData Meta => _metaData;
        public TimeSpan UnlockingDuration => TimeSpan.FromSeconds(_unlockingDuration);
        public TimeSpan LockingDuration => TimeSpan.FromSeconds(_lockingDuration);

        public int RewardsCount => _rewardsCount;
        public IReadOnlyList<RewardConfigData> PossibleRewardRoster => _possibleRewardRoster;

        private void OnValidate()
        {
            while(_possibleRewardRoster.Count < _rewardsCount)
                _possibleRewardRoster.Add(new RewardConfigData());
        }
    }
}