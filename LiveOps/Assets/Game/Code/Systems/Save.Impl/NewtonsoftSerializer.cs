﻿using System;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;

namespace Game.Code.Systems.Save.Impl
{
    internal sealed class NewtonsoftSerializer : ISerializer
    {
        public UniTask<string> SerializeData<TData>(TData data) where TData : ISaveData
        {
            string json = data == null
                ? throw new ArgumentNullException(nameof(data))
                : JsonConvert.SerializeObject(data);

            return UniTask.FromResult(json);
        }

        public UniTask<TData> DeserializeData<TData>(string json) where TData : ISaveData
        {
            TData data = string.IsNullOrEmpty(json)
                ? default
                : JsonConvert.DeserializeObject<TData>(json);

            return UniTask.FromResult(data);
        }
    }
}