﻿using System;
using System.Collections.Generic;

namespace Game.Code.Systems.Save.Impl
{
    internal sealed class SaveDataKeysRepository : IDataKeysRepository
    {
        private readonly IReadOnlyDictionary<Type, string> _keys = new Dictionary<Type, string>()
        {
            [typeof(GameState)] = "GameState"
        };

        public string GetKey<TType>() =>
            _keys[typeof(TType)];
    }
}