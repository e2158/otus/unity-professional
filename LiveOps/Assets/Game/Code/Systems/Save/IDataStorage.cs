﻿using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace Game.Code.Systems.Save
{
    public interface IDataStorage
    {
        UniTask<string> Read([NotNull] string key);
        UniTask Write([NotNull] string key, [NotNull] string data);
    }
}