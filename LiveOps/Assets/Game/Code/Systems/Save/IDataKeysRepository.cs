﻿namespace Game.Code.Systems.Save
{
    public interface IDataKeysRepository
    {
        string GetKey<TType>();
    }
}