﻿using System;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace Game.Code.Systems.Save
{
    public sealed class SaveService
    {
        [NotNull] private readonly ISerializer _serializer;
        [NotNull] private readonly IDataStorage _dataStorage;
        [NotNull] private readonly IDataKeysRepository _keysRepository;

        public SaveService([NotNull] ISerializer serializer,
            [NotNull] IDataStorage dataStorage, [NotNull] IDataKeysRepository keysRepository)
        {
            _serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));
            _dataStorage = dataStorage ?? throw new ArgumentNullException(nameof(dataStorage));
            _keysRepository = keysRepository ?? throw new ArgumentNullException(nameof(keysRepository));
        }

        public async UniTask SaveAsync<TData>(TData data) where TData : ISaveData
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            try
            {
                string dataKey = _keysRepository.GetKey<TData>();
                string serializeData = await _serializer.SerializeData(data);
                await _dataStorage.Write(dataKey, serializeData);
            }
            catch (Exception e)
            {
                throw new Exception($"Can't save data of type \"{typeof(TData).Name}\":", e);
            }
        }

        public async UniTask<TData> LoadAsync<TData>() where TData : ISaveData
        {
            TData data;

            try
            {
                string dataKey = _keysRepository.GetKey<TData>();
                string serializeData = await _dataStorage.Read(dataKey);
                data = await _serializer.DeserializeData<TData>(serializeData);
            }
            catch (Exception e)
            {
                throw new Exception($"Can't load data of type \"{typeof(TData).Name}\":", e);
            }

            return data;
        }
    }
}