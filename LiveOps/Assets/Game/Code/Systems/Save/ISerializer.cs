﻿using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace Game.Code.Systems.Save
{
    public interface ISerializer
    {
        UniTask<string> SerializeData<TData>([NotNull] TData data) where TData : ISaveData;
        UniTask<TData> DeserializeData<TData>([CanBeNull] string json) where TData : ISaveData;
    }
}