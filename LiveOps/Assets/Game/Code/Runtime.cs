﻿using System.Linq;
using System.Collections.Generic;
using Game.Code.Chest;
using Game.Code.Systems.Save;
using Essentials.Runtime;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Cysharp.Threading.Tasks;

namespace Game.Code
{
    public sealed class Runtime : MonoBehaviour
    {
        private const int ChestsCount = 3;

        [Title("Configuration")]
        [Required, AssetSelector]
        [SerializeField] private List<ChestConfig> _chestConfigs;

        [Space, Title("Debug")]
        [ShowInInspector]
        private ChestModel[] _chestModels;

        private GameState _gameState;

        [Inject] private SaveService _saveService;
        [Inject] private ChestModelFactory _chestModelFactory;
        [Inject] private GameStateCorrector _gameStateCorrector;

        private void OnValidate()
        {
            while (_chestConfigs.Count < ChestsCount)
                _chestConfigs.Add(null);

            while (_chestConfigs.Count > ChestsCount)
                _chestConfigs.RemoveAt(_chestConfigs.Count - 1);
        }

        private async void Start()
        {
            _gameState = await _saveService.LoadAsync<GameState>() ?? new GameState();
            _gameStateCorrector.Correct(_gameState, _chestConfigs);

            _chestModels = _gameState.ChestStates.Zip(_chestConfigs)
                .Select(_chestModelFactory.Create).ToArray();

            foreach (ChestModel chestModel in _chestModels)
                chestModel.StatusChanged += OnChestStatusChanged;
        }

        private void OnDestroy()
        {
            foreach (ChestModel chestModel in _chestModels)
            {
                chestModel.StatusChanged -= OnChestStatusChanged;
                chestModel.Dispose();
            }
        }

        private void OnChestStatusChanged(ChestModel model)
        {
            _saveService.SaveAsync(_gameState).Forget();
        }
    }
}