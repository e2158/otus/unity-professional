﻿using Zenject;
using Essentials.Runtime;
using Game.Code.Chest;
using Game.Code.Reward;
using Game.Code.Systems.Save;
using Game.Code.Systems.Save.Impl;

namespace Game.Code
{
    public sealed class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<TimeProvider>().AsSingle();
            Container.Bind<RewardService>().AsSingle();
            Container.Bind<ChestModelFactory>().AsSingle();
            Container.Bind<GameStateCorrector>().AsSingle();

            Container.Bind<SaveService>().AsSingle();
            Container.Bind<ISerializer>().To<NewtonsoftSerializer>().AsSingle();
            Container.Bind<IDataStorage>().To<PlayerPrefsDataStorage>().AsSingle();
            Container.Bind<IDataKeysRepository>().To<SaveDataKeysRepository>().AsSingle();
        }
    }
}