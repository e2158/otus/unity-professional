﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Game.Code.Inventory;
using JetBrains.Annotations;

namespace Game.Code.Reward
{
    [UsedImplicitly]
    public sealed class RewardService
    {
        private const string LogTag = nameof(RewardService);
        private static readonly ILogger Logger = Debug.unityLogger;

        public void Collect([NotNull, ItemNotNull] IEnumerable<RewardConfigData> rewards)
        {
            if (rewards == null)
                throw new ArgumentNullException(nameof(rewards));

            foreach (RewardConfigData reward in rewards)
            {
                if (reward == null)
                    throw new ArgumentNullException(nameof(reward), "One of the rewards is null");

                // сложить в инвентарь игроку
                // ...

                if (reward.Item is SingleInventoryItemConfig)
                        Logger.Log(LogTag, $"Collected \"{reward.Item.Meta.Name}\"");

                else if (reward.Item is MultipleInventoryItemConfig)
                    Logger.Log(LogTag, $"Collected {reward.Count} of \"{reward.Item.Meta.Name}\"");
            }
        }
    }
}