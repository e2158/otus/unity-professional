﻿using System;
using UnityEngine;
using Game.Code.Inventory;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace Game.Code.Reward
{
    [Serializable]
    public sealed class RewardConfigData
    {
        [OnValueChanged(nameof(OnItemChanged))]
        [Required, AssetSelector]
        [SerializeField] private InventoryItemConfig _item;

        [TableColumnWidth(80, resizable: false)]
        [Min(0), DisableIf(nameof(IsSingleItem))]
        [SerializeField] private int _count;

        [NotNull] public InventoryItemConfig Item => _item;
        public int Count => _count;

        private bool IsSingleItem => _item is SingleInventoryItemConfig;

        private void OnItemChanged()
        {
            if (IsSingleItem)
                _count = 1;
        }
    }
}