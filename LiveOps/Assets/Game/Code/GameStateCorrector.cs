﻿using System;
using System.Linq;
using System.Collections.Generic;
using Game.Code.Chest;
using JetBrains.Annotations;

namespace Game.Code
{
    [UsedImplicitly]
    public sealed class GameStateCorrector
    {
        public void Correct([NotNull] GameState gameState, [NotNull] IReadOnlyList<ChestConfig> chestConfigs)
        {
            if (gameState == null) throw new ArgumentNullException(nameof(gameState));
            if (chestConfigs == null) throw new ArgumentNullException(nameof(chestConfigs));
            if (chestConfigs.Any(c => c == null)) throw new ArgumentNullException(nameof(chestConfigs));

            RemoveExtraStates(gameState, chestConfigs);
            AddMissingStates(gameState, chestConfigs);
            FixIdsInStates(gameState, chestConfigs);
        }

        private static void RemoveExtraStates(GameState gameState, IReadOnlyCollection<ChestConfig> chestConfigs)
        {
            while (gameState.ChestStates.Count > chestConfigs.Count)
                gameState.ChestStates.RemoveAt(gameState.ChestStates.Count - 1);
        }

        private static void AddMissingStates(GameState gameState, IReadOnlyCollection<ChestConfig> chestConfigs)
        {
            while (gameState.ChestStates.Count < chestConfigs.Count)
                gameState.ChestStates.Add(new ChestState());
        }

        private static void FixIdsInStates(GameState gameState, IReadOnlyList<ChestConfig> chestConfigs)
        {
            for (int i = 0; i < chestConfigs.Count; i++)
                if (chestConfigs[i].Id != gameState.ChestStates[i].Id)
                    gameState.ChestStates[i] = new ChestState
                    {
                        Id = chestConfigs[i].Id,
                    };
        }
    }
}