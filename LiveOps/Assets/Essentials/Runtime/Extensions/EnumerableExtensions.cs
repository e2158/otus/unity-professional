﻿using System.Linq;
using System.Collections.Generic;

namespace Essentials.Runtime
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<(T1, T2)> Zip<T1, T2>(this IEnumerable<T1> first, IEnumerable<T2> second) =>
            first.Zip(second, (x, y) => (x, y));
    }
}