﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Essentials.Runtime
{
    public static class ReadOnlyListExtensions
    {
        public static IEnumerable<T> TakeRandom<T>(this IReadOnlyList<T> source, int count)
        {
            if (count < 0)
                throw new ArgumentOutOfRangeException(nameof(count), count, "Count cannot be negative");

            if (count == 0)
                return Enumerable.Empty<T>();

            HashSet<int> randomIndices = new();

            while (randomIndices.Count != count)
                randomIndices.Add(UnityEngine.Random.Range(0, source.Count));

            return randomIndices.Select(i => source[i]);
        }
    }
}