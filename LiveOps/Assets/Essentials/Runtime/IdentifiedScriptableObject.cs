﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Essentials.Runtime
{
    public abstract class IdentifiedScriptableObject : SerializedScriptableObject,
        IEquatable<IdentifiedScriptableObject>, IEquatable<string>
    {
        [SerializeField, Required] private string _id;

        [NotNull] public string Id => _id;

        private void OnValidate()
        {
#if UNITY_EDITOR
            if (string.IsNullOrEmpty(_id))
                _id = Editor.ObjectExtensions.GetGuid(this).ToString();
#endif

            OnValidateInner();
        }

        protected virtual void OnValidateInner() { }

        public bool Equals(string other) => Id == other;
        public bool Equals(IdentifiedScriptableObject other) => Id == other?.Id;

        public static implicit operator string(IdentifiedScriptableObject obj) => obj.Id;
    }
}