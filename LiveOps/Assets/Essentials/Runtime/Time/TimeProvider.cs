using System;

namespace Essentials.Runtime
{
    public sealed class TimeProvider
    {
        public DateTime Now => DateTime.UtcNow;

    }
}