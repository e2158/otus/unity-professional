﻿using System;
using System.Threading;
using UnityEngine;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace Essentials.Runtime
{
    public interface IReadOnlyTimer
    {
        event Action TimerStopped;
        event Action TimerStarted;
        event Action TimerElapsed;

        bool IgnoreTimeScale { get; }
        TimeSpan Remaining { get; }
        TimeSpan Interval { get; }
        TimeSpan Elapsed { get; }
        float Progress { get; }
    }

    public sealed class Timer : IReadOnlyTimer, IDisposable
    {
        public event Action TimerStopped = delegate {};
        public event Action TimerStarted = delegate {};
        public event Action TimerElapsed = delegate {};

        public bool IgnoreTimeScale { get; }
        public TimeSpan Interval { get; }
        public TimeSpan Elapsed { get; private set; }

        public TimeSpan Remaining => Interval - Elapsed;

        [ShowInInspector, ReadOnly, HideLabel, ProgressBar(Proportion.Min, Proportion.Max, CustomValueStringGetter = "@" + nameof(OutputProgressString))]
        public float Progress => Mathf.Clamp(Proportion.Calc(Remaining.Ticks, Interval.Ticks), 0, 1);
        private string OutputProgressString => $"{Mathf.Max(0, (int)Remaining.TotalSeconds)} / {(int)Interval.TotalSeconds}";

        [CanBeNull] private CancellationTokenSource _cts;

        public Timer(TimeSpan interval, bool ignoreTimeScale = false)
        {
            Interval = interval;
            IgnoreTimeScale = ignoreTimeScale;
        }

        public void Dispose()
        {
            Stop();
        }

        public void Start(TimeSpan elapsed = default)
        {
            Stop();
            Elapsed = elapsed;

            TimerStarted();

            if (Remaining.Ticks > 0)
            {
                _cts = new CancellationTokenSource();
                ElapsingRoutine(_cts.Token, TimerElapsed)
                    .SuppressCancellationThrow();
            }
            else
            {
                TimerElapsed();
            }
        }

        public void Stop()
        {
            if (_cts != null)
            {
                _cts?.Cancel();
                _cts?.Dispose();
                _cts = null;

                TimerStopped();
            }
        }

        private async UniTask ElapsingRoutine(CancellationToken ct, Action onElapsed)
        {
            while (Remaining.Ticks > 0)
            {
                float waitSeconds = Mathf.Clamp((float)Remaining.TotalSeconds, 0.1f, 1f);
                await UniTask.WaitForSeconds(waitSeconds, IgnoreTimeScale, cancellationToken: ct);
                Elapsed += TimeSpan.FromSeconds(waitSeconds);
            }

            onElapsed();
        }
    }
}