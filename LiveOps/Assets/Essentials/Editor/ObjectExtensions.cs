﻿using UnityEditor;

namespace Essentials.Editor
{
    public static class ObjectExtensions
    {
        public static GUID GetGuid(this UnityEngine.Object obj)
        {
            string assetPath = AssetDatabase.GetAssetPath(obj);
            return AssetDatabase.GUIDFromAssetPath(assetPath);
        }
    }
}